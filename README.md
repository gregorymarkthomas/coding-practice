# Computer Science Knowledgebase

## What is it?

A collection of .md files relating to individual terms, technologies and idealogies on the subject of Computer Science.


## Why?

The Internet is full of information on the subject of Computer Science; this is without a doubt a good thing for anyone interested.

Personally, I find some resources difficult to understand due to how they are worded and displayed.


## What specifically do _you_ find difficult?

Most of the resources I come across contain large walls of text and use overcomplicated language. 
Walls of text make keeping track of which line I am on difficult and impedes my flow of understanding said text. 

What helps me is to *break down a subject into smaller and simpler sentences* by converting resources into mini Frequently Asked Questions (FAQ). 
I try to keep it more of a simple and casual conversation.


## What is an example of a "mini FAQ"?

Here is one:

### Public Key Infrastructure (PKI)

#### What is it?

(I write a *brief* one or two sentences on the subject):

*In the real [Public Key Infrastructure](/security/public_key_infrastructure/public_key_infrastructure.md) page, I mention that a PKI expands upon [Public/Private key pairing](/security/public_private_key_pairs.md) to provide a more secure communication channel.*

(This leads me to ask the next question):


#### What is the issue with simple [Public/Private key pairing](/security/public_private_key_pairs.md)?

...and the next question:


#### What is a Public Key Infrastructure's aim?

etc.


## Why have you made this resource public?

1. Someone may find it helpful
2. Knowing that someone else may read it forces me to consider the language used (and that it is factual!)
3. To cement my knowledge for self-improvement 