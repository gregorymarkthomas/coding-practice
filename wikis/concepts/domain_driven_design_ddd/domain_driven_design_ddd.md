# Domain Driven Development/Design

## What is it?

Domain Driven Design (DDD) is a software development _concept_.


## What are the basics of Domain Driven Design?

Domain Driven Design tries to make your software a **model** of a real-world system or process.


## Is this a model of the _current_ business system, or of the _ideal_?

Domain Driven Design _analyses_ **processes** and **information flow** which guides the system design. The analysis may well produce an _ideal_ as opposed to mimicking the current process. 

In other words, Domain Driven Design models how the current business **should** operate.


## What happens when Domain Driven Design analysis does **not** align with the current business practice?

These discrepancies can be highlighted as areas for improvement in the business. 


## What are we using when Domain Driven Design ends up _influencing_ current business practices?

We use the [Inverse Conway Maneuver](https://trello.com/c/LFOd2o5q/116-conways-law), provided permission is granted, of course. 

If the original business cannot change, though, friction between the business and the new system may become apparent.


## How does Domain Driven Design differ from [Decomposition by Business Capability](decomposition_by_business_capability.md)?

[Decomposition by Business Capability](decomposition_by_business_capability.md) is more _lazy_; system design is guided by the **current** business and is not necessarily re-analysed.


## What does Domain Driven Design require?

The development team needs to work closely with a *domain expert* who knows how the real-world system works.
  

## What is an example of a domain expert?

Say a software team is developing online horse-race betting software; they would need a domain expert (i.e. an experienced bookmaker) to determine how the software should be designed.


## How do the developers and domain expert communicate their requirements to each other?

They build an *ubiquitous language* (UL); this is a conceptual description of the system which BOTH parties should be able to understand. 


## What does the ubiquitous language contain?

Words; these words need to be _defined_. In our example, the ubiquitous language would _define_ horse-racing terms such as 'race', 'bet', 'odds', etc.


## What is done with the ubiquitous language?

Using the concepts described by the ubiquitous language, the team can form the basis of the [Object-Oriented Design](/paradigms/object_oriented_programming_oop/object_oriented_programming_oop.md).


## How does the ubiquitous language help the team design object-oriented?

The ubiquitous language provides clear guidance on _how_ the objects interact. 


## What else should we do with our objects?

We should _divide_ objects into these categories:

### **Value objects**

These objects represents values that might have _sub_-parts (e.g. Date has day, month and year).

### **Entities**

These are objects with _identity_ (e.g. each Customer object has its own identity so that two customers of the same name are seen as _different_ customers).

### **Aggregate roots**

These are objects that **own** other objects. These objects do **not make sense unless they have an owner**.

e.g. an 'Order Line' object needs an 'Order' object to belong to. 'Order' object is our _aggregate root_, and 'Order Line' objects **can only be manipulated via methods in the 'Order' object.**


## Once objects are categorised, what is next?

**Subdomains** are created; each subdomain corresponds to a different part of the business.
There are **3** subdomain **classifications**:

### Core

The most valuable part of application and is the key differentiator for the business.

### Supporting

Related to the business but not a differentiator; implemented in-house or outsourced.

### Generic

Not specific to business and ideally are implemented using off-the-shelf software.


## Domain Driven Design example

We have an online store.


### What does the online store consist of?

- product catalog
- inventory management
- order management
- delivery management
- etc.


### Can we use these points as our **subdomains**?

Yes!

![Decompose by subdomain](https://trello-attachments.s3.amazonaws.com/5cfba1b46796a854a63160ba/5e25d2cdcda48646e68300ba/067a7d3638e1ec129de1028cd0206fde/decompose-by-subdomain.png)


### What does this result in?

- Stable architecture
- Development teams are organised around delivering **business value** rather than *technical features*
- Services are loosely coupled and cohesive


## What are the issues in using Domain Driven Development?

Identifying subdomains requires a true *understanding* of the business (structure and identifying different areas of expertise).

### How do we combat this?

Try doing it iteratively.

Starting points:

- different groups within an organisation might correspond to certain subdomains
- use the high-level **domain** object to determine subdomains


## Would Domain Driven Design help for small projects?

Somewhat; it is good to know how the software relates to the business for any-sized project. But, Domain Driven Design is really intended for large (> 6 months) projects.


## Does Domain Driven Design _recommend_ certain development patterns?

Yes:
  
- [Repository pattern](/patterns/repository_pattern.md)
- [Factory pattern](/patterns/factory_pattern/factory_pattern.md)
- [Service Locator pattern](/patterns/service_locator_pattern.md)


## Links

- https://stackoverflow.com/questions/1222392/what-is-domain-driven-design-ddd
- https://www.infoq.com/minibooks/domain-driven-design-quickly/
- https://stackoverflow.com/questions/9361156/service-locator-pattern-and-ddd