# Decomposition by business capability

## What is it?

'Decomposition by business capability' is a software **design** _concept_.


## How do we _design_ software in this way?

We guide the new application's design **by how the existing business is already structured**.


## What are the advantages to decomposition by business capability?

The pro of this is that dev teams and business structural units are **aligned**.


## What are the disadvantages to decomposition by business capability?

The con is that existing business ineffeciences may bake into the new automated system.


## Does decomposition by business capability follow a law?

Yes; it follows [Conway's Law](/laws/conways_law.md).


## How does Decomposition by Business Capability differ from [Domain Driven Design](domain_driven_design_ddd.md)?

[Domain Driven Design](domain_driven_design_ddd.md) guides its system design by _analysing_ the current business practices for the **ideal**, whereas Decomposition by Business Capability's system design is guided by the **current** business practice.


## Links

- https://microservices.io/patterns/decomposition/decompose-by-business-capability.html