---
title:  Hungarian Notation
author: Gregory Thomas
date:   2020-09-09
---

# Hungarian Notation

## What is it?

Hungarian Notation is a [naming convention](naming_convention.md) that **prefixes** the _data type_ of a variable to a variable name.


## Do you have an example of Hungarian Notation?

```
PhoneNumber strPhone;
```

## Why was the _data type_ prefixed to the name?

In past programming languages (like Windows [C](/language_specific/c/c.md)), [compilers](/execution/compiler.md) would not check data types; this meant that it was up to the programmer to ensure they know the type of the variable, so, the Hungarian Notation proved useful.


## Is it useful in modern programming languages?

No. In modern languages like [Java](/language_specific/java/java.md) which are [strongly typed](/terms/types/strongly_typed.md), it is now much easier to see what the variable's type by easily following the variable declaration within the [Integrated Development Environment (IDE)](/integrated_development_environment/integrated_development_environment.md), and viewing detected type errors which will stop the developer from compiling and running.


## Does Hungarian Notation hinder today's developer?

Possibly; if Hungarian Notation is used on a variable and that type is _changed_, then the developer also has to ensure they change the variable name too. Why not just use a more general but descriptive term?

```
PhoneNumber strPhone;
\\ name not changed when type changed!
```