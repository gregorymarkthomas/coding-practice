---
title:  Code blocks
author: Gregory Thomas
date:   2020-03-15
---

# Code blocks

## What are they?

*Blocks* of code that are executed as a block.

## Languages

### Python

Relies on a colon (:) on the line above and the *same indentation* for the proceeding lines to create a block of code.

```
def example_function():
	...
	
```


### Java

Uses { and }. It is typical to place { on the **same** line as the code that is opening the code block.

```
private void exampleFunction() {
	...	
}
```


### C# #

Uses { and }. It is typical to place { on the **next** line as the code that is opening the code block.

```
private void ExampleFunction() 
{
	...	
}
```