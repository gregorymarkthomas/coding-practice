---
title:  Namespaces
author: Gregory Thomas
date:   2020-03-13
---

# Namespaces

## What do they do?

- Think of the name *name space*; namespaces are to **keep one set of names separate from another**.
  - This means that we can have a class *of the same name* that resides in *another namespace*; the name do not conflict.
- Collate several classes (and therefore those classes' methods/variables) into a single point of access.
  - ```com.camlab.blue.util``` in ```com.camlab.blue.util.CapManager```
  - ```System.``` in ```System.Console```

Namespaces are called ```namespace``` in C# but are called ```package``` in Java.

## Languages

### C# #

They use ```namespace``` which wrap ```class```s.

```
namespace SampleNamespace
{
    class SampleClass
    {
        public void SampleMethod()
        {
            System.Console.WriteLine(
              "SampleMethod inside SampleNamespace");
        }
    }
}
```

To import a class from another namespace, the ```using``` directive is required at the top of the class.

```global``` is the *root* namespace and refers to the *.NET System namespace*.

Unlike [Java](https://trello.com/c/bOaAC2H1/198-java-namespaces), access of classes in other namespaces is **not restricted**.

The namespace structure and directory structure **can be different** (unlike Java).

We are able to add multuple namespaces in one file.


### Javascript

Javascript does NOT have namespaces.

Sometimes ```class``` is used to group entities together, though.


### Java

They are called ```package``` in Java.

They are instantiated/connected to by a class via ```package package_name``` at the top (and outside) of the class.

In Java, ```package``` **restricts access of classes to classes in the same package.**

The directory structure **matches** the package structure.
