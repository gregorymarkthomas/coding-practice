---
title:  Pass by reference
author: Gregory Thomas
date:   2020-08-15
---

# Pass by reference

## What is this?

'Pass by reference' refers to the behaviour of [Input Arguments](/input_argments.md) for [functions](functions.md).


## Okay, what is the behaviour of these inputs when we are using "pass by reference"?

"Passing by reference" means that the [Input Argument](/input_argments.md) is a [reference](/memory/reference.md) to a variable value stored in the [heap](/memory/heap.md), but is not the value _itself_.


## Well, what else can you do besides "pass by reference"?

We can instead ["pass by value"](pass_by_value.md); the value of the argument is **copied** into the [function](functions.md) (hence pass by **value**).


## _What_ exactly can be "pass by reference"?

A programming _language_ can be "pass by reference" (or "pass by value").