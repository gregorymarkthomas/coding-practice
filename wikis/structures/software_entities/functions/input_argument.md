---
title:  Input Arguments
author: Gregory Thomas
date:   2020-09-11
---

# Input Arguments

## What are they?

Input Arguments are [variables](../variables/variables.md) that are passed _into_ a [function](functions.md).


## What are Input Arguments used for?

We can use Input Arguments to modify something else, or perhaps we would want to modify the Input Argument itself and return it back to the caller.


## What do we need to think about with Input Arguments?

We should check with the programming language to see if Input Arguments are [pass by reference](pass_by_reference.md) or [pass by value](pass_by_value.md).