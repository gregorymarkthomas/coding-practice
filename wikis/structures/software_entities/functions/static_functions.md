---
title:  Static functions
author: Gregory Thomas
date:   2020-09-11
---

# Static functions

## What are they?

A [static](/modifiers/static.md) function is a [function](function.md) that is **accessible across ALL instances of the [class](/structures/software_entities/classes/classes.md)**.


## How are Static Functions defined?

These functions are defined within the class, as normal, but are defined [static](/modifiers/static.md).


## Can you have a [Static](/modifiers/static.md) Function for an [Instance Class](../classes/instance_class.md)?

Yes, at least in [Java](/language_specific/java/java.md); the developer can access a Static Function _outside_ of the instantiation:

### Java

```
Person person = new Person();
person.setName("Jez");	// instance function
Person.doSomethingGlobal(); // static function called with class name

```

## So, how many copies of this variable are there stored in memory?

Only **one**; there could be many instances of that class, but there will only be one copy of this static value in memory.


### Where specifically are static variables stored?

Static variables are stored in the [heap](/memory/heap.md).

#### Java

Static variables are stored in the [Permanent Generation](/language_specific/java/memory/permanent_generation.md) area of the [heap](/memory/heap.md).