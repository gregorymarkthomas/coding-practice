---
title:  Anonymous closures
author: Gregory Thomas
date:   2020-03-13
---

# Anonymous Closures

## What are they?

Anonymous Closures are [functions](functions.md) that are _immediately invoked_; they _cannot_ be _called_ specifically because they are not assigned to a variable.


### What are some other names for Anonymous Closures?

- Immediately Invoked Function Expression (IIFE)
  - The function is evaluated *and immediately invoked*.
- Lambda expressions
- Function literals


## Why are they used?

Anonymous closures are used for tasks that need doing straight away, and *only to be called once* (or a limited amount of times).


## Do you have an example of an Anonymous closure?

In TRUEscience we use an interface to "callback" from an async database save. In a language which supports anonymous closures we would use one instead of the interface implementation; it looks cleaner, the function will not be called anywhere else, and will be called as soon as the async operation has finished.


## Languages

### Python

TODO


### C# #

TODO


### Java < 8

There are no anonymous closures in Java < 8.

In TRUEscience (Java < 8) we were **not** using anonymous closures; we were passing in an implementation of an interface, the implementation being done in the initial call. This kinda looks like an anonymous closure but is not.

For example:

```
...
DatabaseHelper.getInstance().saveAsync(dto, new DataTransferObject.OnAsyncSaveCallback() {
  @Override
  public void onSaveAsync(DataTransferObject dto) {
    // Do something
  }
});
```

### Java 8

If we did the same example in Java 8 as the one for Java < 8, we would (TODO: should be able to?) do something like this instead:

```
DatabaseHelper.getInstance().saveAsync(dto, -> {
    // Do something
  }
});
```

This looks cleaner and removes need for the OnAsyncSaveCallback interface.


#### Links

- https://dzone.com/articles/java-8-lambas-limitations-closures


### Kotlin

TODO


### Javascript

This example is using an anonymous closure to provide a [Module](https://trello.com/c/gqm8CtFR/200-javascript-module-pattern).


```
(function() {
  'use strict';
  // All function and variable code are scoped to this function
})();
``` 
