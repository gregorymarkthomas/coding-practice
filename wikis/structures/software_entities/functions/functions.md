---
title:  Functions
author: Gregory Thomas
date:   2020-03-13
---

# Functions

## What are they?

## What are some other names?

- Methods
- Procedures
- Subroutines


## How best do we name functions?

Functions should use a [verb](/english/verb.md) in their name:

- ```postPayment```
- ```deletePage```
- ```save```

### What should an [accessor](/)



## How are functions called in various languages?

### Kotlin

```fun``` is used:

```fun convert(arrStr: String): DoubleArray {...```


#### Do we not need to supply an [access modifier](/modifiers/access/access.md)?

Only if we want a function that is anything other than [public](/modifiers/access/public.md).


#### What access can a function have in Kotlin?

- [public](/modifiers/access/public.md)
- [private](/modifiers/access/private.md)
- [protected](/modifiers/access/protected.md)
- [internal](/modifiers/access/internal.md)