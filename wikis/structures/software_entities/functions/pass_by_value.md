---
title:  Pass by value
author: Gregory Thomas
date:   2020-08-15
---

# Pass by value

## What is this?

'Pass by value' refers to the behaviour of [Input Arguments](/input_argments.md) for [functions](functions.md).


## Okay, what is the behaviour of these inputs when we are using "pass by value"?

"Passing by value" means that the [Input Argument](/input_argments.md) value is **copied** into the [function](functions.md) (hence pass by **value**).


## Well, what else can you do besides "pass by value"?

We can instead ["pass by reference"](pass_by_reference.md); this passes a [reference](/memory/reference.md) to a variable value stored in the [heap](/memory/heap.md), but not the value _itself_.


## _What_ exactly can be "pass by value"?

A programming _language_ can be "pass by value" (or "pass by reference").