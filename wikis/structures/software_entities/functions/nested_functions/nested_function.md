# Nested Functions / Nested Procedures / Subroutines

## What are they?

Nested Functions are functions that are defined _within_ another function.


## What _visibility_ does a Nested Function have?

Due to simple recursive [scope](/terms/scope.md) rules, a Nested Function is itself invisible outside of its immediately enclosing function, but can access all local objects (data, functions, types, etc.) of its immediately enclosing function as well as of any function(s) which, in turn, encloses that function.