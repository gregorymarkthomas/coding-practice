---
title:  Accessor Functions
author: Gregory Thomas
date:   2020-09-11
---

# Accessor Functions

## What are they?

Accessor Functions are [functions](functions.md) that exist in an [instance class](../classes/) that are [read-only](/terms/read_only.md); they **return** a property of the _current_ object ['_this_'](../classes/this.md).


## So, an Accessor Function will never edit/write a property of the object?

Yes, in theory.


## An Accessor Function always **returns** _a single_ property of the instance?

Yes.


## What's the point of an Accessor Function?

An Accessor Function allows only _some_ access to the object.

In an effort to reduce potential development bugs, we do not want to provide _full_ access to anything; we want to hide as much as possible. Hiding access to the developer is called [encapsulation](/paradigms/principles/encapsulation.md), a cornerstone of [Object Oriented Programming (OOP)](/paradigms/object_oriented_programming/object_oriented_programming.md).
By hiding the majority of our code, we are [encapsulating](/paradigms/principles/encapsulation.md) our code so that we as developers can only see what we _need_; [**we are protecting us from ourselves**](/tips/protect_us_from_ourselves.md).


## What is another name for an Accessor Function?

- Accessor method
- a "getter" function/method
