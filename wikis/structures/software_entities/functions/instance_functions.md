---
title:  Instance Functions
author: Gregory Thomas
date:   2020-09-11
---

# Instance Functions

## What are they?

Instance Functions are [functions](functions.md) that are defined within an [instance class](../classes/instance_classes.md), and **[access](accessor_functions.md) or [mutate](mutator_functions.md) the current object using ['this'](../classes/this.md)**.


## So, an Instance Function can be an [Accessor](accessor_functions.md) or [Mutator](mutator_functions.md) function?

Yes.


## What about functions within the [instance class](../classes/instance_classes.md) that DON'T utilise ['this'](../classes/this.md)?

They are instead [functions](functions.md) that exist within the [class](../classes/classes.md), and they are not Instance Functions; these are [Class/Static Functions](class_static_functions.md).


## What are some other names for Instance Functions?

Swap another term for ['instance'](../instances.md) and/or swap another term for ['function'](functions.md):

- Instance method
- Instance procedures
- Instance subroutines
- Object function
- Object method
- Object procedure
- Object subroutine