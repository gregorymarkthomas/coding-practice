---
title:  Mutator Functions
author: Gregory Thomas
date:   2020-09-14
---

# Mutator Functions

## What are they?

Mutator Functions are [functions](functions.md) that exist in an [instance class](../classes/) that can change an instance's _state_.


## Hence the word "mutator"?

Yes.


## So, can a Mutator Function _read_ the instance's state?

If it needs to, yes!


## Does a Mutator Function _return_ anything?

A Mutator Function should **not** return anything (i.e. it should return [void](/terms/void.md)); there should be a _separate_ [Accessor Function](accessor_functions.md) to return the required property of the instance.


## What's the point of an Mutator Function?

A Mutator Function should only change a **little part** of the instance's _state_. In an effort to reduce potential development bugs, we need to be very careful about _how much_ we change an instance's state. 

Hiding access to the developer is called [encapsulation](/paradigms/principles/encapsulation.md), a cornerstone of [Object Oriented Programming (OOP)](/paradigms/object_oriented_programming/object_oriented_programming.md). 
By hiding the majority of our code, and providing a _limited_ way to change an instance state, we are [abstracting](/paradigms/principles/abstraction.md) and [encapsulating](/paradigms/principles/encapsulation.md) our code so that we as developers can only do what we _need_; [**we are protecting us from ourselves**](/tips/protect_us_from_ourselves.md).


## What is another name for a Mutator Function?

- Mutator method
- a "setter" function/method
