---
title:  Recursion
author: Gregory Thomas
date:   2020-03-15
---

# Recursion

## What is it?

Code that calls itself.


## What are examples of this?

### Fibonacci sequence

As found in the ["Cracking the Coding Interview"](/books/cracking_the_coding_interview.md) book by [Gayle Laakmann McDowell](http://www.gayle.com/).

See [fibonacci.py](fibonacci.py) for (rough) implementation and notes.