#!/usr/bin/python

import unittest

"""
1. Know that it should be recursive
2. Know that the output should be fib(n-1) + fib(n-2) provided n is > 1, else return n
3. Know that if returning the nth value of the Fibonacci sequence, the recursive function is relying on the 'return n', particularly when it returns 1. The sum of 'return 1's being called is the Fibonacci number we want.
4. Demonstrate it by putting it in a tree
5. Know that the tree will be called in PRE-order
6. Know that the total number of leaves will be the number of times the function will call
7. Know that it is ROUGHLY O(2^n) (it is more like 1.6^n) - the tree is not always balanced and there are not always twice the number of calls to fib() on each level
8. See that we already get the result of fib(n) in the first n calls - we can cache the result so that instead of the function recalling fib(n-1) etc times, we can just refer to the cache as we already have the answer.
"""

class FibberSlow():

	#
	# fib = 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55...
	# n = 	0, 1, 2, 3, 4, 5, 6, 7,  8,  9,  10
	# Return the nth number in the Fibonacci sequence.
	# If n = 3, then fib(n) would return 2, because 1 + 1 = 2.
	# If n = 9, then fib(n) would return 34, because 13 + 21 = 34 
	# The issue is, we do not know what the sequence is unless we work it out on the fly (unless n = 0 or 1 - we know that already).
	# This is why the recursive function was used; to be able to get the nth value, we had to calculate the sequence up until that point.
	# We still just require n-1 + n-2 at the end, but we need to know what they are. And to know that, we need to know 1...n-2.
	# So, we return fib(n-1) + fib(n-2); by doing this we are calling fib() twice per iteration.
	#
	# Why is this so slow?:
	# - We are calculating 
	#
	# Calling tree (where n = 5):
	#											f(5)
	#							f(4)							f(3)
	#				f(3)				f(2)	f(2)					f(1)
	#		f(2)			f(1) f(1)		f(1)
	# f(1)
	#
	# Level	| Nodes	| AKA |	Or..
	# --------------------------------------
	# 0		| 1		| 
	# 1		| 2		| 2 * previous lvl = 2
	# 2		| 4		| 2 * previous lvl = 2 * 2^1 = 2^2
	# 3		| 4		| ??
	# 4		| 1		| ??
	# 
	# This is why CtCI book mentions that this is technically around O(1.6^N) and not just O(2^N).
	# But for the sake of ease, we say O(2^N).
	# So, if n = 5, how many calls are there?:
	# fib(1) -> 2^1 steps = 2
	# fib(2) -> 2^2 steps = 4
	# fib(3) -> 2^3 steps = 8
	# fib(4) -> 2^4 steps = 16
	# fib(5) -> 2^5 steps = 32
	#
	# So, why does the program ACTUALLY call 16 times?
	# The fib(1) ->... table was taken from an example which calls fib(i) in a for loop IN ADDITION - perhaps the extra call would make it so.
	#
	# Does this now make sense?
	# Somewhat. I guess I still struggle to understand WHY it works.
	# Well, the number of times the function calls fib(1) in the flurry of calls equals the nth fib sequence number (i.e. the number we want).
	# The function does not just call itself 'nth in sequence' times; it 
	#
	# From https://stackoverflow.com/questions/8845154/how-does-the-fibonacci-recursive-function-work:
	# "Remember code is always executed left->right and top-> bottom. So whenever a new function is called it is paused and then the next invocation occurs."
	# Useful diagram: fibonacci_app_diagram.png (or https://i.stack.imgur.com/UYuFl.png)
	#
	# 1. fib(5) {
	#     return fib(4) + fib(3);
	# 2.   fib(4) {
	#       return fib(3) + fib(2);
	# 3.     fib(3) {
	#         return fib(2) + fib(1);
	# 4.       fib(2) {
	# A=        return fib(1) + fib(0);
	#			  fib(1) {
	#				return 1;
	#			  };
	#			  fib(0) {
	#			  	return 0;
	#			  }
	#			  return 1 + 0;
	#          };
	# 5.       fib(1) {
	# B=        return 1;
	#          };
	# C=       return 2; // (1 + 1)
	#        };
	# 6.     fib(2) {
	# D=      return 1;
	#        };
	# E=    return 3; // (2 + 1)
	#      };
	# 7.   fib(3) {
	#       return fib(2) + fib(1);
	# 8.     fib(2) {
	# F=      return fib(1) + fib(0);
	#			  fib(1) {
	#				return 1;
	#			  };
	#			  fib(0) {
	#			  	return 0;
	#			  }
	#			  return 1 + 0;
	#          };
	#        };
	# 9.     fib(1) {
	# G=      return 1;
	#        };
	# H=    return 2; // (1 + 1)
	#      };
	# I=  return 5; // (3 + 2)
	#    };
	# 
	def fib(self, n):
		print("fib(" + str(n) + ")")
		if n == 0:
			return 0
		elif n == 1:
			return 1
		else:
			return self.fib(n-1) + self.fib(n-2)

class Fibber():

	def fib(self, n):
		return self.do_fib(n, {})

	#
	# This will check for any saved previous results in a cache.
	# This stops the need for repeated calls to do_fib() with the same n value.
	# The cache is filled in the initial left-hand-side tree calls.
	# But, at what point is 'cache[n] = result' called?
	# 	Well, the LEFT HAND self.do_fib(n-1) is called until n = 2, when 1 is returned (n actually being = 1 because n-1).
	#	So, as soon as that happens, the neighbouring do_fib() call is returned (as do_fib(0), as n = 2 and we're calling n-2).
	# After these calls, the result of n=2 is cached.
	def do_fib(self, n, cache):
		print("-----------")
		print("fib(" + str(n) + ")")
		print("cache = " + str(cache) + "")
		if n in cache:
			print(str(n) + " is IN cache")
			result = cache[n]
		else:
			print(str(n) + " is NOT in cache")
			if n < 2:
				result = n
			else:
				result = self.do_fib(n-1, cache) + self.do_fib(n-2, cache)
			print("Caching result " + str(result) + " for n = " + str(n))
			cache[n] = result		
		return result

class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		self.fibber = Fibber()

	def test_zeroth_in_sequence(self):
		self.assertEqual(0, self.fibber.fib(0))

	def test_third_in_sequence(self):
		self.assertEqual(2, self.fibber.fib(3))

	def test_fifth_in_sequence(self):
		self.assertEqual(5, self.fibber.fib(5))

	def test_tenth_in_sequence(self):
		self.assertEqual(55, self.fibber.fib(10))

	def test_twentieth_in_sequence(self):
		self.assertEqual(6765, self.fibber.fib(20))

	def test_fortyninth_in_sequence(self):
		self.assertEqual(7778742049, self.fibber.fib(49))

unittest.main(exit=False)