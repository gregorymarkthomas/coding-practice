# Data Transfer Object (DTO)

## What is it?

A Data Transfer Object (DTO) is a [data structure](../data_structure.md).

It is a class with public variables and **no** functions.


## What are they used for?

- Communicating with databases
- Parsing messages from sockets


## How are they used when communicating with databases?

DTOs are often the first in a series of translation stages that convert raw data in a database into [objects](\paradigms\object_oriented_programming_oop\objects.md) in application code.