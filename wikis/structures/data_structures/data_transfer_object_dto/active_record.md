# Active Record

## What is it?

Active Records are special forms of [Data Transfer Object (DTO)](data_transfer_object_dto.md).


## How are they like DTOs?

They are [data structures](../data_structure.md) with public (or _bean-accessed_) variables, but they typically have navigational methods such as:

- `save()`
- `find()`


## How are Active Records used?

Active Records are typically direct translations from database tables, or other data sources.


## What are the pitfalls of Active Records?

Some developers treat Active Record data structures as [objects](\paradigms\object_oriented_programming_oop\objects.md) by implementing business logic.
This creates a data-structure/object _hybrid_, which is not good.


## How can we stop this?

Treat Active Records as data structures only, and **create separate objects** that contain business logic and that hide their internal data (which are probably just instances of Active Record anyway).