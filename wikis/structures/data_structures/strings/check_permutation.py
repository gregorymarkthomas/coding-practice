#!/usr/bin/python

# -*- coding: utf-8 -*-
"""A coding challenge.

From 'Cracking the Coding Interview':
'Given two strings, write a method to decide if one is a permuation of the other.'

In other words:
1. Write a function that takes in two strings and returns a boolean.
2. Using the two strings
  a. We want the function to check if firstStr is a permutation of secondStr
  b. If not, then secondStr is NOT going to be a permutation of firstStr, so return False 

Questions to ask:
- If a is a permutation of b, is b a permutation of a?
  - True - it is.
- If one string has more characters than the other, yet contains the characters of the other string, is it still a permutation?
  - I don't think it is - a permuation, I believe, is a string of the same characters as the comparison string.

Brute force:
- We can return False if the string lengths are different.
- Loop through every character of firstStr and find() in secondStr
  - Every char of firstStr has to exist in secondStr before a result is determined.

What are some alternatives?
- Sort both strings and compare


---

After reading solution on p193:

Things I forgot to question:
- white space
- case sensitivity

Solution:
I had thoughts in my mind about something simular to this but didn't make it stick.

Create an array (a 'hash table' of sorts) of zeros that is the size of the number of possible ASCII chars (make sure to ask interviewer if this is case).
Loop through firstStr and INCREMENT hashtable[character at i].
Loop through secondStr and DECREMENT hashtable[character at i].
In theory, if both strings have each others characters, the hashtable will have all zeros again.
If the current value is a negative, then that means the second string contains a value the first string did not have.
In which case, we break and return false.

---

So, things to ask the interviewer:
- whitespace
- case sensitivity
- (if doing fancy solution) ASCII? So I can use 128 size array as our 'hash table' of sorts


GMT - 2020-01-18
"""

import unittest

class CheckPermuationBrute1():
	def is_permutation(self, firstStr, secondStr):
		"""
		Returns true if the strings are permuations of each other.
		This is a brute-force solution; there is a nested loop (find()), times two. This equals O(2n^2) = O(n^2) (is this true?? find() is awfully quick in Python).
		If every char of firstStr is found in secondStr, and vice versa, then return True.
		"""
		if len(firstStr) == len(secondStr):
			for char in firstStr:
				if secondStr.find(char) != -1:
					result = True
				else:
					result = False
					break
			if result:
				for char in secondStr:
					if firstStr.find(char) != -1:
						result = True
					else:
						result = False
						break
		else:
			result = False
		return result

class CheckPermuationBrute2():
	def is_permutation(self, firstStr, secondStr):
		"""
		Returns true if the strings are permuations of each other.
		This is a brute-force solution; we sort both strings and compare the two.
		sorted() uses Timsort, which is much like merge sort. And what is the time complexity of a mergesort? O(n log n).
		sorted() also returns a list, hence the ''.join() we do to join each element to an empty character to recreate the String.

		Notes:
		- I have tried just comparing the two (sorted) lists but it is quicker to recreate into Strings and then compare (using 'result = sorted(firstStr) == sorted(secondStr)').

		So what is the Big O of this?
		n log n + n log n + n + n ???
		= O(n log n) (I think)
		"""
		if len(firstStr) == len(secondStr):
			firstStr = ''.join(sorted(firstStr))
			secondStr = ''.join(sorted(secondStr))
			# print("firstStr = " + firstStr + ",\n secondStr = " + secondStr + "\n")
			result = firstStr == secondStr
		else:
			result = False
		return result

class CheckPermutationBook():
	def is_permutation(self, firstStr, secondStr):
		"""
		Returns true if the strings are permuations of each other.

		Create an array (a 'hash table' of sorts) of zeros that is the size of the number of possible ASCII chars (make sure to ask interviewer if this is case).
		Loop through firstStr and INCREMENT hashtable[character at i].
		Loop through secondStr and DECREMENT hashtable[character at i].
		In theory, if both strings have each others characters, the hashtable will have all zeros again.
		If the current value is a negative, then that means the second string contains a value the first string did not have.
		In which case, we break and return false.
		"""

		if len(firstStr) == len(secondStr):
			hash_table = [0] * 128
			for char in firstStr:
				hash_table[ord(char)] += 1

			# print("hash_table = " + str(hash_table))

			result = True
			for char in secondStr:
				char_index = ord(char)
				hash_table[char_index] -= 1
				if hash_table[char_index] < 0:
					result = False
					break
		else:
			result = False
		return result

"""Test cases."""
class Tester(unittest.TestCase):

	@classmethod
	def setUpClass(self):
		self.check_permutation = CheckPermutationBook()

	def test_single_char(self):
		self.assertEqual(self.check_permutation.is_permutation("a", "a"), True)

	def test_two_chars(self):
		self.assertEqual(self.check_permutation.is_permutation("ab", "ba"), True)

	def test_three_chars(self):
		self.assertEqual(self.check_permutation.is_permutation("abc", "bac"), True)

	def test_different_sized_words(self):
		self.assertEqual(self.check_permutation.is_permutation("abc", "ba"), False)

	def test_duplicate_chars(self):
		self.assertEqual(self.check_permutation.is_permutation("aabb", "bbaa"), True)

	def test_duplicate_chars_second_string_has_single_different_char(self):
		self.assertEqual(self.check_permutation.is_permutation("aabb", "bcaa"), False)

	def test_duplicate_chars_large(self):
		self.assertEqual(self.check_permutation.is_permutation(
			"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb", 
			"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), True)

	def test_duplicate_chars_large2(self):
		self.assertEqual(self.check_permutation.is_permutation("abc"*2500, "cba"*2500), True)

	def test_duplicate_chars_large3(self):
		self.assertEqual(self.check_permutation.is_permutation("abcd"*2500, "dcba"*2500), True)

	def test_large_except_one(self):
		"""
		This does not work when CheckPermutationBook() is used because '£' has a value outside of the ASCII realm (i.e. a value > 128)
		"""
		self.assertEqual(self.check_permutation.is_permutation(
			'!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~', 
			'!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}£'), False)

	def test_case_sensitivity(self):
		"""Assuming that this IS case sensitive."""
		self.assertEqual(self.check_permutation.is_permutation("dog", "DOG"), False)

	def test_whitespace_significance(self):
		"""Assuming that whitespace IS significant."""
		self.assertEqual(self.check_permutation.is_permutation("c b", "b c"), True)		 

unittest.main(exit=False)