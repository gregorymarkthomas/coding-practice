---
title:  Regular Expressions
author: Gregory Thomas
date:   2020-08-04
---

# Regular Expressions

## What are they?

Regular Expressions are a _sequence of characters_ that define a search pattern.


## Where are Regular Expressions used?

**String-searching algorithms** use Regular Expressions to _find_, or _find and replace_ **within** a string.


## What are some examples of Regular Expressions?

### [Android interview app](https://gitlab.com/gregorymarkthomas/intrasonicsassignment/-/blob/9b6a97c8b2207b06039e851f564bf861d0a788d9/android/app/src/main/java/com/gregorymarkthomas/intrasonicsassignment/model/StringToArrayConverter.java)

The app allows a user to enter a [String](strings.md) of numbers in the form of an [array](arrays.md), and be told what the [mean](/mathematics/mean.md) and [median](/mathematics/median.md) are; whole numbers, decimal numbers and negative numbers _separated_ by commas or whitespace can be entered. For example:

```23.0, \n37.0, \n42.0, \n55.0, \n60.0, \n72.0, \n83.0, \n91.0```

A [class](/structures/software_entities/classes/classes.md) within the application converted any [String](strings.md) containing a number array into an array of [doubles](../primitive/double.md). Using [String's](strings.md) ```split()``` function, I could easily split the String into an array for individual element processing. 
With ```split()``` I was able to supply a Regular Expression to define which characters in the input [String](strings.md) were considered _separators_ inbetween the numbers we wanted to convert to [doubles](../primitive/double.md): 

```private static final String ALLOWED_STRING_SEPARATORS = "[,\\s]{1,}|[,]{1,}|[\\s]{1,}|[.]{2,}";```
...
```String[] splitStrings = arrStr.split(ALLOWED_STRING_SEPARATORS);```

#### What does ALLOWED_STRING_SEPARATORS do?

- ```[,\\s]{1,}```: if a comma AND whitespace occurs 1 or more times, note as a separator.
	- This more-encompassing expression is defined **first**; if placed at the end (or not at all), a user entering ", " between numbers will cause the comma and whitespace to be seen as **individual separators** and not _combined_ due to the following expressions, and will cause more empty spaces in the resulting array.
- ```|[,]{1,}```: OR if just a comma occurs 1 or more times, note as a separator.
	- Numbers can be separated by just a comma, or many.
- ```|[\\s]{1,}```: OR if just a whitespace occurs 1 or more times, note as a separator.
	- Numbers can be separated by just whitespace, or many whitespaces (space, line break, tab, etc).
- ```|[.]{2,}```: OR if a full-stop occurs 2 or more times, note as a separator.
	- Decimal numbers will **not** be seen as separators, but if two or more full-stops are used, then it **is** considered a separator.


## What are some other names for Regular Expressions?

- regex
- regexp


## What is a good resource to test Regular Expressions?

- https://www.regextester.com/94479