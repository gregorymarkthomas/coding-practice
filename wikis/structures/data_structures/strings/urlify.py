#!/usr/bin/python

# -*- coding: utf-8 -*-
"""A coding challenge.

From 'Cracking the Coding Interview':
'Write a method to replace all spaces in a string with "%20". 
You may assume that the string has sufficient space at the end to hold the additional characters, and that you are given the "true" length of the string.
(Note: if implementing in Java, please use a character array so that you can perform this operation in place)

E.g.
Input: "Mr John Smith    ", 13
Output: "Mr%20John%20Smith"

Hints: #53, #118'

In other words:
1. Write a function that takes in a string and an int, and returns a new string with certain spaces replaced.
	- The string could be a sentence with normal spaces in, but it will also have "sufficient" (not necessarily the exact) number of spaces at the end for the extra "20" chars being added.
		- "%" will use the existing character space from the space
		- I assume that means that I should trim whitespace at the end?
	- The int is the number of chars the string is, WITHOUT '%20', and MINUS the extra space at the end.

Thoughts:
- Again, do I trim any extra whitespace at the end? 
	- I am assuming the extra whitespace has potentially more than enough space, meaning there could be leftover.
	- Or do I just assume there is always the exact amount required? That may make things easier.
- Can we use the int as a clue?
- There should be two extra whitespaces at the end per inner whitespace.
	- Do I need to check for that? According to the question, there is always sufficient space, so, no.
- What happens when the input is:
	- empty?
	- one space?
		- the input string will always provide space for the extra characters, meaning we should just return 
	- two spaces?
	- three spaces?
		- This is a little different, because if there are TWO ending whitespaces, 
	- text, but with two spaces between words?
	- just non-whitespace characters
	- whitespace, then non-whitespace chars

Bruteforce:
- pseudocode:
	- if valid (i.e. not empty string)
		- define a new string
		- loop through each char of the input string
			- if not whitespace, append to new string
			- if whitespace, 
				- if there IS another non-whitespace char eventually afterwards
					- append '%20' to new string
				- if just whitespace from here on
					- break, because it is extra whitespace set by input
- What is the time complexity?
	- O(n^3)!!! 
- What could we do to improve this?
	- Get rid of the "is_whitespace_or_empty()" calls
		- In Python, surely we can just append() to the original string easy enough...


------

Consulting the book:

Hint #53: "It is often easier to modify a string when going from end to beginning."

- Can I use that info?
	- By counting all the spaces up until the first non-whitespace character, we can determine the number of spaces there will be in the following (backwards) text.
	- We can also strip() it.

Hint #118: "You may need to know the number of spaces required. Can you count them?"

- Can I use that info?
	- Well, I did already find a way to count it.
	- But, in the Improved solution, I don't use this info.
	- Is there another solution I could do which utilises this info?
		- 

------

Improved:

- Time complexity is now O(N)
- Space complexity is O(N)
- I am not utilising the empty space at the end, though.

GMT - 2020-01-18
"""

import unittest

class UrlifyBrute():
	def urlify(self, text, trueLength):
		"""
		Returns a new string that has the inner spaces in 'text' replaced.
		This is a brute-force solution; we loop through each character and decide how we copy that character to a new string.
		"""
		result = ""
		if not self.is_whitespace_or_empty(text):
			for i, char in enumerate(text):
				print("i = " + str(i) + ", char = " + char)
				if char == " ":
					if self.is_whitespace_or_empty(text[i:]):
						print("Breaking")
						break
					else:
						result += "%20"
				else:
					result += char
		return result

	def is_whitespace_or_empty(self, text):
		"""Return false if any non-whitespace is found in this text"""
		# print("text = " + text)
		# print("text.strip() = " + text.strip())
		# print("len(text.strip()) = " + str(len(text.strip())))
		return len(text.strip()) == 0

class UrlifyImproved():
	def urlify(self, text, trueLength):
		"""
		Returns a new string that has the inner spaces in 'text' replaced.
		"""
		text = text.rstrip()
		result = ""
		for i, char in enumerate(text):
			print("-----")
			print("text = " + text + ", i = " + str(i) + ", char = " + char)
			print("text[:i] = " + text[:i])
			print("text[i:] = " + text[i:])
			print("text[i+1:] = " + text[i+1:])
			if char == " ":
				result += "%20"
			else:
				result += char
		return result

"""
TODO: needs some work.

This is the book's solution (edited for Python):
- Is a two-scan approach
- First scan
	- Count the number of spaces WITHIN the trueLength of the string
		- I.E, discounting the extra whitespace.
	- TRIPLING this number equals the number of extra chars in the final string, according to the description.
		- Does it? In the example, we have 2 spaces to fill, meaning there will be 6 extra chars.
		- But, I thought there would be only 2 extra spaces per inner space, due to the "%" of "%20" taking up the original space...
- Mid processing
	- 
- Second scan
	- Done in reverse order
	- loop through original string but MINUS the extra whitespace.
	- when ' ' occurs, *FINISH THIS* 

"""
class UrlifyBook():
	def urlify(self, text, trueLength):
		text = list(text)
		print("------")
		print("text 1st = " + str(text))
		if trueLength > 0:
			spaceCount = 0
			for i in range(0, trueLength):
				if text[i] == ' ':
					spaceCount += 1

			index = trueLength + spaceCount * 2
			if trueLength < len(text):
				text[trueLength] = "\0"

			print("text 2nd = " + str(text))
			print("spaceCount = " + str(spaceCount))
			print("index = " + str(index))
			print("trueLength = " + str(trueLength))

			for i in range(trueLength - 1, 0, -1):
				print("curr i = " + str(i))
				print("curr text[i] = " + str(text[i]))
				print("curr index = " + str(index))
				if text[i] == ' ':
					text[index - 1] = '0'
					text[index - 2] = '2'
					text[index - 3] = '%'
					index = index - 3
				else:
					text[index - 1] = text[i]
					index -= 1

			print("text 3rd = " + str(text))
		return "".join(text)

"""Test cases."""
class Tester(unittest.TestCase):

	@classmethod
	def setUpClass(self):
		self.urlify = UrlifyBook()

	def test_empty(self):
		"""
		I do not know what should happen in this instance.
		But we will test for it anyway.
		"""
		self.assertEqual(self.urlify.urlify("", 0), "")

	def test_single_space(self):
		"""
		I do not know what should happen in this instance.
		But we will test for it anyway.
		I assume the inputs should always give enough space for expected extra characters.
		"""
		self.assertEqual(self.urlify.urlify(" ", 0), "")

	def test_two_spaces(self):
		"""
		I do not know what should happen in this instance.
		But we will test for it anyway.
		I assume the inputs should always give enough space for expected extra characters.
		"""
		self.assertEqual(self.urlify.urlify("  ", 0), "")

	def test_three_spaces(self):
		"""
		I assume that, because there are always two ending whitespaces per inner whitespace (so there is space for extra chars),
		an input like this may assume that the first whitespace is an inner space, and that the other two spaces are there to cater for "20" of "%20".
		"""
		self.assertEqual(self.urlify.urlify("   ", 1), "%20")

	def test_text_with_two_spaces_between_words(self):
		"""
		I assume the question would provide an extra 4 spaces at the end.
		"""
		self.assertEqual(self.urlify.urlify("a  b    ", 4), "a%20%20b")

	def test_text_no_whitespace(self):
		"""
		I assume the question would know no character replacing will occur.
		"""
		self.assertEqual(self.urlify.urlify("abcde", 5), "abcde")

	def test_whitespace_before_nonwhitespace(self):
		"""
		I assume the question would know no character replacing will occur.
		"""
		self.assertEqual(self.urlify.urlify(" abcde  ", 6), "%20abcde")

	def test_two_whitespaces_before_nonwhitespace(self):
		"""
		I assume the question would know no character replacing will occur.
		"""
		self.assertEqual(self.urlify.urlify("  abcde    ", 7), "%20%20abcde")

	def test_book_example(self):
		"""
		Matches the book example.
		"""
		self.assertEqual(self.urlify.urlify("Mr John Smith    ", 13), "Mr%20John%20Smith")

unittest.main(exit=False)