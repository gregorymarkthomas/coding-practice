#!/usr/bin/python

# -*- coding: utf-8 -*-
"""A coding challenge.

From 'Cracking the Coding Interview':
'Given a string, write a function to check if it is a permuation of a palindrome.
A palindrome is a word or phrase that is the same forwards and backwards. A permuation is a rearrangement of letters. 
The palindrome doees not need to be limited to just dictionary words.

E.g.
Input: "Tact Coa"
Output: "True (permuations: 'taco cat', 'atco cta', etc.)

Hints: #106, #121, #134, #136

In other words:
 - The input could be a PERMUATION of a currently unknown palindrome.
 - Basically, find all the palindromes in INPUT
 	- If count(palindromes) > 0 then return TRUE

Thoughts:
 - Find all character rearrangements of input
 - Make input lowercase
 - The space is included

How do you find permuations of a string?
- Put each character in each position of input string

How do you work out if a palindrome?
- abba: how is this a palindrome?
	- Using two pointers, one at first char and one at last char
		- each first and last chars are the same until the two pointers crash together


- What is the time complexity?
- What could we do to improve this?

------

Consulting the book:

Hint #106: "You should not generate all permuations" 

- Can I use that info?
	- I attempted to generate all permuations for the sake of a Brute Force solution but perhaps I should be smarter from the get go
	- Can I just count the number of appearances of each char in the string; if each char count is even, then return True
		- There has to be more than one of a character for it to be part of the palindrome (UNLESS IT IS A SPACE!)
		- If there is more than one, there has to be an EQUAL number of the character so that the two 'halves' of the palindrome have enough of the character.

Solutions:

1#:
The book has a similar solution to how I did it.
Some extras:
- Do not count non-letter chars and capitals (i.e. get decimal representations of characters 0-26)

I have added the book's solution below.

2#:
Not necessarily more optimal, but, we can count the number of ODDs and EVENs as we go through each char of the text/phrase.
	- A cleanliness optimisation, and not necessarily faster.

3#:
We do not need to know the counts of odds and evens.
	- If you flip a light switch that is in the OFF position back to OFF any number of times, you will always be doing an EVEN number of flicks.
	- With this idea in mind we can use a single integer as a BIT VECTOR.
		- We map each letter to an int between 0 and 25 (26, like before). On each occurance of each letter we TOGGLE the bit
		- At the end of the char iteration, we check to see if there is AT MOST one bit in the integer set to 1.
	- We need to check if there NO bits that are of value 1.
		- We do this by comparing the integer to 0.
	- We also need to check if just ONE bit has a value of 1
		- There is an easy trick we can do for this.
		- Picture interger 00010000 (16). How do we check if there is just a SINGLE 1 in this integer?
			- We can shift the integer repeatedly, but that is a bit crass.
			- Alternatively, if we SUBTRACT 1 from 00010000, we get 00001111 (15). Why the hell is this notable, Greg?
				- Well, there is no OVERLAP between the numbers (i.e. 00010000 and 00001111 are completely different - the 1s do not interfere if compared/added together)
				- This is opposed to doing 00101000 (40) - 1 = 00100111 (39), where the '32' in the bit representation of 40 conflicts with the answer's
			- The idea above is that there is NO overlap with ODD numbers, but there IS an overlap with EVEN numbers!!
			- With this trick, we can find out if there is ONE 1 in a bit representation of an ODD number
				- We do this by subtracting 1 from a bit representation of a character and then AND the bit representation with the answer of the subtraction.
				00010000 - 1 = 00001111
				00010000 & 0001111 = 0
------

GMT - 2020-01-18
"""

import unittest

class PalindromeBook3():
	def is_palindrome_permutation(self, text):
		bit_vector = self.create_bit_vector(text.lower())
		return bit_vector == 0 or self.has_exactly_one_bit_set(bit_vector)

	def create_bit_vector(self, text):
		"""
		Create a bit vector for the 'text' string.
		For each letter with value x, toggle the xth bit.
		"""
		bit_vector = 0
		for char in text:
			x = self.get_char_dec_value(char)
			print("---")
			print("x = " + str(x))
			print("bit_vector before = " + str(bit_vector))
			bit_vector = self.toggle(bit_vector, x)
			print("bit_vector after = " + str(bit_vector))
		return bit_vector

	def toggle(self, bit_vector, index):
		"""
		Toggle the indexth bit in the integer.
		'mask' is 
		"""
		if index < 0:
			return bit_vector

		mask = 1 << index
		if (bit_vector & mask) == 0:
			bit_vector |= mask
		else:
			bit_vector &= ~mask
		return bit_vector

	def has_exactly_one_bit_set(self, bit_vector):
		"""
		Check that exactly one bit is set by subracting ONE from the integer and ANDing it with the original integer.
		"""
		return bit_vector & (bit_vector - 1) == 0

	def get_char_dec_value(self, char):
		"""
		The book does not use the full range of ASCII character decimals, though it still just uses a-z.
		We are dealing with 0-25 instead of 97-122.
		Copied from PalindromeBook1.
		"""
		a = ord("a")
		z = ord("z")
		char_dec = ord(char)

		if a <= char_dec and char_dec <= z:
			value = char_dec - a
		else:
			value = -1
		return value


"""
Whilst this solution actually works, the book seemed to have an error where capitals were not being processed.
I have added ".lower()" to the "text" object but unsure how much it will affect time complexity - I think it is still O(n).
"""
class PalindromeBook1():

	def is_palindrome_permutation(self, text):
		"""
		Checks to see if 'text' is a permutation of a palindrome.
		"""
		return self.check_max_one_odd(self.build_char_frequency_table(text.lower()))

	def check_max_one_odd(self, table):
		"""
		Takes in a table/list of size 26 of that contains the counts of instances of a-z of a phrase.
		The definition of a palindrome permuation in this instance is:
		- all a-z letters of the phrase need an even count, though ONE letter is allowed to have an odd count.
		This is why found_odd is used.
		"""
		result = True
		found_odd = False
		for count in table:
			if count % 2 == 1:
				if found_odd:
					result = False
				else:
					found_odd = True
		return result
	
	def get_char_dec_value(self, char):
		"""
		The book does not use the full range of ASCII character decimals, though it still just uses a-z.
		We are dealing with 0-25 instead of 97-122.
		"""
		a = ord("a")
		z = ord("z")
		char_dec = ord(char)

		if a <= char_dec and char_dec <= z:
			value = char_dec - a
		else:
			value = -1
		return value

	def build_char_frequency_table(self, text):
		"""
		Returns a Python list of frequencies of each char in text.
		This limits the character table to a-z of ASCII encoding system (97 to 122).
		However, for ease we will bring it to the 0-25 realm.
		We could just have 128 size table instead.
		"""
		table = [0] * (ord('z') - ord('a') + 1)
		for char in text:
			x = self.get_char_dec_value(char)
			if x != -1:
				table[x] += 1
		return table




class PalindromeImproved():
	def is_palindrome_permutation(self, text):
		"""
		PROBLEM: this still does not fix the problem.
			- 'taco cat' is a palindrome, but is not according to this solution.
				- This is because the 'o' does not require an even number - it just sits in the middle of the string.
		
		---

		Improved:

		- Psuedo
			- Create list of zeros that is 128 spaces (for ASCII chars) to use as count array
			- Loop through every char of input and add to count array
			- Loop through again and check each value above 0 is even
		"""
		char_count = [0] * 128

		for char in text.lower():
			char_count[ord(char)] += 1

		print("char_count = " + str(char_count))

		result = True
		for i, count in enumerate(char_count):
			print("i = " + str(i))
			print("count = " + str(count))
			print("count % 2 != 0 = " + str(count % 2 != 0))
			if count > 0 and count % 2 != 0 and i != 32 and find(FIX_THIS) != len(text)/2:
				result = False
				break

		return result

class PalindromeBrute():
	def is_palindrome_permutation(self, text):
		"""
		PROBLEM: the permutation code is not good enough.
			- why?
				- It only moves one char through the string at a time.
				- e.g. you can get 'taco cat' from 'Tact Coa' but my code does not produce that.
		The palindrome code seems okay, though.

		---

		Bruteforce:
		- tasks:
		 	- Get permuations of input into an array
		 	- Go through permuations array and check for palindrome count

		- pseudocode:
			- Make input lowercase
			- Create new empty list for permuations
			- for every char of input
				- move to every position of input string and save to permuations list
			- for every permutation
				- firstPtr = 0
				- lastPtr = len(perm) - 1
				- isPalindrome = True
				- while firstPtr != lastPtr:
					- if perm[firstPtr] == perm[lastPtr]:
						firstPtr += 1
						lastPtr -= 1
					- else:
						isPalindrome = False
						break
				- if isPalindrome:
					break
			- return isPalindrome

			What the hell am I trying to achieve here??
			- Move the chosen character to each position in the string
				- abba
					- a
						- abba
						- baba
						- bbaa
		"""
		permutations = []
		text = text.lower()
		for i, char in enumerate(text):
			for j in range(0, len(text)):
				# print("i = " + str(i))
				# print("text[:i] = " + text[:i])
				# print("char = " + char)
				# print("text[i+1:] = " + text[i+1:])
				textList = list(text)
				textList.pop(i)
				textList.insert(j, char)
				permutations.append("".join(textList))

		print(str(permutations))

		for perm in permutations:
			firstPtr = 0
			lastPtr = len(perm) - 1
			isPalindrome = True
			while firstPtr < lastPtr:
				print("----")
				print("firstPtr = " + str(firstPtr))
				print("perm[firstPtr] = " + perm[firstPtr])
				print("lastPtr = " + str(lastPtr))
				print("perm[lastPtr] = " + perm[lastPtr])
				if perm[firstPtr] == perm[lastPtr] or perm[firstPtr] == ' ' or perm[lastPtr] == ' ':
					firstPtr += 1
					lastPtr -= 1
				else:
					isPalindrome = False
					break
			if isPalindrome:
				break
		
		return isPalindrome

"""Test cases."""
class Tester(unittest.TestCase):

	@classmethod
	def setUpClass(self):
		self.palindrome = PalindromeBook3()

	# def test_empty(self):
	# 	"""
	# 	Assume that an empty string is not a palindrome.
	# 	"""
	# 	self.assertEqual(self.palindrome.is_palindrome_permutation(""), False)

	# def test_two_chars(self):
	# 	"""
	# 	This should be True. Permutations:
	# 	abba
	# 	baba
	# 	bbaa
	# 	bbaa
	# 	baba
	# 	abba
	# 	abba
	# 	abab
	# 	baba
	# 	abba
	# 	abba
	# 	abab
	# 	aabb
	# 	aabb
	# 	abab
	# 	abba
	# 	"""
	# 	self.assertEqual(self.palindrome.is_palindrome_permutation("abba"), True)

	def test_book_example(self):
		"""
		The book says that this is input will produce True.
		"""
		self.assertEqual(self.palindrome.is_palindrome_permutation("Tact Coa"), True)

	# def test_book_example_obvious(self):
	# 	"""
	# 	The book says that this is input will produce True.
	# 	"""
	# 	self.assertEqual(self.palindrome.is_palindrome_permutation("taco cat"), True)

unittest.main(exit=False)