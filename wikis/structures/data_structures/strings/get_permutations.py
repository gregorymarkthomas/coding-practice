#!/usr/bin/python

import unittest

"""
From Codewars (I think):

I copied this from a trello card that I had saved.
I am assuming that the task is to return all the permutations of a given string.

"""

class Permutator():   
    
    def get_permutations(self, permStr):
        result = []
        if permStr != None:
            for i in range(0, len(permStr)):
                result = self.__permutate(result, permStr[i])
        return result
    
    # 1. Add new result to prevResults which is a copy of 
    # 1. Take previous results ["perm1", "perm2"...] for last char and loop through each
    # 2. Create 
    # 2. At the start and after each char in these results, add newChar 
    def __permutate(self, prevResults, newChar):
        print("-----------------")
        print("prevResults = " + str(prevResults))
        print("newChar = " + newChar)
        
        newResults = []
        
        if len(prevResults) > 0:
            newResults = create_foundation_results(newResults, prevResults)
            
            prevResults.append(prevResults[len(prevResults) - 1])
            prevResults[0] = newChar + prevResults[0][0:]
            print("prevResults = " + str(prevResults))
            for i in range(1, len(prevResults)):
                for j in range(0, len(prevResults[i])):
                    # print("prevResults[i] = " + prevResults[i])
                    print("j = " + str(j))
                    # print("len(prevResults[i]) = " + str(len(prevResults[i])))
                    # print("prevResults[i] = " + prevResults[i])
                    # print("prevResults[i][:j] = " + str(prevResults[i][:j]))
                    print("prevResults[i][j:] = " + str(prevResults[i][j:]))
                    prevResults[i] = prevResults[i][j:] + newChar
                    print("prevResults[i] = " + str(prevResults[i]))
        else:
            newResults.append(newChar)
            
        return prevResults
    
    #
    # For the nth char in the permutation process, (n-1 results count * n) new results are required 
    #
    def create_foundation_results(newResults, prevResults):
        n = len(prevResults)
        for i in range(0, prevResults):
            newResults.append
        
    
class TestMethods(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.permutator = Permutator()
        
    def test_empty(self):
        self.assertEqual(self.permutator.get_permutations(""), [])
        
    def test_none(self):
        self.assertEqual(self.permutator.get_permutations(None), [])
    
    def test_single_char(self):
        self.assertEqual(self.permutator.get_permutations("a"), ["a"])
        
    def test_two_chars(self):
        self.assertEqual(self.permutator.get_permutations("ab"), ["ba", "ab"])
        
    def test_three_chars(self):
        self.assertEqual(self.permutator.get_permutations("abc"), ["cba", "bca", "bac", "cab", "acb", "abc"])

unittest.main(exit=False)


"""Old(?) copy found in https://trello.com/c/0DqEb9Gk.

import unittest

class Permutator():   
    
    def get_permutations(self, permStr):
        result = []
        if permStr != None:
            for i in range(0, len(permStr)):
                result = self.__permutate(result, permStr[i])
        return result
    
    def __permutate(self, prevResults, newChar):
        print("-----------------")
        print("prevResults = " + str(prevResults))
        print("newChar = " + newChar)
        newResults = []
        if len(prevResults) > 0:
            prevResults.append(prevResults[len(prevResults) - 1])
            for i in range(0, len(prevResults)):
                for j in range(0, len(prevResults[i])):
                    print("prevResults[i] = " + prevResults[i])
                    print("j = " + str(j))
                    print("len(prevResults[i]) = " + str(len(prevResults[i])))
                    print("prevResults[i] = " + prevResults[i])
                    print("prevResults[i][:j] = " + str(prevResults[i][:j]))
                    print("prevResults[i][j:] = " + str(prevResults[i][j:]))
                    newPerm = prevResults[i][:j] + newChar + prevResults[i][j:]
                    newResults.append(newPerm)
        else:
            newResults.append(newChar)
        return newResults    
    
class TestMethods(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.permutator = Permutator()
        
    def test_empty(self):
        self.assertEqual(self.permutator.get_permutations(""), [])
        
    def test_none(self):
        self.assertEqual(self.permutator.get_permutations(None), [])
    
    def test_single_char(self):
        self.assertEqual(self.permutator.get_permutations("a"), ["a"])
        
    def test_two_chars(self):
        self.assertEqual(self.permutator.get_permutations("ab"), ["ba", "ab"])

unittest.main(exit=False)

"""

