#!/usr/bin/python

import unittest

"""
From 'Cracking the Coding Interview':
Implement an algorithm to determine if a string has all unique characters. 
What if you cannot use additional data structures?

ANSWER:
- Ask if the string is ASCII or Unicode!
- As you noticed when developing this, there is a finite number of characters that can provide unique values.
  - If ASCII, it would be 128. If Extended ASCII, it would be 256.
  - So, you could return false if the String length is beyond that limit.
"""

class BruteForce():
	def is_unique(self, text):
		"""
		On input of a string ("text"), 
		return a boolean to determine if the characters within 'text' are all different from each other.
		Brute force:
		1. Loop through each element (character) of 'text' and find other instances.
		2. If not equal, continue. If equal, break and return false.

		O(n^2)
		"""
		result = True
		for char in text:
			if text.count(char) > 1:
				result = False
				break

		return result

class Optimised():
	def is_unique(self, text):
		"""
		How can we optimise this?
		Well, if we can get rid of a loop, that would be great.

		According to the book, we can use a cache of sorts.
		It involves having a 128 (or 256 if extended) array of booleans (falses by default).
		Each position represents the index in the alphabet.
		
		We can do a lookup and see...

		This is O(N).

		"""
		if len(text) <= 128:
			print("-------------------")
			char_set = [False] * 128
			for char in text:
				char_int = ord(char)
				print("char_int = " + str(char_int))
				if char_set[char_int]:
					return False
				char_set[char_int] = True
		else:
			return False

		return True

class Tester(unittest.TestCase):
	"""
	Results - BruteForce is faster than Optimised for these tests.
	"""

	@classmethod
	def setUp(self):
		self.is_unique_machine = Optimised()

	def test_success_small_text(self):
		self.assertEqual(True, self.is_unique_machine.is_unique("abcdef"))

	def test_failure_small_text(self):
		self.assertEqual(False, self.is_unique_machine.is_unique("abcdee"))

	def test_success_large_text(self):
		"""
		Remember - ASCII only! This means no £
		"""
		self.assertEqual(True, self.is_unique_machine.is_unique("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!$%^&*()_+-=[]{};'#:@~,./<>?\|"))

	def test_failure_small_text(self):
		"""
		Remember - ASCII only! This means no £
		"""
		self.assertEqual(False, self.is_unique_machine.is_unique("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!$%^&*()_+-=[]{};'#:@~,./<>?\||"))

unittest.main(exit=False)