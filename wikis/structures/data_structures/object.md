# Objects

## What is it?

An Object is an **instance** of a [Class]().

An object is a data structure that contains data fields, like a record does, as well as various methods which operate on the data contents. An object is an in-memory instance of a class from a taxonomy. In the context of object-oriented programming, records are known as plain old data structures to distinguish them from objects.[11]


## What should an object do?

An object should **hide** its _data_ and **expose** its _operations_.

An object should not expose its internal structure through accessors either, because that is exposing.


## What does this mean for further development?

Exposing behaviour and hiding data makes it:

- **easy** to add new kinds of objects without changing existing behaviours
- **hard** to add new behaviours to existing objects


## What is another name for an Object?

An **Instance**.