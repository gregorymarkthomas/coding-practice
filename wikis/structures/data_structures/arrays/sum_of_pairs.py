#!/usr/bin/python

import unittest

"""
From Codewars:

Find all pairs and note the pair's right-hand-partner position next to each.
Pair that closes the soonest is the chosen one.
When finding a partner, search in ints WITHOUT already-chosen partner, but maintain original position (hence left_pos + 1).

"""


def sum_pairs(ints, s):
    pairs = {}
    last_right_pos = None
    for left_pos, left_number in enumerate(ints):
        right_number = (s - left_number)
        try:
            right_pos = ints[left_pos+1:].index(right_number) + left_pos + 1
        except ValueError:
            right_pos = -1
            
        print("-----")
        print("s = " + str(s))
        print("ints = " + str(ints))
        print("left_number = " + str(left_number))
        print("left_pos = " + str(left_pos))
        print("right_number = " + str(right_number))
        print("right_pos = " + str(right_pos))
        
        if right_pos != -1:
            if last_right_pos is None or right_pos < last_right_pos:
                last_right_pos = right_pos
                pairs.update({right_pos: [left_number, right_number]})
                print("pairs = " + str(pairs))
            else:
                break
    
    first_pair_key = None
    for key in pairs:
        if first_pair_key is None or key < first_pair_key:
            first_pair_key = key        
    return pairs.get(first_pair_key)