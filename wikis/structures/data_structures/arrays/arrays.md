# Arrays

## What is it?

An Array is a Data Structure that is a collection of _homeogenous_ elements placed in **adjacent** cells in memory.


## How are elements retrieved?

Elements are retrieved using an index.


## Why is it often considered an [Abstract Data Type (ADT)](/data_type/abstract_data_type_adt.md)?

Some languages turn some structures like Arrays into ADTs _for convenience_, allowing for easy store ([`set()`]()) and retrieval ([`get()`]()) of elements, and other commands such as [append](), [prepend](), [pop](), [slice](), [forEach](), etc.