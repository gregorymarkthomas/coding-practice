# Heap (Data Structure)

## What is it?

A Heap is a specialised tree-based [Data Structure](../data_structure.md).


## What is so special about it?

A Heap is an _almost_ complete tree that satisifies the **heap property**.


## What is the **heap property**?

In a _max heap_, for any given node C, if P is a parent node of C, then the value (or, _key_) of P is **greater** than or equal to the value of C.

In a _min heap_, the value of P is **less** than or equal to the value of C.


## As a Data Structure, is Heap an _implementation_ of something?

Yes! The [Priority Queue](/abstract_data_type-adt/priority_queue.md) (a _maximally efficient_ one). In fact, Priority Queues are often referred to as "heaps".


## What is a common implementation of a Heap?

A common implementation of a Heap is the [Binary Heap](binary_heap.md) Data Structure, in which the tree is a [Binary Tree](binary_tree.md).