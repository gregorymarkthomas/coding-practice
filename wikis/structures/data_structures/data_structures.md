---
title:  Data structures
author: Gregory Thomas
date:   2020-03-15
---

# Data structures

## What is a data structure?

A Data Structure is a data _organisation_, _storage_ and _management_ **classification**.


## What?

A Data Structure is a **collection** of data values, their relationships, and operations that can be applied to said data.


## Is a Data Structure rather [abstract](\paradigms\object_oriented_programming_oop\principles\abstraction.md), then?

Yes, compared to [Data Types](\structures\software_entities\types\data_types.md).


## How does a Data Structure help to store data in [memory](/memory/memory.md)?

A Data Structure is used as a way of storing and arranging data efficiently in memory.


## What are Data Structures _in relation_ to [Data Types](\structures\software_entities\types\data_types.md) and [Abstract Data Types](abstract_data_types_adt.md)?

> Abstract Data Type > **Data Structure** > Data Type

Data Structures are the **implementations** that make-up [Abstract Data Types (ADTs)](abstract_data_types_adt.md). [Data Types](\structures\software_entities\types\data_types.md) are the **implementations** that make-up Data Structures.


## Do you have an example?

- ["Stack"](\memory\allocation\static\stack.md) is the [Abstract Data Type (ADT)](abstract_data_types_adt.md)
- [Array](\structures\software_entities\types\derived\arrays_and_strings\arrays.md) is the [Data Structure](/structures/data_structures/data_structures.md) used to implement the Stack
- [Integer](\structures\software_entities\types\primitives\integer.md) is the [Data Type]() used for the elements within the Array


## What Point of View do we see Data Structures from?

A Data Structure is a **representation of data** from the **implementer's** point of view.


## What should data structures do?

Data structures **expose** data **and have no significant behaviour**.


### What does this mean for data structures and further development?

This makes it easy to add new behaviours to existing data structures but makes it hard to add new data structures to existing functions.


## What are some examples of data structures?

- [binary trees](binary_trees.md)
- [fibonacci heap](fibonacci_heap.md)
- [Adelson-Velsky and Landis (AVL) tree](adelson_velsky_landis_tree.md)
- [skiplist](skiplist.md)
- [arrays](arrays/arrays.md)
- [linked lists](lists/linked_lists/linked_lists.md)
- [records/structs/tuples](record_struct_tuple.md)
- [objects](object.md)