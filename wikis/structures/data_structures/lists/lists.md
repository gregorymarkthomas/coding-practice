---
title:  Lists
author: Gregory Thomas
date:   2020-10-01
---

# Lists

## What are they?

A list is a _derived_ data type that _holds_ a sequence of other [data types](../data_types.md).


## Are Lists similar to [strings](../primitive/strings.md)?

Yes, but [strings](../primitive/strings.md) are [immutable](/terms/immutable.md), whereas **lists are [mutable](/terms/mutable.md)**.


## Links

- https://sites.google.com/a/aims-senegal.org/scipy/derived-data-types