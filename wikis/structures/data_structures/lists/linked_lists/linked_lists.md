# Linked lists

## What are they? 

A Linked List is literally a [list](/structures/software_entities/types/derived/lists.md) that is _linked_.


## Okay. What are they?

A linked list is a collection of nodes.


## What does each node contain?

Each node has its **data** and **a memory reference to the next item in the list**.


![image.png]


## How easy is it to add or remove nodes from the list?

It is easy to add/remove nodes from the _middle_, and not just from the end.


## Are there _different types_ of Linked List?

### Simple linked list

Can only progress nodes forward.


### Doubly linked list

Can traverse forward and backwards


### Circular linked list

Last node points to first node



## Does a Linked List have much in common with List programmatically?

It depends on the programming language; .NET's LinkedList<T> does not even _implement_ IList<T>.


## What is the _first_ node in a Linked List?

The first node in a Linked List is the *Head* node; this points to the first _real_ node, and **does not contain data**.


## What does the _last_ node in a Linked List point to?

The last node points to **null** to mark _end_ of list.


## When is a linked list at its most efficient?

A linked list is at its most efficient when accessing _sequential_ data (either forward or backward).


## In what instance can a Linked List perform slowly?

Random access to a Linked List is **slow** because the it has to walk the chain of nodes to get to the desired node.


## Can we _reverse_ a Linked List?

Yes:

1. Start at **end** node and change 'next' to the _previous_ address
2. Go to previous address and change it's 'next' to the address before
3. Change Head to point to address of end node
4. Change the first node to point to null to make it the new end node


## What's the difference between [Arrays](/structures/software_entities/types/arrays_and_strings/arrays.md) / [Lists](/structures/software_entities/types/derived/lists.md) and Linked Lists when it comes to increasing storage space?

Arrays and Lists that _change_ size need **copying to new memory location with more space** whereas Linked Lists only need to manage memory for **newly inserted/deleted nodes**.


## Where can I see an example of a linked list?

See [dupes.py](dupes.py); this uses a 'runner' technique where a new pointer checks the rest of the nodes, starting from the nth node, to check for duplicate node data in the linked list.