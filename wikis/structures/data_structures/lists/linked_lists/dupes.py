#!/usr/bin/python

"""A coding challenge.

From 'Cracking the Coding Interview':
'Create function to remove duplicates from a linked list. FOLLOW UP: can you do it without a temporary buffer?'

"""

import unittest

class LinkedListBook2():

	def __init__(self, head):
		self.head = head

	def remove_duplicates(self):
		"""
		From the book. Uses a two-pointer system.

		Hints: #40
		- Use a two-pointer system if you can't use a hash_table, but it will be an O(N^2) solution

		E.g. 3, 5, 3, 4, 2, 2

		This uses O(1) space but has O(N^2) complexity
		"""
		current = self.head
		while current != None:
			runner = current
			while runner.next != None:
				if runner.next.data == current.data:
					runner.next = runner.next.next
				else:
					runner = runner.next
			current = current.next

		return self.head

class LinkedListBook1():

	def __init__(self, head):
		self.head = head

	def remove_duplicates(self):
		"""
		From the book. Uses a hashtable.
		I struggled with the implementation of mine. This is good because it uses 'previous' whereas I was struggle with 'current' and 'next' a lot in my solution.

		Hints: #9
		- Use a hashtable - can be done in one pass

		E.g. 3, 5, 3, 4, 2, 2
		"""
		n = self.head
		previous = None
		hash_table = []
		while n != None:
			if n.data in hash_table:
				previous.next = n.next
			else:
				hash_table.append(n.data)
				previous = n
							
			n = n.next
			print("hash_table = " + str(hash_table))

		return self.head

class LinkedList():

	def __init__(self, head):
		self.head = head

	def remove_duplicates(self):
		"""
		How can we remove duplicates from the linked list without needing to sort it first??

		E.g. 3, 5, 3, 4, 2, 2

		The simple way is to have a temporary buffer that we refer to each time we progress to n.next.

		What exactly do we want to do here?
		- We want to loop through the linked list
		- We want to check the NEXT node in case we need to edit the CURRENT node
		- So, while n.next != None:
		- if n.next exists in the buffer, reroute n to next.next and unclip n.next
		"""
		n = self.head
		buff = []
		while n != None and n.next != None:
			# print("remove_duplicates: current n.data = " + str(n.data) + ", n.next.data = " + str(n.next.data) + ", n.next.next.data = " + str(n.next.next.data))
			buff.append(n.data)
			if n.next.data in buff:
				temp_node = n.next.next
				n.next.next = None
				n.next = temp_node
							
			n = n.next
			print("buff = " + str(buff))

		return n

	def sort(self):
		"""
		TODO: this will not work - there needs to be multiple passes!!
		Iterates through each node and compares data values; if next Node's data is less than the current Node's data, move next to where current is now.

		E.g. 3, 5, 3, 4, 2, 2

		5 > 3, so keep 3 and 5 where they are.
		3 < 5, so move 3 to where 5 is now

		E.g. 3, 5, 6, 4, 2, 1

		"""
		n = self.head
		while n.next != None:
			print("sort: current n.data = " + str(n.data) + ", n.next.data = " + str(n.next.data))

			if n.next.data < n.data:
				print("sort: moving " + str(n.next.next.data) + " to where " + str(n.next.data) + " is.")
				temp_node = n.next.next
				n.next.next = n
				n.next = temp_node

			n = n.next

		return n

class Node():

	def __init__(self, data):
		self.data = data
		self.next = None

	def add_to_tail(self, new_data):
		end = Node(new_data)
		n = self
		while n.next != None:
			n = n.next
		n.next = end
		# print("Latest data = " + str(n.data))
		# print("Latest next = " + str(n.next))


class Tester(unittest.TestCase):

	@classmethod
	def setUpClass(self):
		pass

	# def test_sort(self):
	# 	"""TODO: sort() needs multiple passes!"""
	# 	linked_list = LinkedList(Node(3))
	# 	linked_list.head.add_to_tail(5)
	# 	linked_list.head.add_to_tail(6)
	# 	linked_list.head.add_to_tail(4)
	# 	linked_list.head.add_to_tail(2)
	# 	linked_list.head.add_to_tail(1)

	# 	head = linked_list.sort()
	# 	self.assertEqual(head.data, 1)
	# 	self.assertEqual(head.next.data, 2)
	# 	self.assertEqual(head.next.next.data, 3)
	# 	self.assertEqual(head.next.next.next.data, 4)
	# 	self.assertEqual(head.next.next.next.next.data, 5)
	# 	self.assertEqual(head.next.next.next.next.next.data, 6)

	def test_duplication_removal(self):
		"""Simple duplication test"""
		linked_list = LinkedListBook2(Node(3))
		linked_list.head.add_to_tail(5)
		linked_list.head.add_to_tail(3)
		linked_list.head.add_to_tail(4)
		linked_list.head.add_to_tail(2)
		linked_list.head.add_to_tail(2)

		head = linked_list.remove_duplicates()
		self.assertEqual(head.data, 3)
		self.assertEqual(head.next.data, 5)
		self.assertEqual(head.next.next.data, 4)
		self.assertEqual(head.next.next.next.data, 2)
		self.assertEqual(head.next.next.next.next, None)


unittest.main(exit=False)
