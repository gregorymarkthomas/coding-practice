# Binary Digits (_Bits_)

## What are they?

Binary Digits (_bits_) are the individual _digits_ used to represent numbers **of different mathematical number systems** in a computer system.


## As the name suggests, I'm guessing bits use the [binary](/mathematics/number_systems/binary.md) number system?

Correct.


## But we can still represent numbers of different [mathematical number systems](/mathematics/number_systems/number_systems.md), yes?

Yes; they're just coverted down into binary for the computer to understand.


## How do we represent larger numbers than 1?

We use _multiple_ bits together.


## What are the common _collections_ of bits?

- [Crumbs](crumb.md) are **2** bits
- [Nibbles](nibble.md) are **4** bits
- [Bytes](byte.md) are **8** bits


## What's an example of using multiple bits together?

Say we have four bits (a [nibble](/data/computer_number_format/nibble.md)), all with the value of 1:

`1111`

The right-most 1 bit is similar to a [decimal](decimal.md)'s _units_, but remember that we are in [base](base.md) **2** and not base 10.
The right-most bit is _enabled_ (i.e. has a value of 1 and not 0), and this represents a decimal value of 1. 

The second-from-right-most bit is similar to decimal's _tens_, except we're dealing with _twos_; this bit is also enabled, and it represents a decimal value of 2.

The second-from-left-most bit is similar to a decimal's _hundreds_; this bit is also enabled, and it represents a decimal value of 4.

The left-most bit is similar to a decimal's _thousands_; this bit is also enabled, and it represents a decimal value of 8.

These are the _thousands_, _hundreds_, _tens_ and _units_ (**but in base 2**) that `1111` represents: 

'8421'

This means that `8`, `4`, `2` and `1` are _enabled_, therefore the decimal value of binary `1111` is 8+4+2+1, which equals **15**.


## How do we encode _negative_ numbers in binary for _computers_ (i.e. not pure mathematics)? 

There are different [signed number representations](signed_number_representations/signed_number_representations.md) we use: 

- [sign-and-magnitude](signed_number_representations/sign_and_magnitude.md)
- [ones' complement](signed_number_representations/ones_complement.md)
- [two's complement](signed_number_representations/twos_complement.md) (**most-used**)
- [offset binary](signed_number_representations/offset_binary.md)



