# Computer Number Format

## What is it?

A Computer Number Format is the _internal representation_ of **numeric values** in both [hardware](/hardware/hardware.md) and [software](/software/software.md).

## So, how _are_ numbers represented in a computer system?

_Most_ computers use the [binary](/mathematics/number_systems/binary.md) number system, though some use [octal](/mathematics/number_systems/octal.md) or [hexadecimal](/mathematics/number_systems/hexadecimal.md).