# Signed numbers

## What are these?

Numbers represented as

## What is a _signed_ number?

A **signed** variable/number allows us to represent numbers in both _positive_ **and** _negative_ ranges.


## What is an _unsigned_ number?

An **unsigned** variable/number will _only_ allow a _positive_ range.


## What affects the maximum _size_ of the number?

The [data type](../data_types.md) chosen will affect how many [bits](/memory/binary_digit_bit.md) can be used to represent the number.


## So, the _sign_ of the number affects the _range_ of number within that [data type](../data_types.md)?

Yes; e.g, a _signed_ `byte` can be a value between -128 and 127, but an _unsigned_ `byte` can be from 0 to 255.