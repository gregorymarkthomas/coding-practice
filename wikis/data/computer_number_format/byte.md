# Bytes

## What is it?

A _byte_ describes **8 bits**. A byte is also a [*primitive* **data type**](\structures\software_entities\types\primitives\byte.md) in most programming languages as _implementations_ for the term _byte_ to store numbers up to 8 bits.


## How much memory does one use?

A byte uses **8** bits.


## What's the biggest number we can represent with a byte?

It depends on the [implementation of the byte](\structures\software_entities\types\primitives\byte.md).


## What is another name for a byte?

An _octet_.