---
title:  Unit testing
author: Gregory Thomas
date:   2020-03-13
---

# Unit testing

## What is it?

Unit testing tests small sections of code. This is opposed to [integrated testing](integrated_testing.md) which tests view/controller/model within one test.


## Languages

### Python

```unittest``` is a [unit testing](testing/) library for Python.

Any coding test that uses Codepad will require a basic ```unittest``` setup.

Here is an example setup:

```
import unittest

...

class TestMethods(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.permutator = Permutator()
        
    def test_empty(self):
        self.assertEqual(self.permutator.get_permutations(""), [])
        
    def test_none(self):
        self.assertEqual(self.permutator.get_permutations(None), [])
    
    def test_single_char(self):
        self.assertEqual(self.permutator.get_permutations("a"), ["a"])
        
    def test_two_chars(self):
        self.assertEqual(self.permutator.get_permutations("ab"), ["ba", "ab"])
        
    def test_three_chars(self):
        self.assertEqual(self.permutator.get_permutations("abc"), ["cba", "bca", "bac", "cab", "acb", "abc"])

unittest.main(exit=False)
```