# Dynamic memory allocation

## What is it?

A type of [memory allocation](../allocation.md).


## _Where_ is Dynamically allocated memory stored?

In the [heap](free_store-heap.md) (also known as the _free store_).


## For **Dynamic** memory allocation, _when_ is memory allocated to the application?

At ['runtime'](/execution/runtime.md).


## Why at [runtime](/execution/runtime.md)?

Dynamic memory allocation is done at runtime as its **typically used for data of an unknown size**.


## _How_ is memory dynamically (de)allocated, in general?

How memory is allocated depends on the programming language; [C](/language_specific/c/c.md) requires _manual_ memory management of Dynamically allocated memory with the use of the [`malloc`](/language_specific/c/memory/allocation/dynamic/malloc.md) function to allocate, and the [`free`](/language_specific/c/memory/allocation/dynamic/free.md) function to deallocate. Newer languages such as [Java](/language_specific/java/java.md) dynamically allocate memory blocks _automatically_, and use a [Garbage Collector (GC)](/memory/garbage_collector-gc.md) to automatically deallocate memory blocks. 


## What is the point of Dynamic memory allocation?

Modern computer systems run more than one [process](/concurrency/processes.md) at a time, and each process needs the use of the computer's resources, so freeing up memory allows for other processes to use it.


## Is the term Memory Management a _general_ term for Dynamic memory allocation?

Yes.