# Static memory allocation

## What is it?

A type of [memory allocation](../allocation.md).


## For **static** allocation, _when_ is memory allocated to the application?

At [compile time](/execution/compile_time.md).


## What 