# Memory Allocation

## What is it?

Memory allocation is the process of "setting-aside" _sections_ of memory for applications.


## What does an application store in memory?

- [Variables](/memory/variables/variables.md)
- Instances of [data structures](/structures/data_structures/data_structures.md) and [classes](/structures/software_entities/types/classes/classes.md)


## Are there different _types_ of memory allocation?

Yes: 

- [static](static/static.md)
- [dynamic](dynamic/dynamic.md)