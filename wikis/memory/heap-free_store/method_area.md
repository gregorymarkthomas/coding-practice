# Method Area

## What is it?

A Method Area is an area in the [Stack](stack.md) for [methods/functions](/structures/software_entities/functions/functions.md).


## Some runtime data areas are shared among all of an application's threads and others are unique to individual threads.



## Useful. What's another description?

 Each instance of the JVM has one method area and one heap. These areas are shared by all threads running inside the VM. When the VM loads a class file, it parses information about a type from the binary data contained in the class file. It places this type information into the method area.


## Links

- https://www.artima.com/insidejvm/ed2/jvm2.html
- https://stackoverflow.com/questions/22921010/what-is-run-time-constant-pool-and-method-area-in-java