---
title:  Class/Static variables
author: Gregory Thomas
date:   2020-07-16
---

# Class/Static variables

## What are they?

A [class](../classes/classes.md) or [static](/modifiers/static.md) variable is a [variable](variables.md) whose value is *consistent* across ALL instances of the [class](/structures/software_entities/classes/classes.md).


## How are they instantiated?

Class variables are declared _outside_ of any class [function](/structures/software_entities/functions/functions.md), [constructor](/structures/software_entities/classes/constructors.md) and [block](/structures/software_entities/code_block.md).

Normally the _static_ [keyword](/terms/keyword.md) is used to announce that the following variable will be a class variable.


## Do you have a code example of this?
### Java

```
class Foo() {
  public static int BAR = 0;
}

System.out.println(Foo.BAR);  // Returns 0
Foo.BAR = 1;
System.out.println(Foo.BAR);  // Returns 1
```

### Kotlin

Kotlin uses ```companion``` objects within a class to allow [variables](variables.md) and [functions](../functions/functions.md) to be accessed statically:

```
class Foo {
  companion object {
     fun a() : Int = 1
  }
}
```

The ```companion``` object is accessed using ```Foo.a();``` (or ```Foo.Companion.a();``` if accessing Kotlin code via Java).


#### Why?

Kotlin has made it more difficult to create and access static objects in an effort to encourage more _maintainable_ coding practices.

Static objects are considered _anti [Object-Oriented Programming (OOP)](/paradigms/object_oriented_programming/object_oriented_programming.md)_, but are quite convenient. Kotlin has allowed static objects but can only be accessed in an OOP-friendly way.



## So, how many copies of this variable are there stored in memory?

Only **one**; there could be many instances of that class, but there will only be one copy of this static value in memory.


### Where specifically are static variables stored?

Static variables are stored in the [heap](/memory/heap.md).

#### Java

Static variables are stored in the [Permanent Generation](/language_specific/java/memory/permanent_generation.md) area of the [heap](/memory/heap.md).