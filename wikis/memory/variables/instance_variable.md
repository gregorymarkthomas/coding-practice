---
title:  Instance variables
author: Gregory Thomas
date:   2020-07-16
---

# Instance variables

## What are they?

An _instance_ variable is a [variable](variables.md) whose value is unique to that class *instance*.


## Right, so, why would you use these?

We would use an instance variable if we need to reference a [class instance's](../instances.md) state throughout the application.

The instance variable is a part of the [class instance's](../instances.md) _state_; the value of this variable is set when the [instance](../instances.md) is _created_, and this means we can retrieve the [instance](../instances.md) state wherever that instance is shared.


## What is an example of this in code?

```
class Foo() {
  public int bar = 0;
}

Foo foo = new Foo();
foo.bar = 1;
System.out.println(foo.bar);  // Returns 1

Foo foo2 = new Foo();
System.out.println(foo2.bar);  // Returns 0
```

## What is another name for instance variables?

- _member_ variables
- [predicates](/mathematics/predicate.md) (**providing the return datatype is a [boolean](data_types/primitive/boolean.md)**)


## How is an instance variable stored in memory?

When space is allocated for a [class instance](../instances.md) in the [heap](/memory/heap.md), a slot for each instance variable is created.