---
title:  Variables
author: Gregory Thomas
date:   2020-03-15
---

# Variables

## What are they?

A variable is a **reference** to a space in memory.

In most languages, a variable declaration (such as ```var myVariable = 1```) is **creating a reference** to a **space in memory**; ```myVariable``` is a reference to a memory space and that memory space has a value of *whatever was assigned to it* (in this case, *1*).

The reference is like the address on a letter to your house; it references your house *but is not your actual house*.


## So, this is not a [data structure](/structures/data_structures/data_structures.md)?
No; a [data structure](/structures/data_structures/data_structures.md) *contains* references and it's own data.


## What are some other names?

- Attributes
- Fields
- Properties


## Is the term "variable" a general term?

Yes; **there are different types of variable depending on where they are instantiated.**


## What specific types of variable are there?

[Instance variables](instance_variable.md) (also called _member_ variables) are created within a [class](/structures/software_entities/classes/classes.md), and their values are unique to that _instantiation_ of that class.

[Class/Static variables](class_static_variable.md) are created within a [class](/structures/software_entities/classes/classes.md), and its value is _same across all instances of that class_.

[Local variables](local_variable.md) are created within [functions](/structures/software_entities/functions/functions.md), [constructors](/structures/software_entities/classes/constructors.md) and [blocks](/structures/software_entities/code_blocks.md) and can only be used within those contexts.  