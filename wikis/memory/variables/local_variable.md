---
title:  Local variables
author: Gregory Thomas
date:   2020-07-16
---

# Local variables

## What are they?

A _local_ variable is a [variable](variables.md) that is created within (and **related only to**) the current [function](../functions/functions.md), [contructor](../classes/constructors.md) or [block](../code_blocks.md). 


## So, what happens to the local variable once the code jumps out of the function/constructor/block?

The local variable is collected by the [garbage collector](/terms/garbage_collector.md).


## Do you have an example of a local variable being defined?

```
public static void main(String args[]) {
  int a = 100;
  System.out.println("Value of local variable a: " + a);
}

// Trying to access "a" outside of main() will throw a compiler error.
```


## This all sounds like [Automatic Variables](automatic_variable.md); are they?

The term 'local variable' is usually synonymous with [Automatic Variables](automatic_variable.md), since these are the _same thing in many programming languages_, **but local is more general**; _most_ local variables **are** automatic local variables.

Disparity arises due to the fact that **static local variables also exist**, notably in [C](/language_specific/c/c.md). For a static local variable, the allocation is static (the lifetime is the entire program execution), not automatic, **but it is only in scope during the execution of the function**. 