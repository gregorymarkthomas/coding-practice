# Automatic variable

## What is it?

An Automatic Variable is a [**local** variable](local_variable.md) which is allocated and deallocated _automatically_ when the program flow **enters and leaves the variable's [scope](/terms/scope.md)**.


## What happens to local data of a function when it calls another function?

Due to the local data's [scope](/terms/scope.md), it is invisible and inaccessible to a _called_ function (unless it is a [nested function](\structures\software_entities\functions\nested_function.md)), but **it is not deallocated from the heap** as the calling function will come back into scope as the execution thread returns to the caller. 


## This all sounds like [Local Variables](local_variable.md); are they?

The term '[Local Variable](local_variable.md)' is usually synonymous with Automatic Variables, since these are the _same thing in many programming languages_, **but local is more general**; _most_ local variables **are** automatic local variables.

Disparity arises due to the fact that **static local variables also exist**, notably in [C](/language_specific/c/c.md). For a static local variable, the allocation is static (the lifetime is the entire program execution), not automatic, **but it is only in scope during the execution of the function**. 


## Links

https://en.wikipedia.org/wiki/Automatic_variable