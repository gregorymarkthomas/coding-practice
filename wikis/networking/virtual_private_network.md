---
title:  Virtual Private Network (VPN)
author: Gregory Thomas
date:   2020-08-06
---

# Virtual Private Network (VPN)

## What is it?

A Virtual Private Network (VPN) is an _extension_ of a private network to allow users to connect to it over a public network.


## What do you mean by a private networks?

Think of your home [Local Area Network (LAN)](local_area_network.md); multiple devices communicate with each other and access the [Internet](/networking/internet.md) via your [router](/networking/router.md).


## What do you mean by a public network?

The [Internet](/networking/internet.md).


## So, essentially, a user using a VPN has their home [LAN](local_area_network.md) _extended_ to them over the [Internet](/networking/internet.md)?

Yes; the user is able to communicate with and control other devices on their home [LAN](local_area_network.md) from anywhere on the [Internet](/networking/internet.md).
To allow communication between other VPN users can be turned off for security purposes.


## Are there any _security_ perks with using a VPN?

Yes; VPNs use a VPN "tunnel" for client and server to communicate through. The VPN tunnel [encrypts](/security/cryptography/encryption.md) network traffic so that third-parties cannot see what is being sent by the client, and third-parties cannot see _where_ traffic is going because all traffic flows through the VPN server. 

VPN implementations such as [OpenVPN](/operating_system/linux/software/openvpn.md) use [Transport Layer Security (TLS)](/security/transport_layer_security.md)/[Secure Sockets Layer (SSL)](/security/secure_sockets_layer.md) to [encrypt](/security/cryptography/encryption.md) traffic between the VPN server and its clients.


## How is a VPN set up in the real world?

A machine (perhaps a [Linux](/operating_system/linux/linux.md) server, or some more advanced [network routers](/network/routers.md)) acts as the VPN _server_ (using, say, [OpenVPN](/operating_system/linux/software/openvpn.md)) and is connected to the Internet. Using the VPN server's Internet IP address (or domain name if setup) and some sort of authentication, clients can connect to said server from anywhere on the Internet. Depending on the setup of the VPN, clients on the VPN can now communicate with each other from any location via the Internet.


## What is an example of a VPN implementation?

[OpenVPN](/operating_system/linux/software/openvpn.md) is one such implementation of a VPN.


## How do users authenticate with the VPN?

We _can_ setup a VPN to be accessible with a username and a passphrase per user. However, if login credentials ended up in the wrong hands, then our VPN will be _compromised_; a malicious third-party could log in to the VPN as that client from anywhere using their _own_ machine and could potentially control your machines on your [LAN](local_area_network.md) from afar.


## Okay, so, is there a _more_ secure way for users to authenticate with the VPN?

Yes.
A more secure way of using the VPN between clients is to set up a [Public Key Infrastructure (PKI)](/security/public_key_infrastructure/public_key_infrastructure.md); we want to restrict server communication to _specific_ (and authorised) clients, and to do this involves establishing [Public/Private Key Pairs](/security/public_private_key_pair.md) per client only granted by a [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md). The [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) should be a **dedicated machine separate from the VPN server machine**, and should be offline unless doing the one-time grant of access for a VPN client.