---
title:  Secure Shell (SSH)
author: Gregory Thomas
date:   2020-04-10
---

# Secure Shell (SSH)

## What is it?

Secure Shell (SSH) is a *[network protocol](protocol.md)*.

It's 