---
title:  'abstract' modifier
author: Gregory Thomas
date:   2020-10-01
---

# 'abtract' modifier

## What is it?

The ```abtract``` modifier is found in higher-level programming languages; ```abstract``` sets limits on [classes](/structures/software_entities/classes/classes.md) and [functions](/structures/software_entities/functions/functions.md) in order to [abstract](/paradigms/object_oriented_programming_oop/principles/abstraction.md) code from a developer.