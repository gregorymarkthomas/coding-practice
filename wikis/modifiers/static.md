---
title:  'static' modifier
author: Gregory Thomas
date:   2020-03-13
---

# 'static' modifier

## What is it?

A *non-access* modifier found in higher-level programming languages.

It can be applied to:

- blocks (?)
- variables
- methods
- nested classes


## What does the "static" modifier do?

- It makes the object accessible *before* any of its class is created, *without reference to any object*.
- A **single copy** of the variable is *shared* amongst all objects at **class level**.
  - They are **global** variables in the context of the class.
  - **All instances of the class share the same single static variable**.


## What is a static 'block'/'clause'?

This block (demo'd below) is executed only *once* either **when you make an object of that class** or **the first time you access a static member of that class**.
It is called **before** the constructor.

```
// Java program to demonstrate use of static blocks 
class Test 
{ 
    // static variable 
    static int a = 10; 
    static int b; 
      
    // static block 
    static { 
        System.out.println("Static block initialized."); 
        b = a * 4; 
    } 
  
    public static void main(String[] args) 
    { 
       System.out.println("from main"); 
       System.out.println("Value of a : "+a); 
       System.out.println("Value of b : "+b); 
    } 
} 
```


## What order are static items called?

In the order they are defined in code. For example:

```
...
static int a = m1();

static {
  System.out.println("Inside static block");
}

static int m1() {
  System.out.println("from m1");
  return 20;
}

public static void main(String[] args) {
  System.out.println("Value of a : " + a);
  System.out.println("from main");
}
...
```

Output:

```
from m1
Inside static block
Value of a: 20
from main
```


## What are special about static methods?

Think Java's ```main()``` method.
They can be accessed without a class instantiation object.

- Can only call other static methods
- Can only access *static data*
- Cannot refer to ```this```/```super```


## Why use static methods/members?

- To *maintain* the same data between all instantiations.