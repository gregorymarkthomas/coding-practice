---
title:  Is number an odd number
author: Gregory Thomas
date:   2020-03-13
---

# Is number an odd number

## What is the problem?

We want to find out if a number is an _odd_ number.


## How would we do this?

We would use the [modulo operator](/mathematics/operators/modulo.md) in this way:

```
bool isOdd = x % 2 != 0
```

The above is just a way to work out if a number is odd.


## Can you show this working?

## An odd number

```x % 2``` checks to see _how many times_ ```2``` fits in ```x```, _and returns the remainder_; this is done with the [modulo operator](/mathematics/operators/modulo.md).

For example, we want to check if the number ```1``` is an odd number, so now ```x``` equals ```1```.
```1 % 2``` checks to how many times ```2``` fits in ```1```, and returns the remainder.
In this case, ```2``` does not fit in ```1``` at all, but, there is still a _remainder_ value (an overspill, of sorts) of ```1```; this changes the result of the comparison (```!= 0```).
The remainder ```1``` is definitely not ```0```, so this comparison would cause ```isOdd``` to have a value of ```true```.


### An even number

```
int x = 2
bool isOdd = x % 2 != 0
print(isOdd)

# false
```

Say ```x``` is 2. 

```2``` fits in ```2``` once, with a remainder of ```0```; this means that the comparison will return ```false``` (i.e. isOdd will be ```false```, which could be seen as _even_).


### A larger odd number

```
int x = 35
bool isOdd = x % 2 != 0

# true
```

Say ```x``` is 35. 

```2``` fits in ```35``` 17 times (```2 * 17 = 34```), with a remainder of ```1```; because ```1``` does not equal zero, this means that isOdd will return ```true```.


### A larger even number

```
int x = 36
bool isOdd = x % 2 != 0

# false
```

Say ```x``` is 36. 

```2``` fits in ```36``` 18 times (```2 * 18 = 36```), with a remainder of ```0```; because ```0``` _does_ equal zero, this means that isOdd will return ```false```.