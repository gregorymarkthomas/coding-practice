# F# (F Sharp)

## What is it?

F# (F Sharp) is a programming language under the [.NET](../dot_net.md) software development platform.


## What are its draws?

It is apparently easy to write succinct, robust and performant code with F#.


## What is F# primarily used on?

It _seems_ to be used more for data science.