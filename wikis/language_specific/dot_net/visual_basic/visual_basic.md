# Visual Basic

## What is it?

Visual Basic is a programming language under the [.NET](../dot_net.md) software development platform.


## What are its draws?

Visual Basic is apparently an approachable language with a simple syntax for building type-safe, object-oriented apps.


## What is Visual Basic primarily used on?

It _seems_ to be used more for Windows applications before [C#](../c_sharp/c_sharp.md) took over.