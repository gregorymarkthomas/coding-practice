---
title:  Common Language Runtime (CLR)
author: Gregory Thomas
date:   2020-03-15
---

# Common Language Runtime (CLR)

## What is it?
Common Language Runtime is a [Virtual Machine](/execution/virtual_machine.md) for the [.NET](../dot_net.md) framework.

It can be called a *managed execution environment*.

It is much like Java's [Java Virtual Machine (JVM)](../../java/execution/java_virtual_machine.md), except it accepts *any* language written for the [.NET](../dot_net.md) framework.

The CLR Virtual Machine **translates** source code (of different languages, like C#) into a form of [bytecode](/execution/bytecode.md) called [Common Intermediate Language (CIL)](common_intermediate_language.md) so that it can be translated into native executable code.

This is why .NET applications do not need recompiling for different operating systems.
