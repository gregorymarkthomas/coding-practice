---
title:  C#
author: Gregory Thomas
date:   2020-03-15
---

# C# #

## What is it?

A programming language.


## Is it multi-paradigm?

No; it uses the [Object-Oriented Programming (OOP)](/paradigms/object_oriented_programming.md) paradigm.


## What is the process of execution?

C# uses the [**write-compile-test-recompile**](/execution/write_compile_test_recompile_cycle.md) cycle.