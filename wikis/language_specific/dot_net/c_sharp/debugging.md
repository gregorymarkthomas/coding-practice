# Debugging
## How to force debugger to run anywhere

```
           // TODO: debug
            if (!System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Launch();
```

This can be done in code called from areas like PackageManagerConsole - put it at top of a function (e.g. Seed() in Configuration.cs) and see that you can use the debugger!
