# Structure Types

## What is a Structure Type?

It is a Value Type that can _encapsulate_ data and related functionality.


## How do you define a Structure Type?

Using the `struct` keyword:

```c#
public struct Coords
{
    public Coords(double x, double y)
    {
        X = x;
        Y = y;
    }

    public double X { get; }
    public double Y { get; }

    public override string ToString() => $"({X}, {Y})";
}
```


## Links

- https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/struct
