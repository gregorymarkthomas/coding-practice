# Value Types

## What are Value Types?

A Value Type is one of the two C# types.


## What does a _variable_ of a Value Type contain?

The variable contains the direct _instance_ of the type.


## What is the other C# type?

[Reference types](../reference_types/reference_types.md)


## How do Value Types differ from Reference Types, then?

Reference Type variables contain a **reference** to an instance of the type.


## What happens when you _use_ a variable of a Value Type?

On [assignment](/wikis/language_specific/dot_net/c_sharp/assignment_operators/assignment_operators.md), passing an argument to a method, and returning a method result, **variable values are _copied_**.


## Do you have an example to show this copying?

```c#
using System;

public struct MutablePoint
{
    public int X;
    public int Y;

    public MutablePoint(int x, int y) => (X, Y) = (x, y);

    public override string ToString() => $"({X}, {Y})";
}

public class Program
{
    public static void Main()
    {
        var p1 = new MutablePoint(1, 2);
        var p2 = p1;
        p2.Y = 200;
        Console.WriteLine($"{nameof(p1)} after {nameof(p2)} is modified: {p1}");
        Console.WriteLine($"{nameof(p2)}: {p2}");

        MutateAndDisplay(p2);
        Console.WriteLine($"{nameof(p2)} after passing to a method: {p2}");
    }

    private static void MutateAndDisplay(MutablePoint p)
    {
        p.X = 100;
        Console.WriteLine($"Point mutated in a method: {p}");
    }
}
// Expected output:
// p1 after p2 is modified: (1, 2)
// p2: (1, 200)
// Point mutated in a method: (100, 200)
// p2 after passing to a method: (1, 200)
```

Operations on a Value Type variable **affect only that instance** of the Value Type that is stored in the variable.



## What happens when a Value Type contains a _Reference Type_ data member?

```c#
using System;
using System.Collections.Generic;

// 'struct' being key here.
public struct TaggedInteger
{
    public int Number;
    private List<string> tags;

    public TaggedInteger(int n)
    {
        Number = n;
        tags = new List<string>();
    }

    public void AddTag(string tag) => tags.Add(tag);

    public override string ToString() => $"{Number} [{string.Join(", ", tags)}]";
}

public class Program
{
    public static void Main()
    {
        var n1 = new TaggedInteger(0);
        n1.AddTag("A");
        Console.WriteLine(n1);  // output: 0 [A]

        // Copy made
        var n2 = n1;
        n2.Number = 7;
        n2.AddTag("B");

        // Result: 'Number' is unique to n2, but 'tags' is not as it references the same object.
        Console.WriteLine(n1);  // output: 0 [A, B]
        Console.WriteLine(n2);  // output: 7 [A, B]
    }
}
```

**The _Reference Type_ data member's _reference_ will be copied to the new Value Type variable's version.**


## Links

- https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/value-types