---
title:  App crashing on 32-bit devices
author: Gregory Thomas
date:   2020-03-27
---

# App crashing on iOS version < 9.3

This was spurred by bug #530, which was specific to the **Debug** build for the **iPhoneSimulator**, but this also goes for the _Release_ build on the _iPhone_ platform. 

The title of this is incorrect; the app crashing relates to only supporting 64bit architectures and the iOS version is only somewhat related.

The Kokpitt app supports older devices (seeing as it is just a WebView plus some extras). But during testing the app would crash when I was trying iPhoneSimulators that were running 8.3. I initially thought this was due to the lower iOS version, but was actually due to those iPhoneSimulators being 32-bit and our app not supporting 32-bit architectures.


## What option am I changing?

To get to this option, right-click the HBS.Kokpitt.MobileApp.iOS project in the Solution Pad on the left-hand side, go to Options -> iOS Build. 


## What did I change to fix the crashing?

With a "Configuration" of "Debug" and "Platform" of "iPhoneSimulator", the "Supported architectures" were set to "x86_64", which is the architecture of the Intel Core I7 inside the [MacBook Pro 2011](/Home/Tips-for-devs/iOS/IMPORTANT-re:-MacBook-Pro-late-2011) I am using for development. "x86_64" is fine if you are simulating **64-bit** iPhones/iPads, but there were times I was trying to test the app on iOS version 8.3, which, does exist on 64-bit devices like the 5s, 6 and 6 Plus, but is more prevalent on 32-bit devices.

For Release Configuration on the "**iPhone**" Platform, I set the Supported Architectures to "ARMv7 + ARMv64" to support 32-bit and 64-bit iPhones.


## So what does that mean?

Having any configuration (Debug, Release) running on a 32-bit iPhone**Simulator** (like the 5) requires _supporting_ the 32-bit architecture; by supporting just 64-bit architectures, you are saying that the Kokpitt app will run **only** on an 64-bit architecture, and if run on a 32-bit operating system (like iOS 8.3 on the iPhoneSimulator 5), it will not work.

The **Platform** will alter which 32-bit and 64-bit architectures you you want to support. When _simulating_ the app using the iPhoneSimulator, we are using the processor inside the device that is doing the simulating (in my case, the Intel Core I7 inside the [MacBook Pro 2011](/Home/Tips-for-devs/iOS/IMPORTANT-re:-MacBook-Pro-late-2011)); in this case, I am dealing with i386 (32-bit) and x86_64 (64-bit).
When changing the Platform to _iPhone_, we are now dealing with the processors inside the real iPhones; ARMv7 (32-bit) and ARMv64 (64-bit). ARMv**6** (another 32-bit architecture) is for old iPhones, devices that I chose not to support.

The Configuration (i.e. Release, Debug, etc) does not matter.


## So, it is not to do with the iOS version then?

No; there are some iPhones that are 32-bit that have a more recent iOS version (e.g. the iPhone 5 uses the A6 processor 32-bit processor yet supports iOS 9.3). So if this combination is chosen, the app will crash, though _this is due to the 32-bit architecture and not the iOS version_.

According to the official ["Why can't users install my application on an iPhone 4s, iPhone 5, or iPhone 5c?"](https://developer.apple.com/library/archive/qa/qa1910/_index.html) question, devices that run recent versions of iOS but do not support the arm64 instruction set include:

    iPhone 4

    iPhone 4s

    iPhone 5

    iPhone 5c

    iPod touch (5th Generation)

    iPad 2

    iPad 3rd generation

    iPad 4th generation

    iPad Mini

## Why does the iPhoneSimulator 5 on iOS 8.3 simulate okay when **not** running the app?

Because the iOS Build "Supported Architectures" option is specific to _Kokpitt_ and not for the iPhoneSimulators in general; the simulator itself is setup to support i386.


## What option do we set it to?

So, by setting the Supported Architectures for Debug builds on the iPhoneSimulator Platform to "i386 + x86_64", we allow any 32-bit or 64-bit iPhoneSimulator to run Kokpitt. This stops the app from crashing on start-up.

Same for the iPhone Platform; we set it to "ARMv7 + ARMv64".


## Why does this work?

The x86_64 architecture in the [MacBook Pro 2011](/Home/Tips-for-devs/iOS/IMPORTANT-re:-MacBook-Pro-late-2011) **supports** i386 32-bit applications, so when given a 32-bit iPhoneSimulator (like the iPhoneSimulator 5), it will run. But, as mentioned, we have to specify that Kokpitt can run on 32-bit systems first by setting the Supported Architectures option to "i386 + x86_64".

When the app is installed on an ARMv64 iDevice, it will run the 64-bit version of the app; if run on an ARMv7 iDevice, it will run the 32-bit version of the app.
 

## What are the downsides?

This may mean that the app filesize is greater.


## Links

See https://developer.apple.com/support/required-device-capabilities/ and check "arm64" support for each device.

https://docs.microsoft.com/en-us/xamarin/cross-platform/macios/32-and-64/?tabs=macos