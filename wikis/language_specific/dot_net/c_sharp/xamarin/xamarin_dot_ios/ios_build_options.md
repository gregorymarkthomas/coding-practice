---
title:  "iOS Build" options
author: Gregory Thomas
date:   2020-03-27
---

# "iOS Build" options

## What are they?

Options which affect building of our Xamarin.iOS app.


## How are these options accessed?

1. Right-click the Xamarin.iOS project in the Solution pad
2. Click Options
3. Click "iOS Build"


## What are some tips?

- **If 32-bit and 64-bit architectures are supported**, keep "Perform all 32-bit float operations as 64-bit float" set to TRUE


### iPhoneSimulator platform

- Disable "Optimize PNG images", regardless of build
- Stick to supporting just 64-bit architecture (x86_64) to reduce app size and speed up build time (unless you need to test/support app on 32-bit devices)
- If the iPhone platform configuration uses [LLVM](/execution/llvm.md) optimizing compiler, then iPhoneSimulator should too


### iPhone platform

- Use [LLVM](/execution/llvm.md) optimizing compiler if possible


### Debug 

- Enable "Strip native debugging symbols" to speed up build time
- Enable "Enable device-specific builds" to speed up build time
- "Don't [Link](/execution/linking.md)"; we need to be able to see where in code something crashes and it not be obfuscated.
  - Also [Just-In Time (JIT)](/execution/just_in_time.md) compilation is used instead of [Ahead Of Time](/execution/ahead_of_time.md), which is *faster*.


### Release

- "[Link](/execution/linking.md) SDK" is the default and is fine.
	- "[Link](/execution/linking.md) all" may not be safe because code not my own (NuGets, Components etc) may be obfuscated and cause a crash when accessed.


## Links

- https://docs.microsoft.com/en-us/xamarin/ios/deploy-test/ios-build-mechanics?tabs=macos