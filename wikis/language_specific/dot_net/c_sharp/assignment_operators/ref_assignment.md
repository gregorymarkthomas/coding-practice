# `ref` assignment

## What is a `ref` assignment?

Ref assignment `= ref` makes its left-hand operand an **alias** to the right-hand operand.


## Got an example?

```c#
void Display(double[] s) => Console.WriteLine(string.Join(" ", s));

double[] arr = { 0.0, 0.0, 0.0 };
Display(arr);

ref double arrayElement = ref arr[0];
arrayElement = 3.0;
Display(arr);

arrayElement = ref arr[arr.Length - 1];
arrayElement = 5.0;
Display(arr);
// Output:
// 0 0 0
// 3 0 0
// 3 0 5
```

## So basically, `arrayElement` variable is set as a **reference**?

Yes I think so.


## And whenever `arrayElement` is updated, it updates the array element too because they both point to the same reference?

I think so.


## Links

- https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/assignment-operator#ref-assignment