# Assignment operators

## What is an Assignment operator?

`=`: it assigns the _value_ of its right-hand operand to a **variable**, a [property](properties.md), or an [indexer](indexer.md) given by its left-hand operand.


## Do both sides need to be the same type?

Yes, or at least compatible.


## Links

- https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/assignment-operator