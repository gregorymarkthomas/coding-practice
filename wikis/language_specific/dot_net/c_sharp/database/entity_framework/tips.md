From https://stackoverflow.com/questions/58444410/joined-tables-in-entity-framework-c-sharp-efficient-method:


if by "joined tables" you are referring to a relational model with foreign keys relating records, then Entity Framework is designed specifically to work with this. For the best performance you want to ensure that EF has the relationships between entities mapped, and then leverage projection using Select when reading data to ensure you are only reading as much data as you need at any given time.

I don't recommend using Sprocs with EF. It does support them, but frankly I find mapping relationships is far more flexible. Views can be good for read-only representations of complex data models. It doesn't work for cases where you want to insert/update data. Explicit joins in EF is also an option, but should be used sparingly. Mapping the relationships and referencing those relationships in expressions is simple to read and use. When I see developers writing joins in LINQ, they may as well just be using ADO, it's missing out on the flexibility and power offered by EF.

Common performance pitfalls I see from developers new to EF:

Querying too much data
Querying data too often
Getting tripped up by lazy loading
Fixing EF errors with ToList
Big queries that should be queued
Excessive use of async
Querying too much data: This covers stuff like eager loading entire entity graphs (related data) and loading entities when all the code wants to do is get a count, check if a row exists, etc. Learn about IQueryable and how you can leverage it to perform counts, exists checks (Any) and other operations rather than fetching entities to do those checks. Passing entities to views for instance as a data container is a common issue when the view such as a search result page only displays a handful of fields. leveraging Select to populate a simple view model can speed up queries and reduce resource use. Additionally not factoring for potentially big result sets can kill performance. Searches should always have limits to the total # of rows returned and leverage paging. Loading full entity graphs should only really ever be needed when performing an update.

Querying too often: This covers relying heavily on lazy loading to load related data, and inefficient coding practices such as loading single entities in loops. Often this leads developers to do silly things like implementing caching, which lead to memory issues and stale data bugs. Caching should only be used for static data is not expected to change. Instead, where you need multiple entities, look to load sets of data in a single query. For instance when you are creating an order where there may be 10 order lines referring to a Product, rather than iterating over each order line and loading the single product to associate with that line, execute 1 query to load the 10 (or fewer) distinct identified products by ID.

Getting tripped by Lazy Loading: Probably the biggest unexplained performance hit I see people get caught on is lazy loading calls, either in code they don't expect it, or due to serializing entities. Serializers will touch every property on an entity so each and ever lazy load property will trigger a query if it wasn't eagerly loaded. When loading entities for search results, this can result in many individual queries getting triggered for every single search result returned. Basically, never return entities, and that includes never putting references to entities inside view models.

Fixing EF errors with ToList: This can happen when devs have a query where they want to use something like a function in their code or an unmapped property that does formatting or a calculation. EF6 will throw an error that the expression cannot be converted to SQL so the quick and dirty solution is to call ToList before the Where/Select call using that function. The consequence of this is that it can result in a query executing that loads all, or a considerable amount more data than expected before applying the condition. Where EF6 will throw an error, EFCore will materialize this partial query automatically. While it can still apply partial filtering, there is no guarantee that your queries could effectively be running a SELECT * FROM type query against the database. Watch the SQL that EF is generating as you're going through test runs and pay attention to the performance metrics.

Big queries that should be queued. Very heavy queries such as reports and very open-ended searches (I.e. text searches, etc.) should consider using a background process and a queue to prevent too many requests getting kicked off simultaneously. Queries that need to touch a lot of records should be executed against a synchronized read-only replica, or consider dirty reads if there is no replica, provided these are practical for the data being reported on. Queries that touch a lot of rows result in, and wait for a lot of locks, plus result in a lot of I/O requests from the database server. Queues help ensure that a lot of these don't get kicked off at once. The more of these that run, the more they interfere with each other (and other queries) compounding the performance problems.

Excessive use of async. EF supports async operations to help make things like web servers more responsive while potentially heavy queries are running. Unfortunately many devs think that this makes queries "faster", or that they should be consistent and use async operations everywhere. It doesn't, and they shouldn't. Asynchronous queries are nominally slower that synchronous ones, and should be used for queries that need to take a bit longer, freeing up the web server to serve other requests while it waits. async is not a performance boost, and should not be the default suggestion when investigating performance issues. Eliminate all of the above scenarios before resigning to the query just needing to take longer.

To really keep a tab on EF and performance issues, it helps a lot to get familiar with SQL profiling. This captures all queries that EF executes and you can look for any weird extra queries, or "heavy" queries. (lots of row hits or slow execution times) Queries with high row "touches" should be inspected even if tests seem fast enough. These can be problematic in production if a few of them get kicked off at once, or run under load with other operations competing for locks.

Edit: For Order & OrderType example:

For example as you said we have a table for Order and one for OrderType which each has its own DBSet. Now we want to join them and get a new model out of it in EF to reference in our LINQ expressions (OrderJoinTable) , How is this possible please show how you create this model in EF ?

We have an Order entity pointing to an Order table, and an OrderType entity pointing to an OrderType table. In your example you have DbSets for both Order and OrderType. I would assume that the Order table contains a FK to the OrderType, OrderTypeId

Initial Entities:

public class Order
{
   [Key]
   public int OrderId { get; set; }
   public int OrderTypeId { get; set; }
   public string OrderNumber { get; set; }
   /* Other order fields... */
}

public class OrderType
{
   [Key]
   public int OrderTypeId { get; set; }
   public string Name { get; set; }
}
In the DbContext you have two DbSets:

public DbSet<Order> Orders { get; set; }
public DbSet<OrderType> OrderTypes { get; set; }
From your question you want to populate something like a list of orders displaying the names of the order types rather than just their keys, so "joining" the two. Yes, this is certainly possible with EF, but it somewhat defeats the whole benefit of using an ORM over plain old ADO. With EF, we set up the relationship between entities (navigation properties) so that we can resolve this information automatically. Looking back at the entity declarations:

public class Order
{
   [Key]
   public int OrderId { get; set; }
   public int OrderTypeId { get; set; }
   public string OrderNumber { get; set; }
   /* Other order fields... */

   [ForeignKey("OrderTypeId")]
   public virtual OrderType OrderType { get; set; }
}
By setting up this navigation property, EF has all it needs to be able to build a query that can return the order type details when you query an order. You'll still likely have an OrderType DbSet since we'll want to look up Order Types to assign to lookup lists and associate against an order; However, for something like OrderLines which only exist as part of an order, these would not need DbSets, they would just be accessed through their respective orders via the navigation properties.

what the Query looks like:

To get the order with it's order type:

var order = context.Orders.Include(x => x.OrderType).Where(x => x.OrderId == orderId).Single();
This is an eager load scenario using Include. It will retrieve the single order, and also populate it's OrderType. From there you can get the order type name using order.OrderType.Name. Eager loading is optional. As long as the OrderType navigation is declared virtual and lazy loading isn't disabled on the DbContext, EF can load lazy-load properties on demand. OrderType would not be fetched with the order in the above expression, but accessing the .OrderType property on an order would signal EF to run another query to fetch and populate the order type for that order. This only works as long as the DbContext that loaded the order has not yet been exposed. Lazy loading can result in performance issues if relied on heavily.

For better performance you can utilize projection with .Select to populate view models / DTOs rather than relying on whole entities. If we want to perform a search listing orders with their order type name, we can declare an OrderSummaryViewModel as such:

// Something like your OrderJoinTable?
[Serializable]
public class OrderSummaryViewModel
{
   public int OrderId { get; set; }
   public string OrderNumber { get; set; }
   public string OrderType { get; set; }
}
Then when pulling those records:

var orders = context.Orders.Select( x => new OrderSummaryViewModel
{
    OrderId = x.OrderId,
    OrderNumber = x.OrderNumber,
    OrderType = x.OrderType.Name
}).ToList();
Note that this does not need to resort to eager loading, and populates view models so we don't need to worry about lazy loading either. We can access navigation properties within our Linq expression to filter as needed and populate our display models without worrying about explicit joins.

Explicit joins would be reserved for cases where we need to link purely unrelated entities to one another, such as with cases where an entity has a relationship to one of several possible entities. For example if I have an Address table which had an EntityId key that could contain a CustomerId or a BusinessId from a Customer or Business table respectively. However, this kind of table structure is highly inefficient and usually only exists when people are convinced it "saves space" or some other optimization. It will suffer from performance issues with no FK associations, and be prone to "breakages" without proper constraints. Still, when dealing with legacy systems with stuff like this you can still leverage explicit joins between entities.

EF can map relationships for many-to-one references, (like above) 1-to-many child collections, (such as Order to OrderLines) 1-to-1 relations (Order to OrderDeliveryDetails) and many-to-many relations (such as Customer to Address where 1 customer can have many addresses, and 1 address may be linked to many customers, all using a CustomerAddress table (CustomerId+AddressId)) You can find examples on how each of these scenarios can be mapped out with EF.

Share
Improve this answer
Follow
edited Oct 20, 2019 at 6:21
answered Oct 18, 2019 at 11:18
Steve Py's user avatar
Steve Py
24k22 gold badges2424 silver badges4040 bronze badges
From your explanation , you said the best way is to map the relations. After that I should make the join using LINQ syntax and EF would use the relation ? In this method there is another thing that Im confused with that how to define the newly generated joined table and use it as IQueryable when I dont have it in my model. – 
hamid mosavi
 Oct 18, 2019 at 15:37 
Why wouldn't it be part of your model? Each entity corresponds to a table. You don't need a DbSet for each entity, just for entities you want to be able to load on their own. An Order can reference an OrderType entity and contain OrderLines that reference Products. Each are tables with foreign keys, and mapped to entities. EF queries can access them through these references in Linq expressions without explicit joins. I.e. var productOrders = context.Orders.Where(o => o.OrderLines.Any(ol => ol.Product.ProductId == productId); Provide some examples to your question for any specific concerns. – 
Steve Py
 Oct 19, 2019 at 3:38
For example as you said we have a table for Order and one for OrderType which each has its own DBSet. Now we want to join them and get a new model out of it in EF to reference in our LINQ expressions (OrderJoinTable) , How is this possible please show how you create this model in EF ? – 
hamid mosavi
 Oct 19, 2019 at 7:00
I've expanded the answer to hopefully cover your question. – 
Steve Py
 Oct 20, 2019 at 6:21
Thank you for your help it was a complete walk-through for joining tables in EF. – 
hamid mosavi
 Oct 20, 2019 at 6:33