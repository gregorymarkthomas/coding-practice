# .NET

## What is .NET?

.NET (dot net) is a software development _platform_.


## What does .NET allow for?

Applications can be written over different platforms ("cross-platform"), in a variety of languages.


## What platforms are we talking here?

- Web
- Mobile
- Desktop
- Games
- Internet of Things (IoT)
- more


## What languages are we talking here?

- [C#](c_sharp/c_sharp.md)
- [F#](f_sharp/f_sharp.md)
- [Visual Basic](visual_basic/visual_basic.md)
