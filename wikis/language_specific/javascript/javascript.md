---
title:  Javascript
author: Gregory Thomas
date:   2020-03-13
---

# Javascript

## What is it?

A programming/scripting language used primarily for [web development](/terms/web_development.md).


## So, as it is used for web development, is it a [front-end](/terms/front_end.md) language or a [back-end](/terms/back_end.md) language?

It is *primarily* a [front-end](/terms/front_end.md) language, used for implementing *dynamic behaviour* in [HTML](/language_specific/html/html.md) and [CSS](/language_specific/css/css.md).

However, [node.js](/language_specific/javascript/frameworks/node_js/node_js.md) is a popular framework that uses Javascript that is used on the [back-end](/terms/back_end.md).


## What does Javascript have over other languages?

Its [**read-evaluate-print**](/execution/read_evaluate_print_cycle.md) cycle of the *console* is quick and simple; a user can use it as a calculator, AND an environment to write and test code.

Javascript is baked into [web development](/terms/web_development.md); it can be written *within* [HTML](/language_specific/html/html.md), like so:

```
		...

		<script>
			const para = document.querySelector('p');

			para.addEventListener('click', updateName);

			function updateName() {
			  let name = prompt('Enter a new name');
			  para.textContent = 'Player 1: ' + name;
			}
		</script>
	</body>
</html>
```

```
<button onclick="myFunction()">Click me</button> 
```


## Is there much other choice in language for what Javascript does?

No, but there are [Javascript frameworks](frameworks/frameworks.md) that improve on vanilla Javascript.


## Are there other distributions of Javascript?

No.


## Are there other versions of the Javascript *console*?

No.