---
title:  Strict mode
author: Gregory Thomas
date:   2020-03-13
---

# Strict mode

## What is it?

A keyword that enables a special restrictive mode.


## When was it introduced?

New to Javascript ES5.


## What does it do?

- Some previously-silent errors are now thrown
- Fixes mistakes which made it difficult for some Javascript engines to perform optimisations
  - Strict mode code can sometimes be faster than non-strict mode code
- Prohibits use of some future syntax


## How do you use it?

```

(function() {
  'use strict';
  // All function and variable code are scoped to this function
})();


```