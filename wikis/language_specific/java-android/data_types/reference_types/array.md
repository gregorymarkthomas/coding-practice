# Arrays in Java

## So this is an [Abstract Data Type (ADT)](/data_types/abstract_data_types-adt/abstract_data_types-adt.md)?

The Java Array type is an example of an implementation of the Array Abstract Data Type that uses an Array [Data Structure](/structure/data_structure/data_structure.md).


## Do indexes start at Zero?

Yes!



## What is the maximum size of an Array in Java?

The maximum size of an Array in Java is the same as the maximum size of an [Integer](); 2^32 - 1.


## Is an array an object?

Yes.