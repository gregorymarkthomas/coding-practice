# Object type

This term is not standardised, but, refers to the _run-time_ type of the **referent** (reference type).


## Explanation (from https://stackoverflow.com/questions/16730109/difference-between-object-type-and-reference-type)

Consider this code:

```Java
Object o = new Integer(3);
```

The reference o is of type Object. The object that it references is of type Integer.

So the "reference type" would be Object and the "object type" would be Integer.

What makes this confusing is that there's the (standardized, official) term "reference type" that encapsulates types that can be referenced. In Java that includes all classes, enums, interfaces, arrays. It excludes only the primitive types (int, ...).