# androidx

## What is it?

`androidx` is a _package_.


## When was it introduced?

API level 28.


## What is it used for?

`androidx` was introduced to **bundle** all [APK](\language_specific\android\compilation\android_package_apk.md)-related [packages](\language_specific\android\packages.md) together.


## Does this include [`support`](\language_specific\android\packages\android\support\support.md)/[`appcompat`](\language_specific\android\packages\androidx\appcompat\appcompat.md) packages too?

Yes; for example, `android.support.v7.app.AppCompatDialogFragment` is now stored under `androidx.appcompat.app.AppCompatDialogFragment`.


## What about the [`android`](android.md) package?

This _used_ to bundle all [APK](\language_specific\android\compilation\android_package_apk.md)-related [packages](\language_specific\android\packages.md) **together with all _operating system_-packages**.


## So `androidx` _separates_ OS packages from [APK](\language_specific\android\compilation\android_package_apk.md) packages?

Correct.


## Are packages that were [deprecated](/terms/deprecated.md) in `android` still deprecated in `androidx`?

My experience has been mixed; for example, `androidx.fragment.app.DialogFragment` for [creating custom dialogs](\language_specific\android\fragments\dialogs\creating_custom_dialogs.md) is [deprecated](/terms/deprecated.md) in `android` but **not** in `androidx`, even though Android Studio wanted me to use the [support FragmentManager](\language_specific\android\fragments\support_fragment_manager.md), which **only manages the [AppCompat](\language_specific\android\packages\androidx\appcompat\appcompat.md) version of DialogFragment!**s