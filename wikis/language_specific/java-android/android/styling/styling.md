# Styling

## Best practices

### Derive from pre-built style as much as possible

Deriving from a pre-built style helps us to not re-invent the wheel. e.g.:

```xml
<style name="Widget.Calendar.Button" parent="Widget.MaterialComponents.Button">
    <item name="backgroundTint">#f00</item>
</style>
```

### Use [material-components](material_components.md)

[material-components](material_components.md) has the latest design flares.


## What should I be aware of?

### Accidently overriding **global** styles for Widget

There are many options here, such as:

- `<item name="android:buttonStyle">@style/Widget.Calendar.Button</item>`
- `<item name="buttonStyle">@style/Widget.Calendar.Button</item>`
- `<item name="android:button">@style/Widget.Calendar.Button</item>`

Make sure to **whittle-down** the links above as some can **override** others. **REMEMBER to check other custom child themes**, as elements within those may _override_ another widget.

See [Material Components](material_components.md) as an example of the problem above.