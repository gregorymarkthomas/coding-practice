# Material Components

## What is it?

A library to allow for an _arguably_ easier way to bring Material Design into your apps.


## What do I need to be aware of?

### Auto-inflating View classes

According to the [material-components github](https://github.com/material-components/material-components-android/blob/master/docs/components/Button.md), `<Button>`s will **auto-inflate** as `<com.google.android.material.button.MaterialButton>` if your theme derives from any **non-Bridge** MaterialComponents theme (e.g. `Theme.MaterialComponents.*`)


### Setting a **global** style for Widget

I spent a lot of time trying to set all material-component Buttons to use a _custom_ style in styles.xml.
For a long time I had **all** of these links in an effort to make it work (`Widget.Calendar.Button` being my custom button theme that extends `Widget.MaterialComponents.Button`):

- `<item name="materialButtonStyle">@style/Widget.Calendar.Button</item>`
- `<item name="android:buttonStyle">@style/Widget.Calendar.Button</item>`
- `<item name="buttonStyle">@style/Widget.Calendar.Button</item>`
- `<item name="android:button">@style/Widget.Calendar.Button</item>`

What **FINALLY** worked is **removing all but `materialButtonStyle`**; perhaps I was manually overriding something in the background and causing issues by having calls to `buttonStyle` etc.

**REMEMBER** what is mentioned in [styling](styling.md): to check other custom child themes, as elements within those may _override_ another widget. For example, for the above `materialButtonStyle` to work, I also had to fix my link to `materialAlertDialogTheme`!


#### Theme-able elements

```xml
        <!-- MaterialComponents -->
        <item name="colorPrimary">@color/blue_purple</item> <!-- Buttons etc -->
        <item name="colorPrimaryVariant">@color/blue_purple_dark</item>
        <item name="colorOnPrimary">@color/white_off</item>
        <item name="colorSecondary">@color/red</item>
        <item name="colorSecondaryVariant">@color/pink_hot</item>
        <item name="colorOnSecondary">@color/pink_hot</item>
        <item name="colorError">@color/deep_chestnut</item>
        <item name="colorOnError">@color/white_off</item>
        <item name="colorSurface">@color/pink_hot</item> <!-- ToolBar -->
        <item name="colorOnSurface">@color/pink_hot</item>
        <item name="android:colorBackground">@color/green</item> <!-- Activity -->
        <item name="colorOnBackground">@color/pink_hot</item>
```