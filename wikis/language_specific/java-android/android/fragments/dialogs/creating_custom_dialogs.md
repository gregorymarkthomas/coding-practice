# Creating custom dialogs

## What class should we implement?

We should extend `androidx.appcompat.app.AppCompatDialogFragment` to create our own dialogs.


## Why not `androidx.fragment.app.DialogFragment`?

Well, it seems to be _semi-_[deprecated](/terms/deprecated.md); `android.app.DialogFragment` is deprecrated, but `androidx.fragment.app.DialogFragment` is **not**. 


## What happens when you use `androidx.fragment.app.DialogFragment`?

I can extend my custom dialog class without Android Studio telling me `androidx.fragment.app.DialogFragment` is deprecated.
Android Studio does, however, want me to use the **support** [FragmentManager](\language_specific\android\fragments\fragmentmanager.md), which, as far as I can tell, can only manage [AppCompat](\language_specific\android\packages\androidx\appcompat\appcompat.md) `DialogFragment`s.


## Should we not instantiate/extend Dialog?

No; our Dialog class cannot be managed by (support) [FragmentManager](\language_specific\android\fragments\fragmentmanager.md) if we do this.


## Who told you not to extend Dialog, eh?

[https://developer.android.com](https://developer.android.com/guide/topics/ui/dialogs?hl=ru):

> The Dialog class is the base class for dialogs, but you should avoid instantiating Dialog directly. 

> The DialogFragment class provides all the controls you need to create your dialog and manage its appearance, instead of calling methods on the Dialog object.

> Using DialogFragment to manage the dialog ensures that it correctly handles lifecycle events such as when the user presses the Back button or rotates the screen. The DialogFragment class also allows you to reuse the dialog's UI as an embeddable component in a larger UI, just like a traditional Fragment (such as when you want the dialog UI to appear differently on large and small screens).


## What about `AlertDialog`?

We can just return a new `AlertDialog` in `onCreateDialog()` of our custom class that extends `DialogFragment`:

### Kotlin

```kotlin
class FireMissilesDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage(R.string.dialog_fire_missiles)
                    .setPositiveButton(R.string.fire,
                            DialogInterface.OnClickListener { dialog, id ->
                                // FIRE ZE MISSILES!
                            })
                    .setNegativeButton(R.string.cancel,
                            DialogInterface.OnClickListener { dialog, id ->
                                // User cancelled the dialog
                            })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
```

```java
public class FireMissilesDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_fire_missiles)
               .setPositiveButton(R.string.fire, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // FIRE ZE MISSILES!
                   }
               })
               .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // User cancelled the dialog
                   }
               });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
```