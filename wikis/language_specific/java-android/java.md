# Java

## What is it?

Java is a programming language.


## Is it multi-paradigm?

No; it uses the [Object-Oriented Programming (OOP)](/paradigms/object_oriented_programming.md) paradigm.


## What is the process of execution?

Java uses the [**write-compile-test-recompile**](/execution/write_compile_test_recompile_cycle.md) cycle.


## Polymorphism

### [Ad-hoc polymorphism](/paradigms/object_oriented_programming_oop/principles/polymorphism/adhoc/adhoc_polymorphism.md)

#### [Function overloading](/paradigms/object_oriented_programming_oop/principles/polymorphism/adhoc/function_overloading.md)?

Yes.

#### [Operator overloading](/paradigms/object_oriented_programming_oop/principles/polymorphism/adhoc/operator_overloading.md)?



### [Parametric polymorphism](/paradigms/object_oriented_programming_oop/principles/polymorphism/parametric/parametric_polymorphism.md)

#### [Generic functions](/paradigms/object_oriented_programming_oop/principles/polymorphism/parametric/generic_functions.md)?

Yes.

#### [Generic programming](/paradigms/object_oriented_programming_oop/principles/polymorphism/parametric/generic_programming.md)?



### [Subtype polymorphism](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/subtype_polymorphism.md)

#### [Virtual functions](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/virtual_functions.md)?



#### [Single dispatch](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/single_dispatch.md)?

Yes.

#### [Double dispatch](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/double_dispatch.md)?

No.

#### [Multiple dispatch](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/multiple_dispatch.md)?

No.