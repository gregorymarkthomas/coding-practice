# Java Platforms

## What are they?

A Java Platform **is a particular environment** in which [Java programming language](/language_specific/java-android/java.md) applications _run_.


## What Java platforms are there?

- [Java Standard Edition (Java SE)](java_standard_edition-java_se/java_standard_edition-java_se.md)
- [Java Micro Edition (Java ME)](java_micro_edition-java_me/java_micro_edition-java_me.md)
- [Jakarta Enterprise Edition (Jakarta EE)](jakarta_enerprise_edition-jakarta_ee/jakarta_enerprise_edition-jakarta_ee.md)
- [Java FX](java_fx/java_fx.md)


## What does **each** Java Platform consist of?

Each Java Platform contains:

- a [Java Virtual Machine (JVM)](shared_components/java_virtual_machine-jvm.md)
	- of various flavours
- an [Application Programming Interface (API)](shared_components/application_programming_interface-api.md)


## Links

- https://docs.oracle.com/javaee/6/firstcup/doc/gkhoy.html
- https://en.wikipedia.org/wiki/Java_(software_platform)
