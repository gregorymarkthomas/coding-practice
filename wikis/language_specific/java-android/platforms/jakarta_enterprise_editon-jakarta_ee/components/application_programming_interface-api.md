# Jakarta Enterprise Edition (Jakarta EE)'s Application Programming Interface (API)

The Jakarta EE platform provides an API and runtime environment for developing and running large-scale, multi-tiered, scalable, reliable, and secure network applications.