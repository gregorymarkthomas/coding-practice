# Java Platform Application Programming Interface (API)

## What is it?

Every [platform](/language_specific/java-android/platform/platform.md) has its own [Application Programming Interface (API)](/software/interfaces/application_programming_interface-api.md).


## Does each Platform's API define different things?

Yes.