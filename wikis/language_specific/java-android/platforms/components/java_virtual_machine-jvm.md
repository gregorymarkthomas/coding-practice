# Java Virtual Machine (JVM)

## What is it?

The Java Virtual Machine (JVM) is a component of **every** [Java platform](/language_specific/java-android/platform/platform.md).


## What is the JVM's basic operation?

The JVM is a program created for a particular hardware and software platform to allow it to run Java technology applications.



## This _is_ a [Virtual Machine](/execution/virtual_machine-vm.md), yes?

Yes.