# Java Micro Edition (Java ME)

## What is it?

Java Micro Edition (Java ME) is a Java (_platform_)(/language_specific/java-android/platform/platform.md).


## What does _Micro_ Edition do?

Java Micro Edition specifies several different sets of libraries (known as profiles) **for devices with limited storage, display, and power capacities**. 


## What sort of devices does Java ME target?

- mobile devices
- PDAs
- TV set-top boxes
- printers