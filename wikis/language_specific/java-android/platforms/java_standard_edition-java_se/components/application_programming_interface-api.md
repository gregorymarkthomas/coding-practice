# Java Standard Edition (Java SE)'s Application Programming Interface (API)

## What is it?

The API for Java SE is considered the **core** API for Java.


## What does the core API define?

- the basic types and objects of the Java programming language 
- high-level classes used for:
	- networking
	- security
	- database access
	- graphical user interface (GUI) development
	- XML parsing