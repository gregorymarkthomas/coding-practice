# Permanent Generation (PermGen)

## What is it?

Permanent Generation (or _PermGen_) is a special part of Java's memory structure.


## Is PermGen a part of the [heap](heap.md)?

No; it is separate.


## Why is PermGen called as such?

Java's memory ([heap](heap.md) and [non-heap](non-heap.md)) is all divided into _generations_, and PermGen holds data _permanently_, hence the name "Permanent Generation".


## What does Permanent Generation hold?

- [classes](/structures/software_entities/classes/classes.md)
- class _definitions_:
	- name of class
	- associated object arrays
	- optimisation information
	- [Java Virtual Machine (JVM)](../execution/java_virtual_machine.md)-internal class descriptors
- [static member variables](/memory/variables/static_variable.md)


## So what about dynamically generated data?

Dynamically generated data is stored in the [heap](heap.md)


## So Java still has a _regular_ [heap](heap.md) in addition to PermGen?

Yes.


## And [non-heap](non-heap.md)?

Yes.


## Is Permanent Generation still relevant to Java?

**No**; Permanent Generation was **replaced** by [MetaSpace](../8/meta_space.md) from [Java 8](../8/java_8.md).


## But for Java <= 7, Permanent Generation is still relevant?

Yes; that means that [Android](/language_specific/android/android.md) still uses a [heap](/memory/heap.md) with Permanent Generation, as Android mostly uses Java 7 with some elements of [Java 8](../8/java_8.md).



## How do we solves the `OutOfMemoryError: PermGen Space` error?

The OutOfMemoryError: PermGen Space error occurs when the permanent generation heap is full. Although this error can occur in normal circumstances, usually, this error is caused by a memory leak.

In short, such a memory leak means that a classloader and its classes cannot be garbage collected after they have been undeployed/discarded.

To give an example on how this can happen, let’s say we have a Payment class, which is part of a jar in a web application that is deployed on some webserver. In the lib folder of the web server, there is some logging framework present, which has a Log class with the method register(Class clazz) with which classes can be registered for logging. Let’s say that the Payment class gets registered by this method and the Log class starts keeping a reference to the clazz object. When the Payment class gets undeployed, it is still registered with the Log class. The Log class will still have a reference to it and hence, it will never be garbage collected. Moreover, since the Payment Class has a reference to its ClassLoader in turn, the ClassLoader itself will never be garbage collected either, and so will none of the classes it loaded.

An even more typical example is with the use of proxy objects. Spring and Hibernate often make proxies of certain classes. Such proxy classes are loaded by a classloader as well, and often, the generated class definitions – which are loaded like classes and stored in permanent generation heap space – are never discarded, which causes the permanent generation heap space to fill up.
Avoiding the error
1. Increasing the maximum size of the permgen heap

The first thing one can do is to make the size of the permanent generation heap space bigger.
This cannot be done with the usual –Xms(set initial heap size) and –Xmx(set maximum heap size) JVM arguments, since as mentioned, the permanent generation heap space is entirely separate from the regular Java Heap space, and these arguments set the space for this regular Java heap space. However, there are similar arguments which can be used(at least with the Sun/OpenJDK jvms) to make the size of the permanent generation heap bigger:

-XX:MaxPermSize=256m

would set its maximum size to 256m, which is 4 times bigger than the default size.
2. Use common sense when using static fields on classes.

Make sure you do not write classes that have static variables keeping references to class definitions and the like.
Using JDK dynamic proxies instead of cglib proxies

Some third party frameworks, such as cglib(although it might be better for newer versions of the library?), appear to be permgen monsters.

So using jdk dynamic proxies instead of cglib might be a good idea when getting the error.

Also, newer versions of Hibernate appear to not use cglib as a bytecode provider anymore, so upgrading your version of Hibernate, might drastically lower your chances on getting the error.
Summary

In general, when getting the error, one needs to determine why certain class definitions are not garbage collected. Once that is known, it should be possible to battle the error.


## Link

http://www.integratingstuff.com/2011/07/24/understanding-and-avoiding-the-java-permgen-space-error/


## Can I see a diagram?

Sure:

![JVM memory diagram](jvm_memory_diagram.jpg)