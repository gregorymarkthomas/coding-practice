# Heap in a [_Oracle_ Java Virtual Machine (JVM)](/language_specific/java-android/execution/java_virtual_machine-jvm/oracle/oracle.md)

## Where can I read about heaps in general?

[Read more about the heap here](/memory/heap-free_store.md).


## What is the Heap for JVM?

The Heap for JVM is the _runtime_ data area for which the Java VM allocates memory for all Dynamically generated data.


## What do you mean by Dynamically generated data?

Data such as [objects](/structures/data_structures/object.md) and [arrays](/structures/data_structures/arrays/arrays.md).


## How is the Heap structured?

Java Virtual Machine's Heap in memory is structured into _regions_, called **Generations**.


## Is the Heap a fixed size, or is it variable?

Either!


## How does an object's lifespan affect which Generation it stays in? 

The longer an object lives, the higher the chance it will be promoted to an older generation. 


## How is data stored on the Heap **de**allocated?

The [Garbage Collector (GC)](/memory/deallocation/garbage_collection-gc.md) is the system that reclaims memory from objects in the Heap that are no longer required.


## So does that mean that the [Garbage Collector (GC)](/memory/deallocation/garbage_collection-gc.md) collects garbage more often within _younger_ generations?

Young generations (such as the [Eden Space](eden_space.md) on Sun/Oracle JVM) are more garbage collected than older generations ([Survivor Space](survivor_space.md) and the [Tenured Generation](tenured_generation.md) on Sun/Oracle JVM).


## Where are [Static Variables](/memory/variables/static_variable.md) and [Class](/data_types/types/classes/classes.md) definitions stored in memory?

[Static Variables](/memory/variables/static_variable.md) and [Class](/data_types/types/classes/classes.md) definitions are stored in [Permanent Generation (PermGen)](permanent_generation-permgen.md); this is **NOT** a part of JVM's heap but a separate region in memory.


## Can I see a diagram?

Sure:

![JVM memory diagram](jvm_memory_diagram.jpg)