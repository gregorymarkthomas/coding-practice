# JRockit JVM

## What is it?

JRockit JVM _was_ a [Java Virtual Machine]() originally made by Appeal Virtual Machines, acquired by BEA Systems, then acquired by Oracle themselves.


## So JRockit was integrated into Oracle's [HotSpot JVM](/language_specific/java-android/) and discarded?

Yes.