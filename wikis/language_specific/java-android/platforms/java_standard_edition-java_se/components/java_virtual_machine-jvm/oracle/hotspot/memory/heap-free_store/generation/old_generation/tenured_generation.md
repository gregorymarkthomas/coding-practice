# Tenured Generation in the [Heap](heap-free_store.md)

## What is it?

The Tenured Generation is a pool containing [objects](\structures\data_structures\object.md) _that have existed for some time in the [Survivor Space](survivor_space.md)_.


## So objects that are still of use but have not been used in a while are _relegated_ to the Tenured Generation?

Yes.