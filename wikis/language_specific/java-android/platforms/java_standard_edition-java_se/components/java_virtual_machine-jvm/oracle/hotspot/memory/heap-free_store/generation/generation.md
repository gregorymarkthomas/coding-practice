# Generations in the memory Heap for Java Virtual Machines (JVM)

## What are they?

Generations are the _regions_ that divide up the JVM's [Heap](../heap-free_store.md) memory.


## How many Generations are there in JVM's Heap?

Two: the [Young Generation](young_generation/young_generation.md) and the [Old Generation](old_generation/old_generation.md).


## Wait; is the [Permanent Generation](\language_specific\java-android\execution\java_virtual_machine-jvm\oracle\memory\non_heap\permanent_generation-permgen.md) **not** a part of the Heap?

No! Some diagrams _suggest_ that it is, but those diagrams are more showing the idea of Generations.


## Is there a diagram to show the layout of Generations in JVM's memory?

Yes:

![Java Virtual Machine heap generations](jvm_generation_diagram.png)