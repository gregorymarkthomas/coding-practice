# Code Cache in the [Non-Heap](non_heap.md)

## What is it?

The Code Cache is [memory](/memory/memory.md) space dedicated to the [compilation](/execution/compiler.md) and storage of native code.