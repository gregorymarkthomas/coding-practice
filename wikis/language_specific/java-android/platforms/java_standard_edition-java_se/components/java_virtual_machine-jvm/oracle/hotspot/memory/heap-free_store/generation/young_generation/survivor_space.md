# Survivor Space in the [Heap](../heap-free_store.md)

## What is it?

The Survivor Space is the pool containing [objects](\structures\data_structures\object.md) _that have **survived** the [Garbage Collection](/memory/deallocation/garbage_collection-gc.md) of the [Eden Space](eden_space.md)._


## Hence the name '_Survivor_ Space'?

Yes.