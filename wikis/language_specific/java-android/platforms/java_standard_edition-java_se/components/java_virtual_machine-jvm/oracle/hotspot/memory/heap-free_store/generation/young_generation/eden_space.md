# Eden Space in the [Young Generation](young_generation.md) of the [Heap](heap-free_store.md)

## What is it?

Eden Space is the pool from which memory is **initally** allocated for most objects.


## So, when we create an [object](/structures/data_structures/object.md) using `new`, memory is [allocated](/memory/allocation/allocation.md) in Eden Space?

Yes.