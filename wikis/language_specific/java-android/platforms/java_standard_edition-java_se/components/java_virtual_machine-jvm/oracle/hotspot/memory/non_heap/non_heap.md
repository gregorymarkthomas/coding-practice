# Non-Heap/Stack memory in a Java Virtual Machine (JVM)

## Is the name 'Non-Heap' synonymous with 'Stack'?

Yes.


## Where can I read about the [stack](/memory/stack.md) in general?

[Here](/memory/stack.md).


## What does the Non-Heap store for a [JVM](../java_virtual_machine-jvm.md)?

Non-Heap memory includes:

- a [Method Area](/memory/stack/method_area.md) that is **shared among all [threads](/concurrency/threads/threads.md)** 
- memory used for [Just In Time (JIT)](/execution/just_in_time-jit.md) optimisation for the Java VM
- per-class structures such as a runtime constant pool, [local variable/field](/memory/variables/local_variables.md) and [function](/structures/software_entities/functions/functions.md) data, and the code for methods and constructors**.


## How is the non-heap structured?

Java's non-heap is structured into **regions**, called _generations_.



## _Where_ is the Non-Heap located?

The method area is logically part of the heap but, depending on the implementation, a Java VM may not garbage collect or compact it. Like the heap memory, the method area may be of a fixed or variable size. The memory for the method area does not need to be contiguous.



## This is not [Garbage Collected (GC)](/memory/deallocation/garbage_collection-gc.md), is it?

No; the lifespan of objects is finite by design due to [Scope](/terms/scope.md) and the use of [Automatic Variables](/memory/variables/automatic_variables.md).


## Where are [Static Variables](/memory/variables/static_variable.md) and [Class](/data_types/types/classes/classes.md) definitions stored in memory?

[Static Variables](/memory/variables/static_variable.md) and [Class](/data_types/types/classes/classes.md) definitions are stored in [Permanent Generation (PermGen)](permanent_generation-permgen.md); this is **NOT** a part of JVM's heap but a separate region in memory.


## Can I see a diagram?

Sure:

![JVM memory diagram](jvm_memory_diagram.jpg)