# Java Standard Edition (Java SE)

## What is it?

Java Standard Edition (Java SE) is a Java [_platform_](/language_specific/java-android/platform/platform.md).


## What devices does Java SE target?

_General-purpose_ applications on: 

- desktop PCs
- servers 
- similar devices


## So, _Standard Edition_ is the _default_ Java platform?

Of sorts, yes.


## What does
When most people think of the Java programming language, they think of the Java SE API. 


## What does Java SE contain?

- an [Application Programming Language (API)](/language_specific/java-android/platform/components/application_programming_language-api.md) 
	- Java SE's API is considered the [core API](/language_specific/java-android/platform/java_standard_edition-java_se/components/application_programming_language-api.md)
- a [Java Virtual Machine (JVM)](/language_specific/java-android/platform/components/java_virtual_machine-jvm.md)
	- Flavours such as [Oracle's Hotspot JVM](components/java_virtual_machine-jvm/oracle/hotspot/hotspot.md) and [Oracle's JRockit JVM](components/java_virtual_machine-jvm/oracle/jrockit/jrockit.md)
- development tools
- deployment technologies
- other class libraries and toolkits commonly used in Java technology applications


## What does the core API contain?

Java SE's API provides the _core_ functionality of the Java programming language. 




