# Automatic Variables in [Java](/language_specific/java-android/java.md)

## What is the meaning of "Automatic Variable"?

[See the general description of Automatic Variable](/memory/variables/automatic_variable.md).


## What are "Automatic Variables" called in [Java](/language_specific/java-android/java.md)?

**Local** Variables.


## How do Automatic Variables behave in [Java](/language_specific/java-android/java.md)?

Automatic (or, ["Local"](/memory/variables/local_variable.md)) Variables in Java work similarly to those in [C](/language_specific/c/variables/automatic_variable.md) and [C++](/language_specific/c_plus_plus/variables/automatic_variable.md), but there is no `auto` or `register` keyword. However, the Java compiler will not allow the usage of a not-explicitly-initialized local variable and will give a compilation error (unlike C and C++ where the compiler will usually only give a warning). 
The Java standard demands that every local variable must be explicitly initialized before being used. **This differs from [Instance Variables](/memory/variables/instance_variable.md), which are implicitly initialized with default values (which are 0 for numbers and null for objects).** 