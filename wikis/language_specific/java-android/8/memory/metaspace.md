 ---
title:  MetaSpace
author: Gregory Thomas
date:   2020-08-15
---

# MetaSpace

## What is it?

MetaSpace is a special part of Java 8's [heap](/memory/heap.md).


## So, does it replace [Permanent Generation](../../memory/permanent_generation.md) from Java <= 7, then?

**Yes**; [Permanent Generation](../../memory/permanent_generation.md) was **replaced** by MetaSpace from [Java 8](../java_8.md).


## What does MetaSpace hold?

- Loaded and [Just In Time (JIT)](/execution/just_in_time.md)-compiled [classes](/structures/software_entities/classes/classes.md)