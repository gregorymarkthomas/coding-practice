---
title:  Python
author: Gregory Thomas
date:   2020-03-13
---

# Python

## What is it?

A programming/scripting language.


## What does Python have over other languages?

Its [**read-evaluate-print**](/execution/read_evaluate_print_cycle.md) cycle of the *console* is quick and simple; a user can use it as a calculator, AND an environment to write and test code.

Data scientists use Python for this reason.


## What do some other languages have if it does not use "read-evaluate-print" cycle?

Other languages (such as [C](../c/c.md), [C++](../c_plus_plus/c_plus_plus.md) and [Java](../java/java.md)) use [*write-compile-test-recompile*](/execution/write_compile_test_recompile_cycle.md) cycle, which is arguably more established but is definitely slower to develop with.


## Are there other distributions of Python?

Why, yes! There is of course the official CPython that I have been using, and there is also two other popular ones called Anaconda and Canopy; these two are more focused towards science, machine learning and other data applications.


## Are there other versions of the Python *console*?

Yes! Try Ipython or Jupyter.