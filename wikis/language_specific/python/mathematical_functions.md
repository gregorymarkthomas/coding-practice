---
title:  Mathematical functions in Python
author: Gregory Thomas
date:   2020-03-13
---

# Mathematical functions in Python

## floor(x)

Rounds double x DOWN to nearest int


## ceil(x)

Rounds double x UP to nearest int 