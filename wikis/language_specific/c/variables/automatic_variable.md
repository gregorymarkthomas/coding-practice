# Automatic Variables in [C](/language_specific/c/c.md)

## What is the meaning of "Automatic Variable"?

[See the general description of Automatic Variable](/memory/variables/automatic_variable.md).


## What are "Automatic Variables" called in [C](/language_specific/c/c.md)?

Automatic Variables.


## How do Automatic Variables behave in [C](/language_specific/c/c.md)?

All variables declared within a block of code are automatic by default. An uninitialized automatic variable has an undefined value until it is assigned a valid value of its type.[1]

In C, using the storage class register is a hint to the compiler to cache the variable in a processor register. Other than not allowing the referencing operator (&) to be used on the variable or any of its subcomponents, the compiler is free to ignore the hint.