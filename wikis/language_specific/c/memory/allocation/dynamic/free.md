# `free`

## What is is?

`free` is a library function in [C](/language_specific/c/c.md) used to **de**allocate a block of memory on the [heap](/memory/allocation/dynamic/free_store-heap.md) previously allocated by [`malloc`](malloc.md).