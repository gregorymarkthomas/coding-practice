# `malloc`

## What is it?

`malloc` is a library function in [C](/language_specific/c/c.md) used to allocate a block of memory on the [heap](/memory/allocation/dynamic/free_store-heap.md). 


## How does `malloc` work?

The program accesses the block of memory via a [pointer](/memory/pointer.md) that `malloc` returns. When the memory is no longer needed, the pointer is passed to the function [`free`](free.md), which deallocates the memory so that it can be used for other purposes. 


## Example?

Creating an array of ten integers with automatic scope is straightforward in C:

```c
int array[10];
```

However, the size of the array is fixed at compile time. If one wishes to allocate a similar array dynamically, the following code can be used:

```c
int *array = (int*)malloc(10 * sizeof(int));
```

This computes the number of bytes that ten integers occupy in memory, then requests that many bytes from malloc and assigns the result to a pointer named array (due to C syntax, pointers and arrays can be used interchangeably in some situations).

Because malloc might not be able to service the request, it might return a null pointer and it is good programming practice to check for this:

```c
int *array = malloc(10 * sizeof(int));
if (array == NULL) {
  fprintf(stderr, "malloc failed\n");
  return -1;
}
```

When the program no longer needs the dynamic array, it must eventually call free to return the memory it occupies to the free store:

```c
free(array);
```

The memory set aside by malloc is not initialized and may contain cruft: the remnants of previously used and discarded data. After allocation with malloc, elements of the array are uninitialized variables. The command calloc will return an allocation that has already been cleared:

```c
int *array = calloc(10, sizeof(int));
```

With realloc we can resize the amount of memory a pointer points to. For example, if we have a pointer acting as an array of size n {\displaystyle n} n and we want to change it to an array of size m {\displaystyle m} m, we can use realloc.

```c
int *arr = malloc(2 * sizeof(int));
arr[0] = 1;
arr[1] = 2;
arr = realloc(arr, 3 * sizeof(int));
arr[2] = 3;
```

Note that realloc must be assumed to have changed the base address of the block (i.e. if it has failed to extend the size of the original block, and has therefore allocated a new larger block elsewhere and copied the old contents into it). Therefore, any pointers to addresses within the original block are also no longer valid. 


## What about Type Safety?

`malloc` returns a void pointer (void \*), which indicates that it is a pointer to a region of unknown data type. The use of casting is required in [C++](/language_specific/c_plus_plus/c_plus_plus.md) due to the [strong type](/terms/types/strongly_typed.md) system, whereas this is not the case in [C](/language_specific/c/c.md). One may "cast" this pointer to a specific type:

```c
int *ptr, *ptr2;
ptr = malloc(10 * sizeof(*ptr)); /* without a cast */
ptr2 = (int *)malloc(10 * sizeof(*ptr)); /* with a cast */
```