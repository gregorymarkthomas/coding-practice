# Memory Management in C

## How is memory managed in [C](/language_specific/c/c.md)?

Memory (i.e. non-static variables) is managed **manually** in C, with the use of [`malloc`](dynamic/malloc.md) for allocation, and [`free`](dynamic/free.md) for deallocation.