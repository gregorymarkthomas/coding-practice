---
title:  String.split()
author: Gregory Thomas
date:   2020-10-17
---

# String.split()

## What is it?

[Kotlin's](/language_specific/kotlin/kotlin.md) ```String.split()``` is much like [Java's](/language_specific/java/java.md) ```String.split()```.


## What do I need to be aware of?

Kotlin's ```split()``` function's [String](/structures/software_entities/types/derived/string.md) input to define the [delimiter](/terms/delimiter.md) **needs to call ```String.toRegex()``` for ```split()``` to work properly!!