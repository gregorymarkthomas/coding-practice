---
title:  doubleArrayOf()
author: Gregory Thomas
date:   2020-10-17
---

# doubleArrayOf()

## What is it?

```doubleArrayOf()``` is a Kotlin function that replaces [Java's](/language_specific/java/java.md) ```new double[] {}``` syntax.