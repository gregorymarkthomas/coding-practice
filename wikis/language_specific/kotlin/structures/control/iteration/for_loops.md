---
title:  'For' loops in Kotlin
author: Gregory Thomas
date:   2020-10-17
---

# 'For' loops in Kotlin

## What is it?

See [For loops](/structures/control_structures/iteration/for_loops.md).


## How do I create ```i``` with my loop for use within it?

In [Java](/language_specific/java/java.md), we can of course do this:

```java

for(int i = 0; i < 10; i++) {
	println(i); // Prints 0 through to 9
}

```

How do we do this in Kotlin?

```kotlin
for(i in 0 until 10) {
	println(i) // Prints 0 through to 9
}
```

```0 until 10``` will mimic the Java For loop.

Please do notice our use of ```until``` here; ```until``` will **not** count the _final_ (in our case, the tenth) loop, much like our Java example where we use the ['smaller-than' operator](/mathematics/operator/smaller_than.md) (```<```) in the Java For loop.
If we _wanted_ to count the final loop, we can use ```..``` instead of ```until```.
If you ever compare Kotlin's ```..``` to [Python's](/language_specific/python/python.md) [```range()```](/language_specific/python/structures/software_entities/functions/range.md) function, then please be aware that Python's ```range()``` does **not** count that final loop.