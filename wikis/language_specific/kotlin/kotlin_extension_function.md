---
title:  Kotlin extension functions and properties
author: Gregory Thomas
date:   2020-08-14
---

# Kotlin extension functions and properties

## What do you mean?

If you, the developer, want to [extend a class's functionality](/paradigms/object_oriented_programming/open_closed_principle.md) with an extra function or two, but either _don't want to create a whole new class_, or the class you want to extend is _built-in/third party_, then there is a way to do so in Kotlin.


## How is this done?

Well, you could create a function which takes the object [type](/structures/software_entities/variables/data_types) as an input:

```
fun toUnsigned(b: Byte): Int {
	return if (b < 0) b + 256 else b.toInt()
}
```

It would be nicer if we can call ```b.toUnsigned()``` instead of ```toUnsigned(b)```, especially if we will be making a sequence of calls:

```x.foo(y).bar().baz``` vs ```getBaz(bar(foo(x, y)))```

We can instead extend an existing class by using a _Kotlin extension function_:

```
fun Byte.toUnsigned(): Int {
    return if (this < 0) this + 256 else this.toInt()
}
```

## What does ```this``` refer to?

```this``` refers to the object we are calling ```toUnsigned()``` on.


## Can this be converted into a property instead of a function?

Yes.

## How?

```val Byte.unsigned: Int get() = if (this < 0) this + 256 else this.toInt()```


## So, what is happening behind the scenes?

We are **not** actually extending the class; at [compile time](/execution/compiler.md), Kotlin will essentially create this:

```
fun toUnsigned(b: Byte): Int {
	return if (b < 0) b + 256 else b.toInt()
}
```

What we are coding is just [syntactic sugar](/terms/syntactic_sugar.md), and makes our code easier to read.


## Do we have to import these extensions?

Yes; they are not carried with the instances of the class.


## So, these extensions are actually [static](/modifiers/static.md)?

Yes.


## So, we need to be careful when we use extensions?

Yes; they are **not** bad practice to use them, but we must still be careful.


## How do we do that?

Make sure your extension function or property does not access a _shared_ state. 


## Why would we need to do that? 

A [static](/modifiers/static.md) object only exists **once** in memory.

Say multiple [threads](/concurrency/threads.md) access that static object, and one thread _updates_ the object's state; will all other threads see that updated value?

## No?

Correct.

## Is that what [thread safety](/concurrency/threads/thread_safety.md) means?

Yes; we cannot guarantee [thread safety](/concurrency/threads/thread_safety.md) when using static objects.


## Do you have an example of this?

### Thread-safe

```
fun String.countSpaces(): Int {
    return this.count { c -> c == ' ' }
}
```

This is _thread-safe_; [String](/structures/software_entities/variables/data_types/arrays_and_strings/strings.md) is [immutable](/terms/immutable.md), so the developer is unable to change ```this``` within the function.


### Thread-UNsafe

```
data class MutablePerson(val name: String, var speech: String)

fun MutablePerson.count(nextNumber: Int) {
    this.speech = "${this.speech} ${nextNumber}"
}
``` 

This is **not** thread-safe; the ```speech``` property has its state changed. If ```count()``` is called on a different thread, the return value may well be different; not good!


## Where would you place these extension?

They cannot go into the class you are extending, of course.

They can go anywhere else. Though, if you're using a [Model View Controller](/architectures/model_view_controller/model_view_controller.md) or [Model View View Controller](/architectures/model_view_view_controller/model_view_view_controller.md), perhaps this extension should go in the _model_; this is assuming the extension function will be returning data.


  