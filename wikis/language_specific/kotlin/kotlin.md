---
title:  Kotlin
author: Gregory Thomas
date:   2020-03-15
---

# Kotlin

## What is it?

A programming language.


## What is it primarily used for?

Android apps; it is the *new* Android programming language.


## Does it have garbage-collection?

Yes.


## Is it multi-paradigm?

Somewhat; it is mostly [Object-Oriented](/paradigms/object_oriented_programming.md) but also supports [functional-style programming](/paradigms/functional_programming.md).


## What is the process of execution?

Kotlin uses the [**write-compile-test-recompile**](/execution/write_compile_test_recompile_cycle.md) cycle.

It compiles to [Java bytecode](../java/execution/bytecode.md) via the [Java Virtual Machine (JVM)](../java/execution/java_virtual_machine.md), but *produces bytecode that no valid Java could ever produce* due to the extra functionality Kotlin brings; [Java decompilers](../java/execution/java_decompilers.md) would either **fail** when presented with such bytecode, or they would generate Java that *could not be recompiled again*. 