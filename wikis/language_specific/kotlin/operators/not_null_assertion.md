---
title: The !! operator
author: Gregory Thomas
date:   2020-10-28
---

# The !! operator

## What is it?

"!!" is an operator used in Kotlin.


## What is !! used for?

The !! operator is used to allow [Null Pointer Exceptions](/exceptions/null_pointer_exceptions.md) in [Kotlin](../kotlin.md).


## So what does !! actually do?

!! will convert _any_ value to a **non-null** type.


## What will the conversion to a non-null type do?

If the converted value is actually [**null**](/terms/null.md), then a [Null Pointer Exception](/exceptions/null_pointer_exceptions.md) will be thrown.


## Is this why the !! operator is called the "_not-null assertion_" operator?

Yes; an [assertion](/terms/assert.md) is carried out on the value to check if it is _not_ null.


## Why do we have to explicitly ask for a [Null Pointer Exception](/exceptions/null_pointer_exceptions.md) in [Kotlin](../kotlin.md)?

[Kotlin](../kotlin.md) tries its best to _avoid_ the use of [null](/terms/null.md) values, but it still has to handle them, hence other operators such as ['?'](safe_call.md).


## What is an example of the `!!` operator?

```kotlin
val l = b!!.length
```

`b` will be returned if _not_ null, and a [Null Pointer Exception](/exceptions/null_pointer_exceptions.md) will be thrown if `b` is null.