# Automatic Variables in [Perl](/language_specific/perl/perl.md)

## What is the meaning of "Automatic Variable"?

[See the general description of Automatic Variable](/memory/variables/automatic_variable.md).


## What are "Automatic Variables" called in [Perl](/language_specific/perl/perl.md)?

**Lexical**, _my_, or _private_ variables.


## How do Automatic Variables behave in [Perl](/language_specific/perl/perl.md)?

In Perl, Automatic/[Local Variables](/memory/variables/local_variable.md) are declared using the `my` operator. Uninitialized scalars will have the value `undef`; uninitialized arrays or hashes will be `()`.

Perl also has a local operator that does not create automatic variables,[5] instead giving global (package) variables a temporary value, which is dynamically scoped to the enclosing block. When the scope of the variable is left, the old value is restored. 