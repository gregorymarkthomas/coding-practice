---
title:  C++
author: Gregory Thomas
date:   2020-03-15
---

# C++

## What is it?

A programming language.


## What is it primarily used for?

Embedded software/firmware, due to its (potentially) low-memory usage.


## Does it have garbage-collection?

No; you have to do it yourself.


## Is it multi-paradigm?

Yes. C++11 supports [functional-style programming](/paradigms/functional_programming.md).


## What is the process of execution?

C++ uses the [**write-compile-test-recompile**](/execution/write_compile_test_recompile_cycle.md) cycle.


## Polymorphism

### [Ad-hoc polymorphism](/paradigms/object_oriented_programming_oop/principles/polymorphism/adhoc/adhoc_polymorphism.md)

#### [Function overloading](/paradigms/object_oriented_programming_oop/principles/polymorphism/adhoc/function_overloading.md)?

Yes.

#### [Operator overloading](/paradigms/object_oriented_programming_oop/principles/polymorphism/adhoc/operator_overloading.md)?



### [Parametric polymorphism](/paradigms/object_oriented_programming_oop/principles/polymorphism/parametric/parametric_polymorphism.md)

#### [Generic functions](/paradigms/object_oriented_programming_oop/principles/polymorphism/parametric/generic_functions.md)?


#### [Generic programming](/paradigms/object_oriented_programming_oop/principles/polymorphism/parametric/generic_programming.md)?



### [Subtype polymorphism](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/subtype_polymorphism.md)

#### [Virtual functions](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/virtual_functions.md)?



#### [Single dispatch](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/single_dispatch.md)?

Yes.

#### [Double dispatch](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/double_dispatch.md)?

No.

#### [Multiple dispatch](/paradigms/object_oriented_programming_oop/principles/polymorphism/subtype/multiple_dispatch.md)?

No.