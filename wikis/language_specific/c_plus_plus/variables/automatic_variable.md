# Automatic Variables in [C++](/language_specific/c_plus_plus/c_plus_plus.md)

## What is the meaning of "Automatic Variable"?

[See the general description of Automatic Variable](/memory/variables/automatic_variable.md).


## What are "Automatic Variables" called in [C++](/language_specific/c_plus_plus/c_plus_plus.md)?

Automatic Variables.


## How do Automatic Variables behave in [C++](/language_specific/c_plus_plus/c_plus_plus.md)?

All variables declared within a block of code are automatic by default. An uninitialized automatic variable has an undefined value until it is assigned a valid value of its type.[1]

In C++, the constructor of automatic variables is called when the execution reaches the place of declaration. The destructor is called when it reaches the end of the given program block (program blocks are surrounded by curly brackets). This feature is often used to manage resource allocation and deallocation, like opening and then automatically closing files or freeing up memory. See Resource Acquisition Is Initialization (RAII). Note, C++11 has a new auto specifier,[2] which is different. In this case, the variable's type is inferred. 