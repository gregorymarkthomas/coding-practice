---
title:  PHP
author: Gregory Thomas
date:   2020-03-15
---

# PHP

## What is it?

A programming language.


## What is it primarily used for?

Backends for legacy webservers.


## Does it have garbage-collection?

Yes via [gc_enable](https://www.php.net/manual/en/function.gc-enable.php).


## Does it support [anonymous functions](/structures/software_entities/functions/anonymous_closures.md)?

Yes. These, however, [are **not** subject to garbage collection](https://www.php.net/manual/en/function.create-function.php#70691).


## Is it multi-paradigm?

Somewhat; it is mostly [Object-Oriented](/paradigms/object_oriented_programming.md) but also supports [functional-style programming](/paradigms/functional_programming.md).