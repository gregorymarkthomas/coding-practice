---
title:  Code Signing
author: Gregory Thomas
date:   2020-03-27
---

# Code Signing

## What is it?

Code Signing is the *act* of signing code with an *identity*. 


## Why does code need signing?

Users who download your software can see that *you* have signed it, and that the code has **not been modified** since its last signing.


## What is created when code is "signed"?

"Signing code" is quite a general term; we are actually **creating a digital signature** for a piece of code.


## What is a "digital signature"?

A [cryptographic hash](/security/cryptographic_hash.md) that contains *information* on **who signed the code** and **what code was signed**.


## So, *how* does code signing protect the user against modification?

Here is a scenario:

1. Legitimate Ltd. writes a [.exe](/operating_system/windows/file_types/exe.md) application
2. Legitimate Ltd. signs the application with a digital signature using their *private key*
3. User downloads application; when user tries to install, the operating system tells them the code was published by Legitimate Ltd.
4. Application is modified by a hacker and application is re-released to the Internet
5. User downloads modified application; when they try to install, the operating systems tells them **the digital signature is invalid**, **but will still let them install it if they really want to**. 

There is no hard barrier to block installation of an application with an invalid signature; the user has to decide to follow the *advice* of the [operating system](/operating_system/operating_system.md).


## Right, so, how does the [operating system](/operating_system/operating_system.md) check the integrity of the digital signature?

The [operating system](/operating_system/operating_system.md) probably maintains a list of trusted [Certificate Authorities (CAs)](/security/public_key_infrastructure/entities/certificate_authority.md); these CAs make their *public keys* available for operating systems to add if deemed trustworthy.


## Okay, so, what happened when the hacker modified the application?

When the application is installed (or even before, depending on the [operating system](/operating_system/operating_system.md)), the digital signature *hash* is compared to 
If the code is modified and the **signature is not regenerated**, then the current signature will be **invalid**

**Modifying the code will modify the digital signature hash**.

**Code can still be modified, by anyone**; code signing does not provide a hard barrier to stop a user from installing modified (since last signature) code.


## What could it mean if code is modified but the signature is not regenerated?

- Original developer forgot to sign it
	- The [Apple App Store](/operating_system/apple/app_store.md) and [Google Play Store](/operating_system/google/android/play_store.md) will show errors if the uploaded build is *unsigned*.
- Code was edited by **someone else** who does not have access to the original developer's private key
	- **This is the main scenario Code Signing tries to protect against.**


## What process is used to sign code?

A [cryptographic hash](/security/cryptographic_hash.md).


## How does the operating system the software is installed on protect the user?

Operating systems like Apple [iOS](/operating_system/apple/ios/ios.md) and [Android](/operating_system/android/android.md) will *verify* 


## How is the digital signature stored and sent to other parties?

The code signing process *results* in creating a [code signing certificate](code_signing_certificate.md); this *file* contains the digital signature plus a timestamp, and is sent to other parties.