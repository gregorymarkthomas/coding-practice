---
title:  Public/Private Key Pair
author: Gregory Thomas
date:   2020-03-27
---

# Public/Private Key Pair

## What is it?

A means of secure communication between two parties.

Private and Public keys are used between the two parties to ensure that both parties know they are sending and receiving data to and from each other.

Data, during transmission from one party to another, is *[encrypted](encryption.md)* when sent and *[decrypted](decryption.md)* when received. 


## What is it also known as?

- Public-key [cryptography](cryptography/cryptography.md)
- Asymmetric-key [cryptography](cryptography/cryptography.md)


## Why "asymmetric"?

Public/Private Key Pairing is called "Asymmetric-key [cryptography](cryptography/cryptography.md)" because a **different key is used to [decrypt](decryption.md) data from the key used to [encrypt](encryption.md)**.


## So, what does "[symmetric-key cryptography](symmetric_key_cryptography.md)" use, then?

[Symmetric-key cryptography](symmetric_key_cryptography.md) uses the *same* key to encrypt and decrypt data.


## How are the public and private keys related?

They are *mathematically* related using one of many [cryptography](cryptography/cryptography.md) algorithms, such as:

- [Elliptic curve Cryptography (ECC)](cryptography/elliptic_curve_cryptography.md)
- [Rivest-Shamir-Adleman cryptography](cryptography/rivest_shamir_adleman_cryptography.md) 
- [Digital Signature Algorithm cryptography](cryptography/digital_signature_algorithm_cryptography.md)


## Who keeps what key?

The individual *sending* data keeps *their* private key (and keeps it secure; it is a password to this transmission afterall) and distributes their *public* key to the recipient. 

If this is a two-way communication, the receiver party will have their *own* private key and distribute their public key to the original sender.


## Does, say, the private key *only* encrypt data, and the public key *only* decrypt data?

No.

**Remember that either key can be used to [encrypt](encryption.md); the *other* key will [decrypt](decryption.md).**


## If that is the case, what happens in a 1-way communication?

The private key of the sender [encrypts](encryption.md) the data. When the recipient receives the data from the sender, the recipient can [decrypt](decryption.md) the data with the sender's *public* key (that the recipient received before transmission) so it can be understood.


## What about a 2-way communication?

Bob and Alice have their own private keys and have sent their public keys to the other person.

Bob **uses Alice's public key to encrypt** and sends the message.
Alice uses her **private key to decrypt** the message.


## What are the security concerns of this?

The above is a simple implementation and highlights an issue; how does Bob know that the key he used to encrypt the message actually belonged to Alice?
A third party may have substituted a different key.


## How do you get around this security concern?

Use a Public-Key Infrastructure (PKI)](public_key_infrastructure/public_key_infrastructure.md)..


## How are Public/Private Key Pairs related to [Public-Key Infrastructures (PKI)](public_key_infrastructure/public_key_infrastructure.md)?

A [Public-Key Infrastructure (PKI)](public_key_infrastructure/public_key_infrastructure.md) is a collection of hardware and software that *expands upon* public/private key pairing to achieve a *more secure* communication channel between two parties.


## What are some technologies that use Public/Private key pairs?

- [Secure Sockets Layer (SSL)](secure_sockets_layer.md) to provide secure communication between a [webserver](/development_types/web_development/webserver.md) and a [web browser](/development_types/web_development/web_browser.md) client.