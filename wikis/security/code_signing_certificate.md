---
title:  Code Signing Certificates
author: Gregory Thomas
date:   2020-03-28
---

# Code Signing Certificates

## What are they?

A Code Signing Certificate is a type of [digital certificate](certificates.md) that is used to [sign code](code_signing.md).


## What does a Code Signing Certificate contain?

- Identity of the organisation who created the code **binded with the public key**
- Timestamp of signature


## Are there different types of Code Signing Certificate?

Yes:

- Organisation certificates
	- these authenticate an entire company
- Individual certificates
	- these autenticate a single developer 