---
title:  Certificates
author: Gregory Thomas
date:   2020-03-28
---

# Certificates

## What are they?

Certificates, in general, are small files that are primarily used for security.


## What is an example of a certificate?

[*Public Key* certificates](public_key_certificates.md) (or *Digital identity* certificates*) are documents to **prove ownership of a public key**; these contain the public key of an owner, and these must be validated by the [Certificate Authority (CA)](../public_key_infrastructure/entities/certificate_authority.md).