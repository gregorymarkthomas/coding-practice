---
title:  Certificate Signing Request (CSR)
author: Gregory Thomas
date:   2020-03-30
---

# Certificate Signing Request (CSR)

## What is it?

A Certificate Signing Request (CSR) is a request that is sent to the [Certificate Authority (CA)](certificate_authority.md) to *request* a [digital identity certificate](/security/certificate.md).


## How is the request formed?

It can be a simple [HTTP request](/development_types/web_development/hypertext_transport_protocol_request.md) in the context of [HTTPS](/development_types/web_development/hypertext_transport_protocol_secure.md).

For Apple [iOS development](/operating_system/apple/ios/ios.md), a CSR is created via the [Keychain Access](/operating_system/apple/keychain_access.md) file in the form of a '.certSigningRequest' file.


## Who creates the Certificate Signing Request?

For Apple [iOS development](/operating_system/apple/ios/ios.md), Apple forces the **developer** to sign their application when distributing in any way; in this case, the CSR is created on a developer's development device.