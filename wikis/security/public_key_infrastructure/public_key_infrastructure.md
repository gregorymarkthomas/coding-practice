---
title:  Public Key Infrastructure (PKI)
author: Gregory Thomas
date:   2020-03-27
---

# Public Key Infrastructure (PKI)

## What is it?

A Public Key Infrastructure (PKI) *expands upon* [Public/Private key pairing](../public_private_key_pairs.md); a PKI is a collection of hardware and software that is set up to achieve a more secure communication channel between two parties, which uses [Public/Private key pairing](../public_private_key_pairs.md) at its *core*.


## What is the issue with simple [Public/Private key pairing](../public_private_key_pairs.md)?

In a "Bob and Alice" example where both parties have their own private keys and share their public keys with each other, one party will use the other's *public key* to encrypt the message, and the other party will decrypt with their own *private key*.

But, who *is* the *other party*? What if a hacker replaced the original receiver party's *private key* with their own on the *sender party*'s system? This would cause the sender to send encrypted data to the **hacker** instead, and the hacker could decrypt it with their own *public* key.


## What is a Public Key Infrastructure's aim?

The aim of a PKI is to *establish* the integrity and ownership of a public key.


## What entities make up a *typical* Public Key Infrastructure?

- a [Certificate Authority (CA)](certificate_authority.md)
- a [Registration Authority](registration_authority.md)
- a [Certificate Database](certificate_database.md)
- a [Certificate Store](certificate_store.md)
- a [Key Archival Server](key_archival_server.md)


## What does the [Certificate Authority (CA)](certificate_authority.md) do within the PKI?

The [Certificate Authority (CA)](certificate_authority.md) (i.e. the **trusted third-party**) *uses* the other hardware and software elements of the infrastructure to establish the ownership of a public key. The public key is the key that a *sender party* distributes to parties it wants to send data to; the Certificate Authority will ensure that the *sender party* is the one who owns the public key.

In a typical implementation, the [Certificate Authority (CA)](certificate_authority.md) *issues* a **[signed](../signing.md) (and [encrypted](../cryptography/encryption.md)) binary [certificate](../certificates.md).**


## What does this binary [certificate](../certificates.md) contain?

- The public key of the *sender party* that the party wants to distribute to receiver parties
- The identity of the party who owns the public key.


## How does the public key and party identity interact within the [certificate](../certificates.md)?

The identity is **bound** to the public key.


## How does the Certificate Authority [sign](../signing.md) the certificate it issues?

It uses its **own** *private key*; the [certificate](../certificates.md) the Certificate Authority produces is [self-signed](../self_signing.md).


## Who does the Certificate Authority send the [certificates](../certificates.md) to?

The signed certificate is sent to parties the *sender party* wants to send secure data to.


## How do these receiving parties use the [certificate](../certificates.md)?

They use the **public key** that is embedded in the [certificate](../certificates.md).







## So, using these entities, what makes a PKI more secure?







## What are some examples of PKIs?

Apple's [App Store](/operating_system/apple/app_store.md) app system uses a PKI to securely distribute apps from a known source (i.e. a developer of an organisation), and to use secure communication to and from other services (like [Apple Push Notification service (APNs)](/operating_system/apple/apple_push_notification_service.md)).