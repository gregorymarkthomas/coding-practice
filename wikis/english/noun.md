---
title:  Nouns
author: Gregory Thomas
date:   2020-09-10
---

# Nouns

## What are they?

A noun is a word that refers to a _thing_.

## A _thing_?

Yes; any _thing_!


## What are some examples of a noun?

- a thing
	- book
- a person	
	- Betty Crocker
- an animal
	- cat
- a place
	- Omaha
- a _quality_ of something
	- softness
- an idea
	- justice
- an action
	- yodelling

## Is a noun just a single word?

Not always; a _school bus_ and _time and a half_ are nouns.


## What is a Common noun?

A _common_ noun refers to a person, place or thing, **but is not the _name_ of the person, place or thing**:

- animal
- sunlight
- happiness


## What is a Proper noun?

A _proper_ noun **is** the _name_ of the person, place or thing, usually starting with a capital letter:

- Abraham Lincoln
- Argentina
- World War I


## What is a Collective noun?

A _collective_ noun _groups_ things:

- flock
- squad