﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelViewViewModel.Models;
using ModelViewViewModel.ViewModels;

namespace ModelViewViewModel.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            // TODO: Get model from database instead.
            User user = new User { FirstName = "Jimbo", SurnameName = "Jones" };
            return View(new IndexViewModel { User = user });
        }

        public IActionResult About()
        {
            // A TestLogicMethods could be swapped in here.
            return View(new AboutViewModel { LogicMethods = new LogicMethods() });
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
