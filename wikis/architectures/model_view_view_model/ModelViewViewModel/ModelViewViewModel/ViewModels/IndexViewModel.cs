﻿using ModelViewViewModel.Models;
using System;

namespace ModelViewViewModel.ViewModels
{
    public class IndexViewModel
    {
        public User User { get; set; }
        public bool SomeLogicThing { get; }

        public IndexViewModel()
        { 
            this.SomeLogicThing = CalculateSomeLogicThing();
        }

        private bool CalculateSomeLogicThing()
        { 
            return true;
        }
    }
}
