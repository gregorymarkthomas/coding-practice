#pragma checksum "D:\src\coding-practice\architectures\c_sharp\ModelViewViewModel\ModelViewViewModel\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "80585888acbe97c15b3117774c4c3ab34b6c06c7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\src\coding-practice\architectures\c_sharp\ModelViewViewModel\ModelViewViewModel\Views\_ViewImports.cshtml"
using ModelViewViewModel;

#line default
#line hidden
#line 2 "D:\src\coding-practice\architectures\c_sharp\ModelViewViewModel\ModelViewViewModel\Views\_ViewImports.cshtml"
using ModelViewViewModel.ViewModels;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"80585888acbe97c15b3117774c4c3ab34b6c06c7", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"15fe8877dbca14ccc949ae697b8400f73e42d7d2", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ModelViewViewModel.ViewModels.IndexViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\src\coding-practice\architectures\c_sharp\ModelViewViewModel\ModelViewViewModel\Views\Home\Index.cshtml"
  
    ViewData["Title"] = Model.User.FirstName + " " + Model.User.SurnameName + "'s homepage";

#line default
#line hidden
            BeginContext(154, 69, true);
            WriteLiteral("\r\n<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n        <h2>Hello, ");
            EndContext();
            BeginContext(224, 20, false);
#line 8 "D:\src\coding-practice\architectures\c_sharp\ModelViewViewModel\ModelViewViewModel\Views\Home\Index.cshtml"
              Write(Model.User.FirstName);

#line default
#line hidden
            EndContext();
            BeginContext(244, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(246, 22, false);
#line 8 "D:\src\coding-practice\architectures\c_sharp\ModelViewViewModel\ModelViewViewModel\Views\Home\Index.cshtml"
                                    Write(Model.User.SurnameName);

#line default
#line hidden
            EndContext();
            BeginContext(268, 29, true);
            WriteLiteral("!!</h2>\r\n    </div>\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ModelViewViewModel.ViewModels.IndexViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
