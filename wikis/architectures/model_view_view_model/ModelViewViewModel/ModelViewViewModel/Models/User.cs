﻿namespace ModelViewViewModel.Models
{
    public class User
    {
        public string FirstName { get; set; }
        public string SurnameName { get; set; }
    }
}
