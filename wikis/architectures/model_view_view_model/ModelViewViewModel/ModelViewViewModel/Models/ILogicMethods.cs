﻿namespace ModelViewViewModel.Models
{
    public interface ILogicMethods
    {
        bool GetComplicatedLogic();
    }
}
