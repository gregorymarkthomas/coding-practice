---
title:  Model View ViewModel (MVVM)
author: Gregory Thomas
date:   2020-03-13
---

# Model View ViewModel (MVVM)

## What is it?

An architectural pattern.


## How does it work?

Basically, it is closer to MVC than you think. There is just an extra layer between Controller and View (ViewModel) that handles view state/applies model data onto View's XAML.
This allows the View to be dumb and the ViewModel to be dumb.

The basic MVVM guidelines we follow are:

- Views **display** a certain shape of data. They have no idea where the data comes from.
- ViewModels **hold** a certain shape of data and commands; they do not know where the data, or code, comes from or how it is displayed.
- Models hold the actual data (various context, store or other methods)
- Controllers listen for, and publish, events. Controllers provide the logic that controls what data is seen and where. Controllers provide the command code to the ViewModel so that the ViewModel is actually reusable.


---

Each View has a ViewModel.
The view itself is still dumb but **SO IS the ViewModel**; all the ViewModel does is *handle the View's state* instead of Controller/Presenter or in the View itself.

The ViewModel does not *necessarily* replace Controllers.
(raed more here: https://stackoverflow.com/questions/667781/what-is-the-difference-between-mvc-and-mvvm)

*"The problem is: that to be independently testable*, and especially reusable when needed, a view-model has no idea what view is displaying it, but more importantly no idea where its data is coming from.

*Note: in practice Controllers remove most of the logic, from the ViewModel, that requires unit testing. **The VM then becomes a dumb container that requires little, if any, testing. This is a good thing as the VM is just a bridge, between the designer and the coder, so should be kept simple.**

Even in MVVM, **controllers will typically contain all processing logic and decide what data to display in which views using which view models.**

From what we have seen so far the main benefit of the ViewModel pattern to remove code from XAML code-behind to make XAML editing a more independent task. We still create controllers, as and when needed, to control (no pun intended) the overall logic of our applications."* 

A View **only has ONE ViewModel**.
A ViewModel **may have MULTIPLE Views**.

