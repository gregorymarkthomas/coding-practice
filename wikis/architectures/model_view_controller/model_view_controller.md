---
title:  Model View Controller
author: Gregory Thomas
date:   2020-03-13
---

# Model View Controller

## What is it?

An architectural pattern.


## What does it consist of?

Model: 

- handles data (from either database, remote database, cache) requested by the Controller/Presenter

View: 

- view component (buttons etc)
- notifies Controller/Presenter of user's button presses etc.
- dumb - needs to be told by Controller/Presenter on what to do

Controller/Presenter:

- The middle-man between views and model.
- Controls what the view shows
- Requests data from model and handles it on receive
 
 
## How is it helpful?

It separates model logic, view logic and views to promote encapsulation.

Each area communicates via controlled interfaces. This allows an area to be mocked in testing to allow atomic tests. 