---
title:  REpresentational State Transfer (REST)
author: Gregory Thomas
date:   2020-03-13
---

# REpresentational State Transfer (REST)

## What is it?

**RE**presentational **S**tate **T**ransfer (REST) is an *architectural style* that allows [clients](/terms/client.md) and [servers](/terms/server.md) to communicate **simply**.


## What are the basics?

Representational State Transfer allows for a [client](/terms/client.md) to receive a _textual representation_ of a web resource hosted on a [server](/terms/server.md).


## What kind of web resources are we talking about?

- [Hypertext Markup Language (HTML)](/language_specific/markup_languages/hypertext_markup_language_html.md)
- [Javascript Object Notation (JSON)](/data_interchange_formats/javascript_object_notation_json.md)
- [Extensible Markup Language (XML)](/language_specific/markup_languages/extensible_markup_language_xml.md)
- etc.


## What does the [client](/terms/client.md) need to _send_ to the server to receive the textual representation of the web resource?

The client sends a [request](/terms/request.md); this contains:

### A [HyperText Transfer Protocol (HTTP) _method_](/protocols/hypertext_transfer_protocol_http/methods/http_methods.md):

- [GET](/protocols/hypertext_transfer_protocol_http/methods/get.md)
- [POST](/protocols/hypertext_transfer_protocol_http/methods/post.md)
- [PUT](/protocols/hypertext_transfer_protocol_http/methods/put.md)
- [DELETE](/protocols/hypertext_transfer_protocol_http/methods/delete.md)
- etc.


### A [HyperText Transfer Protocol (HTTP) _header_](/protocols/hypertext_transfer_protocol_http/header.md)

This may contain authentication data should the client need authenticating with the server.


### A REST API path

e.g. `/teachers/id/2993`


### (optional) [HyperText Transfer Protocol (HTTP) _body_](/protocols/hypertext_transfer_protocol_http/body.md)

Perhaps the client is sending a [POST](/protocols/hypertext_transfer_protocol_http/methods/post.md) request, say, to update a record with new data from client.



## Why exactly is it called REpresentational State Transfer?

It is supposed to evoke an image of *how a well-designed web application behaves*.


### What do you mean by this?

The website is a network of Web resources (like a *virtual state machine*) where the user can *progress* through the server's application state by hitting a resource with a particular method (GET/POST etc)


## What is a REST [Application Programming Interface (API)](/interfaces/application_programming_interface_api.md)?

A REST [API](/interfaces/application_programming_interface_api.md) is an Application Programming Interface that **conforms to the constrant of the REST architectural style**.


## What about [state](/terms/state.md); what is the "State Transfer" part of "REST"?

The REST server is **stateless** in that it **does not store client state**, but the client **transfers its state** to the server as and when it is required.


## What do you mean by 'stateless'?

Read about [REST statelessness](rest_statelessness.md).


## Is REST easy to modify?

Read about [REST scaling](rest_scaling.md).


## Right, so why would you choose the REST architecture?

- Performance
- Scalable
- Reliability


## What are the 5 principles to a REST architecture?

### Give every “thing” an ID

### Link things together

### Use standard methods
    
### Resources with multiple representations
    
### Communicate statelessly



## What is an example of something that uses REST?

- [Create Read Update Delete](../paradigms/database/create_read_update_delete.md)


## POST/PUT/PATCH problem

What if we wanted to create a new record for, say, a Person, on the server? We would normally sent a POST with all the Person data in the body that we want to populate the server's database record with. 
But, what if a connection error occurred which stopped the *response* from being received by the client? The database has been filled with the new record but the client thinks the server has not received the request. The client may re-send the POST, which, if it worked, would result in a **duplicated record**.

A solution to this is to first send an **empty** POST (i.e. no Person data) so that an **empty** record is created in the database on the server. The server will return the database row ID of the empty record in the response.
If that response was successfully received by the client, then the client could then PUT/PATCH (replace/update, respectively) with the Person data in the body to update the database record with data.
If that response was thwarted by a connection error, then the worst thing is that the server has an empty record in the database instead of a duplicated Person record. This empty record can be easily removed in a monthly cronjob.


## Links

- https://stackoverflow.com/questions/3105296/if-rest-applications-are-supposed-to-be-stateless-how-do-you-manage-sessions
- https://www.ics.uci.edu/%7Efielding/pubs/dissertation/rest_arch_style.htm
- https://www.infoq.com/articles/rest-introduction/