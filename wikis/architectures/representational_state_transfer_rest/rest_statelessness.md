# [REST](representational_state_transfer_rest.md) statelessness

## What do you mean by 'stateless'?

The server does **not** store any state about the client's [session](/protocols/hypertext_transfer_protocol_http/sessions.md). 

Statelessness means that every [HTTP request](/protocols/hypertext_transfer_protocol_http/http_request.md) happens in complete isolation.


## Where _is_ the client session stored, then?

On the **client**.


## So, the relevant session information is then _sent_ from the client to the server **as needed**?

Yes.


## With this setup, what does this mean for the server?

The server can service any client at any time, _because it is stateless_.


## But, the web server can still maintain state about _business objects_, right?

The web server will hold state about shopping carts etc.; it just will not hold the client's application/session state.


## How is the [HTTP request](/protocols/hypertext_transfer_protocol_http/http_request.md) sent in complete isolation?

All the information necessary for the server to fulfill that request is packed into the [HTTP request](/protocols/hypertext_transfer_protocol_http/http_request.md). The server, on receiving the HTTP request will _never_ rely on information from previous requests.


