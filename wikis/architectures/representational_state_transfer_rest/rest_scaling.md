# [REST](representational_state_transfer_rest.md) scaling

## Is a REST architecture scalable?

Yes; this is helped by the [statelessness of REST](rest_statelessness.md).


### How so?

The REST server does not need to hold millions of client's states due to it's [statelessness](rest_statelessness.md).

Thanks to this, implementation on [clients](/terms/client.md) **and** [servers](/terms/server.md) can change because they will not affect the other; this is so long as the client and server still adhere to the REST [Application Programming Interface (API)](/interfaces/application_programming_interface_api.md).


## How is [REST](representational_state_transfer_rest.md) modifible?

Provided the [Application Programming Interface (API)](/interfaces/application_programming_interface_api.md) does not change, the implementation behind it can change without having to notify the client.
Likewise, the server just returns data - it does not care how the client takes that data. This means the client can change how it handles data without having to tell the server.


## What can we use to scale-up a REST architecture?

[Proxies](/networking/proxy.md), [gateways](/networking/gateway.md) and [firewalls](/security/firewall.md) (perhaps for [caching](/terms/caching.md) and other performance/[load balancing](/terms/load_balancing.md)/other scaling techniques) can be used at various points in the communication.

The fact that the API makes things simple and understandable means we can be more flexible with our server's components; we could run a component on a separate server dedicated to performance.


## Caching and stale data

Clients can cache responses. Responses must define themselves as [cacheable](/terms/caching.md) or non-cacheable so that clients are not plagued with stale data.
Well-managed caching eliminates *some* client-server communication, improving performance and scalability.