# Scaling

## What is it?

Scaling, in computer science terms, is the term to define the _extension_ of software and/or a service to allow for **more** (or less) throughput.


## Are there different types of scaling?

Yes: X axis, Y axis and Z axis scaling.


## Can this be shown as a diagram?

Yes; this is The Scale Cube.

![The Scale Cube](https://microservices.io/i/DecomposingApplications.021.jpg)


## Can you explain the three types of scaling?

### X-axis scaling

- Scaling is done by **cloning _horizontally_**
- Running N copies of an application behind a load balancer.
- Each copy handles 1/Nth of the load
- Simple, dumb.
  - All copies potentially access the same data.
    - Caches may require more memory
  - It is a simple solution **but does not make the application any simpler**


### Y-axis scaling

- *Splitting*/decomposing the application into different services (i.e. [Microservice pattern](/patterns/microservice_pattern.md))
  - Split by [verb](/english/verb.md) ("checkout") and/or by [noun](/english/noun.md) ("customer management")
  - Also known as **functional decomposition**
- Each service is responsible for one or more closely related functions
- Can tackle application complexity by reducing to micro services and making everything atomic


### Z-axis scaling

- Similar to X axis scaling
- Multiple servers run identical copies of code
- Difference is that each server is responsible for a **subset** of the data
  - A component of the system routes requests to the appropriate server
  - Improves cache utilisation as each server has it's own unchanging cache
  - If a fault occurs it can be tracked down to a particular server that uses that part of the data
- Perhaps a customer type is routed to a different and better server (premium customers)
- Commonly used to **scale databases**
  - Data is *partitioned* across servers based on an attribute of each record (say, by PRIMARY KEY)
  - X-axis cloning can also be applied to each partition for backups
- Searching
  - An example search service consists of a number of partitions
  - Each content/searchable item is send by a router service to the appropriate partition, where it is *indexed* and *stored*.
  - The query aggregator sends each query to all of the partitions and the results from each are combined
- Complex solution
  - Requires creating a partitioning scheme
  - Similar to X-axis scaling; it does not make the application simpler