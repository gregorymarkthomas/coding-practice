# Serverless computing

- These are **NOT** just Google Cloud Functions, Azure Functions, AWS Lambda; these are FaaS platforms
- 'Serverless' is **not** a synonym for FaaS
- It is **not** Google App Engine which does "on demand compute"
- It is **not** Google Cloud Run which runs a container on demand

It is, apparently:

“A serverless application is one that provides maximum business value over its application lifecycle and one that costs you nothing to run when nobody is using it, excluding data storage costs.”

Serverless and [Functions as a Service (FaaS)](functions_as_a_service_faas.md) are intertwined because:

"FaaS is the simplest and most obvious enabling technology for applications using a serverless approach"

Serverless is a mindset and not about the technology. The idea is to reduce running costs; if it costs less to use FaaS then do so, and if it is more cost effective to run a server, do so.

Serverless means we offload server maintenance to other services.


## Links

- https://medium.com/swlh/what-is-serverless-the-2020-edition-5a2f21581fe5