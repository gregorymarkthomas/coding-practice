---
title:  PuTTYGen
author: Gregory Thomas
date:   2020-09-15
---

# PuTTYGen

## What is it?

PuTTYGen is an application that can generate a [Public/Private key pair](/security/public_private_key_pair.md) for use in communications such as [Secure Shell (SSH)](/networking/secure_shell.md).


## Is PuTTYGen related to [PuTTY](putty.md)?

Yes; PuTTYGen generates the [.ppk](../file_types/ppk.md) in which our new [Public/Private key pair](/security/public_private_key_pair.md) is stored in for [PuTTY](putty.md) to authenticate our user.


## How do I export traditional ```id_rsa``` files from a [.ppk](../file_types/ppk.md)?

[SSH](/networking/secure_shell.md) applications outside of [PuTTY](putty.md) still use the traditional private (```id_rsa```) and public (```id_rsa.pub```) files:

1. Use the _Generate_ button to generate a new public/private key pair
2. Use the _Save private key_ to export the private key; call it ```id_rsa``` and save it to your local user's ```.ssh``` folder.
3. Manually copy the _public key_ from the greyed-out edit box and paste it into a new file called ```id_rsa.pub``` in your local user's ```.ssh``` folder.
	- The _Save public key_ button does **not** produce the correct output.