---
title:  (Code) Signing Certificates
author: Gregory Thomas
date:   2020-03-27
---

# (Code) Signing Certificates

## What are they?

A Signing Certificate is a digital *identity*.


## What is it used for?

A (Code) Signing Certificate is used to *sign* code during the build and archive process.

Users who are about to download your app can see that it came from a *known* source and **has not been modified since it was last signed**.


## When do we need to sign our code with a Signing Certificate?

- When integrating with app services (like [Apple Push Notification service (APNs)](apple_push_notification_service.md))
- When installing a Debug build on a test device
- When submitting the app to the [App Store](app_store.md).


## How do we get a Code Signing Certificate?

Via [developer.apple.com](developer.apple.com); the certificate must be issued by Apple via your logged-in developer account.


## How is the Signing Certificate stored?

It is downloaded from Apple and is stored on the developer's development device (e.g. a MacBook Pro); it is added to the developer's [keychain](/security/keychain.md).