---
title:  Provisioning profiles
author: Gregory Thomas
date:   2020-03-27
---

# Provisioning profiles

## What are they?

Provisioning profiles are *files* that control *how* an application build can be deployed/*distributed*.


## What do these files contain?

- The [certificate](certificates.md) used to *sign* the app
- The app's [App ID](app_id.md)
- Where the app can be installed


## Are there different types of Provisioning profile?

Yes, for the different types of *distribution*; adhoc, development and App Store.


## What are the differences?

Adhoc and Development provisioning profiles include the [certificate](certificates.md) and the [App ID](app_id.md), but it *also* has a list of **allowed** devices that can deploy the app; this is for security reasons and to stop illegal distribution of the app.

App Store provisioning profiles do **not** have the *allowed devices* list as the [App Store](app_store.md) controls distribution.