"In iOS, apps run in a sandbox, which provides a set of rules that limit access between the application and certain system resources or user data. Entitlements are used to request that the system expand the sandbox to give your app additional capabilities."

https://docs.microsoft.com/en-us/xamarin/ios/deploy-test/provisioning/entitlements?tabs=macos