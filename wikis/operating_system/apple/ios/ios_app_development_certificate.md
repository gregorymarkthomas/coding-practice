---
title:  'iOS App Development' certificate
author: Gregory Thomas
date:   2020-03-30
---

# 'iOS App Development' certificate

## What is it?

The 'iOS App Development' certificate is a [certificate](/security/certificates.md) that allows a *developer* to sign **development** builds so that the build can be "distributed" to one (or two) select *real* iPhones **using a 'Development'-flavoured [provisioning profile](provisioning_profiles.md)**.


## Why do we need to "distribute" a development build?

Apple does not want developers to distribute development builds to any iPhone. 

The idea here is to force the *signing* of development builds using a developer-specific 'iOS App Development Certificate', and to force *limited distribution* of development builds via the 'development'-flavoured [provisioning profile](provisioning_profiles.md).

The term "distribute", in this context, is overstated; in reality it is normally a singular *deployment* to one iPhone.


## How is our 'iOS App Development Certificate' authorised by Apple, the [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md)?

On [developer.apple.com](developer.apple.com), we log in to our Apple Developer account. 

We [create a new "iOS App Development" certificate](developer.apple.com/account/resources/certificates/add) by first creating a [Certificate Signing Request (CSR)](certificate_signing_request.md) on the *local* machine using the [Keychain Access](keychain_access.md) application, which is a [keychain](/security/keychain.md). Here, we are requesting *authorisation* and *validation* for our *developer*-specific [Siging Certificate](../signing_certificate.md) from [Apple Developers](developer.apple.com); they are the [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) in this [Public Key Infrastructure (PKI)](/security/public_key_infrastructure/public_key_infrastructure.md).

We *upload* the [Certificate Signing Request](certificate_signing_request.md) for the *developer*-specific [Siging Certificate](../signing_certificate.md) to [Apple Developers](developer.apple.com); if the email address matches the developer's email address (and no other 'iOS App Development Certificate' exists for this developer), the [certificate](/security/certificates.md) is granted. We then download the [certificate](/security/certificates.md) to [Keychain Access](keychain_access.md); this is so we can sign our *release* app *locally* for when we want to upload it to the [App Store](../app_store.md) for distribution.