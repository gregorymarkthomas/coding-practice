---
title:  'iOS Distribution' certificate
author: Gregory Thomas
date:   2020-03-30
---

# 'iOS Distribution' certificate

## What is it?

The 'iOS Distribution' certificate is a [certificate](/security/certificates.md) that allows an *organisation* to sign **release** builds so that they can be **distributed via the Apple [App Store](../app_store.md) using an 'App Store'-flavoured [provisioning profile](provisioning_profiles.md)**.


## How is our 'iOS Distribution' certificate authorised by Apple, the [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md)?

On [developer.apple.com](developer.apple.com), we log in to our Apple Developer account. 

We [create a new "iOS Distribution" certificate](developer.apple.com/account/resources/certificates/add) by first creating a [Certificate Signing Request (CSR)](certificate_signing_request.md) on the *local* machine using the [Keychain Access](keychain_access.md) application, which is a [keychain](/security/keychain.md). Here, we are requesting *authorisation* and *validation* for our *organisation*-specific certificate from [Apple Developers](developer.apple.com); they are the [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) in this [Public Key Infrastructure (PKI)](/security/public_key_infrastructure/public_key_infrastructure.md).

We *upload* the [Certificate Signing Request](certificate_signing_request.md) for the *organisation*-specific [Siging Certificate](../signing_certificate.md) to [Apple Developers](developer.apple.com); if the email address matches the developer's email address (and no other 'iOS App Development Certificate' exists for this developer), the [certificate](/security/certificates.md) is granted. We then download the [certificate](/security/certificates.md) to [Keychain Access](keychain_access.md); this is so we can sign our *development* app *locally* when we want to "distribute" the development build to a real iPhone.