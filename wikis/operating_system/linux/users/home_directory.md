---
title:  Linux user's home directory
author: Gregory Thomas
date:   2020-09-16
---

# Linux user's home directory

## What is it?

The user's home directory is the user's own personal directory in which they can store whatever they require/fancy.


## How do I _change_ the home directory of my user?

### Why on Earth would you do that?

Perhaps we have a dedicated [utility user](utility_user.md) that does one job (i.e. handles the [git server](../software/git.md)); we may need a dedicated space to store files for this user, but perhaps their traditional home directory (e.g. ```/home/git```) does not reside on the correct harddrive.


### How to edit a user's _home_ directory

We need to edit ```/etc/passwd```. But, for safety, we should use the built-in ```sudo vipw```; this will edit ```/etc/passwd``` with safety features like [file locking](/term/file_locking.md).
