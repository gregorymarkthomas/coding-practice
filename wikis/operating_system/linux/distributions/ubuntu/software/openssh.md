---
title:  OpenSSH
author: Gregory Thomas
date:   2020-09-15
---

# OpenSSH

## What is it?

OpenSSH is the built-in [Secure Shell (SSH)](/networking/secure_shell.md) solution for most Linux distributions.


## What does OpenSSH allow us to do?

We can be logged in as _a_ user on any local machine, and using that user, we can log into a _remote machine_ as _a_ user.


## So, any user can log in as any user on that remote machine?

Yes, provided the local user is authorised to do so.


## What types of authentication are there?

- Username and password
	- On your _local_ machine, you will be entering the _remote_ user's username and password; this will allow you to control their account.
- [Public Key authentication](/security/public_private_key_pair.md)
	- The user on the _local_ machine is authorised to control the _remote_ user's account; this is done with a Public/Private key pair.


## What is the most secure type of authentication for OpenSSH?

[Public Key authentication](/security/public_private_key_pair.md).


## Setting up OpenSSH with [Public Key authentication](/security/public_private_key_pair.md)

Assumptions:

- you have a user you want to control _remotely_ that resides on your _remote_ machine
- your _remote_ user's [home directory](../users/home_directory.md) is at the default location of ```/home/<remote_username>```
- you have a user with [sudo](/operating_system/linux/sudo.md) rights to setup OpenSSH (this may be the user you want to control _remotely_).
- your firewall (such as [Uncomplicated FireWall (UFW)](../uncomplicated_firewall.md)) has [SSH](/networking/secure_shell.md)'s incoming port of 22 open firewall
- your version/[distribution](../distributions/distributions.md) of Linux uses [systemd](../systemd/systemd.md) and therefore uses [systemctl](../systemd/systemctl.md) to control system services.
- OpenSSH is already installed on your Linux version/[distribution](../distributions/distributions.md)
- you're fine with the [nano](../software/text_editors/nano.md) text editor; feel free to use what you want instead.
- **You have local access to the _remote_ machine (or can SSH into it already)**


### 1. Get the OpenSSH service running on the _remote_ machine

This is assuming OpenSSH is installed.

1. Check the status of the OpenSSH service with ```sudo systemctl status sshd```
2. If _disabled_, enable OpenSSH with ```sudo systemctl enable sshd```
3. Check the status of the OpenSSH service _again_ with ```sudo systemctl status sshd```
4. If _stopped_, **start** OpenSSH with ```sudo systemctl start sshd```


### 2. Generate a Public/Private key pair

This is so our user on the _local_ machine is authorised to act as the user on the _remote_ machine.

Read how to generate a Public/Private key pair for [Windows here](/operating_system/microsoft/windows/tasks/generate_public_private_key_pair.md), and for [Linux here](/operating_system/linux/tasks/generate_public_private_key_pair.md).


### 3. Keep ```PasswordAuthentication``` ON in OpenSSH's configuration

#### Wait, isn't this the opposite of what we want?

Yes, but, I will explain why in the next main step. To check:

1. ```sudo nano /etc/ssh/sshd_config```
2. Check to see if ```PasswordAuthentication``` is either commented out (the default is set to ```yes```), or it is uncommented but has ```yes``` next to it.
3. Ctrl+X to exit, and save changes.
4. Reload ```sshd``` service with ```sudo systemctl reload sshd``` 


### 4. Ensure _remote_ user has authorised the _local_ user

The user on the _remote_ machine needs to authorise the _local_ machine's _user_. 

To authorise an incoming user on another machine for [SSH](/networking/secure_shell.md) access, the user on the _remote_ machine **needs a copy of the _incoming_ user's _public_ key**. This public key copy is stored in the _remote_ user's  ```authorized_keys``` file, which resides within the ```.ssh``` directory within their _home_ directory.

To achieve this easily, we keep ```PasswordAuthentication``` at ```yes``` (step **#3**) and we do the following depending on the _local_ machine's operating system:


#### [Windows](/operating_system/microsoft/windows/windows.md):

Regardless of whether you can _locally_ control the _remote_ machine, it is easier to SSH into the _remote_ machine from the _local_ machine because you will need to copy and paste a key:

1. SSH into your remote machine (using an application such as [PuTTY](/operating_system/microsoft/windows/software/putty.md) with the _remote_ user's username and password.
2. In the _remote_ user's home directory, create a new directory if it does not already exist: ```sudo mkdir .ssh```
3. Use ```sudo chmod 700 .ssh``` to set [read/write/execute](/operating_system/linux/commands/chmod.md) permissions to the ```.ssh``` directory
4. Use ```sudo chown <remote_username> .ssh``` to ensure the remote user owns the directory and not root
5. Create a new file if it does not already exist: ```sudo nano .ssh/authorized_keys```
6. Use ```sudo chmod 600 .ssh/authorized_keys``` to set [read/write](/operating_system/linux/commands/chmod.md) permissions to the ```authorized_keys``` file
7. Use ```sudo chown <remote_username> .ssh/authorized_keys``` to ensure the remote user owns authorized_keys and not root
8. On your _local_ machine, go to where your _public_ key is stored (typically ```c:/Users/<local_username>/.ssh/id_rsa.pub```) and _open_ the file
9. Copy the full contents of the ```id_rsa.pub``` file
10. Paste into the SSH window so that the ```authorized_keys``` file on the _remote_ machine has the _local_ machine user's _public_ key
11. Ctrl+X to exit, and save changes. 


#### [Linux](/operating_system/linux/linux.md):

Assuming you are logged _out_ of any SSH sessions to the _remote_ machine:

1. ```ssh-copy-id <remote_username>@<remote_machine_name/ip>``` will attempt to copy your _local_ user's public key into a (new, if not already created) ```authorized_keys``` file within a ```.ssh``` directory on the _remote_ machine. 
2. (optional) Enter your private key password, if you set one during the generation stage.


### 5. Try it out

Keep ```PasswordAuthentication``` set to ```yes``` (**ON**) while we try this; OpenSSH will first _try_ [Public Key authentication](/security/public_private_key_pair.md) before falling back to username/password authentication.

1. Type ```logout``` to log out of the SSH window
2. Retry SSH for the _same_ user; you should be asked to store the host address for your _local_ user to trust
3. Type ```yes``` to store the host address
4. (optional) Enter your private key password, if you set one during the generation stage.

You should now be logged in via [Public Key authentication](/security/public_private_key_pair.md).


### 6. Turn OFF ```PasswordAuthentication``` in OpenSSH configuration

To tighten security, we should disable ```PasswordAuthentication```; doing this will limit who can use SSH to log into your _remote_ machine.

You should still be logged into the _remote_ machine from the last step.

1. ```sudo nano /etc/ssh/sshd_config```
2. Uncomment ```PasswordAuthentication```
3. Type ```no``` next to ```PasswordAuthentication```
4. Ctrl+X to exit, and save changes.
4. Reload ```sshd``` service with ```sudo systemctl reload sshd``` 


## Troubleshooting

### Why does [Public Key authentication](/security/public_private_key_pair.md) not work?

#### Have you generated a Public and Private key for your _local_ user?

The user on your _local_ machine which will connect to the _remote_ machine will require a [Public/Private key pair](/security/public_private_key_pair.md); this also needs to be authorised by the user on the _remote machine_.

To generate a key pair:

- On Linux: [ssh-keygen](/operating_system/linux/software/ssh_keygen.md)
	- Produces [id_rsa](/networking/secure_shell/id_rsa.md) and [id_rsa.pub](/networking/secure_shell/id_rsa_pub.md) files.
- On Windows: [PuTTYgen](/operating_system/microsoft/windows/software/puttygen.md)
	- Produces a [proprietary](/english/proprietary.md) [.ppk](../file_types/ppk.md) file


#### Do you have your Public/Private key pair in the correct format for your [SSH](/networking/secure_shell.md) client?

If you are using Windows, then you may be using [PuTTY](/operating_system/windows/software/putty.md) for your [SSH](/networking/secure_shell.md) needs.

[PuTTY](/operating_system/windows/software/putty.md) uses a [proprietary](/english/proprietary.md) [.ppk](../file_types/ppk.md) file to retrieve your _local_ user's Public/Private key pair, and [PuTTYgen](/operating_system/microsoft/windows/software/puttygen.md) produces this file.

But if you want to use, say, [Git for Windows](/operating_system/windows/software/git_for_windows.md), which has an OpenSSH client built-in, we _cannot_ use the [.ppk](../file_types/ppk.md) file; we must use generic [id_rsa](/networking/secure_shell/id_rsa.md) and [id_rsa.pub](/networking/secure_shell/id_rsa_pub.md) files instead. 
To convert the [.ppk](../file_types/ppk.md) file, use the [PuTTYGen](/operating_system/windows/software/puttygen.md) application. 


#### Does your generated Public/Private key pair reside in the correct place?

If you are using a [.ppk](../file_types/ppk.md) file with [PuTTY](/operating_system/windows/software/putty.md), then, file location does not matter; you manually select the .ppk file within the PuTTY application (_Connection_->_SSH_->_Auth_->_Private key file for authentication_).

However, for something like [Git for Windows](/operating_system/windows/software/git_for_windows.md), then the Public/Private key pair should reside in the ```.ssh``` directory of your _local_ machine's user's home directory:

- ```C:/Users/<local_username>/.ssh``` on typical Windows
	- You may need to show hidden folders in Windows Explorer to check this.
- ```~/home/<local_username>/.ssh``` on typical Linux


#### Does the generated Public/Private key pair have the correct permissions and owner?
	
We need to ensure that our _local_ user's private key has [read/write](/operating_system/linux/commands/chmod.md) permissions for _that_ user; this applies to both [.ppk](../file_types/ppk.md) _and_ [id_rsa](/networking/secure_shell/id_rsa.md) / [id_rsa.pub](/networking/secure_shell/id_rsa_pub.md) files.


#### Does user on the _remote_ machine have a ```.ssh``` directory in their _home_ directory? 

Wherever the _remote_ user's home directory is (typically ```/home/<remote_username>```, but 
[Linux Users](/operating_system/linux/users.md) to find out how to change a user's home directory.), there should be an ```.ssh``` folder within it.

The ```.ssh``` directory should have ```0700``` permissions ([read/write/execute](/operating_system/linux/commands/chmod.md) for owner) and should be _owned_ by the _remote_ user we want the _local_ machine's user to log in as.


#### Does the ```.ssh``` directory on the _remote_ machine contain a file called ```authorized_keys```?

The ```authorized_keys``` file contains your _local_ machine user's _public_ key; this allows our _local_ user to log into the _remote_ machine as the _remote_ user.

```authorized_keys``` should have ```0600``` permissions ([read/write](/operating_system/linux/commands/chmod.md)) and should be owned by the _remote_ user.


#### Is the _remote_ user's _home_ directory permissive (**including all of its parent directories**)?

OpenSSH needs **all the parent directories** of the user's _home_ directory to have the correct permissions.
I tried using a directory within _another_ user's directory as my _remote_ user's home directory; as far as I can tell, the ```.ssh``` directory will **not** be accessed if the permissions are not correct.



### Why can't I connect with either authentication type?

#### Do you have a [firewall](/security/firewall.md) enabled on your _local_ machine?

Check that port 22 (the default [SSH](/networking/secure_shell.md) port) allows traffic to go _outwards_.


#### Do you have a [firewall](/security/firewall.md) enabled on your _remote_ machine?

Check that _incoming_ traffic can flow through port 22 (the default [SSH](/networking/secure_shell.md) port).


#### Does the _remote_ machine have [SSH](/networking/secure_shell.md) installed and enabled?