# Create new OpenVPN client (.ovpn file)

Check the [basic ovpn](basic_ovpn.ovpn) file for a general idea.

## Quick process

Seeing as we do not have easy access to the TomatoFresh OpenVPN server on the Asus AC68U router, we now generate the client on the Certificate Authority (CA):

1. Create a **copy** of basic_ovpn.ovpn and name it after the _user-device-model.ovpn_ (e.g. greg-phone-oneplus3.ovpn)
2. Edit file and copy in data from TomatoFresh -> OpenVPN Server:
    1. "Certificate Authority" (NOT "Certificate Authority Key") into user-device-model.ovpn's \<ca\> section
    2. "Static Key" into user-device-model.ovpn's \<tls-auth\> section
3. SSH into Certificate Authority (CA) machine
4. `cd ~/EasyRSA-v3.0.8` (or whatever version of EasyRSA you have installed)
5. `./easyrsa gen-req client1 nopass`
6. Press enter to confirm common name
7. `./easyrsa sign-req client client1`
8. Type `yes`
9. `cat pki/issued/<user-device-model>.crt` and COPY newly-created certificate to clipboard
10. PASTE client certificate into user-device-model.ovpn's \<cert\> section
11. `cat pki/private/<user-device-model>.key` and COPY newly-created private key to clipboard
12. PASTE client private key into user-device-model.ovpn's \<key\> section
13. REMOVE private key from CA for security: `rm pki/private/user-device-model.key`
13. Save and transfer .ovpn to client for them to try connecting

Remember that the private key still exists in the client's .ovpn file, so try to keep these files safe!

---

# (old) Generate new OpenVPN client (.ovpn file)

## Quick process

1. SSH into OpenVPN machine
2. `cd ~/EasyRSA-v3.0.6` (or whatever version of EasyRSA you have installed)
3. `./easyrsa gen-req client1 nopass`
4. Press enter to confirm common name
5. `cp pki/private/client1.key ~/client-configs/keys/`
6. `scp -i ~/.ssh/id_rsa /home/username/EasyRSA-v3.0.6/pki/reqs/client1.req certificateAuthority_machine:/tmp`
7. Log into Certificate Authority (CA) machine
8. `./easyrsa import-req /tmp/client1.req client1`
9. `./easyrsa sign-req client client1`
10. Type `yes`
11. Either log into OpenVPN machine or stay on CA machine
12. Transfer newly created `.crt` file to OpenVPN machine: `scp -i ~/.ssh/id_rsa certificateAuthority_machine:EasyRSA-3.0.8/pki/issued/client1.crt /home/username/client-configs/keys/`
13. `cd ~/client-configs`
14. `sudo ./make_config.sh client1`
15. Log into client machine
16. Copy `.ovpn` file to client machine `scp username@openvpnserver:client-configs/files/client1.ovpn Downloads/`


## In-depth process

Copied from [https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04).

---

We will generate a single client key and certificate pair for this guide. If you have more than one client, you can repeat this process for each one. Please note, though, that you will need to pass a unique name value to the script for every client. Throughout this tutorial, the first certificate/key pair is referred to as client1.

Get started by creating a directory structure within your home directory to store the client certificate and key files:

    mkdir -p ~/client-configs/keys

Since you will store your clients’ certificate/key pairs and configuration files in this directory, you should lock down its permissions now as a security measure:

    chmod -R 700 ~/client-configs

Next, navigate back to the EasyRSA directory and run the easyrsa script with the gen-req and nopass options, along with the common name for the client:

    cd ~/EasyRSA-3.0.4/
    ./easyrsa gen-req client1 nopass

Press ENTER to confirm the common name. Then, copy the client1.key file to the /client-configs/keys/ directory you created earlier:

    cp pki/private/client1.key ~/client-configs/keys/

Next, transfer the client1.req file to your CA machine using a secure method:

    scp pki/reqs/client1.req sammy@your_CA_ip:/tmp

Log in to your CA machine, navigate to the EasyRSA directory, and import the certificate request:

    ssh sammy@your_CA_ip
    cd EasyRSA-3.0.4/
    ./easyrsa import-req /tmp/client1.req client1

Then sign the request as you did for the server in the previous step. This time, though, be sure to specify the client request type:

    ./easyrsa sign-req client client1

At the prompt, enter yes to confirm that you intend to sign the certificate request and that it came from a trusted source:

Output
Type the word 'yes' to continue, or any other input to abort.
  Confirm request details: yes

Again, if you encrypted your CA key, you’ll be prompted for your password here.

This will create a client certificate file named client1.crt. Transfer this file back to the server:

    scp pki/issued/client1.crt sammy@your_server_ip:/tmp

SSH back into your OpenVPN server and copy the client certificate to the /client-configs/keys/ directory:

    cp /tmp/client1.crt ~/client-configs/keys/

Next, copy the ca.crt and ta.key files to the /client-configs/keys/ directory as well:

    cp ~/EasyRSA-3.0.4/ta.key ~/client-configs/keys/
    sudo cp /etc/openvpn/ca.crt ~/client-configs/keys/

With that, your server and client’s certificates and keys have all been generated and are stored in the appropriate directories on your server. There are still a few actions that need to be performed with these files, but those will come in a later step. For now, you can move on to configuring OpenVPN on your server.