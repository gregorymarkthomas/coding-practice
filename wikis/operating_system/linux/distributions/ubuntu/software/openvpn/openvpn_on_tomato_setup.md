# OpenVPN on Tomato

## What is my setup?

- Asus AC68U as switch
- FreshTomato version 2021.7
- separate device for [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md)


## Process

Under "VPN Tunneling" and "OpenVPN Server":

### Basic

![basic.png]


#### Start with WAN

I am using my router as a switch, so WAN is not being used.


#### Interface Type

TAP is more in-depth but TUN is more compatible.


#### Protocol

UDP is faster.


#### TLS control channel security (tls-auth/tls-crypt)

This should be _Incoming Auth (0)_ as the **client** will have a key direction of **1** to complement it.


#### Auth digest

This should match whatever is in the \*.ovpn for the client.


#### VPN subnet/netmask

Ensure that this IP subnet is the one mentioned in any firewalls to any services that you want VPN clients to use.


### Advanced

![advanced.png]


#### Push LAN0 (br0) to clients

Enabled; I want my VPN clients to be able to access devices on my local network subnet.


#### Direct clients to redirect Internet traffic

Depends; do you want your VPN clients to use the Internet connection that is available to the server?


### Keys

#### Static Key

This is the TLS HMAC key that is generated on the **server**; select _Generate static key_ to create one, and _Save_ afterwards.

This is only available if _TLS control channel security (tls-auth/tls-crypt)_ under the Basic tab is set as something other than _Disabled_.


#### Certificate Authority Key

Ignore this and keep empty; this would be relevant if our [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md) was also the server (the AC68U), so that we could sign client certificate requests.


#### Certificate Authority

This is the content of `ca.crt` that resides on the CA. `ca.crt` is generated on the CA when `./easyrsa build-ca nopass` is run.


#### Server Certificate

Unless you opt to have the server also be the CA (by using the _Generate Keys_ button at the bottom of Keys tab), there is no means for this server (FreshTomato on AC68U) to generate a `server.req` file for the CA to sign. Instead, we need to _build_ our server on the CA:

`./easyrsa build-server-full SERVERNAME nopass`

This creates `SERVERNAME.key` in `pki/private` and `SERVERNAME.crt` in `pki/issued`; the contents of `SERVERNAME.crt` needs to go into Server Certificate on the server GUI.


#### Server Key

Similar to Server Certificate, we take the contents of `SERVERNAME.key` on the CA and paste it into the Server Key box on the server GUI. **Remove `SERVERNAME.key` from the CA for security reasons; we want to give as little information to hackers about the VPN as much as possible if they hack the CA**.


#### CRL file

I believe we can put blacklisted client keys (?) here as a means to block them without having to recreate the whole [Public Key Infrastructure (PKI)](/security/public_key_infrastructure/public_key_infrastructure.md) for the VPN.


#### Diffie Hellman parameters

This is used and is generated on the **server**.


## What about clients?

See topic on [generating new clients](generate_new_client.md).