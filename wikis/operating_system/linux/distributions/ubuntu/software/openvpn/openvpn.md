# OpenVPN

This has good information regarding [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md) creation, but, I have since opted for an OpenVPN setup via [FreshTomato on my Asus AC68U router](openvpn_on_tomato_setup.md).

Below is unfinished.

---


# (old) OpenVPN

## What is it?

OpenVPN is [open source](/terms/open_source.md) software to create a [Virtual Private Network (VPN)](/networking/virtual_private_network.md).


## What exactly are we creating when we install OpenVPN?

We are creating a [Virtual Private Network (VPN)](/networking/virtual_private_network.md).


## Okay, so, what will this allow me to do?

Please read [Virtual Private Network (VPN)](/networking/virtual_private_network.md) for more information.


## What is needed to use OpenVPN?

- a machine ([Windows](/operating_system/windows/windows.md) or [Linux](/operating_system/linux/linux.md)-based) to run the OpenVPN server
- (optional but recommended) a machine to act as the [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md)
- a client machine to use said [VPN](/networking/virtual_private_network.md)


## Why is using a [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) recommended?

Please read [Virtual Private Network (VPN)](/networking/virtual_private_network.md) for more information.
Basically, security is much-improved if we put in the effort to create a [Public Key Infrastructure (PKI)](/security/public_key_infrastructure/public_key_infrastructure.md) so that only select-users can log in to the [Virtual Private Network (VPN)](/networking/virtual_private_network.md).


## What is _my_ set up?

- a HP Proliant N40L with [Ubuntu](/operating_system/linux/distributions/ubuntu/ubuntu.md) Server [18.04](/operating_system/linux/distributions/ubuntu/versions/18_04.md) installed
- a dedicated [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) machine in the form of an old Raspberry Pi
	- has the latest [Raspberry Pi OS (formerly Raspbian)](/operating_system/linux/distributions/raspberry_pi_os.md) installed
	- this is kept offline for security purposes
- various clients in the form of Android phones, a [Windows](/operating_system/windows/windows.md) laptop, a [Linux Mint](/operating_system/linux/distributions/linux_mint/linux_mint.md) laptop, and another HP Proliant N40L with [Ubuntu](/operating_system/linux/distributions/ubuntu/ubuntu.md) Server [14.04](/operating_system/linux/distributions/ubuntu/versions/14_04.md) installed


## What will this document describe from here on?

This document will outline the process to install OpenVPN.


## Who do you have to thank for your "guide"?

Thankyou to [digitalocean.com](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04) for their guide. 


---


Below are notes from my own OpenVPN installation; some steps may need further reading using guides hosted on other websites.

## How do I setup my [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) (i.e. my Raspberry Pi)?

These steps assume that the SD card in the Raspberry Pi is formatted, empty and ready to use.

### 1. Plug in monitor and keyboard into Pi but **leave out the network cable/Wifi USB stick until we make the machine more secure**

For the initial setup of the Pi, we of course need to be able to see and control what we are doing. But, we should avoid connecting the Pi to the Internet before tightening up its security measures to reduce the chance of said Pi liasing with malicious third-parties.


### 2. Set up Pi with the latest version of Raspberry Pi OS

Please use another guide for this.


### 3. (optional) Add my own user with ```sudo``` permissions to Raspberry Pi OS and remove the default ```pi``` sudo user

I do this so that I can use a familiar user, but feel free to continue using the ```pi``` user already-installed on [Raspberry Pi OS](/operating_system/linux/distributions/raspberry_pi_os.md).


### 4. Enable [Uncomplicated Firewall (UFW)](/operating_system/linux/uncomplicated_firewall.md) in Raspberry Pi OS

[UFW](/operating_system/linux/uncomplicated_firewall.md) is, in later versions of [Ubuntu](/operating_system/linux/distributions/ubuntu/ubuntu.md), _installed_ by default, but it is **not enabled by default**; this means that you as the ```sudo``` user need to _enable_ it to block unwanted network traffic.
With [UFW](/operating_system/linux/uncomplicated_firewall.md) enabled, it defaults to _allowing_ all _outgoing_ traffic, but _denies_ all _incoming_ traffic; this allows you to be strict about what traffic comes to our [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md).


### 5. (optional) _Allow_ incoming traffic for port 22 for use of [Secure Shell (SSH)](/networking/secure_shell.md)

To allow the creation of VPN client certificates easier, I will control my [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) Pi remotely via [SSH](/networking/secure_shell.md).

#### Wait; what about the security?

Good point; ideally our [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) Pi should be as offline as possible. 


#### So, why use [SSH](/networking/secure_shell.md) then?

We need to be able to transfer several small files between the Pi and the OpenVPN server machine, during setup and when creating a client certificate. To make life easier we can use [SSH](/networking/secure_shell.md), though **we should restrict its usage to the confines of our [Local Area Network (LAN)](local_area_network.md)**, or even just to the one LAN [IP address](/networking/internet_protocol_address.md) of the machine that will be logging into the Pi. 


#### How do you restrict our use of [SSH](/networking/secure_shell.md) to our [LAN](local_area_network.md)?

```sudo ufw allow from 192.168.1.0/24 proto tcp to any port 22```

The above statement assumes that your [LAN](local_area_network.md)'s [subnet](/networking/subnets.md) is ```192.168.1```, and that you may have potentially 256 different machines/[hosts](/networking/hosts.md) on your network that could connect to the Pi using [SSH](/networking/secure_shell.md). The 256 value is controlled with the ```\24``` option; here we are [subnet masking](/networking/subnet_mask.md) using the [Classless Inter-Domain Routing (CIDR)](/networking/classless_inter_domain_routing.md) notation.


#### How do you restrict our use of [SSH](/networking/secure_shell.md) to a single device?

Perhaps you would prefer that only one machine on your network can connect to the Pi; in which case, change the [subnet mask](/networking/subnet_mask.md) from ```\24``` to ```\32``` and change the fourth [binary octet](/mathematics/binary_octet.md) block (i.e. the ```x``` in ```192.16.1.x```) to the address of the device you want to [SSH](/networking/secure_shell.md) _from_: 

```sudo ufw allow from 192.168.1.17/32 proto tcp to any port 22```

This change will only allow [SSH](/networking/secure_shell.md) network traffic from ```192.168.1.17``` into the Pi, _and nothing else_.

Remember that the [Dynamic Host Configuration Protocol (DHCP)](/networking/dynamic_host_configuration_protocol.md) server that your [router](/networking/router.md) is (probably) running will change your machines' local [IP addresses](/networking/internet_protocol_address.md), so make sure to set the chosen machine's IP to a static [IP address](/networking/internet_protocol_address.md) in the router settings so that the new firewall rule will allow your chosen machine for the rest of time.


### 6. Update Pi with ```sudo apt update``` and ```sudo apt upgrade```

We want the latest patches for our Pi for security purposes. ```sudo apt``` will work with our newly enabled ufw rules, as we have allowed all outgoing traffic; ```sudo apt``` needs outgoing [Transmission Control Protocol (TCP)](/networking/transmission_control_protocol.md) traffic on port 80 open for [HTTP](/networking/hypertext_transport_protocol.md), and outgoing [User Datagram Protocol (UDP)](/networking/user_datagram_protocol.md) traffic on port 53 open for [Domain Name Service (DNS)](/networking/domain_name_service.md). 


### 7. (optional) Enable [SSH](/networking/secure_shell.md) with [public key authentication](/security/public_private_key_pair.md) only

If you are enabling [SSH](/networking/secure_shell.md) so that you can control your [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md) Pi from another machine on your local private network (not your VPN) as mentioned in step *5.*, then please set [SSH](/networking/secure_shell.md) up so that only select machines can control the Pi; disable username/password authentication to stop anyone on _any_ machine from logging into the Pi, and enable [public key authentication](/security/public_private_key_pair.md) only.


### 8. Install OpenVPN on the Pi

```sudo apt-get install openvpn```



## Setting up OpenVPN with a [Public Key Infrastructure (PKI)](/security/public_key_infrastructure/public_key_infrastructure.md)

The rest of this document will provide (rough) notes that I took during the steps taken in the [digitalocean.com](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04) guide.


### What do we want to achieve?

We want to ensure that there is _secure_ communication between proposed VPN server machine and our [Certificate Authority (CA)](/security/public_key_infrastructure/entities/certificate_authority.md) Pi.


### How do we achieve secure communication between VPN server and [Certificate Authority](/security/public_key_infrastructure/entities/certificate_authority.md)?

We will generate a private key and SERVER certificate request on the server, and we will ask the CA to SIGN the certificate. This certificate is then used for secure comms between the two devices.
  

#### Where does the private key for Server-to-CA communication reside?

The private key remains on the SERVER.


### What's the basic process? 

- SERVER.req created by SERVER is sent to Pi via SCP for it to sign
- On signing of the server certificate, the signed certifcate AND the public certficate file used by all clients of the VPN (ca.crt) is sent back to SERVER
- A **strong** Diffie-Hellman key is created - this shall be used during key exchange
- A HMAC signature is generated to strengthen the server's TLS integrity verifcation capabilities (?)
- Create an easy way for clients to join the VPN - this involves generating client-to-server communication files (private key and certificate request) on the SERVER instead of on the client.
  - Normally the client would need to generate a load of config files and send to the CA for signing, but instead the server will do all of this with a script
  - Generate a client key and place in gregthomas18@SERVER's home dir for such files. **The client's key is kept on the server**.
  - Send the **certificate request** (that was also generated with the key) to the CA for signing, then send the resulting .crt certificate back to the server.
  - After adding ta.key and ca.crt into client-configs/keys, the server's and client's certificates and keys have been generated and are stored in the correct directories.
- Config OpenVPN service to use the newly created credentials
- Enable clients to see other clients on the network
  - We advertise the LOCAL subnet (192.168.**3**.0/24) to all VPN clients via the 'push "route 192.168.3.0 255.255.255.0"' in server.conf. 
- (Optional) Redirect all traffic through VPN
  - What is done above creates VPN connection, *but will not force any connections to use the tunnel*.
  - If I want all traffic to go through VPN tunnel, then I need to push different DNS settings onto the clients
  - Changing option in /etc/openvpn/server.conf will change the default gateway of each VPN client to go through the VPN.
  - May need to sort network interface on server for this
- Keep port for OpenVPN at 1194
- Allow 1194 for server firewall
- Port forward on server
  - /etc/sysctl.conf: net.ipv4.ip_forward = 1
  - Modify UFW rules to masqerade - routes client connections via Network Address Translation (NAT)
    - Edit /etc/ufw/before.rules and add items near the top to allow OpenVPN client traffic to the server's network interface
  - Tell UFW to allow forwarded packets by default too
    - DEFAULT_FORWARD_POLICY="ACCEPT" from "DROP"
- Enable OpenVPN service on server

A script has been made to streamline the client-adding process called ~/client-configs/make_config.sh.

These do not exist in the ~/client-configs/base.conf file: 

- # script-security 2
- # up /etc/openvpn/update-resolv-conf
- # down /etc/openvpn/update-resolv-conf

According to the tutorial they should do, but should only be used if the client is running Linux and has an /etc/openvpn/update-resolv-conf file.
Perhaps this is a difference between 3.0.4 (which the tutorial references) and 3.0.6?

"This script will make a copy of the base.conf file you made, collect all the certificate and key files you’ve created for your client, extract their contents, append them to the copy of the base configuration file, and export all of this content into a new client configuration file. This means that, rather than having to manage the client’s configuration, certificate, and key files separately, all the required information is stored in one place. The benefit of this is that if you ever need to add a client in the future, you can just run this script to quickly create the config file and ensure that all the important information is stored in a single, easy-to-access location.

Please note that any time you add a new client, **you will need to generate new keys and certificates for it before you can run this script and generate its configuration file**. You will get some practice using this script in the next step."

Again, this is for AFTER a key and certificate for the client has been made.



## DHCP server is now on SERVER so that we can set static route for local IPs to see VPN IPs.

---

**Do I need to port forward + DDNS the router for this to work externally? **

---

OpenVPN on SERVER uses **certificates** to encrypt traffic between itself (the VPN server) and clients.
To *issue* trusted certificates, the Certificate Authority is required (i.e. the Pi). To do this we use EasyRSA, which will create our CA Public Key Infrastructure (PKI).

After we build the CA on the Pi, two important files are produced - these make up the private AND public sides of the SSL certificate:

- ca.crt
  - PUBLIC certificate file, used by all clients of this VPN (phone, laptop, etc)
- ca.key
  - PRIVATE key which is used to sign keys and certificates for other servers and clients
  - This should be kept on the CA and the Pi should be turned off when not signing requests


## How to create client .ovpn file

1. Navigate to EasyRSA-v3.0.6 folder in home on SERVER
```
cd ~/EasyRSA-v3.0.6/
```

2. Generate a client request
```
./easyrsa gen-req client1 nopass
```

3. Copy resulting (unsigned) **client private key** to client-configs/keys
```
cp pki/private/client1.key ~/client-configs/keys/
```

4. Send resulting **client certificate request** to the CA Raspberry Pi for signing
```
scp pki/reqs/client1.req raspberrypi:/tmp
```

5. Log into CA Raspberry Pi and navigate to EasyRSA-v3.0.6 folder in home
```
cd ~/EasyRSA-v3.0.6/
```

6. Import the newly acquired certificate request
```
./easyrsa import-req /tmp/client1.req client1
```

7. SIGN the request (say 'yes' to prompt and enter signing password (normal-ish password ("ca")))
```
./easyrsa sign-req client client1
```

8. Transfer the resulting **client certificate** back to SERVER
```
scp pki/issued/client1.crt <<serverHostName>>:/tmp
```

9. Log into SERVER and copy newly acquired client certificate to ~/client-config/keys folder (note: ta.key and ca.crt should already be in this folder)
```
cp /tmp/client1.crt ~/client-configs/keys/
```

10. Create the compiled .ovpn file using the files from the steps above by running make_config.sh - check ~/client-configs/files for the result.
```
    cd ~/client-configs
    sudo ./make_config.sh client1
```

11. Copy client1.ovpn to client device.