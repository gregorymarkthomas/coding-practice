# Logitech Media Server

## How do I get the Server to transcode?

### Why?

This may be required for moments where the _client_ cannot decode the stream themselves (e.g. `*.wma` files require ffmpeg on the client).


### Limitations?

`*.wma` files will play, but the user will **not** be able to _seek_ throughout the track.


### How?

#### 1. Install `lame`

Use this for `*.mp3` files that may need a lower bitrate during playback (say, for a phone that is streaming across the Internet)

`sudo apt update`
`sudo apt install lame`


#### 2. Install `ffmpeg`

Use this for `*.wma` files that need transcoding into another format (i.e. `*.mp3`).

`sudo apt update`
`sudo apt install ffmpeg`

There will be quite a few associated libraries with `ffmpeg` (unfortunately).


#### 3. Create `custom-convert.conf` to tell Logitech Media Server what to do with `*.wma` files

Create a new file called `custom-convert.conf` in the `/etc/squeezeboxserver/` directory; this will override what already exists in the default `convert.conf`. Add this to `custom-convert.conf`:

```
wma mp3 * *
        # F:{PATH=%f}R:{PATH=%F}B:{BITRATE=--abr %B}D:{RESAMPLE=--resample %D}
        [ffmpeg] -i $FILE$ -map 0:0 -b:a 128k -v 0 -f mp3 -
```

Most of my `*.wma` files are 128kbps in bit rate, so transcoding to that same bit rate works for me.


#### 4. Ensure `custom-convert.conf` has the correct owner and permissions

Check who owns the other `*.conf` files in `/etc/squeezeboxserver/`:

`ls -la /etc/squeezeboxserver`

In my case, user `110` and group `nogroup` owned the other files, so, I set the same for `custom-convert.conf`:

`chown 110:nogroup /etc/squeezeboxserver/custom-convert.conf`

The other files had `-rw-r--r--` file permissions, so I did the same for `custom-convert.conf`:

`chmod 644 /etc/squeezeboxserver/custom-convert.conf`


#### 5. Restart `logitechmediaserver.service`

`systemctl restart logitechmediaserver.service`