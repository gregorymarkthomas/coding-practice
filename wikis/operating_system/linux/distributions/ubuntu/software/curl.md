# Client URL (cURL)

## What is it?

Client URL (cURL) is a library and [command line](/interfaces/command_line_interface-cli.md) tool used for **transferring data using various network protocols.**


## What is the library called?

`libcurl`.


## What protocols does `libcurl` support?

- cookies
- DICT
- FTP
- FTPS
- Gopher
- HTTP/1 (with HTTP/2 and HTTP/3 support)
- HTTP POST
- HTTP PUT
- HTTP proxy tunneling
- HTTPS
- IMAP
- Kerberos
- LDAP
- MQTT
- POP3
- RTSP
- RTMP
- SCP
- SMTP
- SMB


## What is `curl` the command-line tool used for?

`curl` is used for getting or sending data.


## What kind of data?

`curl` can be used to get or send _files_, and download [webpages](/internet/web_page.md), using the [Uniform Resource Locator (URL)](/internet/uniform_resource_locator-url.md) syntax.


## Can `curl` be used to test the [ports](/internet/port.md) of a [host](/internet/host.md)?

Yes, particularly using [Transmission Control Protocol (TCP)](/internet/transmission_control_protocol-tcp.md).


## How?

We can test port `22` of the [`localhost`](/internet/localhost.md) via this command:

```shell
[root@localhost ~]# curl -v telnet://127.0.0.1:22

* About to connect() to 127.0.0.1 port 22 (#0)
* Trying 127.0.0.1...
* Connected to 127.0.0.1 (127.0.0.1) port 22 (#0)
```