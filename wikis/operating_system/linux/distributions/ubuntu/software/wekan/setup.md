# Wekan setup on Ubuntu

## Ubuntu 20.04 (manual) with Wekan v5.65

If installing Wekan on an LXC container, this may be the better option; [`snap`](/operating_system/linux/distributions/ubuntu/package_managers/snap.md) has some issues when used within containers.


### 1. Install `unzip`

`sudo apt install unzip`


### 2. Install [Node.js](/operating_system/linux/distributions/ubuntu/software/node_js/node_js.md)

We are installing Wekan v5.65, which requires a version of Node **> 12.0.0**.

Read to bottom of [Node.js setup](/operating_system/linux/distributions/ubuntu/software/node_js/setup.md).


### 3. Create directory for Wekan install

`sudo mkdir /var/lib/wekan`


### 4. `cd` to new Wekan directory

`cd /var/lib/wekan`


### 5. (optional) Remove any previous versions (ENSURE BACKUP EXISTS FIRST)

`sudo rm -r bundle`


### 6. Download latest release

Latest release is `5.65` at time of writing.

`wget https://releases.wekan.team/wekan-5.65.zip`


### 7. `unzip` the download to new Wekan directory

**Assuming you have done step 4**.

`unzip wekan-5.65.zip`


### 8. `cd` to `server` directory

**Assuming you have done step 4**.

`cd bundle/programs/server/`


### 9. Install required libraries

**Assuming you have done step 8**.

`npm install`


### 10. Install `g++`

`apt-get install g++`


### 11. Install `node-gyp` library

`npm install node-gyp`

If error regarding `/var/lib/wekan/bundle/programs/server/node_modules/.bin/node-gyp` not removing itself, remove it manually:

`rm /var/lib/wekan/bundle/programs/server/node_modules/.bin/node-gyp`

and retry install of `node-gyp`:

`npm install node-gyp`


### 12. Install `node-pre-gyp` library

`npm install node-pre-gyp`

If error regarding `/var/lib/wekan/bundle/programs/server/node_modules/.bin/node-pre-gyp` not removing itself, remove it manually:

`rm /var/lib/wekan/bundle/programs/server/node_modules/.bin/node-pre-gyp`

and retry install of `node-pre-gyp`:

`npm install node-pre-gyp`


### 13. Install `fibers` library

This relies on `apt install g++` being installed (step #10), **and** `apt install build-essential` (this should have been done during Node.js installation).

`npm install fibers`


### 14. Create `wekan` user and group to Ubuntu operating system

This will create a new **user** _and_ **group**:

`sudo adduser wekan`

(optional) Set user id:

`sudo adduser -u 2001 wekan`



### 15. Ensure `wekan` user and group owns application files

`sudo chown -R wekan:wekan /var/lib/wekan`



### 16. Create database and database user for Wekan in [MongoDB](\operating_system\linux\distributions\ubuntu\software\mongodb\mongodb.md)

**This will have already been done if you followed the MongoDB [setup](\operating_system\linux\distributions\ubuntu\software\mongodb\setup.md) (step #12).**

**Please make a note of the database name, username and password that you create**; we will need to use these to allow the Wekan service to connect to the database automatically.



### 17. Run Wekan as a _service_

#### a. Create service file

`sudo nano /etc/systemd/system/wekan.service`


#### b. Add service text

Ensure that `User` and `Group` are set to the `wekan` user we created in step #14, `WorkingDirectory` is set to the directory created in step #4, and that `ExecStart` matches `node`s directory (use `which node` to find this):

```
[Unit]
Description=The Wekan Service
After=syslog.target network.target

[Service]
EnvironmentFile=/etc/default/wekan
User=wekan
Group=wekan
WorkingDirectory=/var/lib/wekan/bundle
ExecStart=/usr/local/bin/node main.js
Restart=on-failure
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```

#### c. Create defaults configuration file

`sudo nano /etc/default/wekan`

Copy in the following section; this assumes that 

- MongoDB is installed on the **local** machine (set hostname/IP accordingly)
- the [MongoDB](\operating_system\linux\distributions\ubuntu\software\mongodb\mongodb.md) database that should have been created during [setup](\operating_system\linux\distributions\ubuntu\software\mongodb\setup.md) is called `wekan`
- **there is no authentication** and that [Node.js is being run on port 80](/operating_system/linux/distributions/ubuntu/software/node_js/run_on_port_80.md):

```
NODE_ENV=production
WITH_API=true
MONGO_URL=mongodb://127.0.0.1:27017/wekan
ROOT_URL=http://192.168.0.x
PORT=80
RICHER_CARD_COMMENT_EDITOR=false
```

(optional) If you have a _remote_ MongoDB server, edit `MONGO_URL`; this assumes **authentication is enabled** and **the remote hostname/IP address is binded to the database instance via mongo** via `/etc/mongod.conf`:

```
...
MONGO_URL=mongodb://<database_username>:<database_password>@<hostname/ip_address>:27017/<database_name>
...
```


(optional) Add `DEBUG=true` to see debugger logs in `/var/log/syslog`.

Save the file.



### 18. Start, enable, and check new Wekan service

```
sudo systemctl enable wekan
sudo systemctl start wekan
sudo systemctl status wekan
```


### 19. (optional) Open incoming port of firewall

This assumes that you have a firewall enabled. This tutorial assumes that [Node.js is being run on port 80](/operating_system/linux/distributions/ubuntu/software/node_js/run_on_port_80.md) (and therefore so does Wekan), so, please allow incoming traffic into port 80 in your firewall.


### 20. Test the application

From another machine's browser, try accessing Wekan:

`http://192.168.0.x`



## Ubuntu 18.04+ via [`snap`](/operating_system/linux/distributions/ubuntu/package_managers/snap.md)

### 1. Install `snap`

`sudo apt install snapd`


### 2. Install `wekan` via `snap`

```
sudo snap install wekan
```

### 3. Set the port of Wekan

```
sudo snap set wekan port='3001'
```


### 4. Restart Wekan service

```
sudo systemctl restart snap.wekan.wekan
```


### 5. (optional) Open default ports

```
sudo ufw allow from 192.168.0.0/16 to any port 3001
sudo ufw allow from 10.0.0.0/24 to any port 3001
```


## Troubleshooting

- Add `DEBUG=true` to `/etc/default/wekan` to see debugger logs in `/var/log/syslog`


## Resources

- https://github.com/wekan/wekan/wiki/Platforms
- https://github.com/wekan/wekan/wiki/Raspberry-Pi
- https://github.com/wekan/wekan-snap/wiki/Install
- https://www.howtoforge.com/tutorial/centos-wekan-installation/
- https://community.vanila.io/wekan/general/what-is-the-default-wekan-mongo-db-user-pass~bc2201e3-37be-4b84-bf74-351d9f292c04
- https://docs.mongodb.com/manual/reference/connection-string/
- https://stackoverflow.com/questions/7486623/mongodb-password-with-in-it
- https://mkyong.com/mongodb/mongodb-allow-remote-access/