# Restoring Wekan data

## What does this involve?

Data restoration is done via the database software that Wekan uses, which is [MongoDB](\operating_system\linux\distributions\ubuntu\software\mongodb\mongodb.md).


## How do we restore our data via MongoDB?

### Wekan is installed manually

If you installed [Wekan](\operating_system\linux\distributions\ubuntu\software\wekan\wekan.md), [Node.js](\operating_system\linux\distributions\ubuntu\software\node_js\node_js.md) and [MongoDB](\operating_system\linux\distributions\ubuntu\software\mongodb\mongodb.md) **individually**, and you installed Wekan using the [setup instructions](setup.md), then we can assume that you have created an Administrator account that has the power to backup and restore MongoDB data.


#### 1. `cd` into a directory that holds the MongoDB `dump` directory

`cd <dump_directory>` 


#### 2. (unsure if required) Stop Wekan service

##### Manual Wekan install

`sudo systemctl stop wekan`

Keep `mongod` service running!


##### `snap` Wekan install

```
sudo snap stop wekan
sudo snap stop wekan.caddy
```

I think we need to keep `wekan.mongodb` running!


#### 3. Restore data from MongoDB

Use the MongoDB [database _restore_ instructions](\operating_system\linux\distributions\ubuntu\software\mongodb\restore_database.md) as a guide.
This assumes you have set up Wekan and MongoDB as per the [Wekan setup instructions](\operating_system\linux\distributions\ubuntu\software\wekan\setup.md) and [MongoDB setup instructions](\operating_system\linux\distributions\ubuntu\software\mongodb\setup.md).