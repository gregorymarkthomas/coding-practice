# Backup Wekan data

## What does this involve?

Backups are best done via the database software that Wekan uses, which is [MongoDB](\operating_system\linux\distributions\ubuntu\software\mongodb\mongodb.md).


## How do we backup our data via MongoDB?

### Wekan is installed manually

If you installed [Wekan](\operating_system\linux\distributions\ubuntu\software\wekan\wekan.md), [Node.js](\operating_system\linux\distributions\ubuntu\software\node_js\node_js.md) and [MongoDB](\operating_system\linux\distributions\ubuntu\software\mongodb\mongodb.md) **individually**, and you installed Wekan using the [setup instructions](setup.md), then we can assume that you have created an Administrator account that has the power to backup and restore MongoDB data.


#### 1. `cd` into a directory that the _current_ user has sufficient permissions for

`cd ~` 


#### 2. (unsure if required) Stop Wekan service

I am unsure if this is helpful or a hindrance; I spent ages trying to get the `snap` version of Wekan/MongoDB to backup to no avail, but tried it again when the services were running and it worked! So, who knows.


##### Manual Wekan install

`sudo systemctl stop wekan`

Keep `mongod` service running!


##### `snap` Wekan install

```
sudo snap stop wekan
sudo snap stop wekan.caddy
```

I think we need to keep `wekan.mongodb` running!



#### 3. Dump data from MongoDB

##### Manual Wekan install

Use the MongoDB [database backup instructions](\operating_system\linux\distributions\ubuntu\software\mongodb\backup_database.md) as a guide.
This assumes you have set up Wekan and MongoDB as per the [Wekan setup instructions](\operating_system\linux\distributions\ubuntu\software\wekan\setup.md) and [MongoDB setup instructions](\operating_system\linux\distributions\ubuntu\software\mongodb\setup.md).


##### `snap` Wekan install

I had a lot of trouble with this; the dump would just fail with a generic error. However, I tried again with (seemingly) all services running normally (Wekan, MongoDB, node.js), and with step 'a' already done and... voila!


###### a. Use `snap` version of MongoDB

`sudo nano ~/.bashrc`

Add these two lines to the bottom of `.bashrc`:

```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/snap/wekan/current/lib/x86_64-linux-gnu
export PATH="$PATH:/snap/wekan/current/bin"
```

Note: or maybe this step breaks it! I have not tested the snap version of Wekan enough, nor do I want to continue with it.


###### b. Dump via `mongodump`

`snap` version of Wekan uses port 27019:

`mongodump --port 27019`