# Restore a MongoDB database

## How do we restore our data via MongoDB?

### 1. `cd` into a directory that holds the MongoDB `dump` directory

`cd <dump_directory>` 


### 2. Restore data into MongoDB

As per the [MongoDB setup instructions](\operating_system\linux\distributions\ubuntu\software\mongodb\setup.md), this assumes you have _created a new service-specific database and associated user_:

- authentication is enabled for MongoDB
- created a service-specific database in MongoDB
- created a service-specific database _user_ with a password
- service-specific database user **has backup and restore rights to the database you would like to backup**

This assumes you have done step #1 as we are assuming the _current_ directory is our `dump/<service_name>` directory from a [MongoDB backup](backup_database.md):

```
mongorestore mongodb://<database_user_name>@<hostname>:<port>/<database_name> .
```

The above command should ask for a password:

`Enter your password: _`


After entering your password, logging files of the restoration process should appear.


#### 3. (optional) Try the service in question