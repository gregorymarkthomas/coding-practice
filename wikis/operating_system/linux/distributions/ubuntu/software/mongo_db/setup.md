# MongoDB setup on Ubuntu

## Installing MongoDB version 4.4.x

### Ubuntu 20.04

Please note that `4.4` is used throughout each instruction; this is important as these will specify version 4.4 and not the latest (5.0 at time of writing).

#### 1. Install repository for the Ubuntu version and MongoDB version

`echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list`


#### 2. Ensure `gnupg` is installed

`sudo apt-get install gnupg`


#### 3. Import public key used by package management system

`wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add -`


#### 4. Reload package database

`sudo apt update`


#### 5. Install MongoDB packages

Do not install `mongodb`, as this version is old and is maintained by Ubuntu and not MongoDB themselves.

Do not bother trying to specify the MongoDB version (`sudo apt-get install -y mongodb-org=4.4.10 mongodb-org-database=4.4.10 mongodb-org-server=4.4.10 mongodb-org-shell=4.4.10 mongodb-org-mongos=4.4.10 mongodb-org-tools=4.4.10`) as this seems to complain that `mongodb-database` does not exist.

`sudo apt install mongodb-org` will install the latest version of 4.4.x.


#### 6. (optional) Stop auto update of MongoDB packages

This should not be required as we are already hamstrung with 4.4.x, and we will have installed the latest version in the previous steps.

```
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-database hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
```


#### 7. Enable and start MongoDB service

`sudo systemctl enable mongod.service`

`sudo systemctl start mongod.service`

`sudo systemctl status mongod.service`


#### 8. Run `mongo` shell and create _admin_ account

`user@hostname:~# mongo`

The mongo shell should open.

Create an admin account for all your mongo-related needs; remember to change the password:

`db.createUser({user:"admin",pwd:"password",roles:[{role:"root",db:"admin"}]});`


#### 9. Enable security measures by forcing authentication

`nano /etc/mongod.conf`

Ensure this appears (it may be commented out):

```
security:
 authorization: enabled
```

Save and exit.


#### 10. Restart `mongod` service

`sudo systemctl restart mongod`


#### 11. Check authentication by logging back into `mongo` shell as new admin user

`user@hostname:~# mongo -u admin -p`

A password prompt should appear. **Be aware that the `-p` is required**; weirdly, if left out, the password prompt will still appear **but the seemingly correct password entry will be deemed incorrect**.


#### 12. Create a service-specific MongoDB user to manage its database

Assuming you are already logged into the mongo shell as per the previous step.

First, _create our database_ (change \<service_name\> to whatever, e.g. `wekan`):

`use <service_name>`

Now create a service-specific user; remember to change \<username\> and \<password\>.

`db.createUser({user:"<username>",pwd:"<password>",roles:["readWrite"]});`

Now exit the mongo shell.


#### 13. Log in as new service-specific MongoDB user

We want to ensure that this works okay:

`user@hostname:~# mongo <service_name> -u <username> -p`

Please ensure that \<service_name\> is mentioned here, otherwise **`mongo` will try to authenticate the user in question to a table it is not authorised to read**; if you do not do this, you may spend ages entering passwords which would otherwise work, but you will be told a generic `Error: Authentication failed.`


#### 14. (optional) Allow for _remote_ connections

`nano /etc/mongod.conf`

```
# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1,<hostname>,<ip_address>
```

Save and exit, and **restart mongod** with `sudo systemctl restart mongod`

**Check that the firewall is allowing traffic IN to your chosen MongoDB port** (normally 27017).



## Troubleshooting

### General

- read logs at `/var/log/mongodb/mongod.log`


### Why can't I log into `mongo` shell?

Did you add the `-p` argument? See step #11 above.


### Why is the connection refused?

Test on the _local_ MongoDB machine:

`mongo mongodb://<username>:<password>@<hostname/ip_address>:27017/<database_name>`

Then check the logs at `/var/log/mongodb/mongod.log`.

- Is the connection URL that you use correct?
	- username, password, database name, port?
- Have you binded the hostname to the MongoDB instance?
	- See step #14