# Backup a MongoDB database

## How do we backup our data via MongoDB?

#### 1. `cd` into a directory that the _current_ user has sufficient permissions for

`cd ~` 


#### 2. Dump data from MongoDB

As per the [MongoDB setup instructions](\operating_system\linux\distributions\ubuntu\software\mongodb\setup.md), this assumes you have:

- authentication is enabled for MongoDB
- created a service-specific database in MongoDB
- created a service-specific database _user_ with a password
- service-specific database user **has backup and restore rights to the database you would like to backup**
- MongoDB uses default port of 27017
- MongoDB is running on the `localhost`

```
mongodump -d <database_name> -u <database_user_name>
```

**Or**, for a more generic command:

```
mongodump mongodb://<database_user_name>@<hostname>:<port>/<database_name>
```

Both options should ask for a password:

`Enter your password: _`


After entering your password, logging files of the backup process should appear, and after a successful backup, the files should appear in a directory named `dump/<service_name>` in the _current_ directory (as done in step #1).


#### 3. (optional) Transfer `dump/<service_name>` to another machine

- a. Ensure that the recipient machine's user has the local machine's _public_ key
	- We will be using [Secure Shell (SSH)](/networking/secure_shell-ssh/secure_shell-ssh) to transfer the directory to the recipient machine.
	- Log into the recipient machine and add the **local machine's** _public_ key to their `~/.ssh/authorized_keys`
- b. Ensure recipient machine's firewall accepts port 22
- c. Transfer `dump` to a directory that the recipient user has write rights to (such as `/home/<recipient_username>`)
	- `scp -r dump/<service_name>/ <recipient_username>@<recipient_hostname>:/<permitted_path>`