* open mongod.conf
```
cd /etc/mongod.conf
```

* Comment security
```
#security:
#  authroization: "enabled"
```

* Restart mongodb
```
sudo service mongod stop
sudo service mongod start
```

* Run mongo
```
mongo
```

* Run these cmds
```
use admin
db.createUser({user:"admin",pwd:"password",roles:[{role:"root",db:"admin"}]});
```

* Enable auth again what you had commented in /etc/mongod.conf file (remove comments)

* Restart mongod service again 
* DONE