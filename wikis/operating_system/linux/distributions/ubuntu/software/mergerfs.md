# MergerFS

## How to install/update

```
$ cd ~
$ git clone https://github.com/trapexit/mergerfs.git
$ # or
$ wget https://github.com/trapexit/mergerfs/releases/download/<ver>/mergerfs-<ver>.tar.gz
```

```
$ tar -xvf mergerfs-<ver>.tar.gz
```

Provided git, g++, make and python are installed:

```
$ cd mergerfs
$ make
$ sudo make install
```

else, try

```
$ cd mergerfs
$ sudo tools/install-build-pkgs
$ make deb
$ sudo dpkg -i ../mergerfs_version_arch.deb
```

(though this failed for me).