# Running Node.js on port 80

## Why run on port 80?

- You don't have to use apache or nginx
- You don't have to run your application as root
- You won't have to forward ports (and handle that each time your machine boots)


## Why do we need to make changes to allow access via port 80?

Via [https://stackoverflow.com/questions/60372618/nodejs-listen-eacces-permission-denied-0-0-0-080](https://stackoverflow.com/questions/60372618/nodejs-listen-eacces-permission-denied-0-0-0-080): Non-privileged user (not root) can't open a listening socket on ports below 1024.

Check [this](https://www.digitalocean.com/community/tutorials/how-to-use-pm2-to-setup-a-node-js-production-environment-on-an-ubuntu-vps#give-safe-user-permission-to-use-port-80):

    Give Safe User Permission To Use Port 80

    Remember, we do NOT want to run your applications as the root user, but there is a hitch: your safe user does not have permission to use the default HTTP port (80). You goal is to be able to publish a website that visitors can use by navigating to an easy to use URL like http://ip:port/

    Unfortunately, unless you sign on as root, you’ll normally have to use a URL like http://ip:port - where port number > 1024.

    A lot of people get stuck here, but the solution is easy. There a few options but this is the one I like. Type the following commands:

    > sudo apt-get install libcap2-bin 
    > sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\`` 

    Now, when you tell a Node application that you want it to run on port 80, it will not complain.



## Are these actually _generic_ instructions that can be used for other platforms apart from `node`?

Apparently so.


## Instructions

### 1. Find where `node` is installed to

`which node`

Perhaps something like `/usr/local/bin/node` is returned.


### 2. Install `libcap2-bin`

`sudo apt-get install libcap2-bin`


### 3. Add _capability_ (whatever that means - bind service to port 80?) to the found path of `node`

E.g. `sudo setcap cap_net_bind_service=+ep /usr/local/bin/node`

Your `node` path may be different.


## Links

- https://www.digitalocean.com/community/tutorials/how-to-use-pm2-to-setup-a-node-js-production-environment-on-an-ubuntu-vps