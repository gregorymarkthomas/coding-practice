# Node.js setup on Ubuntu

## What version of Node.js should I install?

**LTS** version (currently v14.x), probably. 


## Installing Node.js

### Ubuntu 20.04

#### 1. Install `curl`

`sudo apt install curl`


#### 2. Install repository for the Node.js version

`curl -fsSL https://deb.nodesource.com/setup_{lts/#}.x | sudo -E bash -`

`{lts/current/#}`: use `lts` for the LTS version, `current` for the current version, or a number for a specific version.


#### 3. Install `nodejs`

`sudo apt-get install -y nodejs`


#### 4. (optional) Install `n` (Node.js version manager) via `npm`

`npm install -g n`


#### 5. (optional) Install a _different_ version of Node.js using `n`

e.g. `8.16.2` is used for Wekan v3.5x:

`sudo n 8.16.2`


e.g. `14.18.1` (`lts` at time of writing) is used for Wekan v5.65:

`sudo n 14.18.1`