---
title:  Squeezelite
author: Gregory Thomas
date:   2020-08-20
---

# Squeezelite

## What is it?

Squeezelite is a [Logitech Media Server (LMS)](logitech_media_server.md) "Squeezebox" emulator which allows a device to connect to the Logitech Media Server.


## What does it allow us to do?

We can play an organised music collection through a squeezelite client via [LMS](logitech_media_server.md).


## What platforms support Squeezelite?

- [Linux](/operating_system/linux/linux.md)
- [Mac](/operating_system/apple/macos/macos.md)
- [Windows](/operating_system/microsoft/windows/windows.md)
- [Android](/operating_system/google/android/android.md)


## Tutorial: Squeezelite [Linux](/operating_system/linux/linux.md) on ([Raspberry Pi OS](../distributions/raspberry_pi_os.md)) for Raspberry Pi

### Assumptions

- Squeezelite can connect to [Logitech Media Server (LMS)](logitech_media_server.md) running on a device (the same or another)
- [Raspberry Pi OS](../distributions/raspberry_pi_os.md) is installed on your Raspberry Pi (version up to 2020-08-20)
- A ```sudo``` account exists (either user ```pi``` or your own). 
	- To test squeezelite with your own ```sudo``` account, add member to ```audio``` group so it has permission to use audio devices.


### Process
#### 1. Open Logitech Media Server ports in [Uncomplicated FireWall (UFW)](../uncomplicated_firewall.md) (if enabled) 

If you have a _remote_ [LMS](logitech_media_server.md), and the Pi has UFW enabled, open the correct ports using these commands:

- ```sudo ufw allow from <your subnetwork> proto tcp to any port 9000```
	- [Transmission Control Protocol (TCP)](/networking/transmission_control_protocol.md) only
- ```sudo ufw allow from <your subnetwork> to any port 3483```
	- [TCP](/networking/transmission_control_protocol.md) and [User Datagram Protocol (UDP)](/networking/user_datagram_protocol.md)

In an effort to secure the Pi, we only want to allow _incoming_ traffic specifically from the _remote_ [LMS](logitech_media_server.md).
	

#### 2. Install squeezelite via [Advanced Package Tool (APT)](../advanced_package_tool_apt.md)

We want to utilise [systemctl](../systemctl.md), and we can let the [APT](../advanced_package_tool_apt.md) installation of squeezelite do that for us:

```sudo apt install squeezelite```

Install any extra modules that are required.


#### 3. Check audio output device

The Pi (especially the mk.1)'s 3.5mm jack and [High-Definition Multimedia Interface (HDMI)](/hardware/multimedia/high_definition_multimedia_interface_hdmi.md) has _poor_ audio output; this is why I use an external [Universal Serial Bus (USB)](/hardware/io/universal_serial_bus_usb.md) [Audio Interface](/hardware/multimedia/audio/audio_interface.md) (such as the [Behringer UFO202](https://www.behringer.com/product.html?modelCode=P0A12)).

1. ```sudo alsamixer``` 
	- Allows us to see available audio outputs and increase output volume.
2. Press F6 to change to _your_ desired output
3. Press up to increase the output volume _just below the red marker_
4. Press esc to exit
5. ```speaker-test -Dhw:1,0 -c2 -twav```
	- Does a speaker test; ensure your speaker system is set up
6. Ctrl+c to cancel once you hear sounds through your speakers
7. ```squeezelite -l```
	- Displays list of available audio outputs
8. Copy output device _name_ relating to your soundcard to clipboard
	- I have ```plughw:CARD=CODEC,DEV=0``` copied for my setup


#### 4. Create [systemctl](../systemctl.md) startup service file

Installing squeezelite via [Advanced Package Tool (APT)](../advanced_package_tool_apt.md) creates a defaults file at ```/etc/default/squeezelite```, and it would be _reasonable_ to suggest that [systemctl](../systemctl.md) uses this to apply our options to the squeezelite call. 
**However**, it does **not**; no amount of [systemctl](../systemctl.md) service enabling would work. To fix this:

```sudo nano /etc/systemd/system/squeezelite.service```

This will create a _new_ file. Paste this into this file:

```
[Unit]

Description=Squeezelite

After=network.target
[Service]

ExecStart=/usr/bin/squeezelite -o <your chosen audio output device> -n <friendly client name> -s <LMS IP address> -a 80:40::0
[Install]

WantedBy=multi-user.target
```

```-a 80:40::0``` is used to specify [Advanced Linux Sound Architecture (ALSA)](../advanced_linux_sound_architecture_alsa.md) parameters to get the most out of the [Audio Interface](/hardware/multimedia/audio/audio_interface.md).


#### 5. Enable and start squeezelite [systemctl](../systemctl.md)

1. ```sudo systemctl daemon-reload```
	- We have changed the setup for this service, so we need to reload
2. ```sudo systemctl enable squeezelite```
	- This will enable squeezelite to run automatically on startup
3. ```sudo systemctl start squeezelite```
4. ```sudo journalctl -u squeezelite```
	- Check for any errors

In theory, squeezelite should be working! 


#### Troubleshooting

##### Client not showing

Double-check the output of ```sudo journalctl -u squeezelite``` for any errors. 

If all looks okay, try this:

1. ```sudo systemctl stop squeezelite```
2. ```squeezelite -o <your chosen audio output device> -n <friendly client name> -s <LMS IP address> -a 80:40::0```
	- The [shell](../shell.md) should now be occupied with squeezelite; it's now running it in the foreground
3. Check for errors that show up for squeezelite's output
4. Check [LMS](logitech_media_server.md)'s web interface (```\\<LMS IP address>:9000``` in your web browser) for your new squeezelite client

If the device shows in the list, then investigate why squeezelite will not work with [systemctl](../systemctl.md).

If the device does _not_ show, then:

- double check any errors in squeezelite's output
- double check that the <LMS IP address> for the squeezelite command is correct


##### Fix [ffmpeg](ffmpeg.md) transcoding issue for [Windows Media Audio (WMA)](/operating_system/microsoft/windows_media_audio.md) files

###### What is this?

If you have [Windows Media Audio (WMA)](/operating_system/microsoft/windows_media_audio.md) files within your music collection, then you may have issues with the latest version of squeezelite via [Advanced Package Tool (APT)](../advanced_package_tool_apt.md).

###### What happens?

When [WMA](/operating_system/microsoft/windows_media_audio.md) files are played through the new squeezelite client, [ffmpeg](ffmpeg.md) errors will fill the logs; this may be due to the latest squeezelite not [transcoding](/data/transcoding.md) WMA files properly.

###### How do we fix it?

_Replace_ the current squeezelite version; 1.9.7.1218, an older version, works for me, but a fix may be available in a _newer_ version (one that is newer than the version from [APT](../advanced_package_tool_apt.md)).

To do this:

1. ```sudo systemctl stop squeezelite```
2. ```sudo cp /usr/bin/squeezelite /usr/bin/squeezelite_backup```
	- Just in case the _replacement_ does not work
3. ```cd ~```
4. ```mkdir squeezelite_temp```
5. ```cd squeezelite_temp```
6. ```wget <link to squeezelite download>```
7. ```tar -xvzf <filename>.tar.gz``` (assuming the file is a ["tarball"](../tarball.md))
8. ```sudo mv squeezelite /usr/bin/```
	- ```squeezelite``` in-question is the _replacement_; after this, [systemctl](../systemctl.md) should now use to the _replacement_ version.  
9. ```sudo systemctl start squeezelite```
10. ```sudo systemctl status squeezelite```
	- Check there are no startup errors
11. Try a [WMA](/operating_system/microsoft/windows_media_audio.md) file
12. ```sudo journalctl -u squeezelite```
	- Check for any [ffmpeg](ffmpeg.md) errors


###### Are there any issues with doing this?

If squeezelite is updated via [APT](../advanced_package_tool_apt.md), then our _replacement_ version will itself be replaced.


## Links

- [http://www.gerrelt.nl/RaspberryPi/wordpress/tutorial-installing-squeezelite-player-on-raspbian/](http://www.gerrelt.nl/RaspberryPi/wordpress/tutorial-installing-squeezelite-player-on-raspbian/)
- [https://www.hagensieker.com/wordpress/2018/06/12/302/](https://www.hagensieker.com/wordpress/2018/06/12/302/)
- [https://raspberrypi.stackexchange.com/questions/80072/how-can-i-use-an-external-usb-sound-card-and-set-it-as-default](https://raspberrypi.stackexchange.com/questions/80072/how-can-i-use-an-external-usb-sound-card-and-set-it-as-default)