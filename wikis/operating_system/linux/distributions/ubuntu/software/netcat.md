# netcat (nc)

## What is it?

netcat (nc) is a [command line](/interfaces/command_line_interface-cli.md) tool used for **reading from and writing to network connections using the [Transmission Control Protocol (TCP](/internet/protocol/transmission_control_protocol-tcp.md) or the [User Datagram Protocol (UDP)](/internet/protocol/user_datagram_protocol-udp.md)**.


## Can `nc` be used to test the [ports](/internet/port.md) of a [host](/internet/host.md)?

Yes, particularly using [User Datagram Protocol (UDP)](/internet/protocol/user_datagram_protocol-udp.md).


## How?

We can test port `22` of the [`localhost`](/internet/localhost.md) via this command:

```shell
[root@localhost ~]# nc -v -z -u 127.0.0.1 22

Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 127.0.0.1:22.
Ncat: UDP packet sent successfully
Ncat: 1 bytes sent, 0 bytes received in 2.02 seconds.
```