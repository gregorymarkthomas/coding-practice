---
title:  rsync
author: Gregory Thomas
date:   2020-06-05
---

# rsync

## What is it?

'rsync' is a utility for [Linux](/operating_system/linux/linux.md) that is used for *data synchronisation*.


## What exactly does it do?

rsync does two things:

1. manages file transfers between a source and a destination directory
2. decides *how* data is synchronised


## What is the 'source'?

The source is the directory that we want to copy data **from**.


## What is the 'target'/'destination'?

The target is the directory that we want to copy *source* data **to**.


## So, can the source and destination directories be on separate computers?

Yes! The source and destination directories could be on the same harddrive, or separate harddrives on the same computer, or on separate computers.

If source and destination are separate machines, then rsync will use [Secure Shell (SSH)](/networking/secure_shell.md) with whatever authentication is setup between them; this could be username and password or a [Public-Private key pair](/security/public_private_key_pair.md).


## Right, so, how is rsync better than using Linux ['cp'](/operating_system/linux/commands/cp.md)? It sounds like 'cp'.

rsync is smart about _what_ it sends over; it looks at modified data and file size using a [delta transfer algorithm](/algorithms/delta_transfer_algorithm.md). Using such an algorithm means that only the _changes_ of a file will be transferred to the destination, **and not the whole file**; there is less data to send and therefore means a faster overall transfer.


## Can both source and destination be *remote* resources?

No. rsync will not work when both source *and* destination are *remote*; at least one must be a local resource.


## So, does that mean we can push to a remote destination, **and** _pull from_ a remote destination?

Yes; we just have the remote directory as our _source_ (see examples below).


## What are some common options for rsync?

Thanks to [linuxtechi](https://www.linuxtechi.com/rsync-command-examples-linux/) for these descriptions:

- -v, –verbose
	- Verbose output
- -q, –quiet
	- suppress message output
- -a, –archive
	- archive files and directory while synchronizing (-a **equals to following options**: -rlptgoD)
- -r, –recursive
	- sync files and directories recursively
- -b, –backup
	- take the backup during synchronization
- -u, –update
	- do **not** copy the files from source to destination if _destination_ files are newer
- -l, –links
	- copy symlinks as symlinks during the sync
- -n, –dry-run
	- perform a trial run without synchronization
- -e, –rsh=COMMAND
	- mention the remote shell to use in rsync
- -z, –compress
	- compress file data during the transfer
- -h, –human-readable
	- display the output numbers in a human-readable format
- –progress
	- show the sync progress during transfer


## How do I test that my transfer will work _before_ actually transferring files?

I would recommend using ```--dry-run``` **and** logging the output to a .txt file (```> filename.txt```) so that you can see what files will be transferred to the destination before actually doing so:

```rsync -v --dry-run <source_directory> <destination_directory> > rsync_dryrun.txt```

```-v``` returns [verbose](/terms/verbose.md) output so you can see more detailed logging.

With the above command, we are assuming that you are 1) in your Linux user's home directory, and 2) your user has permissions to handle the specified source and destination folders; is so, the rsync call can run without [sudo](/operating_system/linux/sudo.md) and the ```rsync_dryrun.txt``` file can be written to the user's home directory. Use your favourite text editor (```nano```/```vim```, etc) to read the output file and see what files will be copied over before they are copied over.


## What are some rsync usage examples?

### Transferring files from a local source to local destination (simple)

```rsync <options> <local_source_directory> <local_destination_directory>```

```rsync -au /media/primary/ /media/backup/```

Here we are copying **all** data *within* ```/media/primary``` (and not the *directory itself* - read [Linux's Terminal](/operating_system/linux/terminal.md)) **to** ```/media/backup/```.


### _Pushing_ data from local source to _remote_ destination

The _source_ is local, but the _destination_ is now a _remote destination_:

```rsync <options> <local_source_directory> <username>@<remote_host_name>:<destination_directory>```

```rsync -au /media/primary greg@helloserver:/media/primary```

If your local machine's ```<username>``` is the _same_ as the username at the remote destination, you can remove ```<username>``` and the ```@``` from the call, but keep the server.


### _Pulling_ data from _remote_ source to local destination

This is still _source_ then _destination_, it is just that _source_ is a _remote source_, and _destination_ is a _local destination_; this is a **pull from** a remote source:

```rsync <options> <username>@<remote_host_name>:<source_directory> <local_destination_directory>```

```rsync -au greg@helloserver:/media/primary /media/primary```


### Transfer files at source not owned by current user to LOCAL destination

```sudo rsync -a <local_source_directory> <local_destination_directory>```

This call above relies on ```-a``` to transfer files and directories. ```-a``` also contains ```-o``` and ```-p```, which **maintains the files and folder's owners and permissions**. 


### Transferring files at source not owned by current user to REMOVE destination

Perhaps the root user is disabled and we want to transfer files not owned by the current user to a _remote_ destination.

This solution relies on the [Secure Shell (SSH)](/networking/secure_shell.md) having a [Public-Private key pair](/security/public_private_key_pair.md) to authenticate the local user to the remote destination:

```sudo rsync -a -e "ssh -i /home/<current_user_username>/.ssh/id_rsa" --rsync-path="sudo rsync"  <local_source_directory> <current_user_username>@<remote_host_name>:<destination_directory>```

```-a```, as mentioned, also contains ```-o``` and ```-p``` which **maintains the files and folder's owners and permissions**. 
If we want to transfer files and directories the current user does not own and still maintain their owners and permissions on the _remote destination_, we need to use ```sudo``` for the initial rsync call **and** ```--rsync-path``` so that **rsync on the destination machine also uses sudo**.

We use ```-e "ssh -i /home/<current_user_username>/.ssh/id_rsa"``` here to specify an "alternative" remove shell program, but we are still using [SSH](/networking/secure_shell.md); the difference here is that we need to specify the ```<current_user_username>```'s public key, otherwise the ssh call in the rsync call will try to look for **root's** public key (**we are using sudo, remember**).

To stop the _destination_'s sudo call from requiring root's password (which we **cannot** enter from our source machine), we need to allow rsync to run as sudo **without** a password on the **destination** machine; we can do this by adding an 'rsync' item to [/etc/sudoers.d](/operating_system/linux/sudoers.md) directory. 

```<current_user_username>@<remote_host_name>``` **needs** to be specified here otherwise the rsync call will assume we are trying to log in to the destination as **root** (**again, we are using sudo**).

```sudo rsync -avu -e "ssh -i /home/<current_user_username>/.ssh/id_rsa" --delete-before --progress --rsync-path="sudo rsync" --dry-run /media/primary/ greg@destinationserver:/media/primary/ > rsync_dryrun.txt```


## Can we provide an even more secure file transfer over the network?

Yes; one such way is to use a [Virtual Private Network (VPN)](/networking/virtual_private_network.md); I personally use one in my network setup.
Server 'one' and server 'two' are both Linux machines, situated in two separate remote locations. These two servers can only communicate with each other when they are both connected to the same [VPN](/networking/virtual_private_network.md); one server is the 'VPN server', and the other server is a 'VPN client' which has their network traffic forced through the 'VPN server'.

Using a [VPN](/networking/virtual_private_network.md) is not automatically more secure; the security comes from limiting _how_ clients connect to it. 
Each user can have a username and password created by the VPN server, and with that creates a (rather basic) layer of security. However, for increased security, we can use a [Certificate Authority](/networking/public_key_infrastructure.md); this (completely separate) machine is the only machine that can grant clients access to the [VPN](/networking/virtual_private_network.md) on my server. This means that if a client wants access to the [VPN](/networking/virtual_private_network.md), I have to manually create a signed .ovpn file for that specific client using my [Certificate Authority](/networking/public_key_infrastructure.md) machine; a malicious third-party cannot try and guess usernames and passwords with this setup.
This relies on having [Uncomplicated Firewall (UFW)](/operating_system/linux/uncomplicated_firewall.md) running and only allowing incoming traffic on the [VPN](/networking/virtual_private_network.md) server's port.

With the above setup, the two machines are on the same secure network and can call rsync commands to each other. I still require [Secure Shell (SSH)](/networking/secure_shell.md) to communicate between the two machines even when within the same network, which may be a little overkill and does complicate the rsync call (see above).


## Links

- https://www.digitalocean.com/community/tutorials/how-to-copy-files-with-rsync-over-ssh
- https://unix.stackexchange.com/questions/92123/rsync-all-files-of-remote-machine-over-ssh-without-root-user
- http://www.ustrem.org/en/articles/rsync-over-ssh-as-root-en/
- https://askubuntu.com/questions/939677/ubuntu-16-04-rsync-via-ssh-prompts-me-for-password-how-to-make-it-a-cron-job
- https://askubuntu.com/questions/930768/adding-local-content-in-etc-sudoers-d-instead-of-directly-modifying-sodoers-fi
https://www.cyberciti.biz/faq/linux-unix-running-sudo-command-without-a-password/