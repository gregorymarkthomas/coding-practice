# Merging parent project updates into fork

Thankyou to Jörn at https://bassistance.de/2010/06/25/git-fu-updating-your-github-fork/:

GitHub with their web interface makes it really easy to fork a project, but it leaves you alone when it comes to updating your fork with the changes in the original repository. Its actually really easy with a few steps:

`git remote add original git://url-to-original-repo`

This adds another remote repository. You can use `git remote -v` to see your existing remotes. There should be “origin” already, pointing to your GitHub fork. You can use whatever name you like for the new remote repository, above I’ve used “original”.

`git fetch original`

This loads all commits, including branches and tags, from the specified remote repository, using the alias defined above.

`git merge original/master`

This merges all changes from the original master branch in your current branch, eg. your local master branch.

`git push`

By default, push pushes everything to your “origin” repository. Which brings your repo up-to-date. That’s it!
