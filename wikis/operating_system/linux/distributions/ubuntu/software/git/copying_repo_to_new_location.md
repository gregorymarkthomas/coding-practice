---
title:  Copying git repo to new location
author: Gregory Thomas
date:   2020-09-20
---

# Copying git repository to new location

## Why would I want to do this?

You may want to move a [git](git.md) [repository](repository.md) to a new location for these reasons:

- you have a new git repository provider
- you want to create a local backup


## What do we need to ensure when moving [repositories](repository.md)?

We want to maintain the project's _history_; it would be helpful to still be able to look through the previous development of a project.


## What will this document show?

This document will show the process to copy a git repository to a new location, with project history intact.


## Okay. What's the process then?

Pre-process notes:

- I personally use a locally-made repository which has its own directory structure, and this was created [with thanks to Andrew Hoog](https://www.andrewhoog.com/post/howto-setup-a-private-git-server-on-ubuntu-18.04/)
	- Some steps will be a little different

Assumptions:

- You are familiar with [git](git.md) and that you already have an existing [repository](repository.md) of projects.
- You using a local machine as a _middle-man_; at the least we need to [fetch](fetch.md) _all_ branches of each individual project that resides at the existing [repository](repository.md)
	- You may also need to [Secure Shell (SSH)](/networking/secure_shell.md) into your _new_ repository, if a privately owned and managed one. 


### 1. Create an empty project at your new repository

#### I manage my own server via SSH

This assumes:

- You have already setup your own private server
- The git server is set up on it
- There is a dedicated ```git``` user to do git-relating tasks
- You can [SSH](/networking/secure_shell.md) into your private server as a [sudo](/operating_system/linux/sudo.md) user
- You can control the ```git``` user with said sudo user
- git projects are set up in a dedicated directory (e.g. the ```git``` user's [home directory](/operating_system/linux/users/home_directory.md))

1. Log into your server via SSH
2. ```sudo su git``` to become the ```git``` user
3. ```cd``` to the ```git``` user's [home directory](/operating_system/linux/users/home_directory.md)
4. ```mkdir <project_name>.git```
5. ```chmod 700 <project_name>.git``` to tighten security
6. (optional) ```chown git:git <project_name>.git```, if not automatically owned by the ```git``` user
6. ```cd <project_name>.git```
7. ```git init --bare```


#### I let a provider do the work for me

Use their "New project" process; they should provide a web interface to do create a new empty project.


### 2. [Fetch](fetch.md) _all_ remote branches and tags from _existing_ [repository](respository.md) to your local machine

```
cd <path_to_local_git_project>/<project_name>
```

```
git fetch origin
```

### 3. Check for missing branches

I believe the [fetch](fetch.md) command in step 1 should cover this. But, just in case:

```
git branch -a
```


### 4. Create _new_ [remote](remote.md) aimed at _new_ repository ready for [push](push.md)

```new-origin``` can be replaced with anything:

```
git remote add new-origin git@<new_host>:<new_host_username>/<project_name>.git
```

Most git repository providers use a _git_ user to handle git-related tasks on their server; this is why you see ```git@...```.

```<new_host_username>``` is probably required if you are using a dedicated git repository provider.
My own homemade local repository does not use this:

```
git remote add new-origin git@homeserver:<project_name>.git
```


### 5. [Push](push.md) _all_ local [branches](branches.md) and [tags](tags.md) to new [remote](remote.md)

```git push --all new-origin```
```git push --tags new-origin```


### 6. (optional) _Replace_ old [remote](remote.md) with new

This assumes you are _finito_ with your existing repository provider:

```
git remote rm origin
```

```
git remote rename new-origin origin
```


## Resources

- [https://stackoverflow.com/questions/17371150/moving-git-repository-content-to-another-repository-preserving-history#answer-38303789](https://stackoverflow.com/questions/17371150/moving-git-repository-content-to-another-repository-preserving-history#answer-38303789)