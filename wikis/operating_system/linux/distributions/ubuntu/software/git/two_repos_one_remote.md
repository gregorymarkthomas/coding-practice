---
title:  Two repositories using one remote
author: Gregory Thomas
date:   2020-09-20
---

# Two repositories using one remote

## Why would I want to do this?

Say I have two [git](git.md) [repositories](repository.md); it would be nice to be able to ```git push``` to a _single_ [remote](remote.md) so that the two repositories are updated at the same time.


## What will this document show?

This document will show you how to set up a git [push](push.md) so that it uploads to two repositories at the same time.


## Assumptions

- You know what [git](git.md) is and how to use it
- You have an existing [git](git.md) project
- You have _more than one_ [repositories](repository.md) available


## Process

### 1. (optional) Create a new [remote](remote.md)

You can use the existing ```origin``` [remote](remote.md) so that you can use git's default remote name; skip this step and replace ```all``` with ```origin``` in later steps if you want this.

Here, though, I will create a _new_ remote, called ```all```. This new [remote](remote.md) is created with the _primary_ repository's [Uniform Resource Locator (URL)](/networking/uniform_resource_locator.md) added as its destination address:

```
git remote add all git@<primary_repository_host_address>:<username>/<project_name>.git
```

A real-world example:

```
git remote add all git@gitlab.com:gregorymarkthomas/coding_practice.git
```


### 2. _Reapply_ primary repository _push_ URL to stop it being overwritten

Looking at the command in this step suggests that you could just _skip_ it and go straight to step 3...

**However!**

If you do not _reapply_ the [push](push.md) URL to our [remote](remote.md), then the command in step 3 which adds our _secondary_ URL will **overwrite** our _primary_ URL instead of merely _adding_ to it. The ```--add``` option for ```set-url``` in step 3 does not make a difference.

So, please do this step.

Here we are setting the [push](push.md) URL _only_ with the use of the ```--push``` option for ```set-url```; we are **not** changing the [fetch](fetch.md) URL. [See the git-remote documentation](https://git-scm.com/docs/git-remote#Documentation/git-remote.txt-emset-urlem).

```
git remote set-url --add --push all git@<primary_repository_host_address>:<username>/<project_name>.git
```

A real-world example:

```
git remote set-url --add --push all git@gitlab.com:gregorymarkthomas/coding_practice.git
```



### 3. Add another [push](push.md) [URL](/networking/uniform_resource_locator.md) to _new_ remote

Use the ```set-url``` command again to add another URL; this time the ```--add``` option will work. 
When a ```git push``` is called after a [commit](commit.md), the changes will be uploaded to _both_ URLs:

```
git remote set-url --add --push all git@<primary_repository_host_address>:<username>/<project_name>.git
```

A real-world example:

```
git remote set-url --add --push all git@homeserver:coding_practice.git
```


### 4. Check everything is hunky-dory

Look at your new remote setup using this command:

```
git remote -v
```

If a [URL](/networking/uniform_resource_locator.md) looks incorrect, then you're better off removing the remote and starting over:

```
git remote remove all
```


## Links

- Thankyou to Jigar Mehta at [https://jigarius.com/blog/multiple-git-remote-repositories](https://jigarius.com/blog/multiple-git-remote-repositories)
- [git-remote documentation](https://git-scm.com/docs/git-remote#Documentation/git-remote.txt-emset-urlem)
