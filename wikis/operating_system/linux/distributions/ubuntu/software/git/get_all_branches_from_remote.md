# Get all branches from remote source

This assumes you have already used `git clone ...` using the remote source link.

```
git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
git fetch --all
git pull --all
```