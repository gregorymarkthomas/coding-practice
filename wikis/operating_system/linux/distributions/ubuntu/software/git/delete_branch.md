# Deleting a git branch

## How do I delete a branch locally _and_ remotely?

```
// delete branch locally
git branch -d localBranchName

// delete branch remotely
git push origin --delete remoteBranchName
```