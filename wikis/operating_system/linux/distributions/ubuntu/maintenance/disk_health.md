# Disk health

## What do we use to check disk health?

We test a harddrive using [S.M.A.R.T](/hardware/storage/smart.md) (**S**elf-**M**onitoring, **A**nalysis and **R**eporting **T**echnology).


## How do I view S.M.A.R.T information about a harddrive?

### Ubuntu

1. Install `smartctl` which resides in `smartontools`:

```
sudo apt install smartmontools
```

2. Plug harddrive into the local machine 

3. Check the [device label](/hardware/storage/device_label.md) and [partition number](/hardware/storage/partition_number.md) of our harddrive:

```lsblk```

You should see something akin to this:

```
user@server:~$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
...
sdX      8:48   0  10.9T  0 disk
`-sdX1   8:17   0  10.9T  0 part /media/backup


```

The drive above is formatted _and_ mounted, hence the [partition number](/hardware/storage/partition_number.md) underneath the [device label](/hardware/storage/device_label.md), and the arbitrary mountpoint of ```/media/backup```.


4. Check S.M.A.R.T _information_ of our harddrive (**replace _X_ with the device label found in step 3**):

```sudo smartctl --all /dev/sdX```

If you're checking a [Universal Serial Bus (USB)](/hardware/interfaces/universal_serial_bus_usb/universal_serial_bus_usb.md) external drive, you may receive an error like so:

```
/dev/sdX: Unknown USB bridge [0x1058:0x25a3 (0x1030)]
Please specify device type with the -d option.
```

If so, this is because we need to tell smartctl what [USB bridge](/hardware/interfaces/universal_serial_bus_usb/usb_bridge.md)  try specifying the USB-controller vendor-independent [SCSI/ATA Translation (SAT)](/software/interfaces/scsi_ata_translation_sat.md) as the _type_:

```-d sat```

If this does not work, see https://www.smartmontools.org/wiki/Supported_USB-Devices to see what could be compatible.


5. Run a test (add ```-d``` option from step 4 if required)

`sudo smartctl -t offline|short|long|conveyance|force|vendor|N|select|M-N||pending|N|afterselect|[on|off] /dev/sdX`

I opt for `short`.
You should see something like this:

```
=== START OF OFFLINE IMMEDIATE AND SELF-TEST SECTION ===
Sending command: "Execute SMART Short self-test routine immediately in off-line mode".
Drive command "Execute SMART Short self-test routine immediately in off-line mode" successful.
Testing has begun.
Please wait 2 minutes for test to complete.
Test will complete after Wed Jan  6 20:39:50 2021
```

6. After a couple of minutes, read the results (add ```-d``` option from step 4 if required):

```sudo smartctl -l selftest /dev/sdX```

You should _hopefully_ receive the all-clear:

```
=== START OF READ SMART DATA SECTION ===
SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed without error       00%         1         -
```


## Is it worth running a long test?

Well, apparently, drives can fail even if a long test came back okay, so, is there any point in spending 12+ hours running the long test? 