# Formatting a new hard drive

## What are the basics?

### Assumptions

- **You have backed up any existing storage on this harddrive that you want to keep**
	- **This will delete all data that resides on the harddrive!!**
- You are comfortable using Linux Terminal commands
- You are not trying to format the harddrive that the operating system is currently using.


### Steps

1. Plug the drive into the local machine.

2. Check the [device label](/hardware/storage/device_label.md), [partition number](/hardware/storage/partition_number.md) and current mount point of our harddrive:

```lsblk```

You should see something akin to this:

```
user@server:~$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
...
sdX      8:48   0  10.9T  0 disk
`-sdX1   8:17   0  10.9T  0 part /media/backup


```

The drive above is _already_ formatted _and_ mounted, hence the [partition number](/hardware/storage/partition_number.md) underneath the [device label](/hardware/storage/device_label.md), and the arbitrary mountpoint of ```/media/backup```.

We do not care if the drive is already formatted, but, we _do_ want to unmount the drive.


3. **Unmount** the drive to be formatted (if currently _mounted_):

```sudo umount /media/backup```

**Change** the mount destination to that of the partition's mountpoint found in step 2.


4. Check the [mount point](/software/interfaces/mount_point.md) again to check it **no longer exists**:

```lsblk```

You should see something akin to this:

```
user@server:~$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
...
sdX      8:48   0  10.9T  0 disk
`-sdX1   8:17   0  10.9T  0 part

```


5. Choose the partitioning standard; this will more-likely be [GUID Partition Table (GPT)](/software/storage/partitioning/standards/guid_partition_table.md):

```sudo parted /dev/sdX mklabel gpt```

**Replace _X_ with the device label found in step 2**.


6. Check the [partition number](/hardware/storage/partition_number.md) again to check step 5 has **cleared it**:

```lsblk```

You should see something akin to this:

```
user@server:~$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
...
sdX      8:48   0  10.9T  0 disk


```


5. _Create_ a new partition on the drive; this assumes you will eventually format the drive as [ext4](/hardware/storage/filesystems/journaling_file_system_jfs/ext4.md):

```sudo parted -a opt /dev/sdX mkpart primary ext4 0% 100%```


6. Check the [partition number](/hardware/storage/partition_number.md) again to check step 5 has worked:

```lsblk```

You should see something akin to this:

```
user@server:~$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
...
sdX      8:48   0  10.9T  0 disk
`-sdX1   8:17   0  10.9T  0 part

```


7. Create a [filesystem](/hardware/storage/filesystems/fileststems.md) with a partition label. Make sure you pass in the **partition** and not the entire _disk_ (see the **1** next to ```sdX```):

```
sudo mkfs.ext4 -L whatever /dev/sdX1
```

If you want to change the partition label, you can do so with this:

```
sudo e2label /dev/sdX1 newlabel
```

8. Check all is okay:

```
sudo lsblk -o NAME,FSTYPE,LABEL,UUID,MOUNTPOINT
```

9. (Optional) Mount the disk and use it


10. (If external drive) Unmount the disk from **your** chosen mount point:

eg.

```
sudo umount /media/backup
```

or ```udisksctl unmount -b /dev/sdX1``` if you are using udisks2.


11. (If external drive) **Power off the drive**. I have had issue with external harddrive enclosures that are still doing something to the drive even when it has been unmounted, and I have subsequently damaged the filesystem by turning the drive off manually.

```sudo apt-get install udisks2```, if you do not have it already.

```udisksctl power-off -b /dev/sdX```

Note that we are using the [device label](/hardware/storage/device_label.md) for the power-off command.

Your external device should now power off so that it is safe to unplug from the local machine and power down.


## What is a good resource for this?

https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux