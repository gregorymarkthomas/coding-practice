- Remember to ensure that 65535 UIDs and GIDs are noted in /etc/pve/lxc/xxx.conf, and that /etc/subuid and /etc/subgid reference 65536!
	- Why? Well, this ensures that ALL accounts are dealt with.
	- OpenSSH will come up with "setgroups: Invalid argument [preauth]" in /var/log/auth.log and status in systemctl
		- This is because group 65534 is used, for some reason, and if all guids are not accounted for between host and container, then the error will show.
		- https://unix.stackexchange.com/questions/201848/cant-connect-to-the-sshd-in-my-unprivileged-lxc-guest-what-to-do


---

I've been using Proxmox 5.4 for several months and recently upgraded to version 6 for home and work. Love the software. Each new version it gets better and better along with more ways to manage it via WebGUI vs command line. While nothing wrong working with command line but I find WebGUI easier to create clusters, CephFS and so on.

Now I've been playing around with LXC containers in hopes I can use it for Plex and shared local storage (XFS). Creating the container's storage mountpoint via WebGUI is pretty easy. However, I don't see an option to actually use the existing mountpoint on a local host storage without using command lines. Reason I am pointing this out is that WebGUI would have applied the correct permissions for the container to use the mountpoint and it's proper IDs for it. It's not a negative and this may get added in future upgrade.

So reading through the official Proxmox container guide I've use this to bind the mountpoint:​
​
Quote: "For example, to make the directory /mnt/bindmounts/shared accessible in the container with ID 100 under the path /shared, use a configuration line like mp0: /mnt/bindmounts/shared,mp=/shared in /etc/pve/lxc/100.conf. Alternatively, use pct set 100 -mp0 /mnt/bindmounts/shared,mp=/shared to achieve the same result."​
​
The command use pct set 100 -mp0 /mnt/bindmounts/shared,mp=/shared worked fine for me but not sure the proper way of setting the permissions for this shared folder. I don't think using chown / chgrp would be the proper way since it is managed by Proxmox.​
​
I am still getting write permission denied within the container. Any ideas?​
​
EDIT: For now I changed where I can share the folder and then do the pct set command. Basically created a shared folder in /home/root folder inside the container and then chmod -R o+w that shared folder on the host server. From then on I am able to write stuffs on the mounted folder inside the container.​
​
​

## Resources

- https://forum.proxmox.com/threads/writing-to-bind-mount-inside-of-an-unprivileged-container.38898/#post-214852
- https://pve.proxmox.com/wiki/Unprivileged_LXC_containers