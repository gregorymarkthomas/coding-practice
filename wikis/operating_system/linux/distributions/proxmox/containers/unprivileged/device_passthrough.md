# Device passthrough

## CD/DVD ROM drive

### Steps

#### 1. Note path and major/minor numbers of device on **host**

```
root@host:~# ls -la /dev/

...
brw-rw----  1 root cdrom    11,   0 Nov 10 14:20 sr0
...
```

Here we have `sr0` as the device name, and `11` and `0` as our major and minor numbers; make a note of these.


#### 2. Edit container config to passthrough and allow use of device

Swap `x` to whatever number the container you want to give access to uses:

```
root@host:~# nano /etc/pve/lxc/10x.conf
```

In `10x.conf`, add:

```
lxc.cgroup.devices.allow: c 11:* rwm
lxc.mount.entry: /dev/sr0 dev/sr0 none bind,optional,create=file
```

The former command gives the container privilege to access the device specified by its major and minor numbers, and grants **read-write-mount** privileges.
The latter command is for mounting the device _file representation_ into the container space. We are mounting the exact device here (`/dev/sr0` is a device _file_ in general, and hence the use of `create=file`).
Please note the fact that the directory we are mapping to **does not start with a /**; this is intentional.


#### 3. Restart the container

If the restart fails, check 'Syslog' under the host's controls within Proxmox's interface.


#### 4. Test device is visible within the container

```
root@container:~# udevadm info --query=env "/dev/sr0"

DEVPATH=/devices/pci0000:00/0000:00:14.1/ata1/host0/target0:0:1/0:0:1:0/block/sr0
DEVNAME=/dev/sr0
DEVTYPE=disk
MAJOR=11
MINOR=0
SUBSYSTEM=block
```

### Can I use `udev` to extract CD/DVD ROM data through the container?

Maybe; the above steps help towards that goal, but you may need to put more work into it.
See [udev](udev.md).



## USB device

Do the same as for CD/DVD ROM, but use `/dev/bus/usb/00x` for the device.

When it comes to adding the `lxc.mount.entry` command to `10x.conf`, it might be a good idea to map the whole USB device's **directory** (with all its siblings) to the container because it is more than likely that the filename will change. In which case, you could do this:

```
lxc.mount.entry: /dev/bus/usb/00x dev/bus/usb/00x none bind,optional,create=dir
```

Please note the use of `create=dir`, and the fact that the directory we are mapping to **does not start with a /**.