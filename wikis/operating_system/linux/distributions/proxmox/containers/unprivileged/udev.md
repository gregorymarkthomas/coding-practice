# udev in Proxmox container

## What's the matter?

Once you have [passed your device through to the container](device_passthrough.md), you may want to use udev to listen for information about a device on an event (e.g. listen for when a music CD is inserted into the DVD-ROM drive).


## What information are we missing on the container compared to the host?

Run `udevadm info --query=env "/dev/sr0"` on the **host** (we will use DVD-ROM `sr0` for this example):

```
root@host:~# udevadm info --query=env "/dev/sr0"
DEVPATH=/devices/pci0000:00/0000:00:14.1/ata1/host0/target0:0:1/0:0:1:0/block/sr0
DEVNAME=/dev/sr0
DEVTYPE=disk
MAJOR=11
MINOR=0
SUBSYSTEM=block
USEC_INITIALIZED=4866571
ID_CDROM=1
SYSTEMD_MOUNT_DEVICE_BOUND=1
ID_CDROM_CD=1
ID_CDROM_CD_R=1
ID_CDROM_CD_RW=1
ID_CDROM_DVD=1
ID_CDROM_DVD_R=1
ID_CDROM_DVD_RW=1
ID_CDROM_DVD_RAM=1
ID_CDROM_DVD_PLUS_R=1
ID_CDROM_DVD_PLUS_RW=1
ID_CDROM_DVD_PLUS_R_DL=1
ID_CDROM_MEDIA=1
ID_CDROM_MEDIA_CD=1
ID_CDROM_MEDIA_SESSION_COUNT=1
ID_CDROM_MEDIA_TRACK_COUNT=12
ID_CDROM_MEDIA_TRACK_COUNT_AUDIO=12
ID_ATA=1
ID_TYPE=cd
ID_BUS=ata
ID_MODEL=HL-DT-ST_DVDRAM_GH24NS90
ID_MODEL_ENC=HL-DT-ST\x20DVDRAM\x20GH24NS90\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
ID_REVISION=IN01
ID_SERIAL=(REDACTED)
ID_SERIAL_SHORT=(REDACTED)
ID_ATA_SATA=1
ID_ATA_SATA_SIGNAL_RATE_GEN1=1
ID_WWN=0x5001480000000000
ID_WWN_WITH_EXTENSION=0x5001480000000000
ID_PATH=pci-0000:00:14.1-ata-1.1
ID_PATH_TAG=pci-0000_00_14_1-ata-1_1
ID_PATH_ATA_COMPAT=pci-0000:00:14.1-ata-1
ID_FOR_SEAT=block-pci-0000_00_14_1-ata-1_1
DEVLINKS=/dev/disk/by-path/pci-0000:00:14.1-ata-1 /dev/cdrom /dev/disk/by-id/ata-HL-DT-ST_DVDRAM_(REDACTED)_(REDACTED) /dev/cdrw /dev/disk/by-path/pci-0000:00:14.1-ata-1.1 /dev/dvdrw /dev/disk/by-id/wwn-0x5001480000000000 /dev/dvd
TAGS=:systemd:uaccess:seat:
CURRENT_TAGS=:systemd:uaccess:seat:
```

Now run the same command on the **container**:

```
root@container:~# udevadm info --query=env "/dev/sr0"
DEVPATH=/devices/pci0000:00/0000:00:14.1/ata1/host0/target0:0:1/0:0:1:0/block/sr0
DEVNAME=/dev/sr0
DEVTYPE=disk
MAJOR=11
MINOR=0
SUBSYSTEM=block
```

As you can see, we are missing quite a lot of information, particularly `ID`s. In this example, I have created a udev rule to listen for _music_ CDs (see the [CD Rip project](\operating_system\linux\distributions\ubuntu\projects\cd_ripper.md), however, as you can see, I **need** the `ID CDROM MEDIA CD` ID:

```
root@container:~# nano /etc/udev/rules.d/99-cd-audio-processing.rules

...
SUBSYSTEM=="block", KERNEL=="sr0", ENV{ID_CDROM_MEDIA_CD}=="1", RUN+="/bin/systemctl --no-block start cdrip.service"
```


## How do we get around this?

According to [this Reddit post](https://www.reddit.com/r/homelab/comments/7bgtu9/a_simple_solution_for_getting_automated_ripping/), we can make some _allowances_; this will, however, reduce security.




## Links

- https://www.reddit.com/r/homelab/comments/7bgtu9/a_simple_solution_for_getting_automated_ripping/