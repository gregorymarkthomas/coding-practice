# Users and groups on Unprivileged Containers

## Setup

- Proxmox host
	- `/media/primary` mountpoint
		- read/write controlled by users, groups and permissions
- Ubuntu 20.04 _container_
	- **unprivileged**
	- same `/media/primary` mountpoint
		- **want data to be readable/writeable if permitted for users**
	- Users
		- user1
	- Groups
		- group1

## Issue

According to permissions and user/group setup **on the container**, user `user1` **is** permitted to read `/media/primary`. What should return is a directory list:

```bash
user1@localserver:~$ ls -la /media/primary
total 21
drwxr-x---  16 root                group1   4096 Jun 14 20:28  .
drwxr-xr-x   3 root                root     4096 Jul 23 20:07  ..
drwxrwx--T   9 root                group1   4096 Jul 14 15:07  Applications
drwxr-x---   6 nobody              group1   4096 Dec 29  2019  Backups
```

If you look at the permissions for the current directory (`.`), you will see that, provided `user1` is a member of group `group1`, then they will be able to _read_ the current directory (`drwxr-x---`). But, in reality, what is returned is:

```bash
user1@localserver:~$ ls -la /media/primary
total 0
```

If we _remove_ `user1` from group `group1` **on the container**, then we get the expected result:

```bash
user1@localserver:~$ ls -la /media/primary
ls: cannot open directory '/media/primary/': Permission denied
```


## Solution

Either:

### Make the container privileged

This is potentially riskier, security-wise.


### Recreate our container's users and group on the _host_

If we opt to recreate users and groups on the host, their UIDs and GIDs must be [_mapped_](mapping_users.md) correctly.

In this case (seeing as we are using an _unprivileged_ container):

- user `user1` with UID of `1000` on the **container** should be recreated on the host with UID of `101000`
	- plus any other users
- group `group1` with GID of `8000` on the **container** should be recreated on the host with GID of `108000`
- users should be **added to the group on the host too**, if added on the container
- users should have their _primary_ group set to their own named group (e.g. group `user1`), and have `group1` as a _secondary_ group
- **IMPORTANT**: the **host** node must be restarted for changes to be made!!