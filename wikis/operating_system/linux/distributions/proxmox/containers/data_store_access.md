# Data store access

## What do you mean by Data store?

The Data store is the collection of harddrives we have that contain all of our (backed-up) data.


## What do you want to be able to do?

I want to be able to pass access to the data store through to individual containers when using Proxmox as the host/[hypervisor](/terms/hypervisor.md).


## What do you use to _collate_ your data store harddrives?

[MergerFS](\operating_system\linux\distributions\ubuntu\software\mergerfs.md).


## What I have done with my Proxmox install

I have setup MergerFS on the Proxmox **host**. I have then used _bind mounts_ to individual containers/Virtual Machines via their config files to pass the MergerFS directory to them.


## Is this safe?

I think so? Any data IO is done on the _host_ via that MergerFS directory, so, surely it is left upto MergerFS to handle said traffic.


## What are they alternatives?

- Pass through individual **physical** disks to containers (see [here](https://pve.proxmox.com/wiki/Passthrough_Physical_Disk_to_Virtual_Machine_(VM))


## Links

- https://forum.proxmox.com/threads/storage-options-for-vms-cts-and-client-computers.38533/
- https://forums.servethehome.com/index.php?threads/ideal-plex-setup-with-proxmox-lxc-docker-kvm.13966/
- https://forums.servethehome.com/index.php?threads/proxmox-zfs-pools-under-mergerfs-i-wonder-if-anyone-done-that-before.13807/
- https://www.reddit.com/r/Proxmox/comments/l0klyi/best_practice_for_mergerfs_snapraid_on_proxmox/
- https://www.reddit.com/r/HomeServer/comments/crb076/proxmox_data_storage/
- https://unix.stackexchange.com/questions/388539/is-it-safe-to-change-files-that-are-covered-by-a-mount-and-then-bind-mounted-el
- https://unix.stackexchange.com/questions/198590/what-is-a-bind-mount