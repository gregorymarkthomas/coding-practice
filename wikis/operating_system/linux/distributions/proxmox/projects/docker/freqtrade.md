# FreqTrade on Docker

## Resources

- https://www.freqtrade.io/en/stable/docker_quickstart/


## Steps for basic

1. Install [docker](docker.md) if not already done so
2. Log into Docker VM via SSH from client machine so that you can copy and paste in commands
3. In user home directory, create directory `ft_userdata`
4. `cd ft_userdata`
5. Download the docker-compose file from the repository: `curl https://raw.githubusercontent.com/freqtrade/freqtrade/stable/docker-compose.yml -o docker-compose.yml`
6. Pull the freqtrade image: `docker-compose pull`
7. Create user directory structure: `docker-compose run --rm freqtrade create-userdir --userdir user_data`
8. Create configuration - Requires answering interactive questions: `docker-compose run --rm freqtrade new-config --config user_data/config.json`

## Steps from Alex

5. Download files from AlgoTrading
6. `scp <file/-r directory> <user>@<freqtrade-machine-hostname>`
7. `sed -i -e 's/\r$//' scriptname.sh` to convert file endings (?) of Windows-made scripts
	- https://stackoverflow.com/questions/14219092/bash-script-and-bin-bashm-bad-interpreter-no-such-file-or-directory
8. Add `sudo docker-compose run --rm` to any `freqtrade` calls in scripts
	- We use docker as sudo whereas Alex is using a Python implementation