# Install Docker on Proxmox

1. Create a new **VIRTUAL MACHINE** (not container) with Ubuntu Server (20.04 being latest at time of writing)
2. Give VM enough RAM to fix kernel panic on boot
2. Run VM
3. Go through install of Ubuntu Server (20.04 at time of writing)
	- I opted for no LVM to KISS
	- Defaults of all other things
 	- **Tick "docker" when asked about snaps**
 	- Remember the sudo user you create
4. Wait til complete..
5. Log in as the sudo user you created
6. `sudo nano /etc/ssh/sshd_config`, remove root login, keep password auth ON
7. `sudo systemctl restart ssh`
8. On local client machine, run `ssh <sudo-user>@<docker-vm-hostname>`
9. On login, `mkdir .ssh` and `nano authorized_keys` and paste in local client machine's public key, and save.
10. `sudo nano /etc/ssh/sshd_config`, turn password auth OFF
11. `sudo systemctl restart ssh` will probably kick you out of the session
12. Repeat step 8 and hopefully it will ask for a password to private key (if password is set)
13. Just check that docker via snap is actually installed. If not:
	a. Try `sudo snap install docker`
	a. `sudo apt update` and `sudo apt upgrade` to update `snap` if error about unsupported feature shows
	c. Once `apt` completes upgrade, try step a. again