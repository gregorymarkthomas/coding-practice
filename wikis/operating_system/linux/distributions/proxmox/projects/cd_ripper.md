# Install CDRipper on a container

## 1. Create Ubuntu LTS container via host

Stick with LTS; at time of writing the latest LTS is 20.04.


## 2. Add extras to container config file on host

```
nano /etc/pve/lxc/105.conf

...
mp0: /media/primary/Music/sort,mp=/media/primary/Music/sort
...
lxc.cgroup.devices.allow: b 11:* rwm
lxc.mount.entry: /var/lib/lxc/105/devices/sr0 dev/sr0 none bind,optional,create=file
lxc.idmap: u 0 100000 65535
lxc.idmap: g 0 100000 65535
```

Here we create a mountpoint for the directory in our storage that will hold newly ripped music (`mp0:...`). 
We also allow our CD/DVD drive (sr0) to be used by this container by first providing permission to use the block device with the specific values (`lxc.cgroup.devices.allow: b 11:* rwm`), and then create a mountpoint in the container's `/dev/` directory for `sr0` (`lxc.mount.entry: /var/lib/lxc/105/devices/sr0 dev/sr0 none bind,optional,create=file`).
Finally, we map users 0-65535 on the **container** to 100000-1065535 on the host; if, say, group `108000` owns directories on the **host**, then the container could create that same group with GID of `8000` and it would have access to the storage on the host.


## 3. Manually create the `sr0` file in the container files

Referencing the directory mentioned next to `lxc.mount.entry:` in `105.conf` (step 2), we manually create a directory and file under the container's files stored on the host:

```
mkdir /var/lib/lxc/105/devices/
mknod -m 777 /var/lib/lxc/105/devices/sr0 b 11 0
```


## 4. Add `udev` rule on host pointing to new container

Create a new rule on the **host** (e.g. `/etc/udev/rules.d/99-z-autorip.rules`)

```
ACTION=="change", KERNEL=="sr0", ENV{ID_CDROM_MEDIA_CD}!="", SUBSYSTEM=="block", TAG+="systemd", ENV{SYSTEMD_WANTS}="105-autorip.service"
```

The above assumes that

- the container's id is `105` and the service is therefore named as such
- music CDs will trigger the event

### 4a. Re-trigger udev with `udevadm trigger`

### 4b. Create HOST service to tell CONTAINER to run autoripper service

Create a new file at `nano /etc/systemd/system/102-autorip.service`, and fill it with the following:

```
[Unit]
Description=Autorun script to tell container 105 to start CD autoripper service (cdrip.service).

[Service]
Type=oneshot
ExecStart=pct exec 105 -- /bin/systemctl --no-block start cdrip.service
StandardOutput=journal
```


## 5. Container: `apt update` and `apt upgrade` the system

Update the new install.


## 5. Restart the container

This will enable the config file changes and `udev` changes made on the host, as well as cement the `apt upgrade`.


## 6. `apt install abcde`

I do have a backup of this, but we want to install via `apt` as this also installs most of the necessary libraries.


## 7. `apt install flac`

A requirement for our needs.


## 8. Create user `ripper`:

```
useradd --uid 2003 ripper
```

I used `--uid 2003` so that the host can have the same user with a UID of `102003`, but, I don't think it is entirely necessary.


## 9. Create group `nas` with GID of `8000`

```
groupadd --gid 8000 nas
```

`--gid 8000` is necessary as this will allow our `ripper` user to be a part of the `nas` group on the **host**; using the UID/GID mapping in our configuration file on the host allows us to create a group on the container that can map to a group on the host. In this case, group `8000` will map to `108000` on the host, and this is the GID that owns our storage files.


## 10. Add `ripper` to group `ripper` and `nas`

```
usermod -aG ripper ripper
usermod -aG ripper nas
``` 

## 11. Enable Firewall and open port 22 from backup source

## 12. Pull backup files in from backup source

Files include:

```
/etc/
	abcde.conf
	systemd/system/
		cdrip.service
		setup_sr0.service
/root/
	setup_sr0.sh
/usr/local/bin/
	abcde
	abcde-musicbrainz-tool
	cddb-tool
	cdrip.sh
	cdrip_logged.txt
/var/log/
	cdrip.log
```

**Ignore** `abcde` in `/usr/local/bin`, though (and possibly `abcde-musicbrainz-tool` and `cddb-tool`), as we will have the latest version from the previous step. Do, however, install the old `abcde.conf` via `/etc/` as this is already set. This config file should not change too much in newer versions of abcde, so should be safe to use the old one.

- [`abcde.conf`](abcde.conf)
- [`systemd/system/cdrip.service`](cdrip.service)
- [`systemd/system/setup_sr0.service`](setup_sr0.service)
- [`/root/setup_sr0.sh`](setup_sr0.sh)
- [`/usr/local/bin/cdrip.sh`](cdrip.sh)


## 13. Set `ripper` as owner on most new files

Files to set `chown ripper:ripper` on:

```
/etc/
	abcde.conf
/usr/local/bin/
	abcde
	abcde-musicbrainz-tool
	cddb-tool
	cdrip.sh
	cdrip_logged.txt
/var/log/
	cdrip.log
```

## 14. Ensure `RWX` permissions are set on `.sh` files

Run `chmod 0700` on these files:

```
/root/
	setup_sr0.sh
/usr/local/bin/
	abcde
	abcde-musicbrainz-tool
	cddb-tool
	cdrip.sh
```

## 15. Enable `setup_sr0.service`

`systemctl enable setup_sr0.service`

This will create `/dev/cdrom` from `/dev/sr0`. For other media types, we should probably add `dvd` etc. to `setup_sr0.service` too.

**This relies on step #2.**


## 16. Try it out!



---

Old way to get `sr0` on container was to attempt to do it from the host via a script called in 10x.conf:

```
...
lxc.autodev: 1
lxc.hook.autodev: /var/lib/lxc/105/mount_hook.sh
```


```
#!/bin/sh

#cd ${LXC_ROOTFS_MOUNT}/dev
#mknod sr0 b 11 0
#chmod 0777 sr0
#ln -s sr0 cdrom

mknod -m 777 ${LXC_ROOTFS_MOUNT}/dev/sr0 b 11 0
ln -s /dev/sr0 /dev/cdrom
```

These days, though, we use a script on the container (`setup_sr0.sh`) that is run on startup, and we manually call `mknod -m 777 /var/lib/lxc/105/devices/sr0 b 11 0` on the host instead of on the container.