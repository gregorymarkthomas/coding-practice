#!/bin/bash

log_file="/var/log/cdrip.log"
already_ripped_list="/usr/local/bin/cdrip_logged.txt"

if [[ `id -nu` != "ripper" ]];then
   echo "Not user 'ripper' in group 'nas'; ensure this exists! Exiting..." >> $log_file
   exit 1
fi

echo "---------------------------------------------" >> $log_file
echo "$(date)" >> $log_file
PATH=/usr/lib64/ccache:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
  	if test -f "$already_ripped_list"; then
		echo "Reading CD ID..." >> $log_file
		disc_id="$(cd-discid | cut -d' ' -f1)"
		echo "CD ID is $disc_id." >> $log_file

		is_already_ripped=false
		while read LINE
		do 
			if [ "$LINE" == "$disc_id" ]; then
				is_already_ripped=true
				echo "CD $disc_id has already been ripped - ejecting..." >> $log_file
				break
			fi
		done < $already_ripped_list

		if [[ "$is_already_ripped" == false ]]; then
			# Call abcde. MAXPROCS option now set in /etc/abcde.conf instead of using -j 2 here
	  		/usr/local/bin/abcde 2>&1 >> $log_file

			# $? is the result of the last executed command.
  			rc=$?
			if [[ $rc == 0 ]] ; then
  				# Write disc_id to our already-ripped list so that we don't keep ripping the same CD
	                        echo "$disc_id" >> $already_ripped_list
			else
                                echo "CD $disc_id was NOT ripped to storage, perhaps due to premature stop (abcde returned $rc)" >> $log_file
			fi

			eject
			exit $rc
		else
			echo "Stopping script because CD has already been ripped. Goodbye!" >> $log_file
			eject
		fi
	else
		echo "File 'already_ripped_list' does not exist - please create in same dir as 'cdrip.sh'." >> $log_file
		eject
	fi
