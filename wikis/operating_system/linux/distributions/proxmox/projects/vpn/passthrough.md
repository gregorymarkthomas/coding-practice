# VPN `tun` passthrough

UPDATE 28/09/23: the below did not seem to work - reverting.

---


We currently do not utilise `tun` but it would be nice to.

---

From https://forum.proxmox.com/threads/mknod-in-an-unprivileged-lxc-container.94649/:

> I'm sure there are threads on this forum that explain this for running VPN clients in containers, but I could not find the right one.
I use something like this on the Proxmox host to create a tun dev node with permissions for the root user inside unprivileged containers: `/usr/sbin/modprobe tun && /usr/bin/mknod /dev/net/tun-lxc c 10 200 && /usr/bin/chown 100000:100000 /dev/net/tun-lxc` (at every boot because it is not persistent). And then use a bind mount in the container configuration: `lxc.mount.entry: /dev/net/tun-lxc dev/net/tun none bind,create=file`.


We do something similar for iGPU for video...