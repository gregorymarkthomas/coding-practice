# Jellyfin (with Nvidia graphics card hardware acceleration)

## Host

### 1. Update/Upgrade

```
apt update
apt upgrade
```

### 2. Install `pve-headers`

```
apt-cache search pve-header
```
```
pve-headers-5.10.6-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.0-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.12-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.17-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.21-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.22-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.22-2-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.22-3-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.22-4-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.22-5-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.22-6-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.22-7-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11.7-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.11 - Latest Proxmox VE Kernel Headers
pve-headers-5.13.14-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.13.18-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.13.19-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.13.19-2-pve - The Proxmox PVE Kernel Headers
pve-headers-5.13.19-3-pve - The Proxmox PVE Kernel Headers
pve-headers-5.13.19-4-pve - The Proxmox PVE Kernel Headers
pve-headers-5.13.19-5-pve - The Proxmox PVE Kernel Headers
pve-headers-5.13 - Latest Proxmox VE Kernel Headers
pve-headers-5.15.12-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.15.17-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.15.19-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.15.19-2-pve - The Proxmox PVE Kernel Headers
pve-headers-5.15.5-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.15.7-1-pve - The Proxmox PVE Kernel Headers
pve-headers-5.15 - Latest Proxmox VE Kernel Headers
pve-headers - Default Proxmox VE Kernel Headers
```
```
apt install pve-headers
```

### 3. Install `gcc` and `make`

```
apt install gcc
apt install make
```


### 4. Download latest NVIDIA drivers for your graphics card to the **host**

```
wget https://us.download.nvidia.com/XFree86/Linux-x86_64/510.54/NVIDIA-Linux-x86_64-510.54.run
```


### 5. Make the driver .run file executable and run

```
chmod 700 NVIDIA-Linux-x86_64-510.54.run
```


### 6. _Attempt_ to install Nvidia drivers

```
./NVIDIA-Linux-x86_64-510.54.run 
```


### 7. Disable 'Nouveau' drivers and restart the host

This should occur if this is the first time attempting the installation of the Nvidia drivers.


### 8. Attempt install of Nvidia drivers _again_

```
./NVIDIA-Linux-x86_64-510.54.run 
```

### 9. Wonder about this error message

```
 WARNING: nvidia-installer was forced to guess the X library path '/usr/lib64' and X module
           path '/usr/lib64/xorg/modules'; these paths were not queryable from the system.  If
           X fails to find the NVIDIA X driver module, please install the `pkg-config` utility  
           and the X.Org SDK/development package for your distribution and reinstall the
           driver.         
```


### 10. Ensure Nvidia drivers are loaded on **host** boot

```
nano /etc/modules-load.d/modules.conf

# Add to modules.conf: Nvidia modules
nvidia
nvidia_uvm
```


### 11. Update `initramfs`

```
update-initramfs -u
```


### 12. Create `udev` rules

```
nano /etc/udev/rules.d/70-nvidia.rules
```

```
# /etc/udev/rules.d/70-nvidia.rules
# Create /nvidia0, /dev/nvidia1 … and /nvidiactl when nvidia module is loaded
KERNEL=="nvidia", RUN+="/bin/bash -c '/usr/bin/nvidia-smi -L && /bin/chmod 666 /dev/nvidia*'"
# Create the CUDA node when nvidia_uvm CUDA module is loaded
KERNEL=="nvidia_uvm", RUN+="/bin/bash -c '/usr/bin/nvidia-modprobe -c0 -u && /bin/chmod 0666 /dev/nvidia-uvm*'"
```


### 13. Reboot the host


### 14. Test that the Nvidia drivers have been successfully installed

```
nvidia-smi
...
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 510.54       Driver Version: 510.54       CUDA Version: 11.6     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Quadro P400         Off  | 00000000:01:00.0 Off |                  N/A |
| 33%   43C    P0    N/A /  N/A |      0MiB /  2048MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
```

```
ls /dev/nvidia* -l
...
crw-rw-rw- 1 root root 195,   0 Mar 11 02:07 /dev/nvidia0
crw-rw-rw- 1 root root 195, 255 Mar 11 02:07 /dev/nvidiactl
crw-rw-rw- 1 root root 237,   0 Mar 11 02:07 /dev/nvidia-uvm
crw-rw-rw- 1 root root 237,   1 Mar 11 02:07 /dev/nvidia-uvm-tools

/dev/nvidia-caps:
total 0
cr-------- 1 root root 240, 1 Mar 11 02:07 nvidia-cap1
cr--r--r-- 1 root root 240, 2 Mar 11 02:07 nvidia-cap2
```


### 15. Add (?) `nvidia-uvm` module to Linux kernel?
```
modprobe nvidia-uvm

```


## Container

### 1. Set xxx.conf file on **host** for container

Using the response from the command in step 14, add to the config file for the _container_:

```
nano /etc/pve/lxc/102.conf
```

```
# Add to config
mp0: /media/primary/Videos,mp=/media/primary/Videos
lxc.cgroup2.devices.allow: c 195:* rwm
lxc.cgroup2.devices.allow: c 226:* rwm
lxc.cgroup2.devices.allow: c 240:* rwm
lxc.cgroup2.devices.allow: c 242:* rwm
lxc.mount.entry: /dev/nvidia0 dev/nvidia0 none bind,optional,create=file
lxc.mount.entry: /dev/nvidiactl dev/nvidiactl none bind,optional,create=file
lxc.mount.entry: /dev/nvidia-uvm dev/nvidia-uvm none bind,optional,create=file
lxc.mount.entry: /dev/nvidia-uvm-tools dev/nvidia-uvm-tools none bind,optional,create=file
lxc.mount.entry: /dev/nvidia-modeset dev/nvidia-modeset none bind,optional,create=file
lxc.mount.entry: /dev/nvidia-caps/nvidia-cap1 dev/nvidia-caps/nvidia-cap1 none bind,optional,cre>
lxc.mount.entry: /dev/nvidia-caps/nvidia-cap2 dev/nvidia-caps/nvidia-cap2 none bind,optional,cre>
lxc.mount.entry: /dev/dri/card0 dev/dri/card0 none bind,optional,create=file
lxc.mount.entry: /dev/dri/renderD128 dev/dri/renderD128 none bind,optional,create=file
```


### 2. Download the SAME NVIDIA drivers for your graphics card to the **container** as done on host

```
wget https://us.download.nvidia.com/XFree86/Linux-x86_64/510.54/NVIDIA-Linux-x86_64-510.54.run
```


### 3. Make the driver .run file executable and run

```
chmod 700 NVIDIA-Linux-x86_64-510.54.run
```


### 4. Install Nvidia drivers **without kernel module**

This needs to be done **without the kernel modules**:

```
./NVIDIA-Linux-x86_64-510.54.run --no-kernel-module
```


### 5. Wonder about this message

```
 WARNING: Unable to determine the path to install the libglvnd EGL vendor library  
           config files. Check that you have pkg-config and the libglvnd            
           development libraries installed, or specify a path with
           --glvnd-egl-config-path.
```

### 6. Enjoy! Now install Jellyfin...



## Resources

- https://theorangeone.net/posts/lxc-nvidia-gpu-passthrough/
- Mostly https://gist.github.com/dryqin/83ee875b1dcafaa3e9f732bd85a3cfc2