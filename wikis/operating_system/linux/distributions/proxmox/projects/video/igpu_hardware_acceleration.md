# iGPU/Intel QuickSync Hardware acceleration

## 1. Install Intel drivers on *host*

To have both encoding and decoding, you will need to add `non-free` next to each repository in `nano /etc/apt/sources.list`:

```
apt update
apt-get install intel-media-va-driver-non-free
apt-get install vainfo intel-gpu-tools
shutdown -r now
```

### 1a. Check all is okay 

After the reboot, try checking with the tools below:

```
vainfo
intel_gpu_top
```


```
root@chillinode:~# vainfo

error: can't connect to X server!
libva info: VA-API version 1.10.0
libva info: Trying to open /usr/lib/x86_64-linux-gnu/dri/iHD_drv_video.so
libva info: Found init function __vaDriverInit_1_10
libva info: va_openDriver() returns 0
vainfo: VA-API version: 1.10 (libva 2.10.0)
vainfo: Driver version: Intel iHD driver for Intel(R) Gen Graphics - 21.1.1 ()
vainfo: Supported profile and entrypoints
      VAProfileNone                   : VAEntrypointVideoProc
      VAProfileNone                   : VAEntrypointStats
      VAProfileMPEG2Simple            : VAEntrypointVLD
      VAProfileMPEG2Main              : VAEntrypointVLD
      VAProfileH264Main               : VAEntrypointVLD
      VAProfileH264Main               : VAEntrypointEncSlice
      VAProfileH264Main               : VAEntrypointFEI
      VAProfileH264Main               : VAEntrypointEncSliceLP
      VAProfileH264High               : VAEntrypointVLD
      VAProfileH264High               : VAEntrypointEncSlice
      VAProfileH264High               : VAEntrypointFEI
      VAProfileH264High               : VAEntrypointEncSliceLP
      VAProfileVC1Simple              : VAEntrypointVLD
      VAProfileVC1Main                : VAEntrypointVLD
      VAProfileVC1Advanced            : VAEntrypointVLD
      VAProfileJPEGBaseline           : VAEntrypointVLD
      VAProfileJPEGBaseline           : VAEntrypointEncPicture
      VAProfileH264ConstrainedBaseline: VAEntrypointVLD
      VAProfileH264ConstrainedBaseline: VAEntrypointEncSlice
      VAProfileH264ConstrainedBaseline: VAEntrypointFEI
      VAProfileH264ConstrainedBaseline: VAEntrypointEncSliceLP
      VAProfileVP8Version0_3          : VAEntrypointVLD
      VAProfileHEVCMain               : VAEntrypointVLD
      VAProfileHEVCMain               : VAEntrypointEncSlice
      VAProfileHEVCMain               : VAEntrypointFEI
      VAProfileHEVCMain10             : VAEntrypointVLD
      VAProfileHEVCMain10             : VAEntrypointEncSlice
      VAProfileVP9Profile0            : VAEntrypointVLD
      VAProfileVP9Profile2            : VAEntrypointVLD
```

## 2. Passthrough iGPU to container 

Append the below to *<vid>.conf* (`nano /etc/pve/lxc/107.conf`) - this allows /dev/dri/card0 and /dev/dri/renderD128 to be passed through to the container:

```
lxc.cgroup2.devices.allow: c 226:0 rwm
lxc.cgroup2.devices.allow: c 226:128 rwm
lxc.mount.entry: /var/lib/lxc/<vid>/devices/dri/card0 dev/dri/card0 none bind,optional,create=file
lxc.mount.entry: /var/lib/lxc/<vid>/devices/dri/renderD128 dev/dri/renderD128 none bind,optional,create=file
lxc.idmap: u 0 100000 65535
lxc.idmap: g 0 100000 65535
```

This assumes that:

- the IDs (226:0, 226:128) are what are shown when doing `ls -la /dev/dri/`


## 2. Create our nods

```
mkdir -p /var/lib/lxc/<vid>/devices/dri/
mknod -m 666 /var/lib/lxc/<vid>/devices/dri/card0 c 226 0
mknod -m 666 /var/lib/lxc/<vid>/devices/dri/renderD128 c 226 128
chown 100000:100000 /var/lib/lxc/<vid>/devices/dri/card0
chown 100000:100000 /var/lib/lxc/<vid>/devices/dri/renderD128
```


## 3. Install Intel drivers on *container*

```
apt update
apt-get install intel-media-va-driver-non-free
apt-get install vainfo intel-gpu-tools
shutdown -r now
```

### 3a. Check using the same tools mentioned above

There should be similar results for `vainfo` - if not, check `ls -la /dev/dri/` and ensure there is `root` for user and group, and that the permissions (666 I believe) are okay.


## 4. Set video server to use transcoding

For Jellyfin I:

- use Intel QuickSync (QSV)
- Enable AV1
- Disable "Prefer OS native DXVA or VA-API hardware decoders"
- Disable the "Intel Low-Power" options as I am unsure this is setup
	- Not actually tried it due to warning that Jellyfin shows
	- https://jellyfin.org/docs/general/administration/hardware-acceleration/intel/#configure-and-verify-lp-mode-on-linux

## 5. Start transcoding a video

Run `intel_gpu_top` on the **host** to look for activity.


### Notes

- https://geekistheway.com/2022/12/23/setting-up-intel-gpu-passthrough-on-proxmox-lxc-containers/
- https://jellyfin.org/docs/general/administration/hardware-acceleration/intel/#configure-and-verify-lp-mode-on-linux
- https://github.com/TheHellSite/proxmox_tutorials/tree/main/lxc_gpu_passthroug
- https://github.com/TheHellSite/archlinux_lxc/tree/main/jellyfin#jellyfin-lxc-gpu-passthrough-run-as-root-user