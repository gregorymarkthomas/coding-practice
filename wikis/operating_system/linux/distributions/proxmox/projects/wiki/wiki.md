# Gollum Wiki

## How to

### 1. Create new Ubuntu 23.10 container under Proxmox host

### 2. Log-in as `root` and update container

```
apt update
apt upgrade
```

### 3. Install gollum and dependencies

```
apt update
apt install -y libgit2-dev cmake pkg-config ruby ruby-dev make zlib1g-dev libicu-dev build-essential git asciidoc
```

Gollum Wiki is written in Ruby:

```
gem install gollum \
  org-ruby \
  omnigollum \
  github-markup \
  omniauth-github
```

### 4. Setup Gollum Git repo

```
adduser --shell /bin/bash --gecos 'Gollum application' gollum
```

...and enter a password.

Next, add user to sudo group:

```
usermod -aG sudo gollum
```

Log-in as the new user and set the Git credentials:

```
sudo su - gollum
git config --global user.name "Admin Doe"
git config --global user.email "johndoe@example.com"
```

Create the wiki directory:

```
mkdir wiki && cd wiki
```

Either initialise a new git repo:

```
git init .
```

...or, `git clone` in an existing project:

```
git clone <url>
```

### 5. Logout of `gollum` user

```
logout
```

### 6. Configure gollum as a systemd service

```
mkdir /etc/gollum/
```

Create a configuration template for gollum:

```
cat<<EOF | sudo tee /etc/gollum/config.rb
=begin
This file can be used to (e.g.):
- alter certain inner parts of Gollum,
- extend it with your stuff.

It is especially useful for customizing supported formats/markups. For more information and examples:
- https://github.com/gollum/gollum#config-file

=end

# enter your Ruby code here ...
EOF
```

Create a gollum systemd unit file:

```
cat<<EOF | sudo tee /etc/systemd/system/gollum.service
[Unit]
Description=Gollum wiki server
After=network.target
After=syslog.target

[Service]
Type=simple
User=gollum
Group=gollum
WorkingDirectory=/home/gollum/wiki/
ExecStart=/usr/local/bin/gollum --config "/etc/gollum/config.rb"
Restart=on-abort

[Install]
WantedBy=multi-user.target
EOF
```

Reload `systemctl`, restart, and enable:

```
systemctl daemon-reload
systemctl restart gollum.service
systemctl enable gollum.service
systemctl status gollum.service
```

### 7. Ensure Gollum is running its webserver

```
$ ss -tunelp | grep 4567
```

The results should look something like this:

```
tcp   LISTEN 0      4096         0.0.0.0:4567      0.0.0.0:*    users:(("gollum",pid=17240,fd=5)) uid:1000 ino:370108611 sk:1010 cgroup:/system.slice/gollum.service <->
```

### 8. Ensure Proxmox container firewall allows connection

Ensure firewall is enabled under Options tab of Firewall tab.

Accept TCP from source IPs that you want, to destination port of 4567.


### 9. Try it out!

Go to `<ip>:4567/gollum/overview` in your browser.


## Links

- Thanks to https://computingforgeeks.com/how-to-install-gollum-wiki-on-ubuntu/ for initial guide.
- https://github.com/libgit2/rugged#prerequisites: used to add extra dependencies to initial `apt install` call, as the above guide crashes during gollum installation ("No package 'heimdal-gssapi' found")