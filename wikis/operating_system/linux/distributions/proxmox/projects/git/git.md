# Git server as container

1. Install _unprivileged_ container with latest Ubuntu server
2. `nano /etc/pve/lxc/<container-id>.conf`
3. Add `mp0: /media/primary/src,mp=/media/primary/src` (or similar) if opening mount point in storage to container
4. Add `lxc.idmap: u 0 100000 65535` and `lxc.idmap: g 0 100000 65535`
	a. This (the `65535` in particular) is important as `setgroup` error in status of ssh process will appear; "debug3: privsep user:group 101:6553**4** [preauth]" and "setgroups: Invalid argument [preauth]"
	b. See [git_setgroup_issue.html](git_setgroup_issue.html) (or https://unix.stackexchange.com/questions/201848/cant-connect-to-the-sshd-in-my-unprivileged-lxc-guest-what-to-do)
5. Start new container
6. `apt update && apt upgrade`
7. `nano /etc/ssh/sshd_config` and remove password login and root access
8. `systemctl restart ssh`
9. `sudo useradd -r -m -U -d <projects-directory> -s /bin/bash --uid <uid/gid> git`
	a. `<uid/gid>` is the UID/GID that owns your git project files on whatever storage it resides on
	b. `-d <projects-directory>` sets home directory to `<projects-directory>`
10. `su git`
11. `mkdir <projects-directory>/.ssh`
12. `nano <projects-directory>/.ssh/authorized_keys`
13. Paste local client public key into `authorized_keys`
14. `apt install git`
15. `cd` to `<projects-directory>`
16. Create test git project with `git init --bare test.git`
17. Enable firewall
18. Open port 22 TCP as destination port from 192.168.x.x/x sources
19. On local client machine, try `git clone git@<hostname>:test.git` into your source folder
20. Success?