# Memory Management in Linux

## What level is memory managed in Linux?

Memory is managed at the [_application_](/terms/application.md) level.