# Shell

## What is it?

A Shell is a computer program which **exposes** the [operating system's](operating_system.md) _services_ to a human user or other program.


## Can you say that in any other way?

A 'shell' can be any program that constitutes as the [user-interface](/interfaces/user_interface.md).


## Sounds like an [interface](/terms/interface.md); is it?

Kind of; **a Shell _uses_ an [interface](/terms/interface.md)** but is not an interface itself.


## How many _types_ of Shell are there?

Most [operating system](operating_system.md) Shells fall into one of **two** categories:

- _Command Line_ shells, which provide a [Command Line Interface (CLI)](/interfaces/command_line_interface-cli.md)
- _Graphical_ shells, which provide a [Graphical User Interface (GUI)](/interfaces/graphical_user_interface-gui.md)


## Why "Shell"?

A Shell is named as such because it is the outermost layer around the operating system.



## So the Shell is **not** a direct interface to the [kernel](kernel.md)?

No; a Shell is just an [application](/terms/application.md) that uses the kernel [API](/software/interfaces/application_programming_interface-api.md) in the same way as other applications.


## Is the Shell considered _special_?

Yes, as it is the application that manages user-to-system interaction by:

- prompting for input
- interpreting their input
- handling an output from the underlying [operating system](operating_system.md).


## Seeing as a Shell is just an application, can it be easily replaced?

Yes.



## What are some examples of Shells?

- 