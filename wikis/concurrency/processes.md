---
title:  Processes
author: Gregory Thomas
date:   2020-03-13
---

# Processes

## What are they?

Processes:

- Have their own separate memory space so eliminates synchronization issues
- More difficult to share data between them
- Killable
- Larger memory footprint
- Have independent I/O scheduling
- PYTHON: can run them concurrently