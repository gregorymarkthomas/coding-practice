---
title:  Thread safety
author: Gregory Thomas
date:   2020-08-14
---

# Thread safety

## What does this mean?

If multiple [threads](threads.md) access the same object, we want to ensure that object's value is _consistent_ between each thread.


## So, "thread-safety" refers to whether an object's value is consistent when accessed by multiple threads?

Yes.


## What can cause thread-safety issues?

### Mutable objects

[Mutable](/terms/mutable.md) objects that have their values updated.

#### How do we achieve consistency between threads for mutable objects?

Ideally, we should try to avoid sharing a [mutable](/terms/mutable.md) object between threads. But, if we have to, then we must use a [critical section](critical_section.md) of code together with a [lock](lock.md) so that if one thread has to update the object's value, other threads have to **wait** before reading or writing to the object.


### Static objects and functions

A [static](/modifiers/static.md) object only exists **once** in memory.

Say multiple [threads](/concurrency/threads.md) access that static object, and one thread _updates_ the object's state; will all other threads see that updated value?

#### No?

Correct.

#### Do you have an example of this?

##### Thread-safe

```
fun String.countSpaces(): Int {
    return this.count { c -> c == ' ' }
}
```

This is _thread-safe_; [String](/structures/software_entities/variables/data_types/arrays_and_strings/strings.md) is [immutable](/terms/immutable.md), so the developer is unable to change ```this``` within the function.


##### Thread-UNsafe

```
data class MutablePerson(val name: String, var speech: String)

fun MutablePerson.count(nextNumber: Int) {
    this.speech = "${this.speech} ${nextNumber}"
}
``` 

This is **not** thread-safe; the ```speech``` property has its state changed. If ```count()``` is called on a different thread, the return value may well be different; not good!