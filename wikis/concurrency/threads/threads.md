---
title:  Threads
author: Gregory Thomas
date:   2020-03-13
---

# Threads

## What are they?

A thread is a _sequence_ of programmed instructions.


## What is a thread used for?

Threads are used to achieve [concurrent](/concurrency/concurrency.md) programming so that an application can defer certain tasks to a particular thread. 


## Why would we do that?

The idea is to utilise the computer's [processor](/hardware/processor.md) more efficiently and therefore bring performance increases to the application.


## So, there could be multiple threads being run in one application?

Yes; it helps that threads have a low memory footprint.


## Do these threads run in parrellel?

For most languages, yes, but [Python](/language_specific/python/python.md) threads are not.


## How are threads managed?

Threads are managed by the language's threading implementation; this will probably utilise the operating system's [scheduler](/operating_system/scheduler.md).


## Do threads have their own [stack](/memory/stack.md)?

Yes, threads have their own [stack](/memory/stack.md).


## So, any execution a thread does adds to its **own** stack?

Yes.


## But, threads _share_ the _same_ memory space, correct?

Yes; threads utilise the _shared_ memory space that is the [heap](/memory/heap.md).


## When a [variable](/structures/software_entities/variables/variable.md) is created during a thread's execution, where is the _value_ stored?

Those thread-created [variables](/structures/software_entities/variables/variable.md), [static](/structures/software_entities/variables/class_static_variable.md) or not, are **not** stored in the thread's stack; they are stored in the [heap](/memory/heap.md) (i.e. **shared memory**).


## But what about the variable's [reference](/memory/reference.md)?

Thread-created variables' [references](/memory/reference.md) **_are_ stored in the thread's stack**. **Unless**, that is, the variable in question is [static](/structures/software_entities/variables/class_static_variable.md).


## What about [function](/structures/software_entities/functions/functions.md) calls?

[Static](/modifiers/static.md) and non-static [function](/structures/software_entities/functions/functions.md) calls are added to the **thread's** stack.





