---
title:  Dynamically typed
author: Gregory Thomas
date:   2020-03-20
---

# Dyamically typed

## What entity can be "dynamically typed"?

A *programming language* can be "dynamically typed".


## What is a "dynamically typed" programming language, then?

"Dynamically typed" languages do **not** require the programmer to *specify the [data type](/memory/variables/data_types/data_types.md) of the [variables](/memory/variables/variables.md) they create*.

A Python example:

```
i = 3
```

### Why is it NOT needed?

It is all for [type checking](/terms/types/type_checking.md).

Dynamically typed languages do their *dynamic* [type checking](/terms/types/type_checking.md) at *run* time, meaning errors are raised *during* runtime. 
However, dynamic type checking still requires "[data type](/memory/variables/data_types/data_types.md)" information to be defined with [variables](/memory/variables/variables.md); it's just that the Dynamically Typed language will **automatically define the data type** of the [variable](/memory/variables/variables.md) from the *value* it was given.  

This differs from [statically typed languages](statically_typed.md) which require the developer to *manually* set the data type to the variable.


## What languages are considered "statically typed"?

- [Python](/language_specific/python/python.md)
- [Javascript](/language_specific/javascript/javascript.md)