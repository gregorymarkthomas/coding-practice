---
title:  Type checking
author: Gregory Thomas
date:   2020-03-20
---

# Type checking

## What is it?

Type checking is the process of *verifying* and *enforcing* the constraints of [data types](/memory/variables/data_types/data_types.md) in an effort to reduce errors.

Type checking can be done at *compile time* or at *run time*:

- [Statically typed languages](statically_typed_languages.md) are those that do their type checking at compile time; this is called **static type checking**.
- [Dynamically typed languages](dynamically_typed_languages.md) are those that do their type checking at run time; this is called **dynamic type checking**.


## What does the type checker check?

The type checker looks at both the [operators](/mathematics/operators.md) and the [data types](/memory/variables/data_types/data_types.md) of the [variables](/memory/variables/variables.md) *applied* to the operators, and checks whether the operation has followed the *rules*.


## What kind of errors are we talking about?

Say we have the expression ```8 % 3.5```; the modulo operator is expecting two integers but we only have one. 
How should the programming language raise this error? 

In [Java](/language_specific/java/java.md), which is a [statically typed language](statically_typed_languages.md), the type checker would throw up the error at *compile time*. Fortunately for [statically typed languages](statically_typed_languages.md), [linting tools](linting.md) are relied upon to raise that error to the developer *before* compilation.

If a [linting tool](linting.md) was not being used, this following code would error at compile time:

```
int result = 8 % 3.5; 
```

[Python](/language_specific/python/python.md), a [dynamically typed language](dynamically_typed_languages.md), would have its type checker throw the error up at *run time*:

```
result = 8 % 3.5
```

The solution to these is to of course change ```3.5``` to an ```int```; *how* this is done depends on whether the language is [statically typed](statically_typed_languages.md) or [dynamically typed](dynamically_typed_languages.md).


## So, what are the differences between static and dynamic type checking?

**Static** type checking:

- provides earlier detection of errors; earlier *correction* of bugs, in theory, uses less overall development time.
- faster execution speed; type checking has already been done in the compilation stage
- more efficient memory-usage as compilation stage highlights requirements for execution stage


**Dynamic** type checking:

- Increased development flexibility
- Increased development speed