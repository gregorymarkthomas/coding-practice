---
title:  Statically typed
author: Gregory Thomas
date:   2020-03-20
---

# Statically typed

## What entity can be "statically typed"?

A *programming language* can be "statically typed".


## What is a "statically typed" programming language, then?

A "statically typed" language will do its [type checking](/terms/types/type_checking.md)] at **[compile](/execution/compiler.md)** time.


### Why does the language do its [type checking](/terms/types/type_checking.md)] at [compile](/execution/compiler.md) time?

So that errors can be highlighted *before* runtime.


## So, what dpes the type checker need to do its job?

The type checker does not know enough by itself to determine whether an operation is an error or not, so it requires "[data type](/memory/variables/data_types/data_types.md)" information to be defined with any [variables](/memory/variables/variables.md) created.


## So, what does the developer need to do? 

"Statically typed" languages, _for the most part_, require the programmer to *specify the [data type](/memory/variables/data_types/data_types.md) of the [variables](/memory/variables/variables.md) they create*.


## What are some examples?

### Java

```
int i = 3;
```

## What languages are considered "statically typed"?

- [Java](/language_specific/java/java.md)
- [Kotlin](/language_specific/kotlin/kotlin.md)
- [C#](/language_specific/c_sharp/c_sharp.md) 
- [C++](/language_specific/c_plus_plus/c_plus_plus.md).
