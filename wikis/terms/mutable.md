---
title:  Mutable
author: Gregory Thomas
date:   2020-08-14
---

# Mutable

## What is it?

Mutability refers to whether something can be **changed**.


## So, does it mean it _can_ or _cannot_ be changed?

In software, an object is _mutable_ when its state **can be changed** after creation.


## Are mutable objects [thread-safe](/concurrency/threads/thread_safety.md)?

They can be, but any modification to an object between threads must be done carefully with the use of [locks](/concurrency/locks.md).


## So, conversely, [IMmutable](immutable.md) objects CANNOT have their state changed?

Yes.
