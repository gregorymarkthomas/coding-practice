---
title:  Immutable
author: Gregory Thomas
date:   2020-08-14
---

# Immutable

## What is it?

Immutability refers to whether something can be **changed**.


## So, does it mean it _can_ or _cannot_ be changed?

In software, an object is _immutable_ when its state **cannot be changed** after creation.


## Are immutable objects [thread-safe](/concurrency/threads/thread_safety.md)?

Yes; if the immutable object's value cannot be changed, then all threads accessing said object will _always_ see the same value.


## So, conversely, [mutable](mutable.md) objects CAN have their state changed?

Yes.
