---
title:  Terminal
author: Gregory Thomas
date:   2020-04-10
---

# Terminal

## What is it?

The Terminal is a [console](/terms/console.md); it allows users to carry out input and output operations for the [Linux](linux.md) system.

The Terminal is used for any user input on a [Linux](linux.md) installation. If the installation has a [Graphical User Interface (GUI)](/terms/graphical_user_interface.md), then a [Terminal Emulator](terminal_emulator.md) utility can be used to control the system.


## Referencing directories

### How do I select files *within* a directory?

By adding a ```/``` **at the end** when referencing a directory when calling an operation:

```cp /media/primary/ /media/backup```

This will copy the **contents** of ```/media/primary``` into ```/media/backup```. 


### How do I select the directory itself?

Instead of referencing the *contents* of a directory, we may want to reference the **directory itself**:

```cp /media/primary /media/backup```

Notice the **lack** of ```/``` at the end of ```/media/primary```.
The operation above will copy the ```/media/primary``` **directory itself** into ```/media/backup```; this will result in being able to access ```/media/backup/primary```.


## How does this differ to [MS DOS](../microsoft/windows/md_dos.md)?

A user can perform **any** operation on the system using the Terminal, but is more restrained using [MS DOS](../microsoft/windows/md_dos.md).
