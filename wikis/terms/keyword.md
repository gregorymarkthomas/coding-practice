---
title:  Keyword
author: Gregory Thomas
date:   2020-07-16
---

# Keyword

## What is it?

A _keyword_ is a word that is **reserved** for use when writing in the coding language.


## Why?

Well, these keywords will be used to control elements of the code, like set the memory behaviour of a [variable](/structures/software_entities/variables/variables.md).


## What are some examples of keywords?

- [static](/structures/software_entities/variables/class_static_variables.md)