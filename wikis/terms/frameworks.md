---
title:  Frameworks
author: Gregory Thomas
date:   2020-03-13
---

# Frameworks

## What are they?

A framework is an extra layer of functionality *on top* of a programming language. It is an application that the developer consults to write their own application.


## Why would you want to use a framework?

The framework may abstract complexity from the original programming language to provide an easier way complete a task.