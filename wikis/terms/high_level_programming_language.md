---
title:  High level programming languages
author: Gregory Thomas
date:   2020-03-15
---

# High level programming languages

## What are they?

Languages that are *descriptive* (using keywords/names in English).

Languages that abstract low-level operations from the developer.

They are normally [Object Oriented](/paradigms/object_oriented_programming.md).

They are called "managed" languages; code compiles to an intermediate form (like [Bytecode](/execution/bytecode.md)) for it to run on a [Virtual Machine](/execution/virtual_machine.md) (like the [Java Virtual Machine (JVM)](/language_specific/java/execution/java_virtual_machine.md)).

## Why do these abstract low-level operations from the developer?
This is for safety; coding in a [low level programming language](low_level_programming_language.md) is risky due to working on a memory-level and having access to low-level (and very powerful) functions.