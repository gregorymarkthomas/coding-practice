--
title:  Assembly language
author: Gregory Thomas
date:   2020-03-15
---

# Assembly language

## What is it?

[Low-level language](low_level_programming_language.md); there is a **high** correspondence between the language's intructions and the architecture's machine-code instructions.

Basically, **assembly depends on machine code instructions**.


## What is "assembly"?

*Assembly* is the conversion process of turning assembly code into executable machine code. This is done **with an assembler**.

Every assembler has it's own assembly language that is specific to the computer's architecture.


## What are some other names for Assembly language?

- Symbolic machine code 