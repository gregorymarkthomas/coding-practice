---
title:  Terminal Emulator
author: Gregory Thomas
date:   2020-04-10
---

# Terminal Emulator

## What is it?

The Terminal Emulator is a *utility* that allows the user to run [console](/terms/console.md) commands on [operating systems](/operating_system/operating_system.md) with a [Graphical User Interface (GUI)](/terms/graphical_user_interface.md).


## How does this relate to a [Terminal](terminal.md)?

The Terminal Emulator is literally an [emulation](emulation.md) of a [Terminal](terminal.md) (as the name suggests!); a ['window'](window.md) shows the [Terminal](terminal.md) and a user can type commands into it.