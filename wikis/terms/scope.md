# Scope

## What is it?

The scope is the _lexical context_, particularly the **function or block in which a variable is defined**.


## How does Scope affect visibility of local data?

Local data (such as a [local variable](/memory/variables/local_variable.md)) is typically (in most languages) _invisible_ outside of the function or the Scope/lexical context where it is defined. Local data is also invisible and inaccessible to a _called_ function.