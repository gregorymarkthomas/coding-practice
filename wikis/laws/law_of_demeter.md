# The Law of Demeter

_The Law of Demeter_ says that a module should not know about the innards of the [objects](/paradigms/object_oriented_programming_oop/objects.md) it manipulates.


## What should an [object](/paradigms/object_oriented_programming_oop/objects.md) do anyway?

An object should **hide** its _data_ and **expose** its _operations_.



## How else can you describe The Law of Demeter?

A method _f_ of class _C_ should only call the _methods_ of these:

- _C_
- An object created by _f_
- An object passed as an argument to _f_
- An object held in an instance variable of _C_

i.e. Talk to friends, not to strangers.


## What is an example of a breach of this Law?

```java
final String outputDir = ctxt.getOptions().getScratchDir().getAbsolutePath();
```


## Can we say that we should 'only use one dot'?

Yes.


## What is the idea behind The Law of Demeter?

A function should not know _too much_...

Even if we refactor the above code into:

```java
Options opts = ctxt.getOptions();
File scratchDir = opts.getScratchDir();
final String outputDir = scratchDir.getAbsolutePath();
```

this function still does too much.


## What if `ctxt`, `Options` and `ScratchDir` were [data structures](/structures/data_structures/data_structures.md) and not [objects](/paradigms/object_oriented_programming_oop/objects.md)?

If `ctxt`, `Options` and `ScratchDir` are just data structures with no behavior, then they naturally expose their internal structure, and so Demeter does not apply.

Of course, if these [data structures](/structures/data_structures/data_structures.md) had [accessor functions](\structures\software_entities\functions\accessor_functions.md), then it would certainly _look_ like we are breaking The Law of Demeter (as it looks the same as above):

```java
final String outputDir = ctxt.getOptions().getScratchDir().getAbsolutePath();
```

The use of [accessor functions](\structures\software_entities\functions\accessor_functions.md) confuses the issue; if the code had been written as follows, then we probably wouldn’t be asking about Demeter violations:

```java
final String outputDir = ctxt.options.scratchDir.absolutePath;
```


## Where can I get a better understanding of this?

[Data Structures VS. Objects](\structures\software_entities\types\classes\data_structures_versus_objects.md) is a better read.