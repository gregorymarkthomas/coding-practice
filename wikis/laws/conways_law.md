# Conway's Law

> "Any organization that designs a system (defined broadly) will produce a design whose structure is a copy of the organization's communication structure."


## What is the 'Inverse Conway Manoeuvre'?

> "The 'Inverse Conway Manoeuvre' recommends _evolving_ your team and organizational structure to promote your desired architecture. Ideally your technology architecture will display isomorphism with your business architecture."

The idea of this is to take what the business does and **evolve** it for improvement.