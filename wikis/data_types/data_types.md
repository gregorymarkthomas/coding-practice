# Data types

## What are they?

Data Types are _classifications_ (or *formats*) of data.


## What does a Data Type _define_?

- the **meaning** of the data
- what **operations** it can perform
- the possible **values**
- how much **space** it requires in memory.


## Why are they needed?

**The [compiler](/execution/compiler.md) does not know the context in which the cold, hard data will be used**. 


## So how does a Data Type help the [compiler](/execution/compiler.md)?

Data types are used instead to tell the compiler **how the data will be used throughout the code and how much space in memory it will require**.


## What are Data Types _in relation_ to [Data Structures](/structures/data_structures/data_structures.md) and [Abstract Data Types](abstract_data_types_adt.md)?

> Abstract Data Type > Data Structure > **Data Type**

Data Types are the **implementations** that make-up Data Structures, and Data Structures are the **implementations** that make-up [Abstract Data Types (ADTs)](abstract_data_types_adt.md).


## Do you have an example?

- ["Stack"](\memory\allocation\static\stack.md) is the [Abstract Data Type (ADT)](abstract_data_types_adt.md)
- [Array](\structures\software_entities\types\derived\arrays_and_strings\arrays.md) is the [Data Structure](/structures/data_structures/data_structures.md) used to implement the Stack
- [Integer](\structures\software_entities\types\primitives\integer.md) is the Data Type used for the elements within the Array


## Are there *types* of data type?

Yes.


### Well, what are they?

**Primitive** data types, and **derived** data types.

Most modern programming languages provide same primitive data types.


### What are some examples of data types?

- **primitive** data types
	- [byte](primitive/byte.md) / [octet](primitive/octet.md)
	- [short](primitive/short.md) / [word](primitive/word.md)
	- [int](primitive/int.md)
	- [long](primitive/long.md)
    - [double](primitive/double.md)
    - [float](primitive/float.md)
    - [boolean](primitive/boolean.md)
- derived
	- [date](derived/date.md)
	- [dictionary](derived/dictionary.md)
	- [tree](derived/tree.md)
	- [graph](derived/graph.md)
	- [array](derived/arrays_and_strings/arrays.md)
	- [string](derived/arrays_and_strings/strings.md)
	- [list](derived/lists.md)
	

## So, basically, a data type is a *noun*?

Yes.