# Reference Type

# What is it?

A Reference Type has a _value_ that is of a [**reference**](/memory/reference.md) to _another_ value.

https://en.wikipedia.org/wiki/Value_type_and_reference_type