#!/usr/bin/python

import unittest

class HashTableDumb():
	"""
	This is a simple hashtable implementation.
	It requires a fixed list size.
	It does not handle elements added to the same index.
	"""

	def __init__(self):
		self.list = [None] * 10

	def __str__(self):
		return ''.join([str(item) + ", " for item in self.list])

	def add(self, key, value):
		self.list[self.__key_hasher(key)] = value

	def get(self, index):
		return self.list[index]

	def __key_hasher(self, key):
		"""This should not be called 'hash()' or 'hash_function()' because 'hash' and 'function' are already keywords."""
		print("key = " + str(key))
		print("len(list) = " + str(len(self.list)))
		return key % len(self.list)

class HashTable(list):
	"""
	TODO: incomplete.
	This is a simple hashtable implementation.
	Extends 'list'.
	It requires a fixed list size.
	It does not handle elements added to the same index.
	"""
	def __getitem__(self, key):
		return super(HashTableDumb, self).__getitem__(key-1)

	def add(self, key, value):
		self[self.__key_hasher(key)] = value

	def __key_hasher(self, key):
		"""This should not be called 'hash()' or 'hash_function()' because 'hash' and 'function' are already keywords."""
		print("key = " + str(key))
		if len(key) > 0:
			return key % len(self)
		else:
			return 0

class HashTable2():
	"""
	Implement one with dicts/linked list as elements for elements that use the same index.
	"""
	pass


class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		pass
		
	def test_second_item_overwrites_first(self):
		"""
		Because of how dumb this class is, we expect items 0 and 1 to fight over the same index.
		Keys are quite large values because the length of self.list in HashTable is 10.
		"""
		hash_table = HashTableDumb()
		hash_table.add(10, "10")
		hash_table.add(20, "20")
		hash_table.add(25, "25")
		print(hash_table)
		self.assertEqual("20", hash_table.get(0))

unittest.main(exit=False)