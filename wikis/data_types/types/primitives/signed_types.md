# Signed data type

## What is a _signed_ data type?

An **signed** data type can be a **positive** _or_ **negative** number in the available range.


## What affects the available range?

The [data type](../data_types.md) chosen will affect how many [bits](/memory/binary_digit_bit.md) can be used to represent the number.