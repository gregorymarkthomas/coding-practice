# Integer

## What is it?

A *primitive*/built-in **data type**.


## How much memory does one use?

An Integer uses **32** bits.


## Can we represent [negative numbers](/data/computer_number_format/signed_number_representations/signed_number_representations.md) with this data type?

Yes, though it depends on whether the programming language supports a [Signed version](\structures\software_entities\types\primitives\signed_types.md) of this data type.


## What's the biggest number we can represent with an integer?

Again, it depends on whether this is a [signed](\structures\software_entities\types\primitives\signed_types.md) integer or an [unsigned](\structures\software_entities\types\primitives\unsigned_types.md) integer:


### [Signed integer](\structures\software_entities\types\primitives\signed_types.md)

−2,147,483,648 to 2,147,483,647 (-2^31 to 2^31-1)


### [Unsigned integer](\structures\software_entities\types\primitives\unsigned_types.md)

0 to 4,294,967,295 (0 to 2^**32** - 1)

The **32** here refers to the **32** bits an integer consists of.


## What are some common uses of an integer?

It is common to store _currency_ as an integer in [database](/structures/software_entities/database.md).


### Why would we store currency as an integer?

We want to reduce the possibility of prices changing due to [rounding errors](../caveats/rounding_errors.md).


### How are those integer values shown as currencies in the view?

The idea is that we will _always assume that the last 2 numbers of the integer is the [fractional](/mathematics/fractions.md) part_.
When it comes to displaying the currency, we can just divide the database result by ```100```.


### What is another name for this?

This is called [fixed-point representation](/mathematics/fixed_point_representation.md).


### Examples of storing currencies as integers in database

- ```£356.67``` stored as ```35667```.

- ```£400.00``` stored as ```40000```.

- ```£4``` stored as ```400```


### What about oil prices where there are more than 2 fixed points?

Perhaps store currencies with a constant number of points.