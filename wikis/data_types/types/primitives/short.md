# Short

## What is it?

A _short_ is a *primitive* **data type**.


## How much memory does one use?

A byte uses **16** bits.


## Can we represent [negative numbers](/data/computer_number_format/signed_number_representations/signed_number_representations.md) with this data type?

Yes, though it depends on whether the programming language supports a [Signed version](\structures\software_entities\types\primitives\signed_types.md) of this data type.


## What's the biggest number we can represent with a short?

Again, it depends on whether this is a [signed](\structures\software_entities\types\primitives\signed_types.md) short or an [unsigned](\structures\software_entities\types\primitives\unsigned_types.md) short:


### [Signed short](\structures\software_entities\types\primitives\signed_types.md)

-32768 to 32767 (-2^15 to 2^15-1)


### [Unsigned short](\structures\software_entities\types\primitives\unsigned_types.md)

0 to 65535 (0 to 2^**16** - 1)

The **16** here refers to the **16** bits a short consists of.


## What is another name for a short?

A [word](word.md).