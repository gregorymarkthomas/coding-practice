# Bytes

## What is it?

A byte is a *primitive* **data type**.


## This data type is just an implementation of the [byte](/data/computer_number_format/byte.md) computer number format, yes?

Correct.


## Can we represent [negative numbers](/data/computer_number_format/signed_number_representations/signed_number_representations.md) with this data type?

Yes, though it depends on whether the programming language supports a [Signed version](\structures\software_entities\types\primitives\signed_types.md) of this data type.


## What's the biggest number we can represent with a byte?

Again, it depends on whether this is a [signed](\structures\software_entities\types\primitives\signed_types.md) byte or an [unsigned](\structures\software_entities\types\primitives\unsigned_types.md) byte:


### [Signed byte](\structures\software_entities\types\primitives\signed_types.md)

-128 to 127 (-2^7 to 2^7 - 1)


### [Unsigned byte](\structures\software_entities\types\primitives\unsigned_types.md)

0 to 255 (0 to 2^**8** - 1)

The **8** here refers to the **8** bits a byte consists of.