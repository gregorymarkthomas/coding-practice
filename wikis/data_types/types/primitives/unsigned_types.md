# Unsigned data type

## What is an _unsigned_ data type?

An **unsigned** data type can _only_ be a _positive_ number in the available range.


## What affects the available range?

The [data type](../data_types.md) chosen will affect how many [bits](/memory/binary_digit_bit.md) can be used to represent the number.