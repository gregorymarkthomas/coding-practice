# Long

## What is it?

A _long_ is a *primitive*/built-in **data type**.


## How much memory does one use?

A byte uses **64** bits.


## Can we represent [negative numbers](/data/computer_number_format/signed_number_representations/signed_number_representations.md) with this data type?

Yes, though it depends on whether the programming language supports a [Signed version](\structures\software_entities\types\primitives\signed_types.md) of this data type.


## What's the biggest number we can represent with a long?

Again, it depends on whether this is a [signed](\structures\software_entities\types\primitives\signed_types.md) long or an [unsigned](\structures\software_entities\types\primitives\unsigned_types.md) long:


### [Signed long](\structures\software_entities\types\primitives\signed_types.md)

−9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 (-2^63 to 2^63-1)


### [Unsigned long](\structures\software_entities\types\primitives\unsigned_types.md)

0 to 18,446,744,073,709,551,615 (0 to 2^**64** - 1)

The **64** here refers to the **64** bits a long consists of.