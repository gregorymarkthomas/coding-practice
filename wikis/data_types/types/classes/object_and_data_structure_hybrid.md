# Creating a hybrid class of [data structures](/structures/data_structures/data_structures.md) _and_ [objects](/paradigms/object_oriented_programming_oop/objects.md)


## What does this hybrid class consist of?

- Functions that do significant things
- Public variables or public [accessor functions](\structures\software_entities\functions\accessor_functions.md) that make _private_ variables public.


## What does this do?

Due to lack of protection, this hybrid setup can tempt external functions into using those variables in the way a [procedural program](/paradigms/procedural_programming.md) might.


## Is this bad?

Yes.


## What's so bad about this?

- Entices external functions to have too much control over a class, which can lead to dangerous bugs
- Makes it difficult to add new functions to the class
- Makes it difficult to add new data structures to the class