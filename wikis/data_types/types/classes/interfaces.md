---
title:  Interfaces
author: Gregory Thomas
date:   2020-10-15
---

# Interfaces

## What are they?

An Interfaces is an _entity_ of a [type](../types.md).


## What does an Interface do for a [type](../types.md)?

An Interface provides the **definition** of a [type](../types.md).


## What is defined in an Interface?

[Functions]()