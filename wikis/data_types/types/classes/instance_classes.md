---
title:  Instance classes
author: Gregory Thomas
date:   2020-09-11
---

# Instance classes

## What are they?

An Instance Class is a [class](classes.md) that can be [instantiated](../instances.md) as an object of said class.


## So, there can be multiple [instances](../instances.md) of this class in memory?

Yes; it is up to the developer to create the [instances](../instances.md) of this Instance Class.


## Can an Instance Class be a [Singleton](/patterns/singleton_pattern.md)?

If the [instance](../instances.md) is defined _within_ the Instance Class (in the form of a [class static variable](/structures/software_entities/variables/instance_variable.md)), then, with some extra [thread-safety](/concurrency/threads/thread_safety.md), the Instance Class could become a [Singleton](/patterns/singleton_pattern.md).


## What does an Instance class contain?

### [_this_](this.md)

[_this_](this.md), the _current_ [instance](../instances.md), allows the developer to control the instance as if the Instance Class had been instantiated.

_How_ [_this_](this.md) is used depends on the language:

#### [Java](/language_specific/java/java.md) 

[Java](/language_specific/java/java.md) will allow the developer to use [_this_](this.md) anywhere within the class, as if [_this_](this.md) was a class variable.

```
public int getAge() {
	return this.age;
}

...

Person person = new Person();
person.getAge();
```

It is possible that [_this_](this.md) is actually applied to every [instance function](../functions/instance_functions.md) behind the scenes post-[compilation](/execution/compiler.md); we as developers see [_this_](this.md) as a [class variable](../variables/class_static_variables.md) when it could just be the [Integrated Development Environment (IDE)](/integrated_development_environment/integrated_development_environment.md) helping us out.


#### [Python](/language_specific/python/python.md)

[Python](/language_specific/python/python.md) requires each [instance function](../functions/instance_functions.md) to have [_this_](this.md) (called '_self_') manually entered as an [input argument](../functions/input_arguments.md):

```
def getAge(self):
	return self.age
```

### [Instance variables](/structures/software_entities/variables/instance_variables.md)

[Instance variables](/structures/software_entities/variables/instance_variable.md) are the _properties_ of the current instance ['_this_'](this.md).


### [Static variables](/structures/software_entities/variables/class_static_variables.md)

[Class/Static variables](/structures/software_entities/variables/class_static_variables.md) are accessed across _all_ instaces of the class.


### [Accessor Functions](../functions/accessor_functions.md)

Read-only functions that return a property of [_this_](this.md).


### [Mutator Functions](../functions/mutator_functions.md)

Writable functions that alter a property of [_this_](this.md).


## What are some other names for Instance Classes?

Use any other word for [instance](../instances.md):

- object classes