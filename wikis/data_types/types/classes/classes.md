---
title:  Classes
author: Gregory Thomas
date:   2020-03-13
---

# Classes

## What are they?

A Class is a **template** from which [Objects](/structures/data_structures/object.md) are created.


 or [Data Types](../data_types.md).

## What does a Class do for a [type](../types.md)?

A Class provides the **implementation** of a [type](../types.md).


## [Java](/language_specific/java/java.md) and [C++](/language_specific/c_plus_plus/c_plus_plus.md)

Yes; a [type](../types.md) can be seen as an _umbrella_ term for Classes and [primitives](../primitives/primitives.md); a type is an [abstract]



## How are classes used?

A class can be referenced (instantiated) in other classes.

Classes _group_ functions and variables.


## How does a class fit into the [Object Oriented Programming (OOP)](/paradigms/object_oriented_programming/object_oriented_programming.md) paradigm?

Classes are a form of [encapsulation](/paradigms/principles/encapsulation.md).


## How best do we name classes?

Use a [noun](/english/noun.md) to name a class:

- ```Customer```
- ```WikiPage```
- ```Account```
- ```AddressParser```


## What should we avoid when naming a class?

**Avoid** [verbs](/english/verb.md) such as:

- ```Manager```
- ```Processor```
- ```Data```
- ```Info```


## Languages

### Javascript

ES6 brought in "traditional" classes. This was to stop need for outer function closures and only returning "public" functions (see [Javascript's < ES6 module pattern implementation](../architectures/patterns/module_pattern.md#javascript)).


### Kotlin

#### How do I instantiate a class as a new object?

```var doubleArr = DoubleArray(splitStrings.size)```

This uses ```var```, the read-only [variable](../variables/variables.md) declaration.

If we wanted to create an empty variable of the class type, then we can use ```val```:

```val doubleArr: DoubleArray```