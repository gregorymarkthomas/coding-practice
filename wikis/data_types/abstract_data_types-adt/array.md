# Array Abstract Data Type

## Isn't Array considered a [Data Structure]()?

Yes, that too. Some programming languages (such as [Java]()) have Data Structures as Abstract Data Types for convenience.