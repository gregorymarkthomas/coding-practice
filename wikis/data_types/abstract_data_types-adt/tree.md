# Tree (Abstract Data Type) 

## What does a Tree ADT describe?

A Tree ADT simulates a hierarchical [_tree structure_](/structures/tree_structure.md), with a root value and subtrees of children with a parent node, represented as a set of linked nodes. 