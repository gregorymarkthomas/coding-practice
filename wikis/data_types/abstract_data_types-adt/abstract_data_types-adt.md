# Abstract Data Types (ADT)

## What are they?

They are **mathematical models** for [data types](/structures/software_entities/types/types.md).


## Okay. What does _that_ mean?

An **Abstract** Data Type is a mere **theoretical definition**, whereas a [data type](/structures/software_entities/types/types.md) is an _implementation_.


## Can you say that in other words?

An ADT does **not** specify its implementation or any underlying data structure used. 


## Is an ADT any [Data Type](/data_types/data_types.md) that does not specify its implementation?

Yes.


## What are Data Structures _in relation_ to [Data Types](\structures\software_entities\types\data_types.md) and [Abstract Data Types](abstract_data_types_adt.md)?

> **Abstract Data Type** > Data Structure > Data Type

Data Structures are the **implementations** that make-up [Abstract Data Types (ADTs)](abstract_data_types_adt.md). [Data Types](\structures\software_entities\types\data_types.md) are the **implementations** that make-up Data Structures.


## Do you have an example?

- ["Stack"](\memory\allocation\static\stack.md) is the [Abstract Data Type (ADT)](abstract_data_types_adt.md)
- [Array](\structures\software_entities\types\derived\arrays_and_strings\arrays.md) is the [Data Structure](/structures/data_structures/data_structures.md) used to implement the Stack
- [Integer](\structures\software_entities\types\primitives\integer.md) is the [Data Type]() used for the elements within the Array


## Can I think of an [interface](/structures/software_entities/types/interfaces.md) as a way to _define_ an Abstract Data Type?

Yes you can!


## Is a [class](/structures/software_entities/types/classes/classes.md) a way to _implement_ an Abstract Data Type, then?

Yes.


## What is an example of an ADT?

A [Stack](/memory/allocation/static/stack.md); it can be described as a [Last In First Out](/terms/last_in_first_out_lifo.md) Abstract Data Type.


### How so?

A stack can have _any_ Abstract Data Type as an element, but, ultimately it is characterised by two fundamental operations: `push()` and `pop()`.