---
title:  Precision
author: Gregory Thomas
date:   2020-07-22
---

# Precision

## What are we talking about here?

Having a _decimal_ result stored as an integer