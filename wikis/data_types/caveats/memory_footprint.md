---
title:  Data type memory footprint
author: Gregory Thomas
date:   2020-07-22
---

# Data type memory footprint

## What are we talking about here?

When writing software, we should always be mindful of the amount of memory that we use.

With the [variables](/structures/software_entities/variables/variables.md) that we create in our code, we should be mindful of the amount of memory each variable uses.


## What do we need to consider?

For example, say we are writing a sum to a variable ```i```.

If the sum is a small number, say, ```4```, then we _could_ store that variable as an [integer](/data_types/integers.md)... but an integer can have a value of up to 2,147,483,647 (2^31), if it is a [signed](../signed_vs_unsigned_numbers.md) (and 2^32 if _unsigned_).

What we should do is store that result in a data type with a smaller memory footprint (such as a [word](../primitive/word.md) or even a [byte](../primitive/byte.md)). We do, however, need to be wary of whether that data type is [signed or unsigned](../signed_vs_unsigned.md_numbers); we need a _signed_ data type for a negative result.