---
title:  Child types
author: Gregory Thomas
date:   2020-10-13
---

# Child types

## What are they?

Child Types are [classes](../classes.md) that _derive_ from a [parent class](super_types.md).


## What is another name for a Child Type?

- _Sub_ types
- Concrete types
	- This depends on the [parent class](super_types.md) being [abstract](/modifiers/abstract.md), and having the _concrete_ implementation in the child type.