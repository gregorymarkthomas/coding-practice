# Boxing and Unboxing in context to Data Types

Programming languages that distinguish between value types and reference types typically offer a mechanism, called boxing, to wrap some or all of their value types in reference types. This permits the use of value types in contexts expecting reference types. The converse process (to unwrap the value type) is known as unboxing.