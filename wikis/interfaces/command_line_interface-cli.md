# Command-Line Interface (CLI)

## What is it?

A Command-Line interface (CLI) processes commands to a computer program in the form of lines of text. 


## What processes commands from the CLI?

The program which handles the interface is called a _command-line interpreter_ (or command-line processor). 


## How do [Operating Systems](/operating_system/operating_system.md) use Command-Line Interfaces?

Operating systems implement a command-line interface in a shell for interactive access to operating system functions or services. 