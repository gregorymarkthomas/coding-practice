---
title:  Procedural programming
author: Gregory Thomas
date:   2020-03-15
---

# Procedural programming

## What is it?

A *programming paradigm*.

Procedural programming uses a top-down/linear approach; it uses **[procedures](/structures/software_entities/functions/functions.md)** (or subroutines).

Data structures and procedures/functions **remain separated**.

Data structures contain data. These data structures are passed into functions as arguments, and the function uses and/or *modifies* the data of the data structure.

It derives from [structured programming](structured_programming.md).


## What are examples of procedural languages?

- BASIC
- Pascal
- C


## What does the resulting structure look like?
Hierarchical/tree-like.


## How is it related to [Imperative programming](imperative_programming.md)?
Procedural programming **is also** Imperative programming!


## How is it *compared* to [Object-Oriented programming (OOP)](object_oriented_programming.md)?
OOP *merges* functions and data to form *classes*, whereas procedural programming **separates** functions and data.


## How is it *compared* to [Event-driven programming](event_driven_programming.md)?

Procedural programming allows functions to be called linearly; event-driven programming requires functions to be called on an event.


## What is functional decomposition?

## What is step-wise refinement?


## What is top-down and bottom-up in context of procedural programming?
