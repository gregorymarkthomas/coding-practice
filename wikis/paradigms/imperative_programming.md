---
title:  Imperative programming
author: Gregory Thomas
date:   2020-03-15
---

# Imperative programming

## What is it?

A *programming paradigm*.

It describes **how** a program operates, and not *what the program should achieve* with [declarative programming](declarative_programming.md).

