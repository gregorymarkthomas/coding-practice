---
title:  Structural/structured programming
author: Gregory Thomas
date:   2020-03-15
---

# Structural/structured programming

## What is it?

A *programming paradigm*.

Uses "structured control flow constructs":

- selection
  - [if/then/else](/structures/control_structures/selections/if_then_else.md)
- repetition/iteration
  - [for](/structures/control_structures/iterations/for_loops.md)/[while](/structures/control_structures/iterations/while_loops.md) loops

C is a structural programming language, as opposed to C# which is an [Object Oriented Programming (OOP)](object_oriented_programming.md) language.

