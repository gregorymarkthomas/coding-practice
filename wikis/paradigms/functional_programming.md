---
title:  Functional programming
author: Gregory Thomas
date:   2020-03-15
---

# Functional programming

## What is it?

It is a programming *paradigm*.

It is a *form* of [declarative programming](declarative_programming.md).

Functional programming is based on [mathematical functions](/mathematics/mathematical_functions.md).

Most high-level languages support functional **style** programming, in the form of [anonymous classes](/structures/software_entities/functions/anonymous_classes.md), [anonymous closures](/structures/software_entities/functions/anonymous_closures.md), and [lambdas](/structures/software_entities/functions/lambdas.md) 


## What are some languages that **only** support Functional programming?

- [Javascript](/language_specific/javascript/javascript.md)
- [Kotlin](/language_specific/kotlin/kotlin.md) (somewhat)
- [C++](/language_specific/c_plus_plus/c_plus_plus.md) (somewhat in C++ 11)
- [PHP](/language_specific/php/php.md) (somewhat)


## What is an example of Functional programming?

The [fibonacci test](/structures/control_structures/recursion.md) implementation can be done functionally in [Javascript](/language_specific/javascript/javascript.md):

```
const fib = (x) => (function sub_fib(a, b) { return x-- > 0 ? sub_fib(b, a+b) : a})(0,1)
```

The same can be achieved in [Kotlin](/language_specific/kotlin/kotlin.md):

```
fun fib(x: Int): Int = if (x in 0..1) x else fib(x - 1) + fib(x - 2)
```