---
title:  Event-driven programming
author: Gregory Thomas
date:   2020-03-15
---

# Event-driven programming

## What is it?

A *programming paradigm*.

Event-driven programming is a paradigm in which *procedures* are called **only in response to events**.


## What kind of events?

- Mouse clicks
- Keyboard presses
- Removal/attachment of a device
- Arrival of data from external source


## How does this differ to [Procedural programming](procedural_programming.md)?

Because these events are *unpredictable*, the procedures called by the events **cannot be called linearly** like in [procedural programming](procedural_programming.md). 