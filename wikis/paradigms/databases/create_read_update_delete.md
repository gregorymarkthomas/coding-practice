---
title:  Create Read Update Delete (CRUD)
author: Gregory Thomas
date:   2020-03-13
---

# Create Read Update Delete (CRUD)

## Why CRUD?

**C**reate, **R**ead, **U**pdate, and **D**elete (CRUD) 


## What are they?

Create, Read, Update, and Delete are the four basic functions that **models** should be able to do, *at most*.

CRUD reminds us developers on how to construct full, usable models.

This relates to [REpresentational State Transfer (REST)](../../architectures/representational_state_transfer.md) because we use REST HTTP methods (GET, POST, PUT, DELETE) in the implementation to manage our CRUD model 
