---
title:  Single responsibility principle
author: Gregory Thomas
date:   2020-06-03
---

# Single responsibility principle (SRP)

## What is it?

- A class/module should only have a single *responsibility*.
  - A *responsibility* is a *reason to change*; if a change is to occur, there should only be **ONE** reason to change.
  - e.g. A module prints AND compiles a report. This module has TWO reasons to change; perhaps the content of the report could change, or the *format* of the report could change. Using the *Single responsibility principle*, we should really separate these two actions into separate classes/modules; changing one process within a class has the risk of breaking something else.


## How does it relate to [SOLID](solid.md)?

**S**ingle responsibility principle is the "S" of "SOLID".