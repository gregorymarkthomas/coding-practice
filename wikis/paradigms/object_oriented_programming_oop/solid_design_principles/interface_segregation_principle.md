---
title:  Interface segregation principle
author: Gregory Thomas
date:   2020-06-03
---

# Interface segregation principle (ISP)

## What is it?

- No client should be forced to depend on methods it does not use
  - i.e. split large interfaces into smaller ones so the client only implements what it needs. These are called *role interfaces*.
  - Keeps things de-coupled and small, which makes it easier to refactor.


## How does it relate to [SOLID](solid.md)?

**I**nterface segregation principle is the "I" of "SOLID".