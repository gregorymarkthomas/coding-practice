---
title:  Open-closed principle
author: Gregory Thomas
date:   2020-06-03
---

# Open-closed principle (OCP)

## How does it relate to [SOLID](solid.md)?

**O**pen-closed principle is the "O" of "SOLID".


## What is the Open-closed principle?

> "Software entities (classes, modules, functions, etc.) should be **open for extension**, _but closed for modification_"


## What does this mean?

We should strive to write code that **doesn’t need changing when the requirements change**.


## So we should be **extending**, and not _modifying_?

Yes; we should **extend** code via [overrides](\structures\software_entities\functions\function_overriding.md)/[interfaces](\structures\software_entities\types\interfaces.md)/[child class](\structures\software_entities\types\child_types.md) etc. We should not be MODIFYING the code to cater for something.


## What is an example of extension over modification?

### Function to find the area of shapes

We want a function to find the area of different shapes (`Rectangle`, `Circle`, `Triangle`).


### What should we **not** do here?

We should **not** use something like `if(shape instanceof Triangle)` as this means we will have to *modify* our class for a change of requirement.


### How should we achieve this?

We should create a `getArea()` function per `Shape` (perhaps `Triangle`/`Rectangle`/`Circle` inherit the [abstract](/modifiers/abstract.md) class `Shape` which has `getArea()` as a function) so that we can *extend* instead.


## What about coding "in the real world"?

We should not always apply the Open/Closed Principle straight away as it can make the code needlessly complex and consume development time in the initial stages. 
There comes a point where we should refactor our code to allow for these requirement changes, but only when those changes come in from the client.


## Links

- http://joelabrahamsson.com/a-simple-example-of-the-openclosed-principle/