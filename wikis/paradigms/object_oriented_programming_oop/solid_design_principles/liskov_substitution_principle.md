---
title:  Liskov substitution principle
author: Gregory Thomas
date:   2020-06-03
---

# Liskov substitution principle (LSP)

## What is it?

- Substitutability. If ```S``` is a [subtype](/structures/software_entities/classes/types/child_types.md) of [supertype](/structures/software_entities/classes/types/super_types.md) ```T```, and there is an instance of ```T```, we should be able to *substitute* ```T``` for ```S``` *without altering desirable properties*.
- This principle extends the [Open/Closed principle](open_closed_principle.md) in that you should be able to swap a parent class for a child class without any issues
  - the child class should *behave in the same way* as the parent class

Think of the coffee example (https://stackify.com/solid-design-liskov-substitution-principle/); addCoffee(someCoffeeType) WAS added to the interface that both BasicCoffeeMaker and PremiumCoffeeMaker implement. The issue here was that BasicCoffeeMaker only has GroundCoffee, and PremiumCoffeeMaker only has BeanCoffee. We could have made a parent Coffee class and make BeanCoffee and GroundCoffee children of it, but then we would have to **validate** the input object; we don't want to have to check ```coffee instanceof BeanCoffee``` in our implementation. This, by the way, would not follow the Liskov substitution principle.
A solution is to remove addCoffee() from the interface and just let our child coffee maker classes implement their own thing.


## How does it relate to [SOLID](solid.md)?

**L**iskov substitution principle is the "L" of "SOLID".