---
title:  Dependency inversion principle
author: Gregory Thomas
date:   2020-06-03
---

# Dependency inversion principle (DIP)

## What is it?

Think of **[abstraction](../principles/abstraction.md)**.

- High level modules should not depend on low level modules. **Both should depend on interfaces**.
- **Abstractions** should not depend on details (i.e. concrete implementations). *Implementations should rely on abstractions.* 
- Issues:
  - If the mocking tool used relies only on inheritance, it may become necessary to widely apply the dependency inversion pattern. This has major drawbacks:
  - Merely implementing an interface over a class isn't sufficient to reduce coupling; only thinking about the potential abstraction of interactions can lead to a less coupled design.
  - Implementing generic interfaces everywhere in a project makes it harder to understand and maintain. At each step the reader will ask themself what are the other implementations of this interface and the response is generally: only mocks.
  - The interface generalization requires more plumbing code, in particular factories that generally rely on a dependency-injection framework.
    Interface generalization also restricts the usage of the programming language.


## How does it relate to [SOLID](solid.md)?

**D**ependency inversion principle is the "D" of "SOLID".