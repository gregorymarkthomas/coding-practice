---
title:  SOLID
author: Gregory Thomas
date:   2020-09-24
---

# SOLID

## What is it?

SOLID is an [acronym](/english/acronym.md) of _design principles_ that should be followed when using the [Object Oriented Programming (OOP)](../object_oriented_programming_oop.md) paradigm.


## What does SOLID stand for?

- [Single responsibility principle](single_responsibility_principle.md)
- [Open-closed principle](open_closed_principle.md)
- [Liskov substitution principle](liskov_substitution_principle.md)
- [Interface segregation principle](interface_segregation_principle.md)
- [Dependency inversion principle](dependency_inversion_principle.md)