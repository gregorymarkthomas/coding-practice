---
title:  Object Oriented Programming (OOP)
author: Gregory Thomas
date:   2020-03-13
---

# Object Oriented Programming (OOP)

## What is it?

[Object](/structures/data_structures/object.md) Oriented Programming (OOP) is a *programming paradigm*.

OOP Uses *[classes](/structures/software_entities/classes/classes.md)* and *[objects](/structures/software_entities/instances.md)* (instances of classes). The *[object](/structures/data_structures/object.md)'s class* should contain **relevant** methods/data structures and not just a random set.

Relevant data and functions are grouped together in classes. Objects, which are instances of those classes, are the *fundamental abstraction*; each object contains its **own** data and is not easily accessible by other objects of the same class.


## What is another name for it?

*Class*-based programming.


## How are [objects](/structures/data_structures/object.md) accessed?

Objects are accessed somewhat like variables with complex internal structure, and in many languages are effectively *pointers*, serving as actual references to a single instance of said object in memory within a heap or stack. 


## What are the four main principles of OOP?

- [Inheritance](principles/inheritance.md)
- [Abstraction](principles/abstraction.md)
- [Encapsulation](principles/encapsulation.md)
  - Think of **Security**.
- [Polymorphism](principles/polymorphism/polymorphism.md)


## What are the _design_ princples of OOP?

[SOLID](solid_design_principles/solid.md).


## What are some techniques in other languages that are actually OOP?

- traits 
- mixins


## What are some questions to ponder?

https://stackoverflow.com/questions/36552343/is-inheritance-necessary-for-encapsulation-abstraction-and-polymorphism

    Is Encapsulation possible without inheritance?

Yes, because Encapsulation is the ability to hide class properties from the outside world by means of *access methods*.

    Is Abstraction possible without inheritance?

Well, abstraction can refer to many things, but talking about OOP; no, an abstract class cannot be used directly, as you can only instantiate *inherited* classes.

    Is Polymorphism possible without inheritance?

Yes; polymorphism is the construction of a single interface to several types of objects. For instance, a single function call that can receive different classes or data types as arguments. They can be inherited or not.


## How does this compare to [Procedural programming](../procedural_programming.md)?

Procedural programming *does not* merge data and functions like OOP does; they are separate.