---
title:  Abstraction
author: Gregory Thomas
date:   2020-03-13
---

# Abstraction

## What is it?

A *principle* synonymous with [Object Oriented Programming](../object_oriented_programming_oop.md)


## What does it do/mean?

The act of *hiding* complex code behind a simple method.
e.g. On/Off switch on a monitor.
Think of the "abstract" keyword; we are **defining** properties/methods that the extending class has to implement as a way to move complexity further from harm's way.

This can also be seen as a security/"need to know basis" thing.

e.g. I am an abstract class; I am aware of the tasks needed to fix a car in particular ways but I do not know how to do it. A local mechanic, who extends me, knows the implementation to *some* of these tasks, but not all. The car's designer extends the local mechanic and he knows what the local mechanic knows, PLUS further implementations.


## Where would you put _abstracted_ code?

You could define abstract [functions](/structures/software_entities/functions/functions.md) in a [super type](/structures/software_entities/)/[interface](/structures/software_entities/interfaces.md), and _implement_ those abstract functions in [child types](/structures/software_entities/classes/types/child_types.md) of that super type; the two sub types are known as _concrete implementations of the interface_.