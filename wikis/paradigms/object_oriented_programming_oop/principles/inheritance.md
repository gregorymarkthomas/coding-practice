---
title:  Inheritance
author: Gregory Thomas
date:   2020-09-24
---

# Inheritance

## What is it?

A *principle* synonymous with [Object Oriented Programming](../object_oriented_programming_oop.md)

[Inheritance](/data_relationships/inheritance.md) is a **IS A** data relationship.


## What is another name for Inheritance?

- Subclassing