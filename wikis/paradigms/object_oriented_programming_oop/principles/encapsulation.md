---
title:  Encapsulation
author: Gregory Thomas
date:   2020-03-13
---

# Encapsulation

## What is it?

A *principle* synonymous with [Object Oriented Programming](../object_oriented_programming_oop.md)


## What does it do/mean?

**Security**. The **hiding** of variables/methods in a class to stop unnecessary/dangerous use. 