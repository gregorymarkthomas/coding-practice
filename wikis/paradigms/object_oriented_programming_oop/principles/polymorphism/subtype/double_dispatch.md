---
title:  Double dispatch
author: Gregory Thomas
date:   2020-09-24
---

# Double dispatch

## What is it?

Double [dispatch](dispatch.md) is a _type_ of [Multiple dispatch](multiple_dispatch.md), which is a type of [polymorphism](polymorphism.md).


## What are the basics of Double dispatch?

Double [dispatch](dispatch.md) is the act of having two _sibling_ [concrete types](/structures/software_entities/classes/concrete_types.md) trying to interact with each other.


## When executing an operation, what _properties_ are relied upon?

The operation executed depends on:

- the _name_ of the request
- the [types](/structures/software_entities/classes/types/types.md) of **both** the _visitor_ object and the object the visitor _visits_ (**hence DOUBLE dispatch**).


## What happens in languages like [Java](/language_specific/java/java.md) and [C++](/language_specific/c_plus_plus/c_plus_plus.md) where Double dispatch is **not** supported?

See the [DoubleDispatch](DoubleDispatch/) Java application for an example.


## What does the [DoubleDispatch](DoubleDispatch/) Java application show?

The [DoubleDispatch](DoubleDispatch/) Java application shows Double dispatch **NOT** working in Java; we are trying to allow ```Dog```s and ```Cat```s to _interact
with each other_, but, **it fails**.

With thanks to 'The Simple Engineer' at [https://youtu.be/TeZqKnC2gvA?t=452](https://youtu.be/TeZqKnC2gvA?t=452).


### How is the application set up?

```Animal``` supertype has ```makeSound(Dog dog)``` and ```makeSound(Cat cat)``` functions to attempt Double dispatch (i.e. interation _between_ ```Animal``` concrete types):

```java
public interface Animal {
    void makeSound(Dog d);
    void makeSound(Cat c);
}
```

In ```main```, we declare 'dog' and 'cat' with the Animal supertype, and we instantiate them with their concrete types (Dog and Cat, respectively).
When ```dog.makeSound(cat)``` is attempted, **a type-check [compilation error](/execution/compilation_error/compilation_error.md) occurs**; ```dog.makeSound(cat)``` is expecting a concrete Cat input, but we have entered an Animal instead:

```java
public static void main(String[] args) {
    Animal dog = new Dog();
    Animal cat = new Cat();

    dog.makeSound(cat);	// compile-time error
    dog.makeSound(dog); // compile-time error
    cat.makeSound(cat); // compile-time error
    cat.makeSound(dog); // compile-time error
}
```

### What are we going to try to fix it?

We could create ```makeSound(Animal animal)``` in Animal, and implement it in ```Dog``` and ```Cat``` classes:

```java
public interface Animal {
    void makeSound(Dog d);
    void makeSound(Cat c);
    void makeSound(Animal a);
}
```

After this new implementation, the type-check [compilation error](/execution/compilation_error/compilation_error.md) disappears. **Unfortunately, the output shows "Dog interacting with ANIMAL", meaning our ```Cat``` and ```Dog```-instantiated (but ```Animal```-declared) inputs go unrecognised!**

### In other words...?

Basically, Double Dispatch fails because we cannot [polymorphically](polymorphism.md) resolve the concrete type at runtime.



## Can we emulate Double dispatch in Java?

Yes (with thanks to the [Multiple Dispatch Wikipedia page](https://en.wikipedia.org/wiki/Multiple_dispatch#Java)):

``` java
interface Collideable {
    void collideWith(final Collideable other);

    /* These methods would need different names in a language without method overloading. */
    void collideWith(final Asteroid asteroid);
    void collideWith(final Spaceship spaceship);
}

class Asteroid implements Collideable {
    public void collideWith(final Collideable other) {
        // Call collideWith on the other object.
        other.collideWith(this);
   }

    public void collideWith(final Asteroid asteroid) {
        // Handle Asteroid-Asteroid collision.
    }

    public void collideWith(final Spaceship spaceship) {
        // Handle Asteroid-Spaceship collision.
    }
}

class Spaceship implements Collideable {
    public void collideWith(final Collideable other) {
        // Call collideWith on the other object.
        other.collideWith(this);
    }

    public void collideWith(final Asteroid asteroid) {
        // Handle Spaceship-Asteroid collision.
    }

    public void collideWith(final Spaceship spaceship) {
        // Handle Spaceship-Spaceship collision.
    }
}
```

## Is this the fabled [Visitor pattern](/patterns/visitor_pattern.md)?

Not quite; the above example is rather too [tightly-coupling](/terms/tightly_coupled.md) because if we had more objects than just ```Asteroid``` and ```Spaceship```, and more functions than just ```collideWith()```, then we will be constantly having to _change_ our ```Collideable``` [Application Programming Interface (API)](/terms/application_programming_interface_api).
However, if the code above shows the only objects required, then perhaps the [Visitor pattern](/patterns/visitor_pattern.md) would be overkill to fix the Double dispatch problem in [Java](/language_specific/java/java.md) and other such [Object Oriented Programming (OOP)](/paradigms/object_oriented_programming_oop/object_oriented_programming_oop.md) languages.


## Is there an actual example of the [Visitor pattern](/patterns/visitor_pattern.md) in action?

[VisitorPattern](VisitorPattern).