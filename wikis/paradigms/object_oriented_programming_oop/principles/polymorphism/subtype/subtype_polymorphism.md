---
title:  Subtype polymorphism
author: Gregory Thomas
date:   2020-10-01
---

# Subtype polymorphism

## What is it?

Subtype polymorphism is a _type_ of [polymorphism](../polymorphism.md).


## What is a simple example of Subtype Polymorphism?

We have a ```Vehicle``` [supertype](/structures/software_entities/classes/types/super_types.md), and we have ```Boat```, ```Car```, ```Truck``` [child types](/structures/software_entities/classes/types/child_types.md).
With Subtype polymorphism, we can have a [list](/structures/software_entities/variables/data_types/derived/lists.md) that **requires** elements of data type ```Vehicle```, but instead **add instances of our _child_ types**:

```java
List<Vehicle> vehicles = new ArrayList<Vehicle>();
vehicles.add(new Boat(0));
vehicles.add(new Car(4));
vehicles.add(new Truck(6));
```

### What does this mean for a developer?

We can easily add _complex_ classes to a single [list](/structures/software_entities/variables/data_types/derived/lists.md) and easily handle them.

Assuming the ```Vehicle``` supertype has an [abstract](/modifiers/abstract.md) function ```getWheelCount()``` for all child classes to implement, a developer can return child class-specific data from our _generic_ list ```vehicles```:

```java
for(Vehicle veh : vehicles) {
	System.out.println("Current vehicle has " + veh.getWheelCount() + " wheel(s).")
}

// Output:
// 	Current vehicle has 0 wheel(s).
// 	Current vehicle has 4 wheel(s).
// 	Current vehicle has 6 wheel(s).
```

Thanks to Subtype polymorphism, **we as developers are able to write code in a more [abstract](../../abstraction.md) manner**.


### Is there another way we can describe this?

> This code is said to be (subtype) polymorphic, as the exact code that is executed _is determined by the sub class being referenced at runtime_.


## What is _subtyping_ in relation to Subtype polymorphism?

_Subtyping_ is the [verb](/english/verb.md) that _demonstrates_ Subtype polymorphism.


## What is the act of _subtyping_?

> To _subtype_ is to use a [child type](/structures/software_entities/classes/types/child_types.md) instance **in place** of a [supertype](/structures/software_entities/classes/types/super_types.md) instance.


## This sounds like a typical description of the general term "Polymorphism"...

Yes; people that describe [polymorphism](../polymorphism.md) typically end up describing Subtype polymorphism.


## The word "substitution" fits quite well here...

Yes! In which case we can think of the [Liskov substitution principle](/paradigms/object_oriented_programming_oop/solid_design_principles/liskov_substitution_principle.md) when we think of _subtyping_.


## What _terms_ and _structures_ are considered Subtype Polymorphism?

- [Dispatch](dispatch.md):
	- [Single dispatch](single_dispatch.md)
	- [Multiple dispatch](multiple_dispatch.md)
		- [Double dispatch](double_dispatch.md)
- [Function overloading](function_overloading.md)


## What are some other terms for Subtype polymorphism?

- Inclusion polymorphism
- _Subtyping_ (as mentioned above)