---
title:  Multiple dispatch
author: Gregory Thomas
date:   2020-09-30
---

# Multiple dispatch

## What is it?

Multiple [dispatch](dispatch.md) is a _type_ of [polymorphism](polymorphism.md).


## What is another name for Multiple dispatch?

- Multimethods


## Is Multiple dispatch the same as [function overloading](/structures/software_entities/functions/function_overloading.md)?

No.


## How does Multiple dispatch differ from [function overloading](/structures/software_entities/functions/function_overloading.md)? 

[Function overloading](/structures/software_entities/functions/function_overloading.md) is done at [_compile time_](/execution/compile_time.md), whereas Multiple dispatch is done at [_runtime_](/execution/runtime.md).