---
title:  Dispatch
author: Gregory Thomas
date:   2020-09-30
---

# Dispatch

## What is it?

Dispatch refers to the _dispatching_ of functions at [run-time](/execution/runtime.md).


## What types of dispatch are there?

- [Single dispatch](single_dispatch.md)
- [Multiple dispatch](multiple_dispatch.md)
	- [Double dispatch](double_dispatch.md) is a type of Multiple dispatch.


## What can affect the dispatching of a function?

- Whether the language supports a type of dispatch (e.g. Java supports [Single dispatch](single_dispatch.md) but not [Multiple dispatch](multiple_dispatch.md)).