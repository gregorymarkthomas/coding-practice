---
title:  Virtual table
author: Gregory Thomas
date:   2020-10-02
---

# Virtual table

## What is it?

A Virtual Table is a background software structure that each [class](/structures/software_entities/classes/classes.md) has in a typical [Object Oriented Programming (OOP)](../../../object_oriented_programming_oop.md) language.


## What is a Virtual Table used for?

A Virtual Table is used as part of the implementation to achieve [inheritance](../../inheritance.md) (also known as [subtype polymorphism](subtype_polymorphism.md)).