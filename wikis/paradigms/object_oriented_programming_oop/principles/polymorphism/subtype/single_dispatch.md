---
title:  Single dispatch
author: Gregory Thomas
date:   2020-09-24
---

# Single dispatch

## What is it?

Single [dispatch](dispatch.md) is a _type_ of [polymorphism](polymorphism.md).


## What are the basics of Single dispatch?

With Single [dispatch](dispatch.md), we can use _one_ [supertype](/structures/software_entities/classes/types/super_types.md) when instantiating multiple [concrete types](/structures/software_entities/classes/types/concrete_types.md) under that supertype.


## When executing an operation, what _properties_ are relied upon?

The operation executed depends on:

- the _name_ of the request
- the [type](/structures/software_entities/classes/types/types.md) of the **receiver** object.


## What does the [SingleDispatch](SingleDispatch/) Java application show?

The [SingleDispatch](SingleDispatch/) [Java](/language_specific/java/java.md) application shows Single dispatch **working** in Java; we are _declaring_ ```Dog``` and ```Cat``` objects as their supertype ```Animal```, and thanks to Single dispatch, we are able to have the correct output returned.

With thanks to 'The Simple Engineer' at [https://youtu.be/TeZqKnC2gvA?t=403](https://youtu.be/TeZqKnC2gvA?t=403).


### How is the application set up?

```Animal``` supertype has ```makeSound()``` function; this function will be _implemented_ by any [child types](/structures/software_entities/classes/types/child_types.md) of the ```Animal``` [supertype](/structures/software_entities/classes/types/super_types.md):

```java
public interface Animal {
    void makeSound();
}


public class Cat implements Animal {
	public void makeSound() {
		System.out.println("Meow");
	}
}


public class Dog implements Animal {
	public void makeSound() {
		System.out.println("Woof");
	}
}
```

In ```main```, we declare 'dog' and 'cat' **with the ```Animal``` supertype**. We instantiate 'dog' and 'cat' with their respective concrete types (```Dog``` and ```Cat```).
When both ```dog.makeSound()``` and ```cat.makeSound()``` are attempted, [Java](/language_specific/java/java.md) very kindly _finds the correct ```makeSound()``` implementation for each ```Animal``` child type and produces the correct output_:

```java
public class Runner implements Animal {
	public static void main(String[] args) {
	    Animal dog = new Dog();
	    Animal cat = new Cat();

	    dog.makeSound();	// outputs "Woof"
	    cat.makeSound(); 	// outputs "Meow"
	}
}
```