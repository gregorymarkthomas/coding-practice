package main.java;

public interface Animal {
    void makeSound(Dog d);
    void makeSound(Cat c);

    void makeSound(Animal a);
}
