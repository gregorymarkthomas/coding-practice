package main.java;

public class Cat implements Animal {
    @Override
    public void makeSound(Dog d) {
        System.out.println("Cat interacting with Dog");
    }

    @Override
    public void makeSound(Cat c) {
        System.out.println("Cat interacting with Cat");
    }

    @Override
    public void makeSound(Animal a) {
        System.out.println("Cat interacting with ANIMAL");
    }
}