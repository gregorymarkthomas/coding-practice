package main.java;

public class Dog implements Animal {
    @Override
    public void makeSound(Dog d) {
        System.out.println("Dog interacting with Dog");
    }

    @Override
    public void makeSound(Cat c) {
        System.out.println("Dog interacting with Cat");
    }

    @Override
    public void makeSound(Animal a) {
        System.out.println("Dog interacting with ANIMAL");
    }
}
