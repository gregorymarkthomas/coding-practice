package main.java;

public class main {

    /**
     * ATTEMPTED Double dispatch in action; we are trying to allow Dogs and Cats to interact
     * with each other.
     *
     * Animal has new makeSound() functions to attempt this interaction between Animals.
     *
     * Here we declare 'dog' and 'cat' with the Animal supertype,
     * and we instantiate them with their concrete types (Dog and Cat, respectively).
     *
     * When dog.makeSound(cat) is attempted, a type-check compilation error occurs.
     * dog.makeSound(cat) is expecting a concrete Cat input, but we have entered an Animal instead.
     *
     * We could create makeSound(Animal animal) in Animal, and implement it in Dog and Cat
     * (which I have done; see those classes). So, the error disappears..
     * but, the output shows "Dog interacting with ANIMAL", meaning out Cat input is NOT RECOGNISED.
     *
     * Basically, Double Dispatch fails because we cannot polymorphically resolve the concrete type at runtime.
     * We can fix this using the Visitor pattern.
     *
     * @param args
     */
    public static void main(String[] args) {
        Animal dog = new Dog();
        Animal cat = new Cat();

        dog.makeSound(cat);
        dog.makeSound(dog);
        cat.makeSound(cat);
        cat.makeSound(dog);
    }
}
