package main.java;

public class main {

    /**
     * Single dispatch/Polymorphism in action.
     *
     * Here we declare 'dog' and 'cat' with the Animal supertype,
     * and we instantiate them with their concrete types (Dog and Cat, respectively).
     *
     * When makeSound() is called on each instantiation, the correct Animal-specific
     * implementation is called dynamically.
     *
     * Output:
     * > Woof
     * > Meow
     *
     * @param args
     */
    public static void main(String[] args) {
        Animal dog = new Dog();
        Animal cat = new Cat();

        dog.makeSound();
        cat.makeSound();
    }
}
