---
title:  Function overloading
author: Gregory Thomas
date:   2020-09-30
---

# Function overloading

## What is it?

Function overloading is a _type_ of [polymorphism](/paradigms/object_oriented_programming_oop/principles/polymorphism/polymorphism.md).


## What are the basics of Function overloading?

Function overloading is the process of creating [functions](functions.md) **of the same name but with _different_ [input arguments](input_arguments.md)**.


## Why would you use Function overloading?

Function overloading is done more for code _readability_; it may be easier to understand and cleaner to look at if an _existing_ function was overloaded instead of creating separately named functions.


## What does Function overloading look like?

### [Java](/language_specific/java/java.md)

```java
public void foo(int x) {
	...
}

public void foo(String x) {
	...
}

public void foo() {
	...
}
```


### [C++](/language_specific/c_plus_plus/c_plus_plus.md)

```c++
  virtual void CollideWith(SpaceShip&) {
    cout << "Asteroid hit a SpaceShip" << endl;
  }
  virtual void CollideWith(GiantSpaceShip&) {
    cout << "Asteroid hit a GiantSpaceShip" << endl;
  }
```

## _When_ are overloaded functions created during the execution process?

Overloaded functions are created at [compile-time](/execution/compile_time.md).


## What is used to create overloaded functions?

[Naming mangling](/terms/naming_mangling.md) is used; _internal_ function names are used instead of the developer-defined one:


### [C++](/language_specific/c_plus_plus/c_plus_plus.md)

```function f(int x)``` becomes ```__f_i``` rather than ```f```.

```function f()``` becomes ```__f_v```

In usage, the call ```f(123)``` will be internally interpreted as ```__f_i(123);```.

#### What does the ```i``` and the ```v``` mean?

```i``` for ```int```, ```v``` for ```void```.