---
title:  Polymorphism
author: Gregory Thomas
date:   2020-03-13
---

# Polymorphism

## What is it?

A *principle* synonymous with [Object Oriented Programming](../object_oriented_programming_oop.md)


## What does it do/mean?

Allows us to **present the same interface for _differing_ data types**.


## Is Polymorphism a _general_ term?

Yes; there are _specific_ types of Polymorphism.


## What are the _types_ of Polymorphism?

- [Subtype polymorphism](subtype/subtype_polymorphism.md)
- [Ad-hoc polymorphism](adhoc/adhoc_polymorphism.md)
- [Parametric polymorphism](parametric/parametric_polymorphism.md)


## Which of these _types_ is typically used to describe the general term of Polymorphism?

[Subtype polymorphism](subtype/subtype_polymorphism.md).