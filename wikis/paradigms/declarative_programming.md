---
title:  Declarative programming
author: Gregory Thomas
date:   2020-03-15
---

# Declarative programming

## What is it?

A *programming paradigm*.

Declarative programming declares **what the program should accomplish** (hence the name), *without* specifying *how* it should achieve the result. It is left up to the declarative programming language's implementation to accomplish the *how*.

It is the lesser-used paradigm; [imperative programming](imperative_programming.md) is more mainstream as [object oriented programming](object_oriented_programming.md) languages use it.