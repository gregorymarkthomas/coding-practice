---
title:  "Cracking the Coding Interview" book
author: Gregory Thomas
date:   2020-03-15
---

# "Cracking the Coding Interview" book

## What is it?

A book recommended by the Computer Engineering industry; it is common to encounter questions during interviews that are found in the book.

The book was written by [Gayle Laakmann McDowell](http://www.gayle.com/).