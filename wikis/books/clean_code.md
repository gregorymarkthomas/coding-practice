---
title:  "Clean Code" book
author: Gregory Thomas
date:   2020-05-29
---

# "Clean Code" book

## What is it?

A book recommended by the Computer Engineering industry.

It is considered _the_ book to read for knowing how best to design and write your code.

The book was written by [Robert Cecil Martin](https://blog.cleancoder.com/).


## What are the key points?

- Organisation (_sort_)
	- Knowing where things are, and descriptive naming.
- Tidiness (_systematise_)
	- _"A place for everything, and everything in its place."_
	- A piece of code should be where you would expect to find it.
- Cleaning (_shine_)
	- Remove most (if not all) comments, especially TODOs and historic code
- Standardisation
	- Everyone in the team agrees on the same workflow/cleaning/style
- Discipline
	- Be willing to change and evaluate your code


## What is code?

"...code is really the language in which we ultimately express the requirements".


## Making the deadline

"You will not make the deadline by making the mess. Indeed, the mess will slow you down instantly, and will force you to miss the deadline. The only way to make the deadline — the only way to go _fast_ — is to keep the code as clean as possible at all times."


## What _is_ clean code?

Clean code, essentially, is code that can be easily understood just by reading it.


## Ideas from significant figures in Computer Science

- "pleasing"
	- Think how _pleasing_ a well-designed appliance or car is; why can't software provide that feeling?
- "efficiency"
	- wasted cycles are _not_ pleasing
- "tempt"
	- Bad code _tempts_ the mess to grow; when others change bad code, they usually make it worse.
	- "Broken windows theory"
- close attention to detail
	- "error handling _complete_"
	- memory leaks
	- race conditions
	- inconsistent naming
- "Clean code does one thing well"
	- Bad code tries to do too much
	- clean code is _focused_
	- each function, class, module remains undistracted and unpolluted by surrounding details 
- smaller is better
- is readable by humans
	- but should be easy for _other_ people to enhance it; there is a difference between the two
- has and _runs_ all the tests
	- thanks to [Test Driven Development (TDD)](/development_types/test_driven_development.md) this is more common today
- contains no duplication
- minimises classes/methods/functions etc
- it is written with _care_
- _early_ building of simple abstractions
	- "all programs of made up of simlar elements... 'find things in a collection' etc."
	- create an abstract method/class instead of creating a simple dumb hash map solution now
	- "the collection abstraction often calls my attention to what's 'really' going on, and keeps me from running down the path of implementing arbitrary collection behaviour, **when all I really need is a few fairly simple ways of finding what I want**."
- the programmer makes the language appear simple


## What do you mean by "we are authors"?

The ```@author``` field in Javadoc tells us who we are. We are the authors of this code, and we have _readers_. We as authors are responsible for communicating well with our readers.


## What is the reading vs writing ratio?

We are constantly reading old code as part of the effort to write new code.
We want reading of code to be easy, even if it makes writing more difficult.
Make your code easy to read.


## "Leave the campground cleaner than you found it"

Code has to be kept clean over time. Code rots and degrades. We should prevent this by improving a variable name, break up a function if too large, eliminate a small piece of duplication, or clean up one composite if statement.