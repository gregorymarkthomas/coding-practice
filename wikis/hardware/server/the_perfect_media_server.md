# Servers

## Technologies

- MergerFS
- snapRAID
- LVM
- RAID (?)


## Storage setups to try

- MergerFS for two disks
- MergerFS + snapRAID (or normal hardware RAID) for three disks (plus LVM on-top?)
- OR, just ZFS? (see https://joefallon.net/2017/07/warning-to-mergerfs-snapraid-users/)


## OS setups to try

- https://forum.openmediavault.org/index.php?thread/19553-omv-docker-plugin-media-server-plex-plexpy-ombi-libresonic-nzbget-rutorrent-sona/


## Links

- https://blog.linuxserver.io/2016/02/02/the-perfect-media-server-2016/
- https://www.teknophiles.com/2018/02/19/disk-pooling-in-linux-with-mergerfs/
- https://selfhostedhome.com/combining-different-sized-drives-with-mergerfs-and-snapraid/
- https://joefallon.net/2017/07/warning-to-mergerfs-snapraid-users/