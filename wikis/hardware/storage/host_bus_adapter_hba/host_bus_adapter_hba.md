# Host Bus Adapter (HBA)

## What is this?

A Host Bus Adapter (HBA) is a means of connecting **different protocols** to a system.


## What protocols?

- network
- storage


## What is the physicality of an HBA?

The HBA is a piece of hardware (normally as a [Peripheral Component Interconnect (PCI)](/hardware/io/peripheral_component_interconnect_pci/peripheral_component_interconnect_pci.md)card) used to _extend_ the system's capabilities.


## What are some examples of HBAs?

- [Serial AT Attachment (SATA)](/hardware/storage/protocols/serial_at_attachment_sata.md) HBAs
- [Serial Attached SCSI (SAS)](/hardware/storage/protocols/serial_attached_scsi_sas.md) HBAs


## Is an HBA the same as a [Network Interface Card (NIC)](/hardware/networking/network_interface_card_nic.md)?

No.


## Is a [RAID controller](/hardware/storage/raid/raid_controller.md) an HBA?

There are some overlap between these two devices; [RAID controllers](/hardware/storage/raid/raid_controller.md) _can_ have HBA-like functionality.


### What features overlap between RAID controllers and HBAs?

RAID controller may have:

- a [JBOD](../just_a_bunch_of_disks_jbod.md) mode (though this may just be a [single-disk RAID-0](../raid/single_disk_raid.md) scenario)


HBA may have:

- basic [RAID](../raid/redundant_array_of_inexpensive_disks_raid.md) functionality (but will never be as good as a dedicated [RAID controller](/hardware/storage/raid/raid_controller.md)) 