# MergerFS

## What is it? 

> "MergerFS is a union filesystem, similar to mhddfs, UnionFS, and aufs. MergerFS enables the joining of multiple directories that appear to the user as a single directory. This merged directory will contain all of the files and directories present in each of the joined directories. Furthermore, the merged directory will be mounted on a single point in the filesystem, greatly simplifying access and management of files and subdirectories. And, when each of the merged directories themselves are mount points representing individual disks or disk arrays, mergerFS effectively serves as a disk pooling utility, allowing us to group disparate hard disks, arrays or any combination of the two."


## Links

- [https://www.teknophiles.com/2018/02/19/disk-pooling-in-linux-with-mergerfs/](https://www.teknophiles.com/2018/02/19/disk-pooling-in-linux-with-mergerfs/)