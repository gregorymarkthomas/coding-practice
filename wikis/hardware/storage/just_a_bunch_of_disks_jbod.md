# Just a Bunch Of Disks/Drives (JBOD)

## What is this?

Just a Bunch Of Disks/Drives (JBOD) refers to a _collection_ of harddrives that are **independent**.


## Independent of what?

Well, they are not collected together in a [RAID](raid/redundant_array_of_inexpensive_disks_raid.md) array.


## Is a [single-disk RAID-0](raid/single_disk_raid.md) the same as JBOD?

No; some [RAID controllers](raid/raid_controllers.md) _claim_ to allow JBOD disks amongst disks in RAID arrays, but single-disk RAID-0 arrays are still RAID arrays, and have metadata written to them.


## Can JBOD disks be grouped outside of [RAID](raid/redundant_array_of_inexpensive_disks_raid.md)?

Yes, using a [Logical Volume Manager (LVM)](logical_volume_manager/logical_volume_manager.md).