# Single-disk RAID

## What is this?

When [Redundant Array of Inexpensive Disks (RAID)](raid.md) is in use, it can be applied to _single_ harddrives as it's own array.


## Why would you do this?

Say you have _all_ your disks connected to your RAID controller; maybe you want _one_ of these disks to not be in a RAID.


## What [RAID](raid.md) type can be use for a single disk?

[RAID-0](raid_0.md). 


## Will you lose data if you apply an already-filled disk to it's own RAID?

**Probably**, as RAID metadata is required on each disk that is RAIDed.


### What evidence suggests that data loss will occur?

Nimral at [serverfault.com](https://serverfault.com/questions/29349/disabling-raid-feature-on-hp-smart-array-p400) says:

> "It should also be noted, just because I just killed 7TB of data by following LapTop006's, well, lets call it "personal opinion", that a P400 Controller would expose unassigned disks as JBOD, that this is nothing but a guess, and it is false, at least for my P400. There may be other controllers behaving like LapTop006 said, the P400 does not, at least not with the original firmware (V2.75).

> I learned this the hard way today when trying to bring over a 6 disc software RAID-5 from a machine with a faulty 6 channel SATA RAID controller. They had always been part of a software RAID, the RAID functionality of the ICH9 "Fake" RAID controller had never been used anyway.

> The target machine didn't have enough SATA ports, so I thought, well, no problem, it is a SOFTWARE RAID anyway, why not attach the disks to a P400, the disks would - if the controller behaved like stated - appear as JBOD, and the OS would - like it had done many times before when I moved software RAIDs from one machine to another - recognize the RAID.

> In my case, however, the P400 did recognize the disks as new and - without seeking my confirmation - it did auto-create a RAID-5 array at the controller level as soon as I powered up the computer. Bye-Bye software RAID.

> I brought the disks back to the original machine, but the RAID had already been corrupted, the OS saw 6 empty disks now.

> Bye, 7TB of data.

> Damage already done, I played with the disks a little bit. Back at the P400 equipped machine, I deleted the unwanted RAID-5, the disks didn't appear at the OS level. I had to create 6 RAID-0 disks, and they appeared - all empty, however.

> Conclusions:

>- The P400 does not pass unassigned disks to the OS.
>- You need to create RAID-0 configs to get the disks through to the OS.
>- Saving the RAID-0 config (or any other P400 config) will empty the disk(s).
>- The P400 auto-config may have killed anything on the disks anyway, by creating a RAID-5 without asking permission."


## Is a single-disk RAID-0 volume equivalent to a [Just a Bunch of Disks/Drives (JBOD)](../just_a_bunch_of_disks_jbod/just_a_bunch_of_disks_jbod.md)?

No; as mentioned, RAID metadata is written to any disk in a RAID.


## So does that mean that you cannot disable the RAID of a RAID controller by using RAID-0 on each individual disk?

No; see above.


### Why would I want to do this anyway?

Arie K at [https://serverfault.com/questions/29349/disabling-raid-feature-on-hp-smart-array-p400](sererfault.com) wants to let [Zettabyte File System (ZFS)](/hardware/storage/filesystems/zettabyte_file_system_zfs.md) manage the disks in his system _instead_ of the HP Smart Array P400 card that is currently used. Arie wanted to _disable_ the RAID of the card to let ZFS manage the disks instead, and one "solution" was to set each individual disk to it's own RAID-0 array; however, this would probably produce unexpected results.


### How would I get around this?

Use a dedicated [Host Bus Adapter (HBA)](/hardware/storage/host_bus_adapter_hba/host_bus_adapter_hba.md) for [Just a Bunch of Disks/Drives (JBOD)](../just_a_bunch_of_disks_jbod/just_a_bunch_of_disks_jbod.md) disks. 