# Redundant Array of Inexpensive Disks (RAID)

## What is this?

Redundant Array of Inexpensive Disks (RAID) is a way of _grouping_ multiple [hard drive disks](../hard_drive_disk.md) together in an array for the speed and/or reliability to match that of a more expensive disk.


## How can we enable RAID? 

### Software

- FlexRAID
- others


### Hardware

- [SATA-based RAID](sata_raid.md): RAID is enabled via the [motherboard's](/hardware/motherboard/motherboard.md) [BIOS](/hardware/motherboard/bios/bios.md) by setting the [SATA operating mode](/hardware/motherboard/sata/operating_mode.md) to 'RAID'.
- A dedicated [PCI](/hardware/io/pci/pci.md)-based RAID controller card


## What types of RAID are there?

### Standard

- [RAID-0](raid_0.md)
- [RAID-1](raid_1.md)
- [RAID-2](raid_2.md)
- [RAID-3](raid_3.md)
- [RAID-4](raid_4.md)
- [RAID-5](raid_5.md)
- [RAID-6](raid_6.md)

### Nested/hybrid

- [RAID-10 / RAID-1+0](raid_10.md)