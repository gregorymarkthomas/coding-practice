# Parity disk

## What is it?

In a [Redundant Array of Inexpensive Disks (RAID)](redundant_array_of_inexpensive_disks_raid.md) setup, a dedicated drive can be used for **fault tolerance**.


## How does this differ to the parity spread across drives for various RAID types?

Parity is stored on a dedicated drive as opposed to it being spread across several drives in the array.


## What uses a dedicated Parity disk?

- [RAID-4](raid_4.md)
- [unRAID](alternatives/unraid.md)
- [snapRAID](alternatives/snapraid.md)


## How many drives are required for a parity drive?

At least 2 **plus** the parity drive.


## How big a parity drive do we need?

For **single** parity, the size of a parity drive should **match the biggest storage drive's size**.


## What will single parity protect against?

Single parity will allow our system to have **one** drive fail.


## What about double parity?

Double parity will allow our system to have **two** drives fail.


## What happens when a drive fails?

