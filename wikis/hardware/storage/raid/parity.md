# Parity

## What is it?

In a [Redundant Array of Inexpensive Disks (RAID)](redundant_array_of_inexpensive_disks_raid.md) setup, _parity_ is a simple form of **error detection**.


## What is Parity's form?

Parity is in the form of a Parity _bit_.


## Where does this Parity bit live?

The parity bit is added to a string of binary code as part of the message or separately.


## What does parity allow for a RAID?

A disk in RAID is allowed to _fail_ **without** loss of data.


## What faults might occur in a [RAID](redundant_array_of_inexpensive_disks_raid.md)?

In a [RAID-5](raid_5.md) setup, a single drive might fail and cause errors.


## What is another name for Parity bit?

- Check bit