# S.M.A.R.T

## What is it?

S.M.A.R.T (**S**elf-**M**onitoring, **A**nalysis and **R**eporting **T**echnology) is used to test the _health_ of a harddrive.


## How do I view S.M.A.R.T information about a harddrive?

### Linux

### Ubuntu

[Check here](/operating_system/linux/distributions/ubuntu/maintenance/disk_health.md).