# Zettabyte File System (ZFS)

## What is this?

Zettabyte File System (ZFS) is a [file system](file_system.md).


## Well done. What's so special about it?

ZFS is both a [file system](file_system.md) _and_ a [volume manager](../volume_manager/volume_manager.md); we can create a _single_ file system spanning _multiple_ disks.


## What operating systems support ZFS?

- [FreeBSD](/operating_system/bsd/freebsd.md)
- [Ubuntu 19.10](/operating_system/linux/distributions/ubuntu/versions/19_10.md)