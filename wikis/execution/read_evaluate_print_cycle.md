---
title:  Read-Evaluate-Print cycle
author: Gregory Thomas
date:   2020-03-20
---

# Read-Evaluate-Print cycle

## What is it?

A term which describes the process of programming in languages such as [Python](../language_specific/python/python.md). 


## What are some advantages to this process?

Python code can be entered into its console to achieve an instantaneous response; this is useful for quick data science tasks as writing the equivalent code using a [**write-compile-test-recompile**](/execution/write_compile_test_recompile_cycle.md) cycle language could take a lot longer to develop.


## What are some disadvantages to this process?

It lacks the layer of safety the compilation process brings; errors in the code will occur at *runtime* instead.
