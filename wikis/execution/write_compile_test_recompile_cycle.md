---
title:  Write-Compile-Test-Recompile cycle
author: Gregory Thomas
date:   2020-03-20
---

# Write-Compile-Test-Recompile cycle

## What is it?

A term which describes the process of programming in languages such as [C](../language_specific/c/c.md), [C++](../language_specific/c_plus_plus/c_plus_plus.md) and [Java](../language_specific/java/java.md). 


## What are some advantages to this process?

The compilation stage highlights errors before the application is run, providing extra safety.


## What are some disadvantages to this process?

Development time takes longer than the [**read-evaluate-print**](/execution/read_evaluate_print_cycle.md) due to (re)compilation of the code.
