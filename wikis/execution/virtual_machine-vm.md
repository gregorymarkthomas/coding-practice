---
title:  Virtual machine
author: Gregory Thomas
date:   2020-03-15
---

# Virtual machine

## What is it?



These Virtual Machines take [Bytecode](bytecode.md) and runs/interprets/translates it into native [machine code](machine_code.md).

It is **NOT** an **[interpreter](interpreter.md)**.

## What are some examples of Virtual Machines?

- [Java Virtual Machine (JVM)](/language_specific/java/execution/java_virtual_machine.md)