--
title:  Intermediate Representation (IR) and Intermediate language
author: Gregory Thomas
date:   2020-03-15
---

# Intermedate Representation (IR) and Intermediate language

## What is it?

Intermedate Representation (IR) is **code** used *internally* by a [compiler](compiler.md) or [virtual machine](virtual_machine.md) to represent source code.


## What is Intermediate language, then?

They are languages that some [high-level programming languages](/terms/high_level_programming_language.md) output **instead** of executable machine code.

A separate compiler translates the Intermediate language into machine code.


## Why is this useful?

Mostly to increase *portability*; the *separate* compiler can translate the Intermediate language into machine code *for whatever processor and/or operating system is needed*. 