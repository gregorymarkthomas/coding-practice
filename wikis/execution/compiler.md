---
title:  Compiler
author: Gregory Thomas
date:   2020-03-15
---

# Compiler

## What is it?

A compiler is a **computer program**; it **translates** *source code* from one language to another. 

This normally refers to the translation of source code from a [high-level programming language](/terms/high_level_programming_language.md) to a [low-level programming language](/terms/low_level_programming_language.md) **to create an executable program**.


## What is an example of a Compiler in the real-world?

[Javac](/language_specific/java/execution/javac.md) compiles Java code into [bytecode](/terms/bytecode.md), ready for the [Java Virtual Machine (JVM)](/language_specific/java/execution/java_virtual_machine.md).