---
title:  Modulo
author: Gregory Thomas
date:   2020-07-17
---

# Modulo

## What is it?

Modulo (**%** in most programming languages) is an _operator_ that produces the **remainder**.


## The remainder of what?

The modulo operation produces the remainder of the **[Euclidean](/mathematics/euclidean.md) division** of the two numbers surrounding the operator.


## So, we divide the left number by the right, right?

Left. I mean, right.


## What is an example modulo operation?

```
9 % 4
```
First we do the [Euclidean](/mathematics/euclidean.md) division of 9 by 4:

```
9 / 4 = 2 
```

```4``` goes into ```9``` only twice (```4 * 2 = 8```).

So what is the remainder? Well, if ```4``` goes into ```9``` twice, this will equal ```8``` and the remainder will be ```1```.

```
9 % 4 = 1
```


## What if the right-hand-side is greater than the left-hand-side?

```
4 % 9
```
First we _try_ to do the [Euclidean](/mathematics/euclidean.md) division of 4 by 9, but this will return **zero**.

So, what remains? Well, ```4```, so:

```
4 % 9 = 4
```


<!--## What about a negative number?

```
9 % -4
```
First we do the [Euclidean](/mathematics/euclidean.md) division of 9 by -4:

```
9 / -4 = 2 
```

```4``` goes into ```9``` only twice (```4 * 2 = 8```).

So what is the remainder? Well, if ```4``` goes into ```9``` twice, this will equal ```8``` and the remainder will be ```1```.

```
9 % 4 = 1
```
-->

## How is modulo used in Computer Science?

An example is a function to return whether an input int ```x``` [is an odd number](/algorithms/tips/is_number_odd.md).