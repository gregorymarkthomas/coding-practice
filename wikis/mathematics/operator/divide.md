---
title:  Divide
author: Gregory Thomas
date:   2020-07-22
---

# Divide

## What is it?

Divide (**/**, a forward slash, in most programming languages) is an _operator_ that produces the **division** of two numbers.


## Okay. What is an easy way to define _division_?

```
double z = x / y
```

We want to know how many ```y```s there are in ```x```.


## Does the order of the numbers matter?

Yes; the left-most value is divided by the right-most.


## What is an example usegae of the division operation?

```
int i = 16 / 4
print(i)

# 4
```


## What if the _right_-most value is greater than the left-most?

```
double i = 4 / 16
print(i)

# 0.25
```

Division will work as expected, however, **we must ensure that the result is stored in the correct data type!** 


### What must we be aware of?

If the result above is stored as an [integer](/data_types/integers.md), then we will **lose [precision](/structures/software_entities/data_types/caveats/precision.md)**; the result will be ```0``` and will lack decimal places.


### So, we use a [double](/structures/software_entities/data_types/primitive/double.md) instead?

Yes, or a [float](/structures/software_entities/data_types/primitive/float.md); whatever is the most suitable [floating-point representation](/mathematics/floating_point_representation.md) data type.


## What about negative division?

Let's produce a _negative_ whole number:

```
int i = 16 / -4
print(i)

# -4
```
A _negative_ whole number can be stored as an [integer](/data_types/integers.md) without loss of accuracy, **provided the integer is a [signed](/structures/software_entities/data_types/caveats/signed_vs_unsigned.md) integer**.


## What about a negative division producing a decimal point?

If the result had a decimal point, then choose a **[signed](/structures/software_entities/data_types/caveats/signed_vs_unsigned.md)** data type for the result which has a [floating-point representation](/mathematics/floating_point_representation.md).


## How do we explain what negative division is?

```
int x = 10
int y = -2
double z = x / y
```

We're doing the same as before; we're working out how many ```y```s there are in ```x```. In the example above, we're saying that ```z``` holds the number of times ```-2``` fits into ```10```.


## Is there a rule for negative division worth knowing?

Yes; **a positive number divided by a negative number (or vice versa) is always negative**.


## Okay, what is an easy way to remember negative division?

If there is **one** minus sign between two inputs:

>>> Do the division without any minus signs, then add a minus sign to the answer.


## What about if _both_ inputs are negative?

**A negative divided by a negative always equals a positive.**


## So...

So, just do the division as if there were no minus values.


## Can we think of it like "how many Ys in X" for negative values?

Yes and no; it's difficult to picture in our minds as, say, a pie chart. But we can always just think of the pie chart when using our negative division-tricks; these tricks mentioned above involve dividing our numbers as positive numbers anyway.


## Okay, what about dividing with fractions?

```
{\frac {5}{8}}\div -4
=58÷−41{\displaystyle ={\frac {5}{8}}\div {\frac {-4}{1}}}={\frac {5}{8}}\div {\frac {-4}{1}}
=58×−14{\displaystyle ={\frac {5}{8}}\times {\frac {-1}{4}}}={\frac {5}{8}}\times {\frac {-1}{4}}
=−532{\displaystyle ={\frac {-5}{32}}}={\frac {-5}{32}}
```

**Make the whole number into a fraction.**

**Then, convert the fraction division into a fraction multiplication by turning the divider upside down**; this is called *multiplying by the reciprocal*.


## And negative fractions?

The same, but follow the rules mentioned above, particularly that one minus value in the division will cause a negative result:

```
{\frac {-5}{8}}\div -4
=−58÷−41{\displaystyle ={\frac {-5}{8}}\div {\frac {-4}{1}}}={\frac {-5}{8}}\div {\frac {-4}{1}}
=−58×−14{\displaystyle ={\frac {-5}{8}}\times {\frac {-1}{4}}}={\frac {-5}{8}}\times {\frac {-1}{4}}
=532{\displaystyle ={\frac {5}{32}}}={\frac {5}{32}}
```