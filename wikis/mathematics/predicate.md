---
title:  Predicate
author: Gregory Thomas
date:   2020-07-17
---

# Predicate

## What is it?

A predicate (PRED-i-cat) is the part of a sentence that contains the _verb_ and tells us something about the subject.


## What is an example sentence that contains a predicate?

"Mike is eating."


## Okay, so what's what?

"Mike" is the _subject_.


## And "is eating" is the _predicate_?

Correct; it is the part of the sentence that tells us something about the subject.


## Okay, so, what is a predicate in the context of Computer Science?

Well, the same! But in CompSci we have no need to just state the fact that Mike is eating; we _do something_ on whether that fact is _true_ or _false_:

```
Person mike;

if (!mike.isEating())
	feedPerson(mike);
```

## So, the [instance variable](/structures/software_entities/variables/instance_variables.md) ```isEating()``` is considered a "predicate" because it returns a [boolean](/structures/software_entities/varibles/data_types/primitive/boolean.md)?

Yes.


## Can we use the term "predicate" for any function that returns a [boolean](/structures/software_entities/varibles/data_types/primitive/boolean.md) based on the evaluation of the truth of an assertion?

Yes.


## So, the predicate is used here to determine whether we should feed the person or not?

Correct.


## Where else are predicates found?

Callbacks.


## But there is another "predicate" used in CompSci, isn't there?

Yes; the other one is pronounced "predi-KATE", and it means that "one thing depends on another":

> "Graduation is predicated upon attainment of passing grades"


## And how is _that_ "predicate" used in CompSci?

We use it to describe **conditional execution**:

> We can set a predicate (PRED-i-cat) flag that, if true, causes an instruction to be executed, and if false causes instruction to be treated as a NOP.
> > Thus, the execution of the instruction is _predicated_ (predi-KATED) upon the indicated _predicate_ (PRED-i-cat) flag.