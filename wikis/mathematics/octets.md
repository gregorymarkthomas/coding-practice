# Octets

## What are they?

An octet is a _unit_.


## In what field is an octet used?

Octets are used in computing and telecommunications; they are units of digital information.


## What does a single octet consist of?

A single octet consists of 8 [bits](bit.md), very much like a **modern** [byte](byte.md).


## So why do we have both [bytes](byte.md) AND octets? Why only a "modern" byte?

The term 'byte' has represented different _sizes_ (i.e. not 8 bits) in the past. But, the term **oct**et always represents **8** bits (the Romans named October as the eighth month of the year before January and February were added).
Nowadays the terms 'octet' and ['byte'](byte.md) can be used interchangeably, but those working on legacy systems should be wary of how many bits that system deems to be a [byte](byte.md).