---
title:  Bounds
author: Gregory Thomas
date:   2020-03-13
---

# Bounds

## What are they?


## What is a Lower Bound?

Has to be less than or equal to all members of the set
E.g. s = {2, 3, 5, 7, 9, 12, 17, 42}

A lower bound could be 1.99, -3592, etc
The tight lower bound would be 2. I assume it is because it is the lowest actual value in the set
A set with a lower bound is said to be bounded from below


## What is an Upper Bound?

Has to be more than or equal to all members of the set
E.g. s = {2, 3, 5, 7, 9, 12, 17, 42}
An upper bound could be 43, 3592, etc
The tight upper bound would be 42. I assume it is because it is the highest actual value in the set
A set with an upper bound is said to be bounded from above
