---
title:  Asymptotes
author: Gregory Thomas
date:   2020-03-13
---

# Asymptotes

## What are they?

A line is **Asymptotic** (as-im-tot-ik) if it is a line that a curve *approaches* as it heads towards infinity


## What is special about it?
The line is considered asymptotic if the distance between the curve and the line tends to/approaches zero as they both head to infinity.

One or both of the x OR y coordinates tends to infinity


## What is a Horizontal Asymptote
As x goes to infinity/-infinity, the curve approaches some constant value b

![horizonal_asymptote.png]


## What is a Vertical Asymptote
As x approaches some constant value c (from left or right), the curve heads towards infinity/-infinity

![vertical_asymptote.png]


## What is an Oblique Asymptote
As x heads to infinity/-infinity, the curve heads towards a line y=mx+c

![oblique_asymptote.png]