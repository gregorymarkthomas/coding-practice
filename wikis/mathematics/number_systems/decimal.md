---
title:  Decimal
author: Gregory Thomas
date:   2020-11-16
---

# Decimal

## What is it?

Decimal is a number system.


## What is decimal's [base](base.md)?

**10**.



## What does this mean?

There are **10** distinct symbols to represent **all** numbers.



## What's the highest _number_ I can represent with _one_ decimal digit?

A _single_ decimal digit can represent numbers 0 to **9**.

- 0 = 0
- 1 = 1
- 2 = 2
- 3 = 3
- 4 = 4
- 5 = 5
- 6 = 6
- 7 = 7
- 8 = 8
- 9 = 9
