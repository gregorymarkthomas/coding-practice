---
title:  Hexadecimal
author: Gregory Thomas
date:   2020-11-16
---

# Hexadecimal

## What is it?

Hexadecimal is a number system.


## What is hexadecimal's [base](base.md)?

**16**.


## What does this mean?

There are **16** distinct symbols to represent **all** numbers.


## What's the highest _number_ I can represent with _one_ hexadecimal digit?

A _single_ hexadecimal digit can represent numbers 0 to **15**.


## If 0 to 9 is represented normally, how is 10 to 15 represented without two values?

- 10 = A
- 11 = B
- 12 = C
- 13 = D
- 14 = E
- 15 = F


## How does this compare to [decimal](decimal.md)?

[Decimal](decimal.md) only has _10_ symbols (0-9), its maximum number with a single decimal being 9.


## Say we want to represent hexadecimal on a computer system; how many [bits](/data/computer_number_format/binary_digit_bit.md) are required to represent a **single** hexadecimal digit?

**4** bits (i.e. a [nibble/nybble](/data_computer_number_format/nibble.md)).


## And 4 bits can represent a maximum of...?

4 bits can have values ranging from 0 0 0 0 to 1 1 1 1:

1 1 1 1 in [decimal](decimal.md) = **15**.

15 in hexadecimal = **F**.


## So, what can we fit into a [byte](/structures/software_entities/types/primitives/byte.md) of data?

We can fit **two hexadecimal digits** (seeing as they're 4 bits in size each).


## What's the maximum number that can be represented with two hexadecimal digits?

Two hexadecimal digits would be **FF**.

_One_ **F** is 15.

**FF** would be **255**


## Why would FF equal 255?

The right-most F digit is similar to a [decimal](decimal.md)'s _units_, but instead of [base](base.md) _10_, we're using base **16**.

The left-most F digit is similar to decimal's _tens_, except (again) we're dealing with _sixteens_!

F = 15

```FF = 15*16 + 15*1 = 255```


## What number would be represented by _FFF_, then?

The middle F digit is (still) our _tens_ (but _sixteens_).

The left-most F digit is similar to [decimal](decimal.md)'s _hundreds_, but we are still dealing with sixteens!

F = 15

```FFF = 15*256 + 15*16 + 15*1 = 4095```


## Why is the left-most F of FFF 256?

Because, like [decimal](decimal.md) (hundreds, tens, units, in base _10_), we are doing the same but with base _16_ instead.

Units -> tens -> hundreds = multiplying by 10

```10^2 10^1 10^0```
=
```100 10 1```

We do the same for hexadecimal, but with 16 instead of 10:

```16^2 16^1 16^0 ```
=
```256 16 1```