# Binary

## What is it?

Binary is a number system.


## What is binary's [base](base.md)?

**2**.


## What does this mean?

There are **2** distinct symbols to represent **all** numbers.


## What's the highest _number_ I can represent with _one_ binary digit?

A _single_ binary digit can represent numbers 0 to **1**.

- 0 = 0
- 1 = 1


## How does this compare to [decimal](decimal.md)?

[Decimal](decimal.md) has _10_ symbols (0-9), its maximum number with a single decimal being 9.


## So, 1 binary digit can represent a maximum of...?

1 binary digit can have values ranging from 0 to 1:

1 in [decimal](decimal.md) = **1**.


## What is 1 _binary digit_ also known as?

A [bit](/data/computer_number_format/binary_digit_bit.md).


## How do we represent _negative_ numbers in the binary number system?

We just add a minus ('-') to the start of our binary number.


## What about _negative_ numbers in the binary system in a _computer_ system?

Read more on the [bits](/data/computer_number_format/binary_digit_bit.md) page.