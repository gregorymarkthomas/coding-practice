# Python project to run in Ubuntu VM

This is a guide that could be used for 'wellcow' project:

1. Install VirtualBox with bridged adapter networking enabled
2. Get Ubuntu 16.04 LTS (pip cannot install anything on 14.04 due to SSL issue...)
3. Install with guest application option and 10gb virtual drive
	- 5GB not enough space
4. Ensure VM starts, then power off.
5. In VirtualBox Manager, enable Bridged adapter
	- highlight new VM
	- Machine -> Settings -> Network
	- Under 'Adapter 2' tab, set "Attached to:" to "Bridged Adapter"
6. Start VM
7. Enable 'Copy and Paste' between host and client
	- In VM window, Devices -> Shared Clipboard -> Bidirectional
8. Add user to sudoers
	- `su root`
	- `visudo`
	- `<user> ALL=(ALL) NOPASSWD:ALL`
9. `sudo ufw enable`
10. `sudo ufw allow ssh`
11. Run `sudo apt update && sudo apt upgrade`
12. Run `ssh-keygen`
13. Create 'authorized_keys' file in `~/.ssh/`
14. Get public key on HOST, paste into VM 'authorized_keys'
15. Run `ip a` to see IP address
16. `mkdir ~/src`
17. On Host, `scp -r <home>/<project> <user>@192.168.??.??:/home/<user>/src/`
18. Install pip for Python 2.7 on VM
	- `wget https://bootstrap.pypa.io/pip/2.7/get-pip.py`
	- `python get-pip.py`
19. Add Google DNS to resolv.conf
	- `sudo nano resolv.conf`
	- Add `nameserver 8.8.8.8`
	- `ping google.co.uk` to test
20. `sudo apt-get install build-essential libssl-dev libffi-dev python-dev`
21. Install requirements.txt in project
	- `pip install -r requirements.txt`