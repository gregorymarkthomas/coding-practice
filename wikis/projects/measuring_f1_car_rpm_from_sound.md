# Measuring engine Revolutions Per Minute (RPM) of a Formula 1 car via audio

## What techniques were used?

- [peak picking](/sound/frequency/calculation/peak_picking.md)
- [autocorrelation](/sound/frequency/calculation/autocorrelation_autocovariance.md)
- [cepstral analysis](/sound/frequency/calculation/cepstral_analysis.md)


## Results

- Peak finding in time domain 
  - Was least accurate due to inability to find amplitude peaks when the engine's RPM was anything below 15000 RPM; the engine's amplitude peaks were not impactful enough in the audio stream compared to the road and wind noise, and the peak finding algorithm overestimated the RPM of the engine due to noting too many amplitude peaks. Even high RPMs with radio speech caused this issue.
- Autocorrelation 
- Cepstral
  - Window size of 128 samples produces accurate RPM readings when the sound energy is high (15000+ RPM); the peaks appear early within the cepstrum
  - For lower sound energies, the peaks do not show in the cepstrum with a 128 sample window
- 1024 window size is optimal for both low and high sound energies
- Optimised version


## To try

- The autocepstrum is defined as the cepstrum of the autocorrelation. The autocepstrum is more accurate than the cepstrum in the analysis of data with echoes. (https://en.wikipedia.org/wiki/Cepstrum#Cepstral_analysis) 
- Do a cepstral/autocorrelation combination - which one works best at lower frequencies?
- Do another autocorrelation on signal to reduce noise? (https://dsp.stackexchange.com/questions/386/autocorrelation-in-audio-analysis)
- Can we do a real-time app?