---
title:  Big O
author: Gregory Thomas
date:   2020-03-13
---

# Big O

## What is it?

A way to describe the runtime complexity of a [function](../software_entities/functions/functions.md). 
It can be said that Big O describes the *rate of increase* of a function's runtime.


## What are the three ways that are used to describe runtime complexity?

1. Big O = **worst** case **(this is the most useful)**
2. Big Omega = **best** case
3. Big Theta = **expected** case (this is mostly the same as worst case)


## How do we show runtime complexity?

We use a capital (i.e. big) 'O' and our runtime complexity in brackets next to it.

e.g. O(n)


## What do you mean by 'runtime complexity'?

The value in the brackets gives the developer an idea of how long the function will take to complete **given the input**.
The input into a function can differ wildly. Some functions will take the same amount of time regardless of the input whereas others will take a slower time to complete.

e.g. A function with O(n) Big O time will take longer depending on how many elements there are in the input array.

e.g. A function with O(1) Big O time will take the *same* time regardless of input.

However, the Big O notation of a function **does not guarantee that is the *speed*** of the function during runtime; O(n) code can run faster than O(1) code depending on the circumstance.


## What are the typical runtime complexities?

- O(1)
- O(n)
- O(log n)
- O(n log n)
- O(n^2)
- O(n^3)
- O(n!)


## How do we calculate runtime complexity of a function?

Recursion/loops take the most time; we count how many loops for n.

e.g. 

1. A function with a single [for loop](../iterations/for_loops.md) which loops n times has a complexity of O(n).

2. A function with two separate ```for``` loops which both loop n times *still* has a complexity of O(n).

3. A function with a ```for``` loop which has a *nested* ```for``` loop which both loop n times has a complexity of O(n^2).


## Can we have O(2n)?

We can but it is best to drop non-dominant terms.

e.g.

- O(N^2 + N) becomes O(N^2)
- O(N + log N) becomes O(N)


## What are some things to remember?

- Functions which **half** the number of array elements, like a Binary Search, is normally O(log N)
- When someone/the book says "Big O", they will almost always describe the WORST CASE, unless stated.
  - Because of Big Theta/Omega, there could be confusion. But the worst case is the most useful case.


## Big O

### How does Big O relate to mathematical Bounds?
Big O (the *worst* case), is the [Upper bound](../mathematics/bounds.md) runtime; the **slowest** it will be.

It “grows no faster than” / it “grows at the same rate or slower than” / “the running time grows at most this much, but it could grow more slowly.”

Algorithm taking O(n log n) takes at most n log n time (the slowest it can take). It has no lower limit (i.e it could be quicker)
This is why it is considered the upper bound, and why Omega and Theta could be ignored


### What are some examples of Big O?

#### Electronic transfer
O(s), where s is the filesize
Time to transfer file increases linearly with the filesize (though simplified)


#### Aeroplane transfer
O(1). A filesize increase will not affect time, because the plane still takes the same amount of time.


#### Painting a fence
O(whp). Time it takes to paint a fence is (width * height) * number of layers of paint.


#### Merge sort 
O(n log n) and NOT O(log n) 


## Big Omega

### How does Big Omega relate to mathematical Bounds?
Big Omega (the *best* case), is the [Lower bound](../mathematics/bounds.md) runtime; the **fastest** it will be.

Algorithm taking Omega(n log n) takes at least n log n time (the quickest it can take), but has no upper limit (it could take much longer)
This is why it is considered the lower bound


## Big Theta

### How does Big Theta relate to mathematical Bounds?
Big Theta (the *expected* case), is both the [Upper and Lower bounds](../mathematics/bounds.md) runtime; the **average** it will be.

Also known as 'Tight bound' (both the upper and lower bounds)
The industry has merged this in Big O

An algorithm taking Theta(n log n) is preferable to it taking Omega(n log n).
The code takes at least Omega(n log n) and no more than O(n log n)
This is why it is considered Tight Bound - we have a known range of complexity for the given input


## Little O

### What is Little O?
“Grows strictly slower than”.