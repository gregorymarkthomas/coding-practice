---
title:  Atomicity Consistency Isolation Durability
author: Gregory Thomas
date:   2020-03-13
---

# Atomicity Consistency Isolation Durability

## What is it?

A **model** in regards to Database systems.


## What does ACID stand for?

**A**tomicity, **C**onsistency, **I**solation, **D**urability


## What are these terms that make ACID?

These are a *standard set of properties* that guarantee database **transactions** are processed reliably.

- Atomicity
  - The transaction takes place at once, or NOT AT ALL
- Consistency
    - Database must be consistent before and after the transaction
- Isolation
    - Multiple transactions occur independently without interference
- Durability
    - The changes of a successful transaction occur even if a system failure occurs