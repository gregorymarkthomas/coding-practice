# Microservice pattern

## What is it?

The Microservice pattern _contains_ **singular** small tasks into individual **services**.


## What is a real-world example of the Microservice pattern?

Think of how [Linux](/operating_system/linux/linux.md) works; Linux runs many different microservices, each doing a very particular task very well.





## How can we _imagine_ micro services?

Think of micro services as a selection of **individual apps**:

- Each service has its **own** database so that it is decoupled. 
  - Data consistency is maintained using the [Saga pattern](/patterns/saga_pattern.md)
- Each service has it's own technology stack
  - Easily changeable.
- Has it's own faults and does not bring the entire system down.


## What are the _advantages_ of using micro services?

- Maintainable and easily **testable**
  - enables quick deployment
- Loose coupling allows team to work independently without impacting other services
- **Independently deployable** - does not rely on the rest of the application/coordinate with other teams
- Allows for high productivity as very small teams (perhaps one dev per service) can develop without need for constant communication


## What are the _disadvantages_ of using micro services?

- The system as a whole is more complex
  - Devs have to consider inter-service communication and partial failure
  - Requests across multiple services is more difficult (means more comms with other teams)
  - IDEs still cater more towards monolithic systems
- Full system deployment takes more management
- Increased memory consumption
  - Each micro service has its own stack
  - A monolithic app would have N instances (for scalability) whereas now there are N*M services instances
  - If each runs its own JVM (or equivalent, to isolate instances), or perhaps even a VM (like Netflix does), then there are now M times as many runtimes/VMs with memory usage.


## How do the services communicate with each other?

- Using [synchronous](/terms/synchronous.md) 
  - protocols such as [Hypertext Transfer Protocol (HTTP)](/protocols/hypertext_transfer_protocol_http.md) 
  - architectural styles such as [REpresentational State Transfer (REST)](/architectures/respresentational_state_transfer_rest.md)
- Using [asynchronous](/terms/asynchronous.md) 
  - protocols such as [Advanced Message Queuing Protocol (AMQP)](/protocols/advanced_message_queuing_protocol_amqp.md)
  - messaging libraries such as [ZeroMQ](/language_specific/libraries/zero_message_queue_zmq.md)


## When would we use micro services?

- There is a team of devs where new members must pick things up quickly 
- Application must be easy to understand and modify
- When Continuous Deployment is a need
- You can access to multiple machines for scalability
- You want to use emerging technologies
- You want loose coupling


## What type of [scaling](/scaling/scaling.md) is the Microservice pattern project?

The Microservice pattern is a ["Y axis"](/scaling/scaling.md) scaling solution; we _split up_ an application into individual services that run one (or similar, at most) functions.


## What are the pitfalls of using the Microservice pattern?

- The beginning of a project **might not throw up the issues this solution fixes**
- This is slow to architect and goes against rapidly evolving startup businesses
- The task of scaling this way will be a challenge if the existing monolithic application is a "tangled mess" of dependencies.


## How would we go about _splitting_ apart a [monolithic](/terms/monolithic.md) application into micro services?

Split by:

- *subdomain* via [Domain-Driven Design](/concepts/domain_driven_design_ddd/domain_driven_design_ddd.md)
  - subdomains correspond to each different part of the business
- [business capability](/concepts/decomposition_by_business_capability.md)  
- [verb](/english/verb.md) 
- [noun](/english/noun.md) 


## What do we expect of our micro services?

- Our services should conform to the [Common Closure Principle (CCP)](/principles/common_closure_principle_ccp.md) (i.e. things that change together should be packaged together)
- Each service should implement a **small** set of **strongly related** functions
- Loose coupling; each service should be an API that encapsulates an implementation. The *implementation* of a service should NOT affect any other service.
- Testable
- Small enough to be developed by a "two pizza" team (6-10 people)
- Teams should be able to deploy without having to liase with other teams


## Further reading

- https://microservices.io/patterns/microservices.html
- http://chrisrichardson.net/post/microservices/general/2019/02/16/whats-a-service-part-1.html
- http://eventuate.io/exampleapps.html
