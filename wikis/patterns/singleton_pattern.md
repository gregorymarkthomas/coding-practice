---
title:  Singleton pattern
author: Gregory Thomas
date:   2020-03-13
---

# Singleton pattern

## What is it?

It is a pattern.

Only one instance of a Singleton class is allowed. This can save on memory (only one instance stored) and, if lazy-loading, reduces load on CPU by not having to recreate the singleton instance.

Think database connection pool; it handles database actions singularly for the whole application.

Singletons reduce the need for global variables which is especially helpful in Javascript; Javascript uses the [Module pattern](module_pattern.md#javascript) which is much like the Singleton.


## How does it work?

Uses a static instance object of the Singleton class.

This should be *thread-safe*; any thread should be free to use the Singleton class, but we must be careful about how we instantiate and provide access to the instance object

An example of how to do this in Java (taken from CapManager in p1025-camlab.. layout):

```
 /**
     * Only synchronize when instantiating.
     * volatile keyword guarantees visibility of changes to variables across threads by R/W to and from MAIN MEMORY and not CPU cache.
     * Thanks to @oznusem at https://medium.com/@oznusem/the-right-way-to-write-a-singleton-when-developing-for-android-or-any-multithreaded-environment-79782dd48f95
     *
     * Ensure that updateCaps() has been run.
     */
    private static volatile CapManager instance;
    public static CapManager getInstance() {
        if(instance == null) {
            synchronized (CapManager.class) {
                if (instance == null) {
                    instance = new CapManager();
                }
            }
        }
        return instance;
    }
```

```if(instance == null)``` will allow whatever thread to use ```instance``` *provided* it has already been instantiated. To instantiate ```instance``` we need to be careful and only let *one* thread do the job, otherwise we will have two ```instance```s on the memory heap and potentially other issues. We use ```if(instance == null)``` *before* the ```synchronized (CapManager.class) {...``` call because a ```synchronized``` call takes a hit on performance; synchronizing is a purposeful bottleneck that checks with other threads to see if the code is allowed to run. If we had the ```synchronized``` before the ```instance``` null check we would be slowing access performance of this class. By having this setup we allow threads to *quickly* check (and use) ```instance```.

```synchronized (CapManager.class) {...``` should only allow *one* thread to *instantiate* ```instance```. If one thread has already started instantiation, the other thread will *wait* at the first ```synchronized``` call until the first thread is finished instantiating. The second thread will then see the *inner* ```if (instance == null) {...``` call and should see that the first thread has already instantiated, so will exit with a Singleton ```instance```.

```private static volatile CapManager instance;``` has ```static``` and ```volatile```, two important keywords. ```static``` will keep the ```instance``` object in memory and will not be garbage-collected. ```volatile``` ensures that ```instance``` is accessible *across multiple threads*.

With this setup we are both safe in instantiation and provide fast access to the instance from multiple threads.

## What is wrong with Singletons?

According to a post on https://medium.com/p/79782dd48f95/responses/show, Singletons are anti-pattern as they are not *testable*/mockable.

A solution to this is to use a [dependency injection](https://trello.com/c/9iE2Bfvx/186-dependency-injection) framework, such as Dagger (for Java) which allow *mocking* of singletons.
