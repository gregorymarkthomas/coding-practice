# Repository pattern

## What is it?

A software _pattern_ for persistence (saving and loading your data, typically to/from a database).


- Allows for there to be a single place to get data in a way in which the client asking for the data does not care where it comes from.
  - Perhaps the data comes from database, OR it comes from cache.
- Allows easy swap of the code behind the repository
  - Perhaps you want to change database providers
- It makes testing easier
  - You can create a Mocked version which returned dummy data for test purposes.