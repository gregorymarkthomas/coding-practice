---
title:  Saga pattern
author: Gregory Thomas
date:   2020-03-13
---

# Saga pattern

## What is it related to?

**Databases**.


## What does it do?

This relies on there being a **Database per Service** (via microservices, for e.g.). We want data **consistency** across these services.

As (for e.g.) Orders and Customers (in the microservice system) have their own databases, we cannot use a local [ACID](https://trello.com/c/hnGbqsg7/117-acid) transaction.

**A *saga* is a sequence of local transactions.** Each local transaction updates the local database **and triggers an event for the next service to start their local transaction**. If one transaction fails, then **a series of compensating transactions are carried out that undo the changes made up until the point of failure**.

We can do this by using **Choreography** or **Orchestration**.

- Choreography
  - each local transaction publishes domain events that trigger local transactions in other services
- Orchestration
  - a separate service tells participants what local transactions to execute

Read more here: https://microservices.io/patterns/data/saga.html