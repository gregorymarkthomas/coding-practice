---
title:  Module pattern
author: Gregory Thomas
date:   2020-03-13
---

# Module pattern

## What is it?
A way to *collate* software entities into its own usable *single* entity which can be imported for use elsewhere.

It is a way to [encapsulate](https://trello.com/c/8FHtjtxR/158-oop-encapsulation) our code; we can collate complexity into one module and import it easily.

Modules are *not* fully supported in many common languages but can be created via Singletons.


## Languages

### Python

Python uses the Module pattern extensively; each .py file IS automatically a module that can be imported.

```
from scriptname import *
```

Please ensure the scripts are located in the same folder.


### C# #

"Modules" (of sorts) can be created in C# in the form of [Singletons](singleton_pattern.md), with the inclusion of a prepare() and disband() functions, which create local variables on start and destroy them on exit.


### Java

Similar to C#; we can create Modules using a Singleton.


### Javascript

Module pattern is quite a common practice in Javascript.

We wrap our class and methods with an [anonymous closure](../../software_entities/functions/anonymous_closures.md#javascript), like so (see about [strict mode here](../../language_specific/javascript/strict_mode.md)):

```
(function() {
  'use strict';
  // All function and variable code are scoped to this function
})();
``` 

To *export* our module, we assign the anonymous closure to a ```var``` so that it can be accessible from other modules:

```
var myModule = (function() {
  'use strict';
  // All function and variable code are scoped to this function
})();
```

To allow public access to our new module's inner functions (i.e. exposing the var to outside our module), we need to return an ```Object``` with our public methods defined:

```
var myModule = (function() {
  'use strict';
  
  return {
    publicMethod: function() {
      // whatever
    }
  };
})();

myModule.publicMethod();    // Does 'whatever'
```

Now add some ```private``` functions/variables;

```
var myModule = (function() {
  'use strict';

  var _privateVar = "Whatever";

  function _privateMethod() {
    // Do whatever privately
  }  

  return {
    publicMethod: function() {
      _privateMethod();
    }
  };
})();

myModule.publicMethod();    // Does 'whatever'
console.log(myModule._privateProperty); // returns undefined; is protected by module closure
myModule._privateMethod(); // Produces TypeError; is protected by module closure
```

We can call the above *Revealing Module Pattern*.

**This may not be as common/necessary in Javascript now that version [ES6 has classes](../../software_entities_classes.md#javascript)**. Though not as common, Javascript ES6 does now allow for *asynchronous* module loading to help with initial loading performance.


## Modules VS Namespaces

- Both of these *group* related entities by a *single identifier*.
  - These entities can be accessed **globally**.
- However, most languages do not allow namespaces to support *initialisation* and *finalisation*.
  - Modules do support that, though.


## Links

- http://jargon.js.org/_glossary/MODULE_PATTERN.md
- https://coryrylan.com/blog/javascript-module-pattern-basics