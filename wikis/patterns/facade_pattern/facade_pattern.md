---
title:  Facade pattern
author: Gregory Thomas
date:   2020-03-13
---

# Facade pattern

## What is it?

As the name suggests: it hides complexity away by providing a very simple interface to use it.

Just think of Abstraction, and REST APIs.

![Facade](Example_of_Facade_design_pattern_in_UML.png)

```
/* Complex parts */

class CPU {
  freeze() { /* code here */ }
  jump(position) { /* code here */ }
  execute() { /* code here */ }
}

class Memory {
  load(position, data) { /* code here */ }
}

class HardDrive {
  read(lba, size) { /* code here */ }
}

/* Facade */

class ComputerFacade {
  constructor() {
    this.processor = new CPU();
    this.ram = new Memory();
    this.hd = new HardDrive();
  }

  start() {
    this.processor.freeze();
    this.ram.load(this.BOOT_ADDRESS, this.hd.read(this.BOOT_SECTOR, this.SECTOR_SIZE));
    this.processor.jump(this.BOOT_ADDRESS);
    this.processor.execute();
  }
}

/* Client */

let computer = new ComputerFacade();
computer.start();

```

A Facade pattern may use a [Singleton](../singleton_pattern.md) so that only one client can use the facade.