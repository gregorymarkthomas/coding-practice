---
title:  Adapter pattern
author: Gregory Thomas
date:   2020-03-13
---

# Adapter pattern

## What is it?

Think of it as a **converter**.

Say class Sparrow() implements Bird() interface, and Cat() implements Feline() interface. Bird() has fly() and makeSound(), and Feline() has scratch().
We, for some reason, want to be able to use a Sparrow() object in the same context as the Cat(), but we cannot easily do this unless we use an **adapter**.

```
interface Bird 
{ 
    // birds implement Bird interface that allows 
    // them to fly and make sounds adaptee interface 
    public void fly(); 
    public void makeSound(); 
} 
  
class Sparrow implements Bird 
{ 
    // a concrete implementation of bird 
    public void fly() 
    { 
        System.out.println("Flying"); 
    } 
    public void makeSound() 
    { 
        System.out.println("Chirp Chirp"); 
    } 
} 
  
interface Feline
{ 
    // target interface 
    public void scratch(); 
} 
  
class Cat implements Feline 
{ 
    public void scratch() 
    { 
        System.out.println("Scratch"); 
    } 
} 
  
class BirdAdapter implements Feline 
{ 
    // You need to implement the interface your 
    // client expects to use. 
    Bird bird; 
    public BirdAdapter(Bird bird) 
    { 
        // we need reference to the object we 
        // are adapting 
        this.bird = bird; 
    } 
  
    public void scratch() 
    { 
        // translate the methods appropriately 
        bird.fly(); 
    } 
} 
  
class Main 
{ 
    public static void main(String args[]) 
    { 
        Sparrow sparrow = new Sparrow(); 
        Catc cat = new Feline(); 
  
        // Wrap a bird in a birdAdapter so that it  
        // behaves like feline
        Feline birdAdapter = new BirdAdapter(sparrow); 
  
        System.out.println("Sparrow..."); 
        sparrow.fly(); 
        sparrow.makeSound(); 
  
        System.out.println("Feline..."); 
        feline.squeak(); 
  
        // feline behaving like a bird  
        System.out.println("BirdAdapter..."); 
        birdAdapter.squeak(); 
    } 
} 

```

We basically create a new class which holds our object we want to 'extend' with the target class (BirdAdapter) and use it.