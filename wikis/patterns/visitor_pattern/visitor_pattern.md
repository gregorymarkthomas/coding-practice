---
title:  Visitor pattern
author: Gregory Thomas
date:   2020-09-24
---

# Visitor pattern

## What is it?

It is a programming pattern.


## Wow, nice one. What is so different about the Visitor pattern, you smartass? 

Let's start with an example...


## What is an example of a problem that the Visitor pattern could solve?

Say we have an [interface](/terms/interface.md) ```Animal```, and we have three child [classes](/structures/software_entities/classes/classes.md) ```Dog```, ```Cat``` and ```Parrot``` that _implement_ ```Animal``` via [inheritance](/data_relationships/inheritance.md).


### Implementing the parent's ```abstract``` functions

The three child classes have to implement those [abstract](/paradigms/object_oriented_programming_oop/principles/abstraction.md) functions defined in the parent interface. 


### But what if I wanted to add another function to our child classes?

To add another function to I would have to _modify_ the parent interface of ```Animal```; **I would then have to edit every child class to _implement_ this function**.


### This is okay, yeah?

If there is a limited number of functions to implement _in a limited number of child classes_, then, yes, this [tight-coupling](/terms/tightly_coupled.md) would be manageable.


### But, more functions in more child classes?

Unmaintainable!


### What else is wrong with this setup?

By _modifying_ the interface, we are breaking the [Open-closed principle](/paradigms/object_oriented_programming_oop/open_closed_principle.md):

> Keep your [Application Programming Interfaces (API)](/terms/application_programming_interface_api) _open_ for **extensibility** and _closed_ for _modification_.


### So, instead of _modifying_, we want to merely _extend_?

Yes.


## _Why_ does the Visitor pattern exist?

Well, the Visitor pattern exists so that we can _extend_ the class instead of _modify_ the interface.


## What does the Visitor pattern have to do with [double dispatch](/paradigms/object_oriented_programming_oop/principles/polymorphism/double_dispatch.md)? 

The Visitor Pattern is essentially **implementing Double dispatch**:

We have **two** calls ("_double_ dispatch") that specify the element _and_ the right operation for the element (based on its type).


## Is there an actual example I can look at?

[VisitorPattern](VisitorPattern).