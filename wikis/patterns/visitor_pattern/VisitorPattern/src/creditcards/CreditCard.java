package creditcards;

import offers.OfferVisitor;

/**
 * Imagine that, instead of using the Visitor Pattern, I had to implement functions like:
 *
 * void computeCashbackForGas(GasOffer);
 * void computeCashbackForFood(FoodOffer);
 * void computeCashbackForHotel(HotelOffer);
 * void computeCashbackForOther(OtherOffer);
 *
 * EVERYTIME I add a new function for a new offer, I BREAK any concrete class that derives from CreditCard;
 * we're breaking the API.
 *
 * With the Visitor Pattern, though, I was able to add new operations without breaking the API.
 */
public interface CreditCard {
    String getName();
    void accept(OfferVisitor visitor);
}
