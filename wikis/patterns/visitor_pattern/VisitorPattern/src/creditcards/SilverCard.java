package creditcards;

import offers.OfferVisitor;

public class SilverCard implements CreditCard {
    @Override
    public String getName() {
        return "Silver";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitSilverCard(this);
    }
}
