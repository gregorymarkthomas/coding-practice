package creditcards;

import offers.OfferVisitor;

public class GoldCard implements CreditCard {
    @Override
    public String getName() {
        return "Gold";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitGoldCard(this);
    }
}
