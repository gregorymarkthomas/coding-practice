package creditcards;

import offers.OfferVisitor;

public class BronzeCard implements CreditCard {

    @Override
    public String getName() {
        return "Bronze";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitBronzeCard(this);
    }
}
