import creditcards.BronzeCard;
import creditcards.CreditCard;
import creditcards.GoldCard;
import creditcards.SilverCard;
import offers.HotelOfferVisitor;
import offers.OfferVisitor;

/**
 *            $0      $100     $500
 *          Bronze | Silver | Gold
 * Gas      1           2       5
 * Food     2           3       10
 * Hotel    1           2       3
 * Other    1           5       20
 *
 *  We have two objects here; CreditCard and Offer.
 *
 *  We DO NOT WANT TO TIGHTLY COUPLE OFFER TO CREDITCARD.
 *
 * -----------------
 *
 * Notice below that I am able to get past the Double Dispatch problem; I can DEFINE the instance with
 * either the concrete class or the parent, and they both return the correct result.
 * When trying Double dispatch (see DoubleDispatch project), I was unable to polymorphically resolve
 * two abstract types.
 * Now we have two polymorphic types that are both resolved at runtime.
 *
 * Q: When would I use this?
 * If I have classes whose structure I do not change often, but there are OPERATIONS I want to add often,
 * then the Visitor Pattern suits this scenario.
 *
 * -----------------
 *
 * The Visitor Pattern is essentially implementing Double dispatch.
 * Here we have two calls (double dispatch) that specify the element
 * and the right operation for the element (based on its type).
 *
 */
public class Runner {
    public static void main(String[] args) {
        CreditCard bronze = new BronzeCard();
        CreditCard silver = new SilverCard();
        CreditCard gold = new GoldCard();

        HotelOfferVisitor concreteVisitor = new HotelOfferVisitor();
        bronze.accept(concreteVisitor); // Returns "Calculating hotel offer for bronze (Bronze) card"

        OfferVisitor genericVisitor = new HotelOfferVisitor();
        bronze.accept(genericVisitor); // Returns "Calculating hotel offer for bronze (Bronze) card"
    }
}
