package offers;

import creditcards.BronzeCard;
import creditcards.GoldCard;
import creditcards.SilverCard;

public class FoodOfferVisitor implements OfferVisitor {
    @Override
    public void visitBronzeCard(BronzeCard bronzeCard) {
        System.out.println("Calculating food offer for bronze (" + bronzeCard.getName() + ") card");
    }

    @Override
    public void visitSilverCard(SilverCard silverCard) {
        System.out.println("Calculating food offer for silver (" + silverCard.getName() + ") card");
    }

    @Override
    public void visitGoldCard(GoldCard goldCard) {
        System.out.println("Calculating food offer for gold (" + goldCard.getName() + ") card");
    }
}
