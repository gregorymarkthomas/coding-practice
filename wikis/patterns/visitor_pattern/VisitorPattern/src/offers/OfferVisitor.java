package offers;

import creditcards.BronzeCard;
import creditcards.GoldCard;
import creditcards.SilverCard;

/**
 * The visitor object has several visit() methods, based on the element type the appropriate visit method is called.
 */
public interface OfferVisitor {
    void visitBronzeCard(BronzeCard bronzeCard);
    void visitSilverCard(SilverCard silverCard);
    void visitGoldCard(GoldCard goldCard);
}
