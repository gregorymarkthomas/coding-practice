package offers;

import creditcards.BronzeCard;
import creditcards.GoldCard;
import creditcards.SilverCard;

public class HotelOfferVisitor implements OfferVisitor {
    @Override
    public void visitBronzeCard(BronzeCard bronzeCard) {
        System.out.println("Calculating hotel offer for bronze (" + bronzeCard.getName() + ") card");
    }

    @Override
    public void visitSilverCard(SilverCard silverCard) {
        System.out.println("Calculating hotel offer for silver (" + silverCard.getName() + ") card");
    }

    @Override
    public void visitGoldCard(GoldCard goldCard) {
        System.out.println("Calculating hotel offer for gold (" + goldCard.getName() + ") card");
    }
}
