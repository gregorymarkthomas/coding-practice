---
title:  Decorator pattern
author: Gregory Thomas
date:   2020-03-13
---

# Decorator pattern

## What does it do?

Essentially adds a **wrapper** to an existing class to maintain class structure but also allow new fields; a new class is created which **decorates** the existing class that we want to extend but maintain.

The Decorator class **has an** instance of the class it wants to decorate.
The Decorator class also **extends** the same interface as the class it wants to decorate.

![UML of decorator pattern example](decorator_pattern_uml_diagram.jpg)


## Problems

- Causes inheritance hell where there are too many levels/implementations of things.
