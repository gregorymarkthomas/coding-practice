---
title:  Factory pattern
author: Gregory Thomas
date:   2020-03-13
---

# Factory pattern

## What is it?

- Factory pattern allows you to create an object (e.g. a Shape) without exposing the creation logic to the client
  - We can refer to the newly created object using a common interface and **we do not need to use the specific child class**

e.g. 

- abstract Shape has draw().
- child shape classes override draw()
- ShapeFactory() is a standard class that has ```getShape(String shapeType)```
  -  It takes ```shapeType``` and creates the child class that ```shapeType``` refers to: 
```
if(shapeType.equalsIgnoreCase("CIRCLE")){
         return new Circle();
}
```
- In our usage of this setup, we can create a Shape object  via the ShapeFactory, and not have to rely on the child classes: 
```
Shape shape1 = shapeFactory.getShape("CIRCLE"); 
shape1.draw();
```

The alternative would be to create Circle() object directly.