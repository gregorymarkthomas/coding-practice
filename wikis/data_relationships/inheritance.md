---
title:  Inheritance
author: Gregory Thomas
date:   2020-03-13
---

# Inheritance

## What is it?

A data relationship/a form of *association*.

Particularly, **IS a**: a House **IS a** type of LivingQuarter.

LivingQuarter is the **parent** class that carries out operations only the parent can do. House is the child of LivingQuarter; it carries out operations only a House would do. The House *inherits* some carefully chosen tasks from it's LivingQuarter parent. House could have a child of its own, say, SemiDetached, and that would have its own operations plus access to carefully chosen operations from both its parent and grandparent.
A SemiDetached IS A House, and House IS A LivingQuarter.

This is in **contrast** to [Composition](composition.md)/[Aggregation](aggregation.md) which are both *has-a*. The above example would not work, as a SemiDetached does not HAVE A House.


## Why can Inheritance be bad?

- Readability
 - Jumping back and forth between parent and subclasses to find the implementation
- Breaking encapsulation
  - Child class can gain access to methods in parent it does not need and potentially overwrite them
  - Perhaps we need a public parent function but we will be able to overwrite that function in the subclass
- Sometimes inheritance is used for objects that do not have an "is-a" relationship.
  - ServiceBase and Service

Use composition if you can.



## How can we get around using Inheritance?

There are two fundamental approaches to combining objects together:

- The first is Inheritance. As you have already identified the limitations of inheritance mean that you cannot do what you need here.
- The second is Composition. Since inheritance has failed you need to use composition.

The way this works is that you have an Animal object. Within that object you then add further objects that give the properties and behaviors that you require.

For example:

```java
    Bird extends Animal implements IFlier
    Horse extends Animal implements IHerbivore, IQuadruped
    Pegasus extends Animal implements IHerbivore, IQuadruped, IFlier
```

Now IFlier just looks like this:

```java
 interface IFlier {
     Flier getFlier();
 }
```

So Bird looks like this:

```java
 class Bird extends Animal implements IFlier {
      Flier flier = new Flier();
      public Flier getFlier() { return flier; }
 }
```

Now you have all the advantages of Inheritance. You can re-use code. You can have a collection of IFliers, and can use all the other advantages of polymorphism, etc.

However you also have all the flexibility from Composition. You can apply as many different interfaces and composite backing class as you like to each type of Animal - with as much control as you need over how each bit is set up.


### Strategy Pattern alternative approach to composition

An alternative approach depending on what and how you are doing is to have the Animal base class contain an internal collection to keep the list of different behaviors. In that case you end up using something closer to the Strategy Pattern. That does give advantages in terms of simplifying the code (for example Horse doesn't need to know anything about Quadruped or Herbivore) but if you don't also do the interface approach you lose a lot of the advantages of polymorphism, etc.


## What's the problem with **multiple** inheritance?

> "An ambiguity that arises when two classes B and C inherit from A, and class D inherits from both B and C. If there is a method in A that B and C have overridden, and D does not override it, then which version of the method does D inherit: that of B, or that of C?

...It is called the "diamond problem" because of the shape of the class inheritance diagram in this situation. In this case, class A is at the top, both B and C separately beneath it, and D joins the two together at the bottom to form a diamond shape..."*




## Links

- http://neethack.com/2017/04/Why-inheritance-is-bad/
- https://stackoverflow.com/questions/21824402/java-multiple-inheritance