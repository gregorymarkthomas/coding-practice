---
title:  Composition
author: Gregory Thomas
date:   2020-03-13
---

# Composition

## What is it?

A data relationship/a form of *association*.

Particularly, **HAS a**.

A House has many Rooms. BUT, all Rooms would cease to exist if the House disappears. 
**Composition implies that the child cannot exist without the context of the parent.**

This is in **contrast** to [Inheritance](inheritance.md) which is *is-a*.