---
title:  Aggregation
author: Gregory Thomas
date:   2020-03-13
---

# Aggregation

## What is it?

A data relationship/a form of *association*.

It is **HAS a**, much like [composition](composition.md).

BUT, an Aggregation relation is one that does not implode if the parent is removed. 

e.g. A House also has a number of occupants, being instances of Person. That's an aggregation relationship because **those people exist outside of the context of that House**.