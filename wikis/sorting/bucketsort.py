#!/usr/bin/python

"""
bucketsort in Python.
An implementation of it to understand how it works.

INCOMPLETE.

"""

import unittest

def insertionsort(array):
	"""
	Taken from https://gitlab.com/gregorymarkthomas/coding-practice/-/blob/master/sorting/insertionsort.py

	For each element we check the previous elements; if the current element is smaller than the previous elements, gradually move the current element to the front of the array.
	"""
	# Leave the 0th element
	for i in range(1, len(array)):
		for j in range(i, 0, -1):
			if array[j - 1] > array[j]:
				array[j-1], array[j] = array[j], array[j-1]
	return array

def bucketsort(array):
	"""
	https://stackoverflow.com/questions/4461737/what-is-the-difference-between-bucket-sort-and-radix-sort
	"""
	return array

class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		pass

	def test_standard(self):
		self.assertEqual(bucketsort([3,7,8,5,2,1,9,5,4]), [1,2,3,4,5,5,7,8,9])

unittest.main(exit=False)