#!/usr/bin/python

"""
Quicksort in Python.
An implementation of it to understand how it works.

This is another "divide and conquer" algorithm, much like merge sort.
Quicksort involves finding a "pivot" element (perhaps the last element, the first, the median, or random).
Using the pivot, we create "partitions" that consist of values less than and greater than the pivot.

The core of quicksort is the "partition" function. There are two versions of the partition function: Lomuto and .

"""

import unittest

"""
Used https://stackoverflow.com/questions/39665299/understanding-quicksort as a guide.

This is an attempt at making partition_so_confusing() more like the proper Lomuto version.
The problem with the original partition_so_confusing() is that pivot changes a lot when it should not.

The pivot choice for this function uses the STARTING element. 
This returns the INDEX of pivot.

E.g. [3,7,8,5,2,1,9,5,4] becomes [1,2,3,4,5,5,7,8,9]

[3,7,8,5,2,1,9,5,4]
[0,1,2,3,4,5,6,7,8]

begin = 0
end = 8
pivot = 3
i = 0
for j in range(1, 8):
	j = 1
		7 is greater than 3, so ignore
	j = 2
		8 is greater than 3, so ignore
	j = 3
		5 is greater than 3, so ignore
	j = 4
		2 is less than 3
		array = [2,7,8,5,3,1,9,5,4]
		i = 1
	j = 5
		1 is less than 3
		array = [2,1,8,5,3,7,9,5,4]
		i = 2
	j = 6
		7 is greater than 3, so ignore
	j = 7
		9 is greater than 3, so ignore
	j = 8
		5 is greater than 3, so ignore
	j = 9
		4 is greater than 3, so ignore
array = [8,1,2,5,3,7,9,5,4] - WHY? This is not really paritioning.

I think the current solution below is not quite ready.
The Lomuto solution I am basing it off of (above) uses the END element for the pivot, and I am trying to convert it to use the BEGINNING element as the pivot.

It feels like that the pivot should not move.
Perhaps we increment i before we swap the elements?

E.g. increment i before swapping elements:

[3,7,8,5,2,1,9,5,4]
[0,1,2,3,4,5,6,7,8]

begin = 0
end = 8
pivot = 3
i = 0
for j in range(1, 8):
	j = 1
		7 is greater than 3, so ignore
	j = 2
		8 is greater than 3, so ignore
	j = 3
		5 is greater than 3, so ignore
	j = 4
		2 is less than 3
		i = 1
		array = [3,2,8,5,7,1,9,5,4]
	j = 5
		1 is less than 3
		i = 2
		array = [3,2,1,5,7,8,9,5,4]
	j = 6
		7 is greater than 3, so ignore
	j = 7
		9 is greater than 3, so ignore
	j = 8
		5 is greater than 3, so ignore
	j = 9
		4 is greater than 3, so ignore
array = [1,2,3,5,7,8,9,5,4]

That looks better; we have successfully moved 1 and 2 before 3 and anything greater than 3 resides beyond it.
However, the test did fail, and this was because we were not doing "for j in range(begin+1, end+1):"; we were just doing it until "end".
Why did the test fail before we extended to "end+1"?
This is because range(begin, end) ends on 'end-1' and not 'end'! See https://trello.com/c/Yozrwp45/120-range

'array[i], array[j] = array[j], array[i]' moves an element that is LESS THAN pivot TO THE BEGINNING OF THE ARRAY, but is not moved BEFORE pivot.
	- 'i' maintains the position of where the pivot should end up at the end of this call. 
		- 'array[i], array[begin] = array[begin], array[i]' moves the pivot index at the end of the call.

How do we produce the worst case (O(n^2)) time complexity?
	- If ALL elements are either less than OR greater than the pivot EVERY iteration (i.e. the array is ALREADY SORTED and the beginning element is always picked for pivot every iteration).  

How do we produce the best case?
	- The pivot is the MEDIAN of the set of data. With this, the least amount of recursive calls will be done. 

What difference would it make if the pivot was set to the END item?
	- nothing, really. Some pivot choices may be better for certain sets of data.

"""
def partition(array, begin, end):
	pivot = array[begin]
	i = begin
	for j in range(begin+1, end+1):
		# print("partition(): j = " + str(j) + ". array[j] = " + str(array[j]))
		if array[j] <= pivot:
			i += 1
			# print("partition(): i now equals " + str(i) + ". Swapping array[" + str(i) + "] (" + str(array[i]) + ") with array[" + str(j) + "] (" + str(array[j]) + "). Array = " + str(array))
			array[i], array[j] = array[j], array[i]
	array[i], array[begin] = array[begin], array[i]
	return i

def quicksort(array, begin=0, end=None):
	"""
	This function will only process elements of 'array' between 'begin' and 'end'.
	It "partitions" 'array' into "smaller pivot larger". 
	We then recursively run this function on the "smaller than pivot" partition of the array.
		- This means that 'array' never resizes per se; our 'begin' and 'end' does shrink, though.

	pivot is the pivot's INDEX.

	There is a difference between list manipulation in the comments and the call to quicksort().
		- 'end' in quicksort() is extended by +1 so that all 9 elements (in the example array) are actually processed.
		- In list manipulation (list[start:length]) is actually length and not list[start:end], hence why we need to add an extra +1 to end to make it 'length'.

	What is going on with each call below?
		- We return nothing if we have invalid inputs
		- We partition the array with the boundaries that this function is given.
			- The full array is processed on the first iteration 
			- Subsequent inner calls to partition() occur when this function recursively calls quicksort()
				- partition() will only process certain parts of the array in these recursive calls (less than pivot, greater than pivot).
	
	So what is this recursive function actually doing?
		- partition() and the first quicksort() are called multiple times until array is whittled down to 1 element
		- Every recursive call partitions a smaller and smaller array. 
			- partition() sets a pivot on each section of the array it is given and moves elements to the left or right of it.
			- **This is how the sorting is actually done.**
			- The fact that the full array still exists and that we just process sections of it means we do not have to append sections of the array together at the end (though we could).
	"""
	if end is None:
		end = len(array) - 1
	if begin >= end:
		return
	print("quicksort() 1: begin = " + str(begin) + ", end = " + str(end) + ", array 1 = " + str(array[begin:end+1]))
	pivot = partition(array, begin, end)
	print("quicksort() 2: array = " + str(array[begin:pivot]) + ", pivot index = " + str(pivot) + " in array " + str(array))
	quicksort(array, begin, pivot-1)
	print("quicksort() 3: array = " + str(array[pivot+1: end+1]))
	quicksort(array, pivot+1, end)
	print("quicksort() 4: array = " + str(array))
	return array


"""
Running the same example as the one for parition_so_fixed() for comparison.
E.g. [3,7,8,5,2,1,9,5,4] becomes [1,2,3,4,5,5,7,8,9]

[3,7,8,5,2,1,9,5,4]
[0,1,2,3,4,5,6,7,8]

begin = 0
end = 8
pivot = 0 (this is an index)
for i in range(1, 9):
	i = 1
		7 is greater than 3, so ignore
	i = 2
		8 is greater than 3, so ignore
	i = 3
		5 is greater than 3, so ignore
	i = 4
		2 is less than 3
		pivot = 1	- SHOULD WE BE INCREMENTING PIVOT BEFORE OUR ELEMENT SWAP??
		array = [3,2,8,5,7,1,9,5,4]
	i = 5
		1 is less than 3
		pivot = 2
		array = [3,2,1,5,7,8,9,5,4]
	i = 6
		7 is greater than 3, so ignore
	i = 7
		9 is greater than 3, so ignore
	i = 8
		5 is greater than 3, so ignore
	i = 9
		4 is greater than 3, so ignore
array = [1,2,3,5,7,8,9,5,4]

So, this partitioning works...I think.

So what is the difference between this one and the one above???
- Well, my j is their i
- My pivot is their array[begin]
- My i is their pivot

See partition_so_fixed() for a solution that does not have a weird moving pivot.
"""

def partition_so_confusing_version(array, begin, end):
	pivot = begin
	print("pivot INDEX = " + str(pivot))
	for i in range(begin+1, end+1):
		print("i = " + str(i))
		print("array[i] = " + str(array[i]) + ", array[begin] = " + str(array[begin]))
		if array[i] <= array[begin]:
			pivot += 1
			print(str(array[i]) + " is less than or equal to " + str(array[begin]) + ", so up pivot INDEX to " + str(pivot) + " and swap position " + str(i) + " with " + str(pivot))
			print("array WAS = " + str(array))
			array[i], array[pivot] = array[pivot], array[i]
			print("array NOW = " + str(array))
	array[pivot], array[begin] = array[begin], array[pivot]
	print("Position " + str(pivot) + " and " + str(begin) + " have also been swapped.")
	return pivot


class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		pass

	def test_standard(self):
		self.assertEqual(quicksort([3,7,8,5,2,1,9,5,4], 0, 8), [1,2,3,4,5,5,7,8,9])

	def test_all_same_elements_are_maintained(self):
		self.assertEqual(quicksort([4,4,4,4,4,4,4,4,4], 0, 8), [4,4,4,4,4,4,4,4,4])

	def test_full_reverse(self):
		self.assertEqual(quicksort([9,8,7,6,5,4,3,2,1], 0, 8), [1,2,3,4,5,6,7,8,9])

	def test_zeros(self):
		self.assertEqual(quicksort([3,7,8,5,2,0,9,5,4], 0, 8), [0,2,3,4,5,5,7,8,9])

	def test_tens(self):
		self.assertEqual(quicksort([30,70,80,50,20,10,90,50,40], 0, 8), [10,20,30,40,50,50,70,80,90])

	def test_units_and_tens(self):
		self.assertEqual(quicksort([3,70,8,50,2,10,9,50,4], 0, 8), [2,3,4,8,9,10,50,50,70])

	def test_minus_numbers(self):
		self.assertEqual(quicksort([-3,-7,-8,-5,-2,-1,-9,-5,-4], 0, 8), [-9,-8,-7,-5,-5,-4,-3,-2,-1])

	def test_unbalanced(self):
		"""Because we set pivot to beginning element, this should make the process slow. """
		self.assertEqual(quicksort([1,2,3,4,5,6,7,8,9], 0, 8), [1,2,3,4,5,6,7,8,9])

	def test_empty_array_return_nothing(self):
		self.assertEqual(quicksort([], 0, 0), None)

	def test_floats(self):
		self.assertEqual(quicksort([3.1,7.2,8.3,5.4,2.5,1.6,9.7,5.8,4.9], 0, 8), [1.6,2.5,3.1,4.9,5.4,5.8,7.2,8.3,9.7])

unittest.main(exit=False)