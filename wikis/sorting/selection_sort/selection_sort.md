# Selection sort

## What is it?

A *comparison-based* sorting algorithm.


## What are the basics?

Do a linear scan through the array and find the smallest; move it to the front by *swapping that smallest element with the front element*. 
Then, find the *next* smallest and move it with another linear scan.
Keep doing so until sorted.


## Is it good?

It's simple but not a good way to sort.


## What is its [time complexity](/big_o/big_o.md)?

**O(n^2)**


## Where can I view an implementation of Selection sort?

[selectionsort.py](selectionsort.py)