#!/usr/bin/python

"""
Selectionsort in Python.
An implementation of it to understand how it works.



"""

import unittest

def selectionsort(array):
	for i in range(0, len(array)):
		# print("i = " + str(i))
		for j in range(i + 1, len(array)):
			# print("j = " + str(j))
			print("Is array["+str(j)+"] (" + str(array[j]) + ") smaller than array["+str(i)+"] (" + str(array[i]) + ")?")
			if array[j] < array[i]:
				print("array was " + str(array))
				array[i], array[j] = array[j], array[i]
				print("array now " + str(array))
	return array

class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		pass

	def test_standard(self):
		self.assertEqual(selectionsort([3,7,8,5,2,1,9,5,4]), [1,2,3,4,5,5,7,8,9])

unittest.main(exit=False)