#!/usr/bin/python

"""
Mergesort in Python.
An implementation of it to understand how it works.

This is a "divide and conquer" algorithm, like quicksort.

- Divide array in half
- Sort halves
- MERGE back together
	- We end up merging SINGLE ELEMENT ARRAYS as we use recursion.

"""

import unittest

"""
Based on the CtCI book (p147).

Process:
- Copy Java into Python
- Use print() to read logs and try to understand what is going on

Copied description from the book:
"The merge method operates by copying all the elements from the target array segment into a helper array, keeping track of where the start of the left and right halves should be (leftStartIndex and rightStartIndex).
We then iterate through helper, copying the smaller element from each half into the array. At the end, we copy any remaining elements into the target array.
...
You may notice that only the remaining elements from the left half of the helper array are copied into the target array. Why not the right half? The right half doesn't need to be copied because it is *already* there.
Consider, for example, an array like [1, 4, 5 || 2, 8, 9] (the '||' indicates the partition point). Prior to merging the two halves, both the helper and array and the target array segment will end with [8, 9].
Once we copy over four elements (1, 4, 5 and 2) into the target array, the [8, 9] will still be in place in both arrays. There is no need to copy them over."

Translated into something a bit simpler:
- Copies 'array' into 'helper' array
	- also KEEPS track of where STARt of left and right halves are (using leftStartIndex and Right)
- Iterates through 'helper'
	- What does it do?
		- Copies the SMALLER element from each half into 'array'.
	- We also copy REMAINING elements into 'array'
		- In the book example, only the left half is copied in because the right half already exists in 'array'

"""
def mergesort(array):
	"""
	The function that abstracts the sorting algorithm.
	'array' is our array of ints that we want to sort.
	'helper' is currently a zero'ed array, and due to Java Virtual Machine's behaviour with initialised objects (https://trello.com/c/JIJzTVmY/122-default-object-initialisation-values), so is the Java version in the book.

	"""
	helper = [0] * len(array)
	__mergesort(array, helper, 0, len(array) - 1)
	return array

def __mergesort(array, helper, low, high):
	"""
	Similar to quicksort (https://gitlab.com/gregorymarkthomas/coding-practice/-/blob/master/sorting/mergesort.py), we input the WHOLE array every call, but we use a pointer system (low and high) to process a partition of array.
	
	As with all mergesort implementations, we find the middle value of our range.

	This is a recursive function that will continue to call itself until 'middle' 
	(the ever-shrinking value which is input as 'high' in the recursive call) is no longer GREATER THAN 'low'.

	If 'low' is ever higher than 'high', quit function so that recursive call can exit.
	This relies on 'middle' being an INT, hence the forced cast to int(). 
	Python, without the int() call, will set 'middle' to a float.
	'low' starts out having a value of zero, and if 'middle' were to stay as a float, the function would contantly divide by 2 and keep providing values that are ALWAYS greater than zero.
	The system relies on the fact that 'high' eventually becomes less than 'low'.

	e.g. 
	
	mergesort([3,7,8,5,2,1,9,5,4])

	In __mergesort([3,7,8,5,2,1,9,5,4], [0,0,0,0,0,0,0,0,0], 0, 8):
		0 < 8
			middle =  4
			In __mergesort([3,7,8,5,2,1,9,5,4], [0,0,0,0,0,0,0,0,0], 0, 4):
				0 < 4
					middle = 2
					In __mergesort([3,7,8,5,2,1,9,5,4], [0,0,0,0,0,0,0,0,0], 0, 2):
						0 < 2
							middle = 1
							In __mergesort([3,7,8,5,2,1,9,5,4], [0,0,0,0,0,0,0,0,0], 0, 1):
								0 < 1
									middle = 0
									In __mergesort([3,7,8,5,2,1,9,5,4], [0,0,0,0,0,0,0,0,0], 0, 0):
										0 == 0
											return
									In __mergesort([3,7,8,5,2,1,9,5,4], [0,0,0,0,0,0,0,0,0], 1, 1): 	# RIGHT HALF call
										1 == 1
											return

									# WHAT THE FUCK DO WE HAVE HERE EXACTLY??? What have we just figured out??
									# Well, think back to the mergesort diagram (https://www.geeksforgeeks.org/wp-content/uploads/Merge-Sort-Tutorial.png).
									# We are merely splitting the array in two, then four, then eight etc.
									# We are NOT finding the median or anything like that. 
									# This means that whatever 'helper' in __merge() becomes at any moment is NOT "one from left side and one from right side";
									# it is literally just the first two array elements.
									# The point is, in any array size (> 1, of course), we will always have a LEFT side and a RIGHT side.
									# This is why we have leftStartIndex and rightStartIndex.
									#

									In __merge([3,7,8,5,2,1,9,5,4], [0,0,0,0,0,0,0,0,0], 0, 0, 1):		
										for 0 in range(0, 2):
											helper[0] = array[0]
										for 1 in range(0, 2):
											helper[1] = array[1]
										helper is now [3,7,0,0,0,0,0,0,0]
										leftStartIndex = 0
										rightStartIndex = 0 + 1 = 1
										current = 0

										# Here is where we do our sorting; we are comparing two values and putting the SMALLEST in the original array first.

										while 0 <= 0 AND 1 <= 1:
											if 3 <= 7:
												array[0] = 3
												array is now [3,7,8,5,2,1,9,5,4] (the same as original as we are just handling the first two elements)
												leftStartIndex = 1
											ELSE STATEMENT NOT APPLICABLE
											current = 1

										while 1 <= 0 AND .... NOT APPLICABLE - exiting

										remaining = 0 - 1 = -1
										for i in range(0, 0)..... NOT APPLICABLE - exiting
							In __mergesort([3,7,8,5,2,1,9,5,4], [3,7,0,0,0,0,0,0,0], 2, 2):		# Remember that helper has been added to (and so has array but that first element has not changed)
								2 == 2
									return
							In __merge([3,7,8,5,2,1,9,5,4], [3,7,0,0,0,0,0,0,0], 0, 1, 2):
								for 0 in range(0, 3):
									helper[0] = array[0]
								for 1 in range(0, 3):
									helper[1] = array[1]
								for 2 in range(0, 3):
									helper[2] = array[2]
								helper is now [3,7,8,0,0,0,0,0,0]
								leftStartIndex = 0
								rightStartIndex = 1 + 1 = 2
								current = 0

								while 0 <= 1 and 2 <= 2:
									if 3 <= 8:
										array[0] = 3
										leftStartIndex = 1
									ELSE not applicable
									current = 1

								while 1 <= 1 and 2 <= 2:
									if 7 <= 8:
										array[1] = 7
										leftStartIndex = 2
									ELSE not applicable
									current = 2

								remaining = 1 - 2 = -1
								for i in range(0, 0):... NOT APPLICABLE
					In __mergesort([3,7,8,5,2,1,9,5,4], [3,7,8,0,0,0,0,0,0], 3, 4):		# Remember that helper has been added to (and so has array but that first element has not changed)
						3 < 4
							middle = 3 + 4 / 2 = 3
							In __mergesort([3,7,8,5,2,1,9,5,4], [3,7,8,0,0,0,0,0,0], 3, 3):
								3 == 3
									return
							In __mergesort([3,7,8,5,2,1,9,5,4], [3,7,8,0,0,0,0,0,0], 4, 4):
								4 == 4
									return
							In __merge([3,7,8,5,2,1,9,5,4], [3,7,8,0,0,0,0,0,0], 3, 3, 4):
								for 3 in range(3, 5):
									helper[3] = array[3]
								for 4 in range(3, 5):
									helper[4] = array[4]
								helper is now [3,7,8,5,2,0,0,0,0]
								leftStartIndex = 3
								rightStartIndex = 3 + 1 = 4
								current = 3

								while 3 <= 3 and 4 <= 4:
									if 5 <= 2:
										NO.
									else:
										array[3] = 2
										array is now [3,7,8,2,1,9,5,4] 	# 5 has been REPLACED with 2, but the 5 will be reinstated later!
										rightStartIndex = 5
									current = 4

								while 3 <= 3 and 5 <= 4:
									NO - EXITING.

								remaining = 3 - 3 = 0
								for i in range(0, 1):
									array[4 + 0] = helper[3 + 0]
									array is now [3,7,8,2,5,1,9,5,4] 	# 5 has been reinstated AFTER the 2!

					In __merge([3,7,8,2,5,1,9,5,4], [3,7,8,5,2,0,0,0,0], 0, 2, 4):
						for 0 in range(0, 5):
							helper[0] = array[0]
						for 1 in range(0, 5):
							helper[1] = array[1]
						for 2 in range(0, 5):
							helper[2] = array[2]
						for 3 in range(0, 5):
							helper[3] = array[3]
						for 4 in range(0, 5):
							helper[4] = array[4]
						helper is now [3,7,8,5,2,0,0,0,0]
						leftStartIndex = 0
						rightStartIndex = 2 + 1 = 3
						current = 0

						while 0 <= 3 and 3 <= 4:
							if 3 <= 3:
								array[0] = 3
								array stays the same
								leftStartIndex = 1
							else - NOT APPLICABLE
							current = 1

						while 1 <= 3 and 3 <= 4:
							if 7 <= 3:
								NO
							else:
								array[1] = helper[3]
								array is now [3,5,7,8,2,1,9,5,4]	# 5 placed near beginning
								rightStartIndex = 4
							current = 2

						while 1 <= 3 and 4 <= 4:
							if 7 <= 2 (4th index in HELPER):
								NO
							else:
								array[2] = helper[4]
								array is now [3,5,2,8,1,9,5,4]	# 2 has REPLACED the 7 - but the 7 will be reinstated later!
								rightStartIndex = 5
							current = 3

						while...NOPE.

						remaining = 2 - 1 = 1
							for i in range(0, 2):
								array[3 + 0] = helper[1 + 0]
								array is now [3,5,2,7,8,1,9,5,4] 	# 7 has been reinstated after the 2!
			In __mergesort([3,5,2,7,8,1,9,5,4], [3,7,8,5,2,0,0,0,0], 5, 8):
				if 5 <= 8:
					middle = (5 + 8) / 2 = 6
					In __mergesort([3,5,2,7,8,1,9,5,4], [3,7,8,5,2,0,0,0,0], 5, 6):
						if 5 <= 6:
							middle = (5 + 6) / 2 = 11 / 2 = 5
							In __mergesort([3,5,2,7,8,1,9,5,4], [3,7,8,5,2,0,0,0,0], 5, 5):
								5 <= 5:
									return
							In __mergesort([3,5,2,7,8,1,9,5,4], [3,7,8,5,2,0,0,0,0], 6, 6):
								6 <= 6:
									return
							In __merge([3,5,2,7,8,1,9,5,4], [3,7,8,5,2,0,0,0,0], 5, 5, 6):
								for 5 in range(5, 7):
									helper[5] = array[5]
								for 6 in range(5, 7):
									helper[6] = array[6]
								helper is now [3,7,8,5,2,1,9,0,0]
								leftStartIndex = 5
								rightStartIndex = 5 + 1 = 6
								current = 5

								while 5 <= 5 and 6 <= 6
									if 1 <= 9:
										array[5] = 1
										array is now [3,5,2,7,8,1,9,5,4]	# Stays the same
										leftStartIndex = 6
									else: N/A
									current = 6
								while 6 <= 5 and 6 <= 6
									NOPE

								remaining = 5 - 6 = -1
								for 0 in range(0, 0):
									NOPE.

							... Any point continuing??


	So, after all of that, how does this algorithm work?
	- It processes elements in halves. Just think of halving.


	"""
	if low < high:
		middle = int((low + high) / 2)
		print("__mergesort(): low = " + str(low) + ", middle = " + str(middle) + ", high = " + str(high))
		__mergesort(array, helper, low, middle)		# Sort LEFT half
		__mergesort(array, helper, middle+1, high)	# Sort RIGHT half
		__merge(array, helper, low, middle, high)	# Merge them

def __merge(array, helper, low, middle, high):
	"""
	This function is eventually called in the recursive process.
	With each recursive iteration it compares elements of larger and larger arrays; it starts off comparing just two elements, but other elements are added as each __mergesort returns a single (new) element (?)
	"""

	print("__merge(): ---------")
	print("__merge(): low = " + str(low) + ", middle = " + str(middle) + ", high = " + str(high))
	print("__merge(): copying indexes " + str(low) + " to " + str(high+1) + " from array (" + str(array) + ") into helper (" + str(helper) + ")")

	# Copy both halves into a helper array.
	for i in range(low, high+1):
		helper[i] = array[i]

	print("__merge(): helper, after copying, now is " + str(helper))

	leftStartIndex = low
	rightStartIndex = middle + 1
	current = low

	# Iterate through helper array. Compare left and right halves; copy the SMALLER element into the ORIGINAL array.
	while leftStartIndex <= middle and rightStartIndex <= high:
		print("__merge(): leftStartIndex = " + str(leftStartIndex) +  ", rightStartIndex = " + str(rightStartIndex) + ", middle = " + str(middle) + ", high = " + str(high))
		if helper[leftStartIndex] <= helper[rightStartIndex]:
			array[current] = helper[leftStartIndex]
			leftStartIndex += 1
		else:
			array[current] = helper[rightStartIndex]
			rightStartIndex += 1
		current += 1

	# Copy the rest of the left side of the array into the target array
	# This is not used until the end (I think).
	remaining = middle - leftStartIndex
	for i in range(0, remaining + 1):
		array[current + i] = helper[leftStartIndex + i]


class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		pass

	def test_standard(self):
		self.assertEqual(mergesort([3,7,8,5,2,1,9,5,4]), [1,2,3,4,5,5,7,8,9])

unittest.main(exit=False)