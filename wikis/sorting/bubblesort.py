#!/usr/bin/python

"""
Bubblesort in Python.
An implementation of it to understand how it works.

Time complexity: O(n^2)

"""

import unittest

def bubblesort(array):
	"""
	We want to go through the array, swapping pairs.
	"""
	print("array = " + str(array))
	arrayLength = len(array)
	for i in range(0, arrayLength):
		for j in range(0, arrayLength):
			if j+1 < arrayLength:
				print("---")
				print("Is " + str(array[j+1]) + " smaller than " + str(array[j]) + "?")
				if array[j+1] < array[j]:
					print("YES;")
					print("was " + str(array))
					array[j+1], array[j] = array[j], array[j+1]
					print("now " + str(array))
	return array

class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		pass

	def test_standard(self):
		self.assertEqual(bubblesort([3,7,8,5,2,1,9,5,4]), [1,2,3,4,5,5,7,8,9])

unittest.main(exit=False)