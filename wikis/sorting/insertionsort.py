#!/usr/bin/python

"""
insertionsort in Python.
An implementation of it to understand how it works.

Time complexity: O(n^2)

https://stackoverflow.com/questions/17270628/insertion-sort-vs-bubble-sort-algorithms

"""

import unittest

def insertionsort_2(array):
	"""
	Taken from https://stackoverflow.com/questions/28379397/simple-insertion-sort-in-java

	For each element we check the previous elements; if the current element is smaller than the previous elements, gradually move the current element to the front of the array.
	"""
	# Leave the 0th element
	print("==================")
	print("insertionsort_2")
	print("==================")
	print("array = " + str(array))
	for i in range(1, len(array)):
		for j in range(i, 0, -1):
			print("i = " + str(i) + ", j = " + str(j))
			if array[j - 1] > array[j]:
				print("	" + str(array[j-1]) + " at index " + str(j-1) + " is greater than " + str(array[j]) + " at index " + str(j))
				print("	array was " + str(array))
				array[j-1], array[j] = array[j], array[j-1]
				print("	array now " + str(array))
			else:
				print("	" + str(array[j-1]) + " at index " + str(j-1) + " is ALREADY SMALLER than " + str(array[j]) + " at index " + str(j) + "; ignoring.")
	return array

def insertionsort(array):
	"""
	Taken from https://www.geeksforgeeks.org/insertion-sort/ as a means to understand, but found it rather complicated.
	"""
	# Leave the 0th element
	print("==================")
	print("insertionsort")
	print("==================")
	print("array = " + str(array))
	for i in range(1, len(array)):
		key = array[i]

		# Move elements of array[0 to i-1] that are GREATER THAN KEY
		# to ONE position AHEAD of their current position.
		j = i - 1
		print("Checking other elements against key of " + str(key))
		while j >= 0 and key < array[j]:
			print("		" + str(key) + " IS less than " + str(array[j]) + " (key < array[" + str(j) + "])")
			print("		Moving " + str(array[j]) + " to where " + str(array[j + 1]) + " is.")
			print("		array was " + str(array))
			array[j + 1] = array[j]
			print("		array now " + str(array))
			j -= 1
		print("key (" + str(key) + ") is now being moved to where " + str(array[j + 1]) + " is.")
		print("array was " + str(array))
		array[j + 1] = key 
		print("array now " + str(array))
	return array


class Tester(unittest.TestCase):
	
	@classmethod
	def setUp(self):
		pass

	def test_insertionsort(self):
	 	self.assertEqual(insertionsort([3,7,8,5,2,1,9,5,4]), [1,2,3,4,5,5,7,8,9])

	def test_insertionsort_2(self):
		self.assertEqual(insertionsort_2([3,7,8,5,2,1,9,5,4]), [1,2,3,4,5,5,7,8,9])

unittest.main(exit=False)