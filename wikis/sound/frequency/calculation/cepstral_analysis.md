# Cepstral analysis

## What is it?

Cepstral analysis is a way to measure the [frequency](../frequency.md) of a sound signal. 


## What are the basics?

Cepstral analysis is the process of separating of [source](/sound/source.md) and [filter](/sound/filter.md) components (e.g. separating excitation signal of air passing through the glottis and vocal tract filtering).


## Where do the [filter](/sound/filter.md) properties reside?

FILTER properties reside in the lower quefrencies. 


## Where do the [source](/sound/source.md) properties reside? 

SOURCE info resides in the upper quefrencies.


## Where should I see a peak? 

The SOURCE is where we should see a peak.


## What can we do to the audio signal to potentially help the calculation?

[Mean](/mathematics/averages/mean.md) and [median](/mathematics/averages/median.md) filtering on the audio [source](/sound/source.md) smoothes the signal and reduces the effect _noise_ has on the application.


## What is the downsides of mean and median filtering?

[Mean](/mathematics/averages/mean.md) and [median](/mathematics/averages/median.md) filtering can result in _reduced_ accuracy during moments where the sound we want to measure the frequence of changes note a lot in a short amount of time (e.g. a Formula One car engine note).