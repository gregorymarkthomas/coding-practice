# Peak Picking

## What is it?

Peak picking is a way to measure the [frequency](../frequency.md) of a sound signal.


## How is [frequency](../frequency.md) measured with Peak Picking?

Frequency is measured by, as the name suggests, counting the peaks found in a sound wave over time.


## Can peaks be calculated with an algorithm instead of manually?

Of course.


## What is required of the sound wave for us to pick peaks from it?

The sound we want to measure the frequence of must be _loud_ enough so that the [peaks](/sound/peaks.md) and [troughs](/sound/troughs.md) are distinguishable from other noise.


## Is outside noise and interference an issue?

Yes; some noises can be loud enough to produce their own frequency peaks within the sound wave. This can interfere with peak-picking algorithms as the peak-picking algorithm may pick other loud noises from the the audio stream, and can therefore produce frequencies that are too high.