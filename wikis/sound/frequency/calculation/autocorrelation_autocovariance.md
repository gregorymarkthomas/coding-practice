# Autocorrelation

## What is it?

Autocorrelation is a way to measure the [frequency](../frequency.md) of a sound signal. Autocorrelation is used for finding repeating patterns


## What is another way to say "autocorrelation"?

The correlation of a signal with a **delayed** copy of itself.


## What are the basics?

We compare samples in the sample window with the same one in the lagged copy.


## What happens when we compare samples with a lagged copy?

Correlation will naturally decrease because of the difference between the two signals, but a periodic signal that correlation will increase again at certain points.

The distance between Lag 0 and first peak is an estimate of pitch 


## What is another name for Autocorrelation?

- Autocovariance