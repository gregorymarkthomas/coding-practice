# Common Closure Principle (CCP)

## What are the basics?

Classes that change for the **same reason** should be in the same package.

Perhaps two classes implement different aspects of the same business rule.

The goal is: **when a business rule changes, devs should only need to edit 1-2 packages**. i.e. each change should inpact only one service.